package ru.infodev.golos27.Common.SocialNetwork.listener;

import ru.infodev.golos27.Common.SocialNetwork.listener.base.SocialNetworkListener;

/**
 * Interface definition for a callback to be invoked when login complete.
 */
public interface OnLoginCompleteListener extends SocialNetworkListener {
    /**
     * Called when login complete.
     * @param socialNetworkID id of social network where request was complete
     */
    void onLoginSuccess(int socialNetworkID);
}
