package ru.infodev.golos27.Common.Interfaces;

import ru.infodev.golos27.Common.Utils.Json.JSONObject;

public interface DialogResultInterface {
    void DialogSendResult(int dialog_id, int cmd);
    void DialogSendParamResult(int dialog_id, int cmd, JSONObject params);
}

