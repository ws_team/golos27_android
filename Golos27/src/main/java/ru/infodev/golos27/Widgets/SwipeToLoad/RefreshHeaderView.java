package ru.infodev.golos27.Widgets.SwipeToLoad;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class RefreshHeaderView extends TextView implements SwipeRefreshTrigger, SwipeTrigger {

    public RefreshHeaderView(Context context) {
        super(context);
    }

    public RefreshHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onRefresh() {
        setText("Обновляем...");
    }

    @Override
    public void onPrepare() {
        setText("");
    }

    @Override
    public void onSwipe(int yScrolled, boolean isComplete) {
        if (!isComplete) {
            if (yScrolled >= getHeight()) {
                setText("Отпустите для обновления");
            } else {
                setText("Потяните для обновления");
            }
        } else {
            setText("Возвращаем...");
        }
    }

    @Override
    public void onRelease() {
    }

    @Override
    public void complete() {
        setText("Обновление завершено");
    }

    @Override
    public void onReset() {
        setText("");
    }
}
