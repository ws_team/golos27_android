package ru.infodev.golos27.Widgets.Adapters;

import android.content.Context;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Notification;
import ru.infodev.golos27.Common.Interfaces.RecycleViewClickInterface;
import ru.infodev.golos27.R;

import static ru.infodev.golos27.Common.Utils.Utils.isToday;
import static ru.infodev.golos27.Common.Utils.Utils.isYesterday;

/**
 * Класс адаптера наследуется от RecyclerView.Adapter с указанием класса, который будет хранить
 * ссылки на виджеты элемента списка, т.е. класса, имплементирующего ViewHolder. В нашем
 * случае класс объявлен внутри класса адаптера.
 */
public class NotificationListAdapter extends RecyclerView.Adapter {

    private LayoutInflater inflater;
    public int grey_color;
    public int blue_color;
    public int colorModeration;
    public int colorInTheWork;
    public int colorCompleted;
    public int colorBeenRejected;
    public int colorRated;
    public int colorOther;
    private List<Notification> data;
    private Image test;
    private RecycleViewClickInterface clickInterface;

    /***
     * При создании адаптера загружаются все иконки для ускорения отрисовки
     * и инициализируется список с обращениями.
     *
     * @param context    контекст
     * @param appealList список обращений
     */
    public NotificationListAdapter(Context context, List<Notification> appealList) {
        this.data = appealList;
        inflater = LayoutInflater.from(context);
        grey_color = ContextCompat.getColor(context, R.color.colorGrayText);
        blue_color = ContextCompat.getColor(context, R.color.colorPrimary);
        colorModeration = ContextCompat.getColor(context, R.color.colorModeration);
        colorCompleted = ContextCompat.getColor(context, R.color.colorCompleted);
        colorInTheWork = ContextCompat.getColor(context, R.color.colorInTheWork);
        colorBeenRejected = ContextCompat.getColor(context, R.color.colorBeenRejected);
        colorRated = ContextCompat.getColor(context, R.color.colorRated);
        colorOther = ContextCompat.getColor(context, R.color.colorOther);
    }

    /***
     * Устанавливает данные в адаптер
     *
     * @param appealList список обращений
     */
    public void setData(List<Notification> appealList) {
        this.data = appealList;
    }

    /***
     * Возвращает список с данными привязанный к адаптеру
     *
     * @return список обращений
     */
    public List<Notification> getData() {
        return this.data;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые
     * впоследствии могут переиспользоваться.
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = inflater.inflate(R.layout.item_notification_data, viewGroup, false);
        return new ItemViewHolder(v);
    }

    /**
     * Заполнение элементов данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int i) {
        boolean isFirstOfDay = false;
        Notification notification = getData().get(i);
        final ItemViewHolder viewHolder = (ItemViewHolder) holder;

        // Текст, номер и тематика
        Calendar added = Calendar.getInstance();
        added.setTimeInMillis(notification.Date());
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", Locale.getDefault());
        viewHolder.dateTimeView.setText(formatter.format(added.getTime()));
        viewHolder.objectNumberView.setText(getString(R.string.appeal_number).replace("null", String.valueOf(notification.ObjectNumber())));
        viewHolder.titleView.setText(notification.ObjectName());
        viewHolder.statusView.setText(notification.Title());
        String title = notification.EventType();
        int textColor = colorOther;
        int iconId = R.drawable.prinyato_v_rabotu;
        switch (title) {
            case Notification.CREATED:
                textColor = colorInTheWork;
                iconId = R.drawable.prinyato_v_rabotu;
                break;
            case Notification.ACCEPTED:
                textColor = colorInTheWork;
                iconId = R.drawable.prinyato_v_rabotu;
                break;
            case Notification.ACCEPTED_AND_HIDED:
                textColor = colorInTheWork;
                iconId = R.drawable.prinyato_v_rabotu;
                break;
            case Notification.REJECTED:
                textColor = colorBeenRejected;
                iconId = R.drawable.otkloneno;
                break;
            case Notification.VIOLATED_REVIEWING:
                textColor = colorBeenRejected;
                iconId = R.drawable.prosrocheno;
                break;
            case Notification.SOLVED:
                textColor = colorCompleted;
                iconId = R.drawable.blue_list;
                break;
            case Notification.PROLONGED:
                iconId = R.drawable.blue_time_plus;
                break;
            case Notification.GRADED:
                textColor = colorRated;
                iconId = R.drawable.star;
                break;
            case Notification.FORWARDED:
                iconId = R.drawable.blue_direction;
                break;
            case Notification.HIDED:
            case Notification.HIDED_BY_OPERATOR:
                iconId = R.drawable.skrito;
                break;
            case Notification.RESPONSE_HIDED:
            case Notification.RESPONSE_HIDED_BY_OPERATOR:
                iconId = R.drawable.blue_crossed_eye;
                break;
            case Notification.SUBJECT_CHANGED:
                textColor = colorInTheWork;
                iconId = R.drawable.prinyato_v_rabotu;
                break;
        }

        viewHolder.headerText.setVisibility(View.GONE);
        viewHolder.statusView.setTextColor(textColor);
        viewHolder.icon.setImageResource(iconId);
        long currItemDate = getItem(i).Date();
        Calendar currItemCal = Calendar.getInstance();
        currItemCal.setTimeInMillis(currItemDate);

        if (i == 0) isFirstOfDay = true;
        if (i > 0) {
            long prevItemDate = getItem(i-1).Date();
            Calendar prevItemCal = Calendar.getInstance();
            prevItemCal.setTimeInMillis(prevItemDate);
            if (prevItemCal.get(Calendar.YEAR) != currItemCal.get(Calendar.YEAR) ||
                    prevItemCal.get(Calendar.DAY_OF_YEAR) != currItemCal.get(Calendar.DAY_OF_YEAR))
                isFirstOfDay = true;
        }

        if (isFirstOfDay) {
            viewHolder.headerText.setVisibility(View.VISIBLE);
            viewHolder.headerText.setText(formatHeaderDate(currItemCal));
        }

        viewHolder.readSign.setVisibility(notification.isRead() ? View.INVISIBLE : View.VISIBLE);
    }

    private String formatHeaderDate(Calendar date) {
        SimpleDateFormat formatter=new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String currentDate=formatter.format(date.getTime());
        if (isToday(date)) currentDate = getString(R.string.today) + ", " + currentDate;
        if (isYesterday(date)) currentDate = getString(R.string.yesterday) + ", " + currentDate;
        return currentDate;
    }

    @Override
    public int getItemCount() {
        return getData().size();
    }

    protected Notification getItem(int position) {
        return data.get(position);
    }

    public void setOnClickListener(RecycleViewClickInterface onClickListener) {
        this.clickInterface = onClickListener;
    }

    /**
     * Реализация класса ViewHolder, хранящего ссылки на виджеты
     * Чтобы их не надо было каждый раз искать с помощью findViewById
     * При инициализации заполняет все ссылки
     */
    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Текстовые поля
        private TextView objectNumberView;
        private TextView dateTimeView;
        public TextView titleView;
        private TextView statusView;
        private TextView headerText;
        private View notificationBody;
        private View readSign;
        public ImageView icon;

        // Конструктор - здесь ищем все заполняемые элементы
        public ItemViewHolder(View itemView) {
            super(itemView);
            notificationBody = itemView.findViewById(R.id.notification_body);
            headerText = (TextView) itemView.findViewById(R.id.header_text);
            objectNumberView = (TextView) itemView.findViewById(R.id.objectNumberView);
            dateTimeView = (TextView) itemView.findViewById(R.id.dateTimeView);
            titleView = (TextView) itemView.findViewById(R.id.theme_appeal);
            statusView = (TextView) itemView.findViewById(R.id.info_appeal);
            readSign = itemView.findViewById(R.id.read);
            icon = (ImageView) itemView.findViewById(R.id.Notification_image);
            notificationBody.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickInterface != null) clickInterface.onItemClick(getAdapterPosition());
        }
    }

    private String getString(int id) {
        return Application.AppContext.getString(id);
    }
}
