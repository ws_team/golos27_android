package ru.infodev.golos27.Common.Catalogs;

import java.util.ArrayList;
import java.util.List;

/***
 * Singleton класс для хранения списка загруженных уведомлений
 * для отображения в ленте обращений
 */
public class NotificationListSingleton {
    private static List<Notification> mNotificationList = new ArrayList<>();
    private static Integer mNotificationsCurrentPage = -1;
    private static boolean needRefresh = false;

    public static List<Notification> getNotificationList() {
        return mNotificationList;
    }

    public static void setNotificationList(List<Notification> notificationList) {
        NotificationListSingleton.mNotificationList = notificationList;
    }

    public static Integer getNotificationsCurrentPage() {
        return mNotificationsCurrentPage;
    }

    public static void setNotificationsCurrentPage(Integer notificationsCurrentPage) {
        NotificationListSingleton.mNotificationsCurrentPage = notificationsCurrentPage;
    }

    public static boolean isNeedRefresh() {
        return needRefresh;
    }

    public static void setNeedRefresh(boolean needRefresh) {
        NotificationListSingleton.needRefresh = needRefresh;
    }
}
