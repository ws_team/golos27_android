package ru.infodev.golos27.RestApi;

import android.os.AsyncTask;

import com.google.firebase.messaging.FirebaseMessaging;

import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Profile;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

public class PushNotifications {

    /***
     * Асинхронный запрос топика для подписки на push-уведомления
     */
    public static class PushSubscribeRequest extends AsyncTask<Void, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(Void... params) {
            ServerTalk server = new ServerTalk();
            return server.subscribePushTopic();
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            if (result == null) return;

            int errorCode = (Integer) result.get("errorCode", ErrorResult.UNKNOWN_ERROR);
            if (errorCode == Transport.SUCCESS) {
                if (result.has(ServerTalk.PUSH_TOKEN_FIELD)) {
                    String topic = result.getString(ServerTalk.PUSH_TOKEN_FIELD, "");
                    if (isNotEmpty(topic)) {
                        Settings.setParameter(Settings.TOPIC_TOKEN, topic);
                        FirebaseMessaging.getInstance().subscribeToTopic(topic);
                    }
                }
            }
        }
    }

    /***
     * Асинхронный запрос на удаление подписки на push-уведомления
     */
    public static class PushUnsubscribeRequest extends AsyncTask<Void, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(Void... params) {
            ServerTalk server = new ServerTalk();
            return server.unsubscribePushTopic();
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            Logout();

            if (result == null) return;
            int errorCode = (Integer) result.get("errorCode", ErrorResult.UNKNOWN_ERROR);
            if (errorCode == Transport.SUCCESS) {
                Logger.Log("Unsubscribe topic success!");
            } else {
                Logger.Log("Unsubscribe topic failed: " + result.getString("errorText", "unknown error"));
            }
        }
    }

    private static void Logout() {
        // Это по идее был последний запрос перед командой logoff. Удаляем данные.
        Profile.RemoveAllData();
        Settings.delDatabaseParameter(Settings.AUTHORIZED_FLAG);
    }

    /***
     * Подписывается на push-уведомления
     * @param reloadTopic если true - запрашивает топик для подписки с сервера
     */
    public static void Subscribe(boolean reloadTopic) {
        String topic = Settings.getStr(Settings.TOPIC_TOKEN);
        if (reloadTopic || isEmpty(topic)) {
            new PushSubscribeRequest().execute();
        } else {
            FirebaseMessaging.getInstance().subscribeToTopic(topic);
        }
    }

    /***
     * Отменяет подписку на push-уведомления
     */
    public static void Unsubscribe() {
        String topic = Settings.getStr(Settings.TOPIC_TOKEN);
        if (isNotEmpty(topic)) {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
            new PushUnsubscribeRequest().execute();
            Settings.delDatabaseParameter(Settings.TOPIC_TOKEN);
        } else {
            Logout();
        }
    }
}
