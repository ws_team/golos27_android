package ru.infodev.golos27;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;

import com.google.firebase.crash.FirebaseCrash;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.infodev.golos27.Common.SImageLoader;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.FontsOverride;
import ru.infodev.golos27.Common.Utils.Logger;

public class Application extends android.app.Application implements Thread.UncaughtExceptionHandler {
    public static final String CACHE_DIR_NAME = "NewAppeal";
    public static final String IMAGES_DIR_NAME = "images";
    public static final String THUMBS_DIR_NAME = "thumbnail";

    public static Context AppContext = null;
    public static String cacheDirectory = null;
    public static String imagesCacheDirectory = null;
    public static String thumbsCacheDirectory = null;

    private String versionName = "0";
    private int versionCode = 0;
    private String stacktraceDir;
    private Thread.UncaughtExceptionHandler previousHandler;
    private final DateFormat formatter = new SimpleDateFormat("dd.MM.yy HH:mm", Locale.getDefault());
    private final DateFormat fileFormatter = new SimpleDateFormat("dd-MM-yy", Locale.getDefault());

    @Override
    public void onCreate() {
        super.onCreate();

        AppContext = getApplicationContext();
        AppContext.setTheme(R.style.AppTheme_NoActionBar);
        Settings.Init(this);
        SImageLoader.Init(AppContext);

        PackageManager mPackManager = AppContext.getPackageManager();
        PackageInfo mPackInfo;
        try {
            mPackInfo = mPackManager.getPackageInfo(AppContext.getPackageName(), 0);
            versionName = mPackInfo.versionName;
            versionCode = mPackInfo.versionCode;
        } catch (PackageManager.NameNotFoundException ignored) {
        }

        previousHandler = null;
        stacktraceDir = String.format("/Android/data/%s/files/", AppContext.getPackageName());
        String logFileName = Environment.getExternalStorageDirectory() + "/golos27.log";
        Logger.SetOptions(true, true, 1024, 512 * 1024, logFileName);

        previousHandler = Thread.getDefaultUncaughtExceptionHandler();
        if (Logger.Enabled) Thread.setDefaultUncaughtExceptionHandler(this);

        FontsOverride.setDefaultFont(this, "DEFAULT", "Roboto-Medium.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "Roboto-Medium.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "Roboto-Medium.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "Roboto-Medium.ttf");

        // Подготовим каталог для кэша фотографий и создадим его
        cacheDirectory = SImageLoader.getCachePath() + File.separator + CACHE_DIR_NAME;
        imagesCacheDirectory = cacheDirectory + File.separator + IMAGES_DIR_NAME;
        thumbsCacheDirectory = imagesCacheDirectory + File.separator + THUMBS_DIR_NAME;
        try {
            //noinspection ResultOfMethodCallIgnored
            new File(thumbsCacheDirectory).mkdirs();
        } catch (Exception e) {
            Logger.Exception(e);
        }
    }

    @SuppressLint("DefaultLocale")
    public void uncaughtException(final Thread thread, final Throwable exception) {
        FirebaseCrash.report(exception);

        final String state = Environment.getExternalStorageState();
        final Date dumpDate = new Date(System.currentTimeMillis());
        if (Environment.MEDIA_MOUNTED.equals(state)) {

            StringBuilder reportBuilder = new StringBuilder();
            reportBuilder
                    .append("\n\n\n")
                    .append(formatter.format(dumpDate)).append("\n")
                    .append(String.format("Version: %s (%d)\n", versionName, versionCode))
                    .append(thread.toString()).append("\n");
            processThrowable(exception, reportBuilder);

            File sd = Environment.getExternalStorageDirectory();
            File stacktrace = new File(
                    sd.getPath() + stacktraceDir,
                    String.format("stacktrace-%s.txt", fileFormatter.format(dumpDate)));
            File dumpdir = stacktrace.getParentFile();
            boolean dirReady = dumpdir.isDirectory() || dumpdir.mkdirs();
            if (dirReady) {
                FileWriter writer = null;
                try {
                    writer = new FileWriter(stacktrace, true);
                    writer.write(reportBuilder.toString());
                } catch (IOException ignored) {
                } finally {
                    try {
                        if (writer != null) writer.close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }

        Logger.Exception(exception);
        Logger.Warn("Unhandled exception! Starting crash dialog!");
        Intent crashDialog = new Intent(getApplicationContext(), CrashActivity.class);
        crashDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        AppContext.startActivity(crashDialog);
        System.exit(-2);

        if (previousHandler != null) previousHandler.uncaughtException(thread, exception);
    }

    private void processThrowable(Throwable exception, StringBuilder builder) {
        if (exception == null)
            return;
        StackTraceElement[] stackTraceElements = exception.getStackTrace();
        builder
                .append("Exception: ").append(exception.getClass().getName()).append("\n")
                .append("Message: ").append(exception.getMessage()).append("\nStacktrace:\n");
        for (StackTraceElement element : stackTraceElements) {
            builder.append("\t").append(element.toString()).append("\n");
        }
        processThrowable(exception.getCause(), builder);
    }
}
