package ru.infodev.golos27.Common;

import android.content.Context;

import com.nostra13.universalimageloader.cache.disc.DiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.IoUtils;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.io.InputStream;

import ru.infodev.golos27.Common.Utils.ExtImageDecoder;
import ru.infodev.golos27.Common.Utils.Logger;

// Simple image loader wrapper
public class SImageLoader {

    private final static int connTimeOut = 5000;
    private final static int loadTimeOut = 30000;
    private static Context lContext = null;
    private static DiskCache dCache = null;

    public static void Init(Context context) {
        DisplayImageOptions displayOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        File cacheDir = StorageUtils.getCacheDirectory(context);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .imageDownloader(new BaseImageDownloader(context, connTimeOut, loadTimeOut))
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator())
                //.diskCache(new UnlimitedDiskCache(cacheDir))
                .defaultDisplayImageOptions(displayOptions)
                .imageDecoder(new ExtImageDecoder(true))
                .diskCacheSize(128*1024*1024)
                .diskCacheFileCount(512)
                .build();

        ImageLoader.getInstance().init(config);
    }

    public static String getCachePath() {
        return ImageLoader.getInstance().getDiskCache().getDirectory().getAbsolutePath();
    }

    public static boolean IsCached(String uri) {
        File f = DiskCacheUtils.findInCache(uri, ImageLoader.getInstance().getDiskCache());
        return f != null;
    }

    @SuppressWarnings("unused")
    public static File GetFromCache(String uri) {
        return DiskCacheUtils.findInCache(uri, ImageLoader.getInstance().getDiskCache());
    }

    @SuppressWarnings("unused")
    public static boolean Download(final String uri, boolean network) {
        if (network) {
            new Thread(new Runnable() {
                public void run() {
                    downloadSimple(uri);
                }
            }).start();
        } else {
            downloadSimple(uri);
        }

        return IsCached(uri);
    }

    // Download in current stream
    public static boolean downloadSimple(String uri) {
        boolean result = false;
        try {
            BaseImageDownloader downloader = new BaseImageDownloader(lContext, 5000, 30000);
            InputStream is = downloader.getStream(uri, null);
            if (is == null) {
                Logger.Warn("No image stream. Can't download");
            } else {
                try {
                    result = dCache.save(uri, is, null);
                } catch (Exception ex) {
                    Logger.Exception(ex);
                    Logger.Warn("Can't save image in disk cache. Exception logged");
                } finally {
                    IoUtils.closeSilently(is);
                }
            }
        } catch (Exception ex) {
            Logger.Exception(ex);
        }

        return result;
    }
}
