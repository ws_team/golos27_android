package ru.infodev.golos27.Fragments.Profile;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Profile;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.CustomEditText;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;

public class ChangeGeneralFragment extends ParamFragment
        implements TextWatcherAdapter.TextWatcherListener, AdapterView.OnItemSelectedListener {
    public static final int FRAGMENT_ID = R.layout.profile_general_information;
    public static final int START_YEAR = 1910;

    private CustomEditText lastNameEdit;
    private CustomEditText firstNameEdit;
    private CustomEditText middleNameEdit;
    private Spinner birthYearSpinner;
    private Spinner birthMonthSpinner;
    private Spinner birthDaySpinner;
    private RadioButton maleButton;
    private RadioButton femaleButton;

    private boolean lastNameGood = false;
    private boolean lastNameGood1 = false;
    private boolean lastNameGood2 = false;
    private boolean lastNameGood3 = false;
    private boolean FirstNameGood1 = false;
    private boolean FirstNameGood2 = false;
    private boolean FirstNameGood3 = false;
    private boolean SurNameGood = false;
    private boolean SurNameGood1 = false;
    private boolean SurNameGood2 = false;
    private boolean SurNameGood3 = false;
    private boolean firstNameGood = false;
    private boolean middleNameGood = false;
    private boolean birthDateGood = true;

    private boolean lastNameChange = false;
    private boolean firstNameChange = false;
    private boolean middleNameChange = false;
    private boolean birthDateChange = false;
    private boolean genderChange = false;
    private TextView help_text_last_name_profile;
    private TextView help_text_first_name_profile;
    private TextView help_text_3rd_name_profile;
    private TextView warn_text_last_name;
    private TextView warn_text_first_name;
    private TextView warn_text_3rd_name;

    private String orig_first = Profile.FirstName();
    private String orig_last = Profile.LastName();
    private String orig_middle = Profile.MiddleName();
    private int orig_gender = Profile.Gender();
    private Calendar orig_date = Calendar.getInstance();
    private Date orig_birth_date = Profile.Birth();

    Pattern patternMainInfo = Pattern.compile("[а-яА-ЯёЁ\\s\\-]*"); // Русские буквы и дефис
    Pattern patternName1 = Pattern.compile("^.*\\-+$"); // В конце 2 и более дефиса
    Pattern patternName2 = Pattern.compile("^\\-+.*"); // Дефис в начале
    Pattern patternName3 = Pattern.compile(".*\\-\\-+.*"); // Дефис в середине

    public ChangeGeneralFragment() {
        titleId = R.string.general_info;
        setFragmentAllowMenu(false);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        setTitle(titleId);

        warn_text_last_name = (TextView) v.findViewById(R.id.warn_text_last_name);
        warn_text_last_name.setTypeface(roboto_light);

        warn_text_first_name = (TextView) v.findViewById(R.id.warn_text_first_name);
        warn_text_first_name.setTypeface(roboto_light);

        warn_text_3rd_name = (TextView) v.findViewById(R.id.warn_text_3rd_name);
        warn_text_3rd_name.setTypeface(roboto_light);

        help_text_last_name_profile = (TextView) v.findViewById(R.id.help_text_last_name);
        help_text_first_name_profile = (TextView) v.findViewById(R.id.help_text_first_name);
        help_text_3rd_name_profile = (TextView) v.findViewById(R.id.help_text_3rd_name);

        // Начальные значения Фамилии Имени Отчества
        lastNameEdit = (CustomEditText) v.findViewById(R.id.edit_last_name_1);
        lastNameEdit.setOnTextChangedListener(this);
        lastNameEdit.setText(orig_last);

        firstNameEdit = (CustomEditText) v.findViewById(R.id.edit_first_name_1);
        firstNameEdit.setOnTextChangedListener(this);
        firstNameEdit.setText(orig_first);

        middleNameEdit = (CustomEditText) v.findViewById(R.id.edit_sur_name_1);
        middleNameEdit.setOnTextChangedListener(this);
        middleNameEdit.setText(orig_middle);

        // Начальные значения радиокнопок пола
        maleButton = (RadioButton) v.findViewById(R.id.male_button);
        femaleButton = (RadioButton) v.findViewById(R.id.female_button);

        if (orig_gender == Profile.MALE) {
            maleButton.setChecked(true);
        } else if (orig_gender == Profile.FEMALE) {
            femaleButton.setChecked(true);
        } else {
            maleButton.setChecked(false);
            femaleButton.setChecked(false);
        }

        maleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) femaleButton.setChecked(false);
                genderChange = !(orig_gender == getRadioGender());
                showSaveButtonIfCan();
            }
        });

        femaleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) maleButton.setChecked(false);
                genderChange = !(orig_gender == getRadioGender());
                showSaveButtonIfCan();
            }
        });

        // Инициализация полей ввода даты рождения
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        birthYearSpinner = (Spinner) v.findViewById(R.id.birth_year);
        List<String> yearList = new ArrayList<String>() {{
            add(0, " ");
        }};
        for (int i = START_YEAR; i <= year; i++) yearList.add(String.valueOf(i));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(Application.AppContext, R.layout.item_spinner_layout, yearList);
        adapter.setDropDownViewResource(R.layout.spinner_list_layout);
        birthYearSpinner.setAdapter(adapter);

        birthMonthSpinner = (Spinner) v.findViewById(R.id.birth_month);
        String[] monthList = getResources().getStringArray(R.array.month);
        adapter = new ArrayAdapter<>(Application.AppContext, R.layout.item_spinner_layout, monthList);
        adapter.setDropDownViewResource(R.layout.spinner_list_layout);
        birthMonthSpinner.setAdapter(adapter);

        birthDaySpinner = (Spinner) v.findViewById(R.id.birth_day);
        List<String> dayList = new ArrayList<String>() {{
            add(0, " ");
        }};
        for (int i = 1; i <= 31; i++) dayList.add(String.valueOf(i));
        adapter = new ArrayAdapter<>(Application.AppContext, R.layout.item_spinner_layout, dayList);
        adapter.setDropDownViewResource(R.layout.spinner_list_layout);
        birthDaySpinner.setAdapter(adapter);

        birthYearSpinner.setOnItemSelectedListener(this);
        birthMonthSpinner.setOnItemSelectedListener(this);
        birthDaySpinner.setOnItemSelectedListener(this);

        if (orig_birth_date != null) {
            Calendar birthCal = Calendar.getInstance();
            birthCal.setTime(orig_birth_date);
            final int init_day = birthCal.get(Calendar.DAY_OF_MONTH);
            final int init_month = birthCal.get(Calendar.MONTH) + 1;
            final int init_year = birthCal.get(Calendar.YEAR);

            birthDaySpinner.post(new Runnable() {
                @Override
                public void run() {
                    birthDaySpinner.setSelection(init_day);
                }
            });

            birthMonthSpinner.post(new Runnable() {
                @Override
                public void run() {
                    birthMonthSpinner.setSelection(init_month);
                }
            });

            birthYearSpinner.post(new Runnable() {
                @Override
                public void run() {
                    birthYearSpinner.setSelection(init_year - START_YEAR + 1);
                }
            });

            orig_date.setTime(orig_birth_date);
            orig_date.set(Calendar.HOUR, 0);
            orig_date.set(Calendar.MINUTE, 0);
            orig_date.set(Calendar.SECOND, 0);
            orig_date.set(Calendar.MILLISECOND, 0);
        }

        return v;
    }

    public int getRadioGender() {
        int gender = -1;
        if (maleButton.isChecked()) gender = Profile.MALE;
        if (femaleButton.isChecked()) gender = Profile.FEMALE;
        return gender;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        setActionButtonIcon(ParamFragment.ICON_CHECK);
        showSaveButtonIfCan();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_item) {
            String firstName = firstNameEdit.getText().toString();
            String lastName = lastNameEdit.getText().toString();
            String middleName = middleNameEdit.getText().toString();
            Integer gender = -1;
            if (maleButton.isChecked()) gender = Profile.MALE;
            if (femaleButton.isChecked()) gender = Profile.FEMALE;

            Long date = null;
            String yearStr = birthYearSpinner.getSelectedItem().toString();
            String monthStr = birthMonthSpinner.getSelectedItem().toString();
            String dayStr = birthDaySpinner.getSelectedItem().toString();
            if (!yearStr.equals(" ") && !monthStr.equals(" ") && !dayStr.equals(" ")) {
                int year = Integer.valueOf(yearStr);
                int month = birthMonthSpinner.getSelectedItemPosition();
                int day = Integer.valueOf(dayStr);
                try {
                    Calendar c = Calendar.getInstance();
                    c.setLenient(false);
                    c.set(year, month - 1, day, 0, 0, 0);
                    c.set(Calendar.MILLISECOND, 0);
                    date = c.getTime().getTime();
                } catch (Exception e) {
                    /* pass */
                }
            }

            new SaveGeneralInfoTask().execute(firstName, lastName, middleName,
                    gender.toString(), date != null ? date.toString() : null);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        CustomEditText editBox = (CustomEditText) view;
        switch (view.getId()) {
            case R.id.edit_last_name_1:
                lastNameGood = editBox.checkValue(patternMainInfo, true, false);
                lastNameChange = !text.equals(orig_last);
                lastNameGood1 = !patternName1.matcher(text).matches();
                lastNameGood2 = !patternName2.matcher(text).matches();
                ((CustomEditText) view).setMistake(!lastNameGood2);
                lastNameGood3 = !patternName3.matcher(text).matches();
                ((CustomEditText) view).setMistake(!lastNameGood3);
                if (text.length() < 1) {
                    help_text_last_name_profile.setText(R.string.down_text_all_entry);
                } else {
                    if (!lastNameGood) {
                        ((CustomEditText) view).setMistake(true);
                        help_text_last_name_profile.setText(R.string.pattern_first_and_last_name_1);
                    } else {
                        if (!lastNameGood2) {
                            ((CustomEditText) view).setMistake(true);
                            help_text_last_name_profile.setText(R.string.pattern_first_and_last_name_2);
                        } else {
                            if (!lastNameGood1) {
                                ((CustomEditText) view).setMistake(true);
                                help_text_last_name_profile.setText(R.string.pattern_first_and_last_name_3);
                            } else {
                                if (!lastNameGood3) {
                                    ((CustomEditText) view).setMistake(true);
                                    help_text_last_name_profile.setText(R.string.pattern_first_and_last_name_4);
                                } else {
                                    help_text_last_name_profile.setText(R.string.down_text_all_entry);
                                    ((CustomEditText) view).setMistake(false);
                                }
                            }
                        }
                    }
                }

                break;
            case R.id.edit_first_name_1:
                firstNameGood = editBox.checkValue(patternMainInfo, true, false);
                firstNameChange = !text.equals(orig_first);
                FirstNameGood1 = !patternName1.matcher(text).matches();
                FirstNameGood2 = !patternName2.matcher(text).matches();
                ((CustomEditText) view).setMistake(!FirstNameGood2);
                FirstNameGood3 = !patternName3.matcher(text).matches();
                ((CustomEditText) view).setMistake(!FirstNameGood3);
                if (text.length() < 1) {
                    help_text_first_name_profile.setText(R.string.down_text_all_entry);
                } else {
                    if (!firstNameGood) {
                        ((CustomEditText) view).setMistake(true);
                        help_text_first_name_profile.setText(R.string.pattern_first_and_last_name_1);
                    } else {
                        if (!FirstNameGood2) {
                            ((CustomEditText) view).setMistake(true);
                            help_text_first_name_profile.setText(R.string.pattern_first_and_last_name_2);
                        } else {
                            if (!FirstNameGood1) {
                                ((CustomEditText) view).setMistake(true);
                                help_text_first_name_profile.setText(R.string.pattern_first_and_last_name_3);
                            } else {
                                if (!FirstNameGood3) {
                                    ((CustomEditText) view).setMistake(true);
                                    help_text_first_name_profile.setText(R.string.pattern_first_and_last_name_4);
                                } else {
                                    help_text_first_name_profile.setText(R.string.down_text_all_entry);
                                    ((CustomEditText) view).setMistake(false);
                                }
                            }
                        }
                    }
                }
                break;
            case R.id.edit_sur_name_1:
                middleNameGood = editBox.checkValue(patternMainInfo, false, false);
                middleNameChange = !text.equals(orig_middle);
                SurNameGood = patternMainInfo.matcher(text).matches();
                SurNameGood1 = !patternName1.matcher(text).matches();
                SurNameGood2 = !patternName2.matcher(text).matches();
                ((CustomEditText) view).setMistake(!SurNameGood2);
                SurNameGood3 = !patternName3.matcher(text).matches();
                ((CustomEditText) view).setMistake(!SurNameGood3);
                if (isEmpty(text)) {
                    SurNameGood = true;
                }
                if (text.length() < 1) {
                    help_text_3rd_name_profile.setText(R.string.down_text_all_entry);
                } else {
                    if (!SurNameGood) {
                        ((CustomEditText) view).setMistake(true);
                        help_text_3rd_name_profile.setText(R.string.pattern_first_and_last_name_1);
                    } else {
                        if (!SurNameGood2) {
                            ((CustomEditText) view).setMistake(true);
                            help_text_3rd_name_profile.setText(R.string.pattern_first_and_last_name_2);
                        } else {
                            if (!SurNameGood1) {
                                ((CustomEditText) view).setMistake(true);
                                help_text_3rd_name_profile.setText(R.string.pattern_first_and_last_name_3);
                            } else {
                                if (!SurNameGood3) {
                                    ((CustomEditText) view).setMistake(true);
                                    help_text_3rd_name_profile.setText(R.string.pattern_first_and_last_name_4);
                                } else {
                                    help_text_3rd_name_profile.setText(R.string.down_text_all_entry);
                                    ((CustomEditText) view).setMistake(false);
                                }
                            }
                        }
                    }
                }
                break;
        }

        showSaveButtonIfCan();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String yearStr = " ";
        String monthStr = " ";
        String dayStr = " ";

        try {
            yearStr = birthYearSpinner.getSelectedItem().toString();
            monthStr = birthMonthSpinner.getSelectedItem().toString();
            dayStr = birthDaySpinner.getSelectedItem().toString();
        } catch (Exception ignored) {
        }

        // Здесь нужно при изменении года и месяца менять количество дней в первом спиннере
        if (!yearStr.equals(" ") && !monthStr.equals(" ") && !dayStr.equals(" ")) {
            int year = Integer.valueOf(yearStr);
            int month = birthMonthSpinner.getSelectedItemPosition();
            int day = Integer.valueOf(dayStr);
            try {
                Calendar now = Calendar.getInstance();
                Calendar c = Calendar.getInstance();
                c.setLenient(false);
                c.set(year, month - 1, day, 0, 0, 0);
                c.set(Calendar.MILLISECOND, 0);
                c.getTime();

                // Будущее время нельзя ставить... Алиса пролетает...
                birthDateGood = !now.before(c);
                birthDateChange = orig_date.get(Calendar.DAY_OF_MONTH) != c.get(Calendar.DAY_OF_MONTH) ||
                        (orig_date.get(Calendar.MONTH) != c.get(Calendar.MONTH)) ||
                        (orig_date.get(Calendar.YEAR) != c.get(Calendar.YEAR));
            } catch (Exception e) {
                birthDateGood = false;
            }
        } else {
            birthDateGood = true;
        }

        showSaveButtonIfCan();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    private void showSaveButtonIfCan() {
        if (birthDateGood && firstNameGood && lastNameGood1 && lastNameGood2 && lastNameGood3 && lastNameGood && middleNameGood) {
            // Проверим что данные поменялись, а то сохранять старые данные некомильфо
            if (firstNameChange || lastNameChange || middleNameChange || birthDateChange || genderChange)
                setActionButtonVisibility(true);
            else
                setActionButtonVisibility(false);
        } else {
            setActionButtonVisibility(false);
        }
    }

    class SaveGeneralInfoTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setActionButtonIcon(ParamFragment.ICON_ADD_PROGRESS);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            Integer gender = Integer.valueOf(params[3]);
            if (gender < 0) gender = null;
            Long dateLong = null;
            if (params[4] != null) dateLong = Long.valueOf(params[4]);

            Date birth = null;
            if (dateLong != null) birth = new Date(dateLong);

            return server.setProfile(params[0], params[1], params[2], gender, birth, Profile.OKTMOCode(),
                    Profile.OKTMOData(), Profile.Address(), Profile.Latitude(), Profile.Longitude());
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            setActionButtonIcon(ParamFragment.ICON_DEL_PROGRESS);
            setActionButtonEnabled(false);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    fragmentResultListener.FragmentSendResult(FRAGMENT_ID, 0, result);
                    setActionButtonEnabled(true);
                } else {
                    ResultDialog.showError(getActivity(), "Ошибка сохранения", errorCode, result.getErrorText());
                }
            } catch (Exception e) {
                Logger.Exception(e);
                ResultDialog.showError(getActivity(), "Ошибка сохранения", ErrorResult.INTERNAL_ERROR);
            }
            setActionButtonEnabled(true);
        }
    }
}