package ru.infodev.golos27.Widgets.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Appeal.Appeal;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Interfaces.RecycleViewClickInterface;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.Widgets.HelpPopup;

import static android.support.v4.content.ContextCompat.getColor;
import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

/**
 * Класс адаптера наследуется от RecyclerView.Adapter с указанием класса, который будет хранить
 * ссылки на виджеты элемента списка, т.е. класса, имплементирующего ViewHolder. В нашем
 * случае класс объявлен внутри класса адаптера.
 */
public class AppealListAdapter extends RecyclerView.Adapter {

    private LayoutInflater inflater;
    private RecycleViewClickInterface clickInterface;
    public int grey_color;
    public int blue_color;
    private List<Appeal> data;
    private Typeface typeface_light;
    private HelpPopup popupWindow;

    public Drawable grey_clock;
    public Drawable blue_clock;
    public Drawable grey_hide;
    public Drawable blue_hide;
    public Drawable blue_rdr;
    public Drawable grey_rdr;
    public Drawable blue_resp;
    public Drawable grey_resp;
    public Drawable cover_empty;

    /***
     * При создании адаптера загружаются все иконки для ускорения отрисовки
     * и инициализируется список с обращениями.
     *
     * @param context    контекст
     * @param appealList список обращений
     */
    public AppealListAdapter(Context context, List<Appeal> appealList) {
        this.data = appealList;
        inflater = LayoutInflater.from(context);

        typeface_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        grey_color = ContextCompat.getColor(context, R.color.colorGrayText);
        blue_color = ContextCompat.getColor(context, R.color.colorPrimary);
        grey_clock = ContextCompat.getDrawable(context, R.drawable.grey_time_plus);
        blue_clock = ContextCompat.getDrawable(context, R.drawable.blue_time_plus);
        grey_hide = ContextCompat.getDrawable(context, R.drawable.grey_crossed_eye);
        blue_hide = ContextCompat.getDrawable(context, R.drawable.blue_crossed_eye);
        grey_rdr = ContextCompat.getDrawable(context, R.drawable.grey_direction);
        blue_rdr = ContextCompat.getDrawable(context, R.drawable.blue_direction);
        grey_resp = ContextCompat.getDrawable(context, R.drawable.grey_list);
        blue_resp = ContextCompat.getDrawable(context, R.drawable.blue_list);
        cover_empty = ContextCompat.getDrawable(context, R.drawable.no_photo_background);
    }

    public void closePopup() {
        try {
            if (popupWindow != null) popupWindow.dismiss();
        } catch (Exception ignored) {
        }
    }

    public void setOnClickListener(RecycleViewClickInterface listener) {
        clickInterface = listener;
    }

    /***
     * Устанавливает данные в адаптер
     *
     * @param appealList список обращений
     */
    public void setData(List<Appeal> appealList) {
        this.data = appealList;
    }

    /***
     * Возвращает список с данными привязанный к адаптеру
     *
     * @return список обращений
     */
    public List<Appeal> getData() {
        return this.data;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые
     * впоследствии могут переиспользоваться.
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = inflater.inflate(R.layout.item_appeal, viewGroup, false);

        // Коррекция высоты изображения обложки (16:9)
        int width = viewGroup.getMeasuredWidth();
        int height = (int) ((float) width / 1.77f);
        ImageView cover = (ImageView) v.findViewById(R.id.cover_image);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) cover.getLayoutParams();
        params.width = width;
        params.height = height;
        cover.setLayoutParams(params);
        return new ItemViewHolder(v);
    }

    /**
     * Заполнение элементов данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int i) {
        Appeal appeal = getData().get(i);
        //Logger.Log("Bind appeal: " + appeal.Subject().Name() + " (isLiked=" + appeal.isLiked() + ")");
        final ItemViewHolder viewHolder = (ItemViewHolder) holder;

        // Текст, номер и тематика
        if (appeal.AppealClaim() != null) {
            viewHolder.message.setText(appeal.AppealClaim().Message());
            viewHolder.number.setText(getString(R.string.appeal_number).replace("null", String.valueOf(appeal.Number())));
            viewHolder.subject.setText(appeal.Subject().Name());
            viewHolder.status.setText(appeal.getStatusText());
            viewHolder.status.setTextColor(getColor(Application.AppContext, appeal.getStatusColorId()));
        } else {
            viewHolder.message.setText("-");
            viewHolder.number.setText("-");
            viewHolder.subject.setText("-");
            viewHolder.status.setText("-");
        }


        // Местоположение
        String address = getString(R.string.address_not_set);
        int address_color = grey_color;
        if ((appeal.AppealClaim() != null) && (appeal.AppealClaim().Address() != null)) {
            String appeal_address = appeal.AppealClaim().Address().CustomAddress();
            if (isNotEmpty(appeal_address)) {
                address = appeal_address;
                address_color = blue_color;
            }
        }
        viewHolder.address.setText(address);
        viewHolder.address.setTextColor(address_color);

        // Дата создания
        Calendar added = Calendar.getInstance();
        added.setTimeInMillis(appeal.CreatingDate());
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy в hh:mm", Locale.getDefault());
        viewHolder.dateAdded.setText(formatter.format(added.getTime()));

        // Лайки
        int li = appeal.isLiked() ? R.drawable.green_man : R.drawable.grey_man;
        viewHolder.likedCount.setCompoundDrawablesWithIntrinsicBounds(li, 0, 0, 0);
        viewHolder.likedCount.setText(String.valueOf(appeal.LikesCount()));
        if (!appeal.isAllowAction(Appeal.ACTION_LIKE) && !appeal.isAllowAction(Appeal.ACTION_DISLIKE))
            viewHolder.likedCount.setEnabled(false);
        else
            viewHolder.likedCount.setEnabled(true);

        // Подписки
        int si = appeal.isSubscribed() ? R.drawable.blue_pin : R.drawable.grey_pin;
        viewHolder.subscribedCount.setCompoundDrawablesWithIntrinsicBounds(si, 0, 0, 0);
        viewHolder.subscribedCount.setText(String.valueOf(appeal.SubscribersCount()));
        if (!appeal.isAllowAction(Appeal.ACTION_SUBSCRIBE) && !appeal.isAllowAction(Appeal.ACTION_UNSUBSCRIBE))
            viewHolder.subscribedCount.setEnabled(false);
        else
            viewHolder.subscribedCount.setEnabled(true);

        // Иконки
        viewHolder.timeExpanded.setImageDrawable(appeal.IsAppealProlonged() ? blue_clock : grey_clock);
        viewHolder.hiddenAnswer.setImageDrawable(appeal.IsPublishResponse() ? grey_hide : blue_hide);
        viewHolder.hasAnswer.setImageDrawable(appeal.IsHasResponse() ? blue_resp : grey_resp);
        viewHolder.forwardedAppeal.setImageDrawable(appeal.IsForwarded() ? blue_rdr : grey_rdr);

        // Оценка ответа
        if(appeal.ResponseRating() != null) {
            viewHolder.ratedIcon.setImageResource(R.drawable.greystar_active);
        } else {
            viewHolder.ratedIcon.setImageResource(R.drawable.greystar);
        }
        String ratingText;
        if(appeal.ResponseRating() == null) {
            ratingText = "";
        } else {
            ratingText = String.valueOf(appeal.ResponseRating());
        }
        viewHolder.ratedText.setText(ratingText);

        // Обложка. Асинхронная загрузка изображений с помощью библиотеки UIL
        // Чтобы обложка не мелькала при обновлении записи проверяем было ли ее изменение
        if (appeal.AppealClaim() == null || isEmpty(appeal.AppealClaim().DocumentCoverId()) ||
                !viewHolder.coverId.equals(appeal.AppealClaim().DocumentCoverId())) {
                    viewHolder.coverImage.setImageDrawable(null);
                    viewHolder.coverId = "";
                    if (appeal.AppealClaim() != null && isNotEmpty(appeal.AppealClaim().DocumentCoverId())) {
                        try {
                            String imageUri = ServerTalk.getDocumentUri(appeal.AppealClaim().DocumentCoverId());
                            ImageLoader.getInstance().displayImage(imageUri, viewHolder.coverImage);
                            viewHolder.coverId = appeal.AppealClaim().DocumentCoverId();
                        } catch (Exception e) {
                            Logger.Exception(e);
                        }
                    }
                }
    }

    private void enableStar(ImageView imageView, boolean enable) {

    }

    @Override
    public int getItemCount() {
        return getData().size();
    }

    public Appeal getItem(int position) {
        return position >= 0 && position < data.size() ? data.get(position) : null;
    }

    public Appeal setItem(int position, Appeal appeal) {
        return position >= 0 && position < data.size() ? data.set(position, appeal) : null;
    }

    /**
     * Реализация класса ViewHolder, хранящего ссылки на виджеты
     * Чтобы их не надо было каждый раз искать с помощью findViewById
     * При инициализации заполняет все ссылки
     */
    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Текстовые поля
        private TextView message;
        private TextView number;
        private TextView subject;
        private TextView address;
        private TextView dateAdded;
        private TextView likedCount;
        private TextView subscribedCount;
        private TextView status;

        // Иконки
        private ImageView timeExpanded;
        private ImageView hiddenAnswer;
        private ImageView hasAnswer;
        private ImageView forwardedAppeal;
        private FrameLayout ratedLayout;
        private ImageView ratedIcon;
        private TextView ratedText;
        private ImageView coverImage;
        private String coverId = "";

        // Конструктор - здесь ищем все заполняемые элементы
        public ItemViewHolder(View itemView) {
            super(itemView);
            message = (TextView) itemView.findViewById(R.id.message_text);
            message.setTypeface(typeface_light);
            message.setOnClickListener(this);

            number = (TextView) itemView.findViewById(R.id.number_text);
            number.setOnClickListener(this);
            subject = (TextView) itemView.findViewById(R.id.subject_text);
            subject.setOnClickListener(this);
            address = (TextView) itemView.findViewById(R.id.address_text);
            address.setOnClickListener(this);
            dateAdded = (TextView) itemView.findViewById(R.id.date_added);
            dateAdded.setTypeface(typeface_light);
            dateAdded.setOnClickListener(this);

            likedCount = (TextView) itemView.findViewById(R.id.liked_text);
            likedCount.setOnClickListener(this);
            subscribedCount = (TextView) itemView.findViewById(R.id.subscribed_text);
            subscribedCount.setOnClickListener(this);

            ratedLayout = (FrameLayout) itemView.findViewById(R.id.rated_icon_layout);
            ratedLayout.setOnClickListener(this);
            ratedIcon = (ImageView) itemView.findViewById(R.id.rated_icon);
            ratedText = (TextView) itemView.findViewById(R.id.rated_text);

            coverImage = (ImageView) itemView.findViewById(R.id.cover_image);
            coverImage.setOnClickListener(this);

            status = (TextView) itemView.findViewById(R.id.appeal_status_text);
            status.setOnClickListener(this);

            hasAnswer = (ImageView) itemView.findViewById(R.id.appeal_answer);
            hasAnswer.setOnClickListener(this);

            timeExpanded = (ImageView) itemView.findViewById(R.id.time_expanded);
            timeExpanded.setOnClickListener(this);

            hiddenAnswer = (ImageView) itemView.findViewById(R.id.answer_hidden_icon);
            hiddenAnswer.setOnClickListener(this);

            forwardedAppeal = (ImageView) itemView.findViewById(R.id.forwarded_icon);
            forwardedAppeal.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Appeal appeal = getItem(getAdapterPosition());
            if (appeal == null) return;
            Logger.Log("Click from appeal: " + appeal.Id() + " (" + appeal.Subject().Name() + ")");

            switch (v.getId()) {
                case R.id.message_text:
                case R.id.subject_text:
                case R.id.number_text:
                case R.id.date_added:
                case R.id.cover_image:
                case R.id.appeal_status_text:
                    // Открытие карточки обращение по клику на один из этих элементов
                    if (clickInterface != null) clickInterface.onItemClick(getAdapterPosition());
                    break;

                case R.id.address_text:
                    // Открытие карты по клику на адресе
                    if (clickInterface != null) clickInterface.onItemClick2(getAdapterPosition());
                    break;

                case R.id.time_expanded:
                    popupWindow = new HelpPopup(Application.AppContext, getString(R.string.time_expanded));
                    popupWindow.show(timeExpanded);
                    break;
                case R.id.answer_hidden_icon:
                    popupWindow = new HelpPopup(Application.AppContext, getString(R.string.hidden_reply));
                    popupWindow.show(hiddenAnswer);
                    break;
                case R.id.appeal_answer:
                    popupWindow = new HelpPopup(Application.AppContext, getString(R.string.has_reply));
                    popupWindow.show(hasAnswer);
                    break;
                case R.id.forwarded_icon:
                    popupWindow = new HelpPopup(Application.AppContext, getString(R.string.appeal_redirected));
                    popupWindow.show(forwardedAppeal);
                    break;
                case R.id.rated_icon_layout:
                    popupWindow = new HelpPopup(Application.AppContext, getString(R.string.appeal_rated));
                    popupWindow.show(ratedLayout);
                    break;

                case R.id.liked_text:
                    if (appeal.isLiked()) {
                        if (appeal.isAllowAction(Appeal.ACTION_DISLIKE))
                            new ChangeLikeTask(appeal, getAdapterPosition(), ServerTalk.ACTION_UNLIKE).execute();
                    } else {
                        if (appeal.isAllowAction(Appeal.ACTION_LIKE))
                            new ChangeLikeTask(appeal, getAdapterPosition(), ServerTalk.ACTION_LIKE).execute();
                    }
                    break;

                case R.id.subscribed_text:
                    if (appeal.isSubscribed()) {
                        if (appeal.isAllowAction(Appeal.ACTION_UNSUBSCRIBE))
                            new ChangeSubscribeTask(appeal, getAdapterPosition(), ServerTalk.ACTION_UNSUBSCRIBE).execute();
                    } else {
                        if (appeal.isAllowAction(Appeal.ACTION_SUBSCRIBE))
                            new ChangeSubscribeTask(appeal, getAdapterPosition(), ServerTalk.ACTION_SUBSCRIBE).execute();
                    }
                    break;
            }
        }
    }

    private String getString(int id) {
        return Application.AppContext.getString(id);
    }

    /***
     * Поддержать или отозвать поддержку для обращения
     */
    class ChangeLikeTask extends AsyncTask<String, Void, JSONObject> {
        private Appeal appeal;
        private int position;
        private String action;

        public ChangeLikeTask(Appeal appeal, int position, String action) {
            super();
            this.appeal = appeal;
            this.position = position;
            this.action = action;
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            return server.appealAction(appeal.Id(), action);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == ErrorResult.SUCCESS) {
                    if (action.equals(ServerTalk.ACTION_LIKE)) {
                        appeal.setIsLiked(true);
                        appeal.setLikesCount(appeal.LikesCount() + 1);
                        Logger.Log("Appeal like success!");
                    }

                    if (action.equals(ServerTalk.ACTION_UNLIKE)) {
                        appeal.setIsLiked(false);
                        appeal.setLikesCount(Math.max(0, appeal.LikesCount() - 1));
                        Logger.Log("Appeal unlike success!");
                    }

                    notifyItemChanged(position);
                    new GetAppealFromServer(position, appeal.Id()).execute();
                    return;
                } else {
                    Logger.Warn("Can't like appeal: " + result.getString("errorText"));
                }
            } catch (Exception e) {
                Logger.Exception(e);
                Logger.Warn("Save address error: Can't parse server reply!");
            }

            // Уведомим пользователя о том, что действие не удалось
            if (appeal.isLiked())
                Toast.makeText(Application.AppContext, R.string.cant_unlike, Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(Application.AppContext, R.string.cant_like, Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * Поддержать или отозвать поддержку для обращения
     */
    class ChangeSubscribeTask extends AsyncTask<String, Void, JSONObject> {
        private Appeal appeal;
        private int position;
        private String action;

        public ChangeSubscribeTask(Appeal appeal, int position, String action) {
            super();
            this.appeal = appeal;
            this.position = position;
            this.action = action;
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            return server.appealAction(appeal.Id(), action);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == ErrorResult.SUCCESS) {
                    if (action.equals(ServerTalk.ACTION_SUBSCRIBE)) {
                        appeal.setIsSubscribed(true);
                        appeal.setSubscribersCount(appeal.SubscribersCount() + 1);
                        Logger.Log("Appeal subscribe success!");
                    }

                    if (action.equals(ServerTalk.ACTION_UNSUBSCRIBE)) {
                        appeal.setIsSubscribed(false);
                        appeal.setSubscribersCount(Math.max(0, appeal.SubscribersCount() - 1));
                        Logger.Log("Appeal unsubscribe success!");
                    }

                    notifyItemChanged(position);
                    new GetAppealFromServer(position, appeal.Id()).execute();
                    return;
                } else {
                    Logger.Warn("Can't subscribe/unsubscribe appeal: " + result.getString("errorText"));
                }
            } catch (Exception e) {
                Logger.Exception(e);
                Logger.Warn("Save address error: Can't parse server reply!");
            }

            // Уведомим пользователя о том, что действие не удалось
            if (appeal.isSubscribed())
                Toast.makeText(Application.AppContext, R.string.cant_unsubscribe, Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(Application.AppContext, R.string.cant_subscribe, Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * Обновить обращение с сервера
     */
    class GetAppealFromServer extends AsyncTask<String, Void, JSONObject> {
        private String id;
        private int position;

        public GetAppealFromServer(int position, String id) {
            super();
            this.id = id;
            this.position = position;
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            return server.getAppeal(id);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == ErrorResult.SUCCESS) {
                    Appeal appeal = Appeal.fromJson(result);
                    setItem(position, appeal);
                    notifyItemChanged(position);
                } else {
                    Logger.Warn("Can't load appeal: " + result.getString("errorText"));
                }
            } catch (Exception e) {
                Logger.Exception(e);
                Logger.Warn("Can't load appeal: Can't parse server reply!");
            }
        }
    }
}
