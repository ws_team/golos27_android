package ru.infodev.golos27.Fragments.Profile;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;

public class ChangeListFragment extends ParamFragment {
    public static final int FRAGMENT_ID = R.layout.profile_change_menu;
    private LinearLayout loginConfirmBaloon;
    private String activeEmail = "";
    private String activePhone = "";
    private TextView change_password;
    private TextView all_info;
    private TextView mail_ad;
    private TextView user_profile;

    public ChangeListFragment() {
        titleId = R.string.my_profile;
        setFragmentAllowMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_change_menu, container, false);
        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        setTitle(titleId);

        all_info = (TextView) v.findViewById(R.id.all_info);
        all_info.setTypeface(roboto_light);

        user_profile = (TextView) v.findViewById(R.id.user_profile);
        user_profile.setTypeface(roboto_light);

        change_password = (TextView) v.findViewById(R.id.change_password);
        change_password.setTypeface(roboto_light);

        mail_ad = (TextView) v.findViewById(R.id.mail_ad);
        mail_ad.setTypeface(roboto_light);

        LinearLayout all_info_button = (LinearLayout) v.findViewById(R.id.all_info_button);
        all_info_button.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                chainStartNextFragment(FRAGMENT_ID, ChangeGeneralFragment.class, null, false);
            }
        });


        LinearLayout user_profile_button = (LinearLayout) v.findViewById(R.id.user_profile_button);
        user_profile_button.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                JSONObject result = new JSONObject();
                result.putSafe("activeEmail", activeEmail);
                result.putSafe("activePhone", activePhone);
                chainStartNextFragment(FRAGMENT_ID, ChangeLoginFragment.class, result, false);
            }
        });

        LinearLayout change_password_button = (LinearLayout) v.findViewById(R.id.change_password_button);
        change_password_button.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                chainStartNextFragment(FRAGMENT_ID, ChangePasswordFragment.class, null, false);
            }
        });

        LinearLayout mail_ad_button = (LinearLayout) v.findViewById(R.id.mail_ad_button);
        mail_ad_button.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                chainStartNextFragment(FRAGMENT_ID, ChangeAddressFragment.class, null, false);
            }
        });

        loginConfirmBaloon = (LinearLayout) v.findViewById(R.id.loginConfirmBaloon);
        TextView baloonHideButton = (TextView) loginConfirmBaloon.findViewById(R.id.buttonHide);
        baloonHideButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loginConfirmBaloon.setVisibility(View.GONE);
                Settings.setParameter(Settings.PROFILE_HIDE_POPUP, "true");
            }
        });

        loginConfirmBaloon.setVisibility(View.GONE);

        // Фоновая задача. Если успели - хорошо. Не успели и ладно.
        new ReceiveUnconfirmedLogin().execute();
        return v;
    }

    class ReceiveUnconfirmedLogin extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected final JSONObject doInBackground(Void... params) {
            ServerTalk server = new ServerTalk();
            return server.getAccounts();
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            try {
                int errorCode = (Integer) result.get("errorCode");
                if ((errorCode == Transport.SUCCESS) && fragmentActive) {
                    // Если есть неподтвержденный логин - надо показать его
                    if ((result.has("replyArray")) && (result.get("replyArray") instanceof JSONArray)) {
                        JSONArray loginList = result.getJSONArray("replyArray");
                        for (int i = 0; i < loginList.length(); i++) {
                            if (loginList.get(i) instanceof JSONObject) {
                                JSONObject loginInfo = loginList.getJSONObject(i);
                                final String login;
                                if (loginInfo.isNull("login"))
                                    login = "";
                                else
                                    login = loginInfo.getString("login", "");
                                final boolean isPhone = loginInfo.getBoolean("isPhone");
                                if (isPhone) activePhone = login;
                                else activeEmail = login;

                                if (loginInfo.has("isActive") && loginInfo.get("isActive") instanceof Boolean) {
                                    if (!loginInfo.getBoolean("isActive")) {
                                        if (!isEmpty(login) && !Settings.getBool(Settings.PROFILE_HIDE_POPUP)) {
                                            TextView loginText = (TextView) loginConfirmBaloon.findViewById(R.id.loginConfirmText);
                                            String text = getResources().getString(R.string.confirm_waiting);
                                            loginText.setText(Html.fromHtml(text.replace("null", "<b>" + login + "</b>")));
                                            loginConfirmBaloon.setVisibility(View.VISIBLE);
                                            TextView baloonConfirm = (TextView) loginConfirmBaloon.findViewById(R.id.buttonConfirm);
                                            baloonConfirm.setOnClickListener(new OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    loginConfirmBaloon.setVisibility(View.GONE);
                                                    JSONObject result = new JSONObject();
                                                    result.putSafe("confirm_login", login);

                                                    Class next_chain = ChangeMobileFragment.class;
                                                    if (!isPhone) next_chain = ChangeEmailFragment.class;
                                                    chainStartNextFragment(FRAGMENT_ID, next_chain, result, false);
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Logger.Exception(e);
            }
        }
    }

}






