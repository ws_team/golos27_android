package ru.infodev.golos27.Fragments.AppealClaim;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.NewAppeal;
import ru.infodev.golos27.Common.Catalogs.Subject;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.Dialogs.YesNoDialog;
import ru.infodev.golos27.Fragments.Dialogs.YesNoDialogFragment;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.Adapters.CategoryAdapter;
import ru.infodev.golos27.Widgets.Lists.NestedGridView;


public class SetCategoryFragment extends ParamFragment implements DialogResultInterface {
    public static final int FRAGMENT_ID = R.layout.set_category;
    private CategoryAdapter categoryAdapter;
    NestedGridView categoriesGrid;
    private boolean notChain = false;

    public SetCategoryFragment() {
        titleId = R.string.category_select;
        setFragmentIconCross(true);
        setFragmentAllowMenu(false);
        setFragmentAllowScroll(false);
        setFragmentAuthNeeded(true);
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (getArguments() != null && getArguments().containsKey(BUNDLE_CONTENT)) {
            try {
                String strParam = getArguments().getString(BUNDLE_CONTENT);
                parameters = new JSONObject(strParam);
            } catch (Exception e) {
                Logger.Exception(e);
            }
        } else {
            throw new IllegalArgumentException("Must be created through newInstance(...)");
        }

        notChain = parameters.getBoolean(ParamFragment.NOT_CHAIN, false);
        if (notChain) {
            setFragmentIconCross(false);
            setFragmentAllowMenu(false);
            setFragmentAllowScroll(false);
        }
    }

    @Override
    public boolean ExitRequest() {
        if (notChain)
            return super.ExitRequest();
        else
            YesNoDialog.makeDialog(Application.AppContext, R.string.exit_question,
                    R.string.cancel_appeal, R.string.yes, R.string.no, this).show(getActivity());

        return false;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        setRetainInstance(true);
        setTitle(titleId);

        notChain = parameters.getBoolean(ParamFragment.NOT_CHAIN, false);
        categoriesGrid = (NestedGridView) v.findViewById(R.id.categories);
        categoryAdapter = new CategoryAdapter(Application.AppContext, Subject.GetCategories());
        categoriesGrid.setAdapter(categoryAdapter);
        categoryAdapter.notifyDataSetChanged();
        categoriesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Subject category = categoryAdapter.getItem(position);
                if (category != null) {
                    JSONObject params = new JSONObject();
                    params.putSafe("categoryId", category.Id());
                    params.putSafe("categoryName", category.Name());
                    params.putSafe(ParamFragment.NOT_CHAIN, notChain);
                    chainStartNextFragment(FRAGMENT_ID, SetSubjectFragment.class, params, false);
                }
            }
        });

        return v;
    }

    @Override
    public void DialogSendResult(int dialog_id, int cmd) {
        Logger.Log("Got result from cancel appeal dialog: " + String.valueOf(cmd));
        if (dialog_id == YesNoDialogFragment.DIALOG_ID) {
            NewAppeal.getInstance().Cleanup(true);
            if (cmd == Activity.RESULT_OK) chainPopFragments(FRAGMENT_ID, 1, false);
        }
    }

    @Override
    public void DialogSendParamResult(int dialog_id, int cmd, JSONObject params) {

    }
}
