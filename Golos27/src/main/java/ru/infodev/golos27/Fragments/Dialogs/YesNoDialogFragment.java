package ru.infodev.golos27.Fragments.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;

@SuppressWarnings("unused")
public class YesNoDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final int DIALOG_ID = R.layout.dialog_yes_no;
    private Context context;
    private String buttonOK = null;
    private String buttonCancel = null;
    private String resultText = null;
    private String fullResultText = null;
    private Drawable icon = null;
    private DialogResultInterface resultInterface;
    private boolean isOkButtonPrimary = false;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Logger.Log("Creating ResultDialog");
        View currentView = inflater.inflate(DIALOG_ID, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        currentView.findViewById(R.id.button_continue).setOnClickListener(this);
        currentView.findViewById(R.id.button_cancel).setOnClickListener(this);
        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);

        ImageView resultImage = (ImageView) currentView.findViewById(R.id.resultImage);
        if (icon != null) {
            resultImage.setImageDrawable(icon);
            resultImage.setVisibility(View.VISIBLE);
        } else {
            resultImage.setVisibility(View.VISIBLE);
        }

        if (buttonOK != null) {
            Button button = (Button) currentView.findViewById(R.id.button_continue);
            button.setText(buttonOK);
            if(isOkButtonPrimary) {
                button.setBackgroundResource(R.drawable.button_selector);
                button.setTextColor(ContextCompat.getColor(context, R.color.colorWhiteText));
            }
        }

        if (buttonCancel != null) {
            Button button = (Button) currentView.findViewById(R.id.button_cancel);
            button.setText(buttonCancel);
            if(isOkButtonPrimary) {
                button.setBackgroundResource(R.drawable.button_selector_trans);
                button.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            }
        }

        TextView resultTextView = (TextView) currentView.findViewById(R.id.resultText);
        if (resultText != null) {
            resultTextView.setText(resultText);
            resultTextView.setVisibility(View.VISIBLE);
        } else {
            resultTextView.setVisibility(View.GONE);
        }

        TextView fullTextView = (TextView) currentView.findViewById(R.id.fullResultText);
        if (fullResultText != null) {
            fullTextView.setText(fullResultText);
            fullTextView.setTypeface(roboto_light);
            fullTextView.setVisibility(View.VISIBLE);
        } else {
            fullTextView.setVisibility(View.GONE);
        }

        return currentView;
    }

    public void setMessages(String resultText, String fullResultText, String buttonOK, String buttonCancel) {
        this.buttonOK = buttonOK;
        this.buttonCancel = buttonCancel;
        this.resultText = resultText;
        this.fullResultText = fullResultText;
    }

    public void setOkButtonPrimary() {
        isOkButtonPrimary = true;
    }

    @SuppressWarnings("unused")
    public void setMessages(int resultTextId, int fullResultTextId, int buttonOKId, int buttonCancelId) {
        this.buttonOK = context.getResources().getString(buttonOKId);
        this.buttonCancel = context.getResources().getString(buttonCancelId);
        this.resultText = context.getResources().getString(resultTextId);
        this.fullResultText = context.getResources().getString(fullResultTextId);
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public void setIcon(int iconId) {
        ContextCompat.getDrawable(Application.AppContext, iconId);
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public void show(FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null) {
            super.show(manager, tag);
        }
    }

    @Override
    public void onClick(View lv) {
        switch (lv.getId())
        {
            case R.id.button_continue:
                if (resultInterface != null) resultInterface.DialogSendResult(DIALOG_ID, Activity.RESULT_OK);
                Logger.Log("User selected OK button...");
                dismiss();
                break;
            case R.id.button_cancel:
                if (resultInterface != null) resultInterface.DialogSendResult(DIALOG_ID, Activity.RESULT_CANCELED);
                Logger.Log("User selected Cancel button...");
                dismiss();
                break;
            default:
                Logger.Log("Unknown click from " + lv.toString());
                break;
        }
    }

    public void setResultListener(DialogResultInterface listener) {
        resultInterface = listener;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if (resultInterface != null) resultInterface.DialogSendResult(DIALOG_ID, Activity.RESULT_CANCELED);
    }
}
