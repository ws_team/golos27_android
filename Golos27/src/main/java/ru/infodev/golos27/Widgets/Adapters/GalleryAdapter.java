package ru.infodev.golos27.Widgets.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ru.infodev.golos27.Common.GalleryEntry;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;

public class GalleryAdapter extends BaseAdapter {
    private ArrayList<GalleryEntry> mData;
    LayoutInflater inflater;

    public GalleryAdapter(Context context, List<GalleryEntry> items) {
        mData = (ArrayList<GalleryEntry>) items;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_image_grid, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.thumb = (ImageView) convertView.findViewById(R.id.image_thumb);
            viewHolder.check = (FrameLayout) convertView.findViewById(R.id.selected_sign);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        GalleryEntry item = (GalleryEntry) getItem(position);
        try {
            String uri = Uri.fromFile(new File(item.ThumbnailPath())).toString();
            uri = Uri.decode(uri);

            // Асинхронная загрузка изображений с помощью библиотеки UIL
            // Перед началом загрузки изображение очищается (ставим пустую серую рамку)
            ImageLoader.getInstance().loadImage(uri, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    viewHolder.thumb.setImageResource(R.drawable.gallery_empty);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    viewHolder.thumb.setImageBitmap(loadedImage);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    Logger.Warn("Can't load image into gallery grid (" + failReason.getType().toString() + ")");
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    Logger.Warn("Can't load image into gallery grid (canceled)");
                }
            });
        } catch (Exception e) {
            Logger.Exception(e);
        }

        viewHolder.check.setVisibility(item.isChecked() ? View.VISIBLE : View.INVISIBLE);
        return convertView;
    }

    static class ViewHolder {
        ImageView thumb;
        FrameLayout check;
    }
}
