package ru.infodev.golos27.Widgets.Lists;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.GridView;

import java.lang.reflect.Field;

import ru.infodev.golos27.R;

public class NestedGridView extends GridView {
    boolean clickable = true;

    public NestedGridView(Context context) {
        super(context);
    }

    public NestedGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public NestedGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NestedGridView, 0, 0);
            try {
                clickable = a.getBoolean(R.styleable.NestedGridView_clickable, true);
            } finally {
                a.recycle();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public NestedGridView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public int getColumnWidthCompat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            return super.getColumnWidth();
        else {
            //noinspection TryWithIdenticalCatches
            try {
                Field field = GridView.class.getDeclaredField("mColumnWidth");
                field.setAccessible(true);
                Integer value = (Integer) field.get(this);
                field.setAccessible(false);
                return value;
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev){
        if (clickable) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT)
                requestDisallowInterceptTouchEvent(true);
            return super.onTouchEvent(ev);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return clickable && super.onInterceptTouchEvent(ev);
    }
}
