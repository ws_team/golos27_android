package ru.infodev.golos27;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;

public class PushMsgListenerService extends FirebaseMessagingService {

    final static String GROUP_APPEALS = "group_appeal_notifications";

    @Override
    public void onMessageReceived(RemoteMessage message) {
        String from = message.getFrom();
        String data = message.getData().toString();
        Logger.Log("FCM Message: from=" + from + " (" + data + ")");
        ru.infodev.golos27.Common.Catalogs.Notification notification;
        try {
            notification = ru.infodev.golos27.Common.Catalogs.Notification.fromJson(new JSONObject(data));
            if (notification != null) sendNotification(notification);
        } catch (Exception e) {
            Logger.Exception(e);
        }
    }

    public void sendNotification(ru.infodev.golos27.Common.Catalogs.Notification notificationData) {
        NotificationManager mNotificationManager =
                (NotificationManager) Application.AppContext.getSystemService(NOTIFICATION_SERVICE);

        int requestID = (int) (System.currentTimeMillis() & 0xfffffff);
        Intent intent = new Intent(Application.AppContext, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.putExtra(MainActivity.OPEN_APPEAL_CARD, true);
        intent.putExtra("NotificationID", notificationData.Id());
        intent.putExtra("NotificationObjectID", notificationData.ObjectId());
        PendingIntent pIntent = PendingIntent.getActivity(Application.AppContext, requestID, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        int color = ContextCompat.getColor(Application.AppContext, R.color.notification_color);
        String messageTitle = Application.AppContext.getString(R.string.appeal_number)
                .replace("null", String.valueOf(notificationData.ObjectNumber()));

        Notification notification = new NotificationCompat.Builder(Application.AppContext)
                .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setSmallIcon(R.drawable.notification_icon)
                .setContentText(notificationData.Title())
                .setContentTitle(messageTitle)
                .setContentIntent(pIntent)
                .setGroup(GROUP_APPEALS)
                .setAutoCancel(true)
                .setOngoing(false)
                .setColor(color)
                .build();

        mNotificationManager.notify(Settings.getOrSetNotificationId(null), notification);
    }
}