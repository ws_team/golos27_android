package ru.infodev.golos27.Fragments.AppealClaim;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.BuildConfig;
import ru.infodev.golos27.Common.Catalogs.NewAppeal;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.GalleryEntry;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.UriToPath;
import ru.infodev.golos27.Common.Utils.Utils;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.Adapters.GalleryAdapter;
import ru.infodev.golos27.Widgets.Lists.NestedGridView;

import static ru.infodev.golos27.Common.Utils.Utils.deleteFile;

public class SetPictureFragment extends ParamFragment {
    public static final int FRAGMENT_ID = R.layout.set_picture;

    private static final int REQUEST_CODE_PERMISSION_CAMERA  = 1273;
    private static final int REQUEST_CODE_PERMISSION_GALLERY = 8678;

    private static final int REQUEST_CODE_OPEN_CAMERA = 100;
    private static final int REQUEST_CODE_OPEN_GALLERY = 101;

    private static final int MAX_GALLERY_SIZE = 5000000;
    private boolean notChain = false;

    private LinearLayout skipScreenView;
    private NestedGridView imagesGrid;
    private ImageView deleteButton;
    private ImageView confirmDeleteButton;
    private ImageView cancelDeleteButton;
    private ImageView captureButton;
    private ImageView galleryButton;
    private TextView skipButton;
    private TextView text_take_picture;
    private TextView bottomHelpView;
    private GalleryAdapter imagesAdapter;
    private Uri mCapturedImageUri = null;
    private boolean deleteMode = false;

    /***
     * Конструктор класса. Инициализация заголовка окна
     */
    public SetPictureFragment() {
        titleId = R.string.Gallery;
        setFragmentAllowMenu(false);
    }

    /***
     * Выполняется при создании меню ActionBar
     *
     * @param menu     меню
     * @param inflater распаковщик меню
     */
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        notChain = parameters.getBoolean(ParamFragment.NOT_CHAIN, false);
        if (notChain)
            setActionButtonIcon(ParamFragment.ICON_CHECK);
        else
            setActionButtonIcon(ParamFragment.ICON_RIGHT);

        List<GalleryEntry> attachmentList = NewAppeal.getInstance().ImagesList();
        showEmptyHelp((attachmentList == null) || (attachmentList.size() == 0));
    }

    /***
     * Выполняется при нажатии на кнопку ActionBar
     *
     * @param item кнопка на которую нажали
     * @return обработано нажатие или нет
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_item) {
            if (notChain) {
                chainPopFragments(FRAGMENT_ID, 1, false);
            } else {
                JSONObject params = new JSONObject();
                params.putSafe(ParamFragment.NOT_CHAIN, notChain);
                chainStartNextFragment(FRAGMENT_ID, SetProblemFragment.class, params, false);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setRetainInstance(true);

        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        notChain = parameters.getBoolean(ParamFragment.NOT_CHAIN, false);
        bottomHelpView = (TextView) v.findViewById(R.id.photo_action_help);

        // Кнопка "Пропустить" - пользователь не хочет добавлять фотографии
        skipScreenView = (LinearLayout) v.findViewById(R.id.skip_screen);
        skipButton = (TextView) v.findViewById(R.id.skip_button);
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewAppeal.getInstance().CleanImages();
                if (notChain) {
                    chainPopFragments(FRAGMENT_ID, 1, false);
                } else {
                    chainStartNextFragment(FRAGMENT_ID, SetProblemFragment.class, null, false);
                }
            }
        });

        // Кнопка подтверждения удаления фотографии
        confirmDeleteButton = (ImageView) v.findViewById(R.id.confirm_delete_button);
        confirmDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwitchBottomPanel(false);
                DeletePhotos();
            }
        });

        // Отмена удаления фотографий
        cancelDeleteButton = (ImageView) v.findViewById(R.id.cancel_del_photo_button);
        cancelDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClearSelection();
                SwitchBottomPanel(false);
            }
        });

        // Кнопка удаления фотографии
        deleteButton = (ImageView) v.findViewById(R.id.del_photo_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwitchBottomPanel(true);
            }
        });

        // Кнопка получения фотографии с помощью фотокамеры
        captureButton = (ImageView) v.findViewById(R.id.capture_button);
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getPermission(REQUEST_CODE_PERMISSION_CAMERA)) takePhoto();
            }
        });

        text_take_picture= (TextView) v.findViewById(R.id.text_take_picture);
        text_take_picture.setTypeface(roboto_light);

        // Кнопка получения фотографии из галереи
        galleryButton = (ImageView) v.findViewById(R.id.gallery_button);
        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getPermission(REQUEST_CODE_PERMISSION_GALLERY)) openGallery();
            }
        });

        // Наполнение альбома изображений из текущего обращения
        NewAppeal.getInstance().ClearImagesSelection();
        List<GalleryEntry> attachmentList = NewAppeal.getInstance().ImagesList();
        showEmptyHelp((attachmentList == null) || (attachmentList.size() == 0));
        showAddButtons(attachmentList);
        imagesAdapter = new GalleryAdapter(Application.AppContext, NewAppeal.getInstance().ImagesList());
        imagesGrid = (NestedGridView) v.findViewById(R.id.images_list);
        imagesGrid.setAdapter(imagesAdapter);
        imagesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (deleteMode) {
                    GalleryEntry item = (GalleryEntry) imagesAdapter.getItem(position);
                    View selectedSign = view.findViewById(R.id.selected_sign);
                    if (item.isChecked()) {
                        selectedSign.setVisibility(View.INVISIBLE);
                        item.setChecked(false);
                    } else {
                        selectedSign.setVisibility(View.VISIBLE);
                        item.setChecked(true);
                    }
                    showConfirmDeleteButton();
                }
            }
        });

        return v;
    }

    private void takePhoto() {
        mCapturedImageUri = null;
        Intent photoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (photoIntent.resolveActivity(getActivity().getPackageManager()) != null) {

            File newFile = Utils.getNewImageFile();
            if (newFile != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    mCapturedImageUri = FileProvider.getUriForFile(getActivity(),
                            BuildConfig.APPLICATION_ID + ".provider",
                            newFile);
                } else {
                    mCapturedImageUri = Uri.fromFile(newFile);
                }
            }

            photoIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageUri);
            startActivityForResult(photoIntent, REQUEST_CODE_OPEN_GALLERY);
        }
    }

    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);

        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, REQUEST_CODE_OPEN_CAMERA);
    }

    private boolean getPermission(int requestCode) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE }, requestCode);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode== REQUEST_CODE_PERMISSION_CAMERA) {
                takePhoto();
            } else if (requestCode== REQUEST_CODE_PERMISSION_GALLERY) {
                openGallery();
            }
        } else {
            //none yet
        }
    }

    /***
     * Вызывается при возврате из приложения фотографии или галереи
     *
     * @param requestCode код, переданный приложению при запуске
     * @param resultCode  код результата выполнения операции
     * @param data        данные (результат выполнения операции)
     */
    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) return;

        switch (requestCode) {
            case REQUEST_CODE_OPEN_GALLERY:
                if (mCapturedImageUri != null) {
                    AddNewPhoto(mCapturedImageUri);
                    deleteFile(UriToPath.getPath(Application.AppContext, mCapturedImageUri));
                }
                break;

            case REQUEST_CODE_OPEN_CAMERA:
                if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) && (data.getData() == null)) {
                    // Множественный выбор
                    ClipData clipdata = data.getClipData();
                    if (clipdata != null) {
                        for (int i = 0; i < clipdata.getItemCount(); i++) {
                            Uri selectedImageUri = clipdata.getItemAt(i).getUri();
                            if (selectedImageUri != null) AddNewPhoto(selectedImageUri);
                        }
                    }
                } else {
                    // В старых версиях до API18 можно выбрать только одно изображение
                    Uri selectedImageUri = data.getData();
                    if (selectedImageUri != null) AddNewPhoto(selectedImageUri);
                }
        }
    }

    /***
     * Переключает нижнюю панель в режим удаления фотографии(й)
     *
     * @param delete если true - включает режим удаления
     */
    public void SwitchBottomPanel(boolean delete) {
        if (delete) {
            deleteMode = true;
            showConfirmDeleteButton();
            cancelDeleteButton.setVisibility(View.VISIBLE);
            bottomHelpView.setVisibility(View.VISIBLE);
            captureButton.setVisibility(View.INVISIBLE);
            galleryButton.setVisibility(View.INVISIBLE);
            deleteButton.setVisibility(View.INVISIBLE);

        } else {
            deleteMode = false;
            bottomHelpView.setVisibility(View.INVISIBLE);
            confirmDeleteButton.setVisibility(View.INVISIBLE);
            cancelDeleteButton.setVisibility(View.INVISIBLE);
            deleteButton.setVisibility(View.VISIBLE);
            showAddButtons(NewAppeal.getInstance().ImagesList());
        }
    }

    private void showConfirmDeleteButton() {
        if (SelectedItemsNumber() > 0) {
            confirmDeleteButton.setVisibility(View.VISIBLE);
            bottomHelpView.setText(R.string.delete_photos);
        } else {
            confirmDeleteButton.setVisibility(View.INVISIBLE);
            bottomHelpView.setText(R.string.select_photos);
        }
    }

    /***
     * Добавляет новую фотографию в текущее обращение
     *
     * @param imageUri - фотография для добавления (Uri)
     */
    public void AddNewPhoto(Uri imageUri) {
        ContentResolver cr = Application.AppContext.getContentResolver();
        String mime = cr.getType(imageUri);
        if (mime == null) {
            String path = UriToPath.getPath(Application.AppContext, imageUri);
            String extension = MimeTypeMap.getFileExtensionFromUrl(path);
            if (extension != null) {
                MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
                mime = mimeTypeMap.getMimeTypeFromExtension(extension);
            }
        }

        if (mime != null) {
            if (!mime.startsWith("image/")) {
                Toast.makeText(Application.AppContext, R.string.only_images_allowed, Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            Toast.makeText(Application.AppContext, R.string.cant_get_document_type, Toast.LENGTH_SHORT).show();
            return;
        }

        if (!ListCheckUri(imageUri)) {
            showEmptyHelp(false);
            GalleryEntry newEntry = GalleryEntry.fromImage(imageUri,
                    imagesGrid.getColumnWidthCompat(), MAX_GALLERY_SIZE);
            if (newEntry != null) {
                NewAppeal.getInstance().AddImage(newEntry);
                imagesAdapter.notifyDataSetChanged();
                showAddButtons(NewAppeal.getInstance().ImagesList());
            } else {
                Toast.makeText(getContext(), R.string.cant_add_image, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), R.string.image_allready_added, Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * Показывает или прячет кнопки добавления изображений в зависимости от
     * того превышен лимит на изображения или нет. Максимально можно добавить 12 изображений
     */
    private void showAddButtons(List<GalleryEntry> galleryEntries) {
        if (galleryEntries != null && galleryEntries.size() >= 12) {
            galleryButton.setVisibility(View.INVISIBLE);
            captureButton.setVisibility(View.INVISIBLE);
        } else {
            galleryButton.setVisibility(View.VISIBLE);
            captureButton.setVisibility(View.VISIBLE);
        }
    }

    /***
     * Снимает выделение с фотографий
     */
    public void ClearSelection() {
        List<GalleryEntry> galleryEntries = NewAppeal.getInstance().ImagesList();
        if (galleryEntries != null)
            for (GalleryEntry entry : galleryEntries)
                if (entry.isChecked()) entry.setChecked(false);

        imagesAdapter.notifyDataSetChanged();
    }

    /***
     * Возвращает количество выделенных элементов в списке
     */
    public int SelectedItemsNumber() {
        int selectedCount = 0;
        List<GalleryEntry> galleryEntries = NewAppeal.getInstance().ImagesList();
        if (galleryEntries != null)
            for (GalleryEntry entry : galleryEntries)
                if (entry.isChecked()) selectedCount++;

        return selectedCount;
    }

    /***
     * Возвращает количество выделенных элементов в списке
     */
    public boolean ListCheckUri(Uri uri) {
        List<GalleryEntry> galleryEntries = NewAppeal.getInstance().ImagesList();
        if (galleryEntries != null)
            for (GalleryEntry entry : galleryEntries)
                if (entry.OriginalUri().equals(uri)) return true;

        return false;
    }

    /***
     * Удаляет одну или несколько фотографий из текущего обращения
     * (только те что помечены на удаление - isChecked = true)
     */
    public void DeletePhotos() {
        List<GalleryEntry> galleryEntries = NewAppeal.getInstance().ImagesList();
        if (galleryEntries != null) {
            for (Iterator<GalleryEntry> it = galleryEntries.iterator(); it.hasNext(); ) {
                GalleryEntry entry = it.next();
                if (entry.isChecked()) {
                    deleteFile(entry.ImagePath());
                    deleteFile(entry.ThumbnailPath());
                    it.remove();
                }
            }
        }
        imagesAdapter.notifyDataSetChanged();
        showEmptyHelp((galleryEntries == null) || (galleryEntries.size() == 0));
        showAddButtons(galleryEntries);
    }

    /***
     * Показывает или прячет кнопку "Пропустить" и помощь
     *
     * @param show если true - показать
     */
    public void showEmptyHelp(boolean show) {
        if (show) {
            setActionButtonVisibility(false);
            skipScreenView.setVisibility(View.VISIBLE);
            skipButton.setVisibility(View.VISIBLE);
            deleteButton.setVisibility(View.INVISIBLE);
        } else {
            setActionButtonVisibility(true);
            skipScreenView.setVisibility(View.INVISIBLE);
            skipButton.setVisibility(View.INVISIBLE);
            deleteButton.setVisibility(View.VISIBLE);
        }
    }
}
