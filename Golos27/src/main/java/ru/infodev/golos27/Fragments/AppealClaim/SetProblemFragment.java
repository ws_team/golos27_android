package ru.infodev.golos27.Fragments.AppealClaim;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.NewAppeal;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.MonitoredEditText;

public class SetProblemFragment extends ParamFragment implements TextWatcherAdapter.TextWatcherListener {
    public static final int FRAGMENT_ID = R.layout.set_problem;
    private boolean problemTextGood = false;
    private MonitoredEditText describeProblemEditText;
    private boolean notChain = false;

    public SetProblemFragment() {
        titleId = R.string.description;
        setFragmentAllowMenu(false);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        notChain = parameters.getBoolean(ParamFragment.NOT_CHAIN, false);
        if (notChain)
            setActionButtonIcon(ParamFragment.ICON_CHECK);
        else
            setActionButtonIcon(ParamFragment.ICON_RIGHT);

        showSaveButtonIfCan();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_item) {
            NewAppeal.getInstance().setMessage(describeProblemEditText.getText().toString());
            if (notChain) {
                chainPopFragments(FRAGMENT_ID, 1, false);
            } else {
                chainStartNextFragment(FRAGMENT_ID, ConfirmAppealFragment.class, null, 5, true);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setRetainInstance(true);

        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        notChain = parameters.getBoolean(ParamFragment.NOT_CHAIN, false);
        describeProblemEditText = (MonitoredEditText) v.findViewById(R.id.describeProblemEditText);
        describeProblemEditText.setTypeface(roboto_light);
        describeProblemEditText.setOnTextChangedListener(this);
        String message = NewAppeal.getInstance().Message();
        describeProblemEditText.setText(message);
        showSaveButtonIfCan();
        return v;
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        if (view.getId() == R.id.describeProblemEditText) problemTextGood = text.length() > 0;
        showSaveButtonIfCan();
    }

    /***
     * Показывает или прячет кнопку "Далее" на доске задач
     * в зависимости от валидности введенных данных
     */
    private void showSaveButtonIfCan() {
        setActionButtonVisibility(problemTextGood);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}
