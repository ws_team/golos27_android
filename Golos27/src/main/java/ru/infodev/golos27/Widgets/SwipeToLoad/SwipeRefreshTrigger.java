package ru.infodev.golos27.Widgets.SwipeToLoad;

public interface SwipeRefreshTrigger {
    void onRefresh();
}
