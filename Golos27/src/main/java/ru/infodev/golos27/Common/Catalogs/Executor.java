package ru.infodev.golos27.Common.Catalogs;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.DBHelper;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

@SuppressWarnings("unused")
public class Executor {
    String id = "";
    String name = "";
    String code = "";
    String orgId = "";
    String orgName = "";
    String orgCategoryId = "";
    String orgCategoryGroupId = "";
    String orgCategoryGroupName = "";
    String orgCategoryGroupIconPrefix = "";
    String orgCategoryGroupIconSuffix = "";

    boolean isDeleted;
    boolean isChecked = false;
    long version = 0;
    //-----------------------------------------------------------------------------

    public Executor(String id, String name, String code, String categoryId, long version) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.orgCategoryId = categoryId;
        this.version = version;
    }

    public Executor(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Executor(String id, String name, String code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    public Executor() {
    }

    public String Id() {
        return id;
    }

    public Executor setId(String id) {
        this.id = id;
        return this;
    }

    public String Name() {
        return name;
    }

    public Executor setName(String name) {
        this.name = name;
        return this;
    }

    public String Code() {
        return code;
    }

    public Executor setCode(String code) {
        this.code = code;
        return this;
    }

    public String OrgCategoryId() {
        return orgCategoryId;
    }

    public Executor setOrgCategoryId(String orgCategoryId) {
        this.orgCategoryId = orgCategoryId;
        return this;
    }

    public long Version() {
        return version;
    }

    public Executor setVersion(long version) {
        this.version = version;
        return this;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public String OrgId() {
        return orgId;
    }

    public Executor setOrgId(String orgId) {
        this.orgId = orgId;
        return this;
    }

    public String OrgName() {
        return orgName;
    }

    public Executor setOrgName(String orgName) {
        this.orgName = orgName;
        return this;
    }

    public String OrgCategoryGroupId() {
        return orgCategoryGroupId;
    }

    public Executor setOrgCategoryGroupId(String orgCategoryGroupId) {
        this.orgCategoryGroupId = orgCategoryGroupId;
        return this;
    }

    public String OrgCategoryGroupName() {
        return orgCategoryGroupName;
    }

    public Executor setOrgCategoryGroupName(String orgCategoryGroupName) {
        this.orgCategoryGroupName = orgCategoryGroupName;
        return this;
    }

    public String OrgCategoryGroupIconPrefix() {
        return orgCategoryGroupIconPrefix;
    }

    public Executor setOrgCategoryGroupIconPrefix(String orgCategoryGroupIconPrefix) {
        this.orgCategoryGroupIconPrefix = orgCategoryGroupIconPrefix;
        return this;
    }

    public String OrgCategoryGroupIconSuffix() {
        return orgCategoryGroupIconSuffix;
    }

    public Executor setOrgCategoryGroupIconSuffix(String orgCategoryGroupIconSuffix) {
        this.orgCategoryGroupIconSuffix = orgCategoryGroupIconSuffix;
        return this;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    @Override
    public String toString() {
        return name;
    }


    public boolean IsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /***
     * Сохраним текущий объект в базу данных
     */
    private boolean save(boolean commit) {
        if (IsDeleted()) return remove(commit);
        DBHelper database = Settings.getDatabase();
        if (database == null) return false;

        SQLiteDatabase db = null;
        try {
            db = database.getWritableDatabase();
            ContentValues cv = new ContentValues();

            cv.put("id", Id());
            cv.put("name", Name());
            cv.put("code", Code());
            cv.put("categoryId", OrgCategoryId());
            cv.put("version", Version());
            cv.put("search", Name().toLowerCase(Locale.getDefault()));

            long iNum = db.insert(Application.AppContext.getString(R.string.executors_table_name), null, cv);
            if (iNum <= 0) Logger.Log("Can't write value for " + toString());
            if ((db.inTransaction()) && commit) db.endTransaction();
            return iNum > 0;

        } catch (Exception ex) {
            if ((db != null) && db.inTransaction()) {
                db.endTransaction();
            }
            Logger.Exception(ex);
        }

        return false;
    }

    public boolean save() {
        return save(true);
    }

    /***
     * Удалим текущий объект из базы данных
     */
    public boolean remove(boolean commit) {
        DBHelper database = Settings.getDatabase();
        if (database == null) return false;
        String[] id = {Id()};
        SQLiteDatabase db = null;
        try {
            db = database.getWritableDatabase();
            long iNum = db.delete(Application.AppContext.getString(R.string.executors_table_name), "id=?", id);
            if (iNum <= 0) Logger.Log("Executors: can't delete value of " + Id());
            if (db.inTransaction() && commit) db.endTransaction();
        } catch (Exception ex) {
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        return false;
    }

    public boolean remove() {
        return remove(true);
    }

    // Статические методы класса
    public static Executor fromJson(JSONObject executorUnit) {
        Executor result = new Executor();
        if (executorUnit == null) return result;
        try {
            result.setId(executorUnit.getString("id"));
            result.setName(executorUnit.getString("name"));

            if (executorUnit.has("code") && !executorUnit.isNull("code"))
                result.setCode(executorUnit.getString("code"));

            if (executorUnit.has("orgId") && !executorUnit.isNull("orgId"))
                result.setOrgId(executorUnit.getString("orgId"));

            if (executorUnit.has("orgName") && !executorUnit.isNull("orgName"))
                result.setOrgName(executorUnit.getString("orgName"));

            if (executorUnit.has("orgCategoryId") && !executorUnit.isNull("orgCategoryId"))
                result.setOrgCategoryId(executorUnit.getString("orgCategoryId"));

            if (executorUnit.has("orgCategoryGroupId") && !executorUnit.isNull("orgCategoryGroupId"))
                result.setOrgCategoryGroupId(executorUnit.getString("orgCategoryGroupId"));

            if (executorUnit.has("orgCategoryGroupName") && !executorUnit.isNull("orgCategoryGroupName"))
                result.setOrgCategoryGroupName(executorUnit.getString("orgCategoryGroupName"));

            if (executorUnit.has("orgCategoryGroupIconPrefix") && !executorUnit.isNull("orgCategoryGroupIconPrefix"))
                result.setOrgCategoryGroupIconPrefix(executorUnit.getString("orgCategoryGroupIconPrefix"));

            if (executorUnit.has("orgCategoryGroupIconSuffix") && !executorUnit.isNull("orgCategoryGroupIconSuffix"))
                result.setOrgCategoryGroupIconSuffix(executorUnit.getString("orgCategoryGroupIconSuffix"));

            if (executorUnit.has("version") && !executorUnit.isNull("version"))
                result.setVersion(executorUnit.getLong("version"));

            if (executorUnit.has("isDeleted") && !executorUnit.isNull("isDeleted"))
                result.setIsDeleted(executorUnit.getBoolean("isDeleted"));

            if (executorUnit.has("privateIsChecked") && !executorUnit.isNull("privateIsChecked"))
                result.setIsChecked(executorUnit.getBoolean("privateIsChecked"));

        } catch (Exception e) {
            Logger.Exception(e);
            return null;
        }

        return result;
    }

    public JSONObject toJson() {
        JSONObject result = new JSONObject();
        result.putSafe("id", id);
        result.putSafe("name", name);
        result.putSafe("code", code);
        result.putSafe("orgId", orgId);
        result.putSafe("orgName", orgName);
        result.putSafe("orgCategoryId", orgCategoryId);
        result.putSafe("orgCategoryGroupId", orgCategoryGroupId);
        result.putSafe("orgCategoryGroupName", orgCategoryGroupName);
        result.putSafe("orgCategoryGroupIconPrefix", orgCategoryGroupIconPrefix);
        result.putSafe("orgCategoryGroupIconSuffix", orgCategoryGroupIconSuffix);
        result.putSafe("isDeleted", isDeleted);
        result.putSafe("privateIsChecked", isChecked);
        result.putSafe("version", version);
        return result;
    }

    public static List<Executor> Search(String search) {
        return Search(search, null, 1024);
    }

    public static List<Executor> Search(String search, String code, Integer limit) {
        Cursor c = null;
        String clause = null;
        String[] selectVal;
        List<Executor> result = null;
        DBHelper database = Settings.getDatabase();
        if (database == null) return null;
        SQLiteDatabase db = null;
        if (search != null) search = search.toLowerCase();

        try {
            if (search != null) {
                clause = "search LIKE ?";
                selectVal = new String[]{"%" + search + "%"};
            } else {
                selectVal = null;
            }

            if (code != null) {
                if (isEmpty(clause)) clause = "";
                if (isNotEmpty(clause)) clause += " AND ";
                clause += "code LIKE ?";
                if (selectVal != null) {
                    selectVal = concat(selectVal, new String[]{code});
                } else {
                    selectVal = new String[]{code};
                }
            }

            db = database.getWritableDatabase();
            c = db.query(Application.AppContext.getString(R.string.executors_table_name),
                    null, clause, selectVal, null, null, null, limit.toString());

            result = new ArrayList<>();
            if ((c.getCount() <= 0)) {
                return result;
            } else {
                // У нас есть результаты!
                if (c.moveToFirst()) {
                    do {
                        // Executor(id, name, code, orgCategoryId, version) {
                        Executor executor = new Executor(c.getString(0), c.getString(1), c.getString(2),
                                c.getString(3), c.getLong(4));
                        result.add(executor);
                    } while (c.moveToNext());

                    return result;
                }
            }

            c.close();
            if (db.inTransaction()) db.endTransaction();
        } catch (Exception ex) {
            if (c != null) c.close();
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        return result;
    }

    public static List<Executor> All() {
        Cursor c = null;
        String clause = null;
        String[] selectVal;
        List<Executor> result = null;
        DBHelper database = Settings.getDatabase();
        if (database == null) return null;
        SQLiteDatabase db = null;
        try {
            db = database.getWritableDatabase();
            c = db.query(Application.AppContext.getString(R.string.executors_table_name),
                    null, null, null, null, null, null);

            result = new ArrayList<>();
            if ((c.getCount() <= 0)) {
                return result;
            } else {
                // У нас есть результаты!
                if (c.moveToFirst()) {
                    do {
                        // Executor(id, name, code, orgCategoryId, version) {
                        Executor executor = new Executor(c.getString(0), c.getString(1), c.getString(2),
                                c.getString(3), c.getLong(4));
                        result.add(executor);
                    } while (c.moveToNext());

                    return result;
                }
            }

            c.close();
            if (db.inTransaction()) db.endTransaction();
        } catch (Exception ex) {
            if (c != null) c.close();
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        return result;
    }

    public static Executor Get(String id) {
        Cursor c = null;
        String clause;
        String[] selectVal;
        DBHelper database = Settings.getDatabase();
        if (database == null) return null;
        SQLiteDatabase db = null;
        try {
            if (id == null) return null;
            clause = "id = ?";
            selectVal = new String[]{id};

            db = database.getWritableDatabase();
            c = db.query(Application.AppContext.getString(R.string.executors_table_name),
                    null, clause, selectVal, null, null, null);

            if ((c.getCount() <= 0)) {
                return null;
            } else {
                // У нас есть результат!
                if (c.moveToFirst()) {
                    return new Executor(c.getString(0), c.getString(1), c.getString(2), c.getString(3), c.getLong(4));
                }
            }

            c.close();
            if (db.inTransaction()) db.endTransaction();
        } catch (Exception ex) {
            if (c != null) c.close();
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        return null;
    }

    public static boolean updateListFromServer(JSONObject serverReply) {
        boolean result = false;
        JSONArray jsonList = null;
        try {
            if (serverReply == null) return false;
            if (serverReply.has(JSONObject.JSON_LONG_STRING)) {
                JSONObject temp = new JSONObject(serverReply.getString(JSONObject.JSON_LONG_STRING));
                if (temp.has("response")) {
                    jsonList = temp.getJSONArray("response");
                }
            } else {
                jsonList = serverReply.getJSONArray("replyArray");
            }

            if (jsonList == null) return false;
            DBHelper database = Settings.getDatabase();
            if (database == null) return false;

            SQLiteDatabase db = database.getWritableDatabase();
            Long maxVersion = Settings.getLong(Settings.EXECUTORS_VERSION);
            try {
                db.beginTransaction();
                for (int i = 0; i < jsonList.length(); i++) {
                    JSONObject row = jsonList.getJSONObject(i);
                    Executor executor = Executor.fromJson(row);
                    if (executor != null) {
                        long version = executor.Version();
                        if (version > maxVersion) maxVersion = version;
                        executor.save(false);
                    }
                }
                db.setTransactionSuccessful();
                result = true;
            } catch (Exception e) {
                if ((db != null) && db.inTransaction()) db.endTransaction();
                Logger.Exception(e);
            } finally {
                if (db != null) db.endTransaction();
                Settings.setParameter(Settings.EXECUTORS_VERSION, maxVersion.toString());
            }

        } catch (Exception e) {
            Logger.Exception(e);
        }

        return result;
    }

    static String[] concat(String[]... arrays) {
        int length = 0;
        for (String[] array : arrays) {
            length += array.length;
        }
        String[] result = new String[length];
        int pos = 0;
        for (String[] array : arrays) {
            for (String element : array) {
                result[pos] = element;
                pos++;
            }
        }
        return result;
    }
}
