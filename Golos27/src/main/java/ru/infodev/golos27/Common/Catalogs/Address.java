package ru.infodev.golos27.Common.Catalogs;

import ru.infodev.golos27.Common.Utils.Json.JSONObject;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

@SuppressWarnings("unused")
public class Address {
    public static double INVALID_COORDINATE = 1000000;

    private OKTMO Settlement;
    private String customAddress;
    private double latitude = INVALID_COORDINATE;
    private double longitude = INVALID_COORDINATE;
    private int zoomLevel;
    private boolean mChangedByUser = false;
    //---------------------------------------------------------

    public boolean isChangedByUser() {
        return mChangedByUser;
    }

    public Address() {
    }

    public Address setChangedByUser(boolean changed) {
        mChangedByUser = changed;
        return this;
    }

    public Address(OKTMO settlement, String address) {
        this.Settlement = settlement;
        this.customAddress = address;
    }

    public OKTMO Settlement() {
        return Settlement;
    }

    public Address setSettlement(OKTMO settlement) {
        Settlement = settlement;
        return this;
    }

    public String CustomAddress() {
        return customAddress;
    }

    public Address setCustomAddress(String customAddress) {
        this.customAddress = customAddress;
        return this;
    }

    public double Latitude() {
        return latitude;
    }

    public Address setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double Longitude() {
        return longitude;
    }

    public Address setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public String FullAddress() {
        return FullAddress(false);
    }

    public void setZoomLevel(int userZoomLevel) {
        this.zoomLevel = userZoomLevel;
    }

    public int ZoomLevel() {
        return this.zoomLevel;
    }

    public String FullAddress(boolean extra) {
        String sep = "";
        String Address;
        String Settlement = "";
        Address = CustomAddress();
        if (Address == null) Address = "";
        if (Settlement() != null) {
            if (extra)
                Settlement = Settlement().getFullSettlement();
            else
                Settlement = Settlement().getFullCity();
        }
        if (isNotEmpty(Address) && isNotEmpty(Settlement)) sep = ", ";
        return String.format("%s%s%s", Settlement, sep, Address);
    }

    @Override
    public String toString() {
        return FullAddress();
    }

    public static Address fromJson(JSONObject source) {
        if (source == null) return null;
        Address newAddress = new Address();
        newAddress.setSettlement(OKTMO.Get(source.getString("oktmoCode", "")));
        if (newAddress.Settlement() == null) {
            newAddress.setSettlement(new OKTMO());
            newAddress.Settlement().setCode(source.getString("oktmoCode", ""));
        }

        newAddress.Settlement().setData(source.getString("oktmoData", ""));
        newAddress.setLatitude(source.getDouble("latitude", INVALID_COORDINATE));
        newAddress.setLongitude(source.getDouble("longitude", INVALID_COORDINATE));
        newAddress.setCustomAddress(source.getString("customAddress", ""));
        newAddress.setZoomLevel(source.getInt("privateZoomLevel", 6));
        return newAddress;
    }

    public JSONObject toJson() {
        JSONObject result = new JSONObject();
        result.putSafe("latitude", this.latitude);
        result.putSafe("longitude", this.longitude);
        result.putSafe("customAddress", this.customAddress);
        result.putSafe("privateZoomLevel", this.zoomLevel);
        result.putSafe("oktmoCode", Settlement != null ? Settlement.getCode() : -1);
        result.putSafe("oktmoData", Settlement != null ? Settlement.getData() : -1);
        return result;
    }
}
