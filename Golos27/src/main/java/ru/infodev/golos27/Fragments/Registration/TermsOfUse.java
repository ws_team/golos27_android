package ru.infodev.golos27.Fragments.Registration;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import java.io.IOException;
import java.io.InputStream;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.PatchedTextView;

public class TermsOfUse extends ParamFragment {
    public static final int FRAGMENT_ID = R.layout.terms_of_use;

    public TermsOfUse() {
        titleId = R.string.terms_of_use_main;
        setFragmentAllowMenu(false);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        setTitle(titleId);
        WebView terms_of_use = (WebView) v.findViewById(R.id.terms_of_use);
        try {
            InputStream is = getActivity().getAssets().open("TermsOfUse.html");
            byte[] buffer = new byte[is.available()];
            if (is.read(buffer) <= 0) Logger.Warn("Can't read embedded assets TermsOfUse.html file!");
            is.close();

            String TermsOfUse = new String(buffer);
            terms_of_use.loadDataWithBaseURL(null, TermsOfUse, "text/html", "utf-8", null);

        } catch (IOException e) {
            Logger.Exception(e);
        }

        return v;
    }
}
