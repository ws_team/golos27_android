package ru.infodev.golos27.Fragments.Restore;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.CustomEditText;

public class RestConfirmFragment extends ParamFragment implements
        View.OnClickListener, TextWatcherAdapter.TextWatcherListener, DialogResultInterface {
    public static final int FRAGMENT_ID = R.id.RestoreConfirmFragment;
    private Button button;
    private CustomEditText codeEditText;
    private ProgressBar progress;
    private Timer timer;
    private TextView timerText;
    private CountdownTask timerTask;
    private String stringTimerText;

    boolean buttonSendAction = true;

    public RestConfirmFragment() {
        titleId = R.string.confirmation;
        setFragmentAllowMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.registration_confirm, container, false);
        setTitle(titleId);

        button = (Button) v.findViewById(R.id.send_security_code_button);
        button.setOnClickListener(this);

        stringTimerText = getResources().getString(R.string.send_the_code_again);
        progress = (ProgressBar) v.findViewById(R.id.progressBar);
        codeEditText = (CustomEditText) v.findViewById(R.id.edit_code);
        codeEditText.setOnTextChangedListener(this);
        TextView changeNotificationView = (TextView) v.findViewById(R.id.notification_by_email);
        changeNotificationView.setVisibility(View.GONE);
        timerText = (TextView) v.findViewById(R.id.send_the_code_again);

        Long ttl = -1L;
        if (parameters.has("codeTTL")) ttl = parameters.getLong("codeTTL", null);

        StopTimer();
        timer = new Timer();
        timerTask = new CountdownTask(ttl);
        timer.schedule(timerTask, 1000, 1000);

        String stringHeader = getResources().getString(R.string.confirmation_entry_up);
        TextView screenHeaderView = (TextView) v.findViewById(R.id.confirmation_of_entry_text);
        screenHeaderView.setText(stringHeader);

        return v;
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        button.setEnabled(text.length() >= 6);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // pass
    }

    @Override
    public void afterTextChanged(Editable s) {
        // pass
    }

    /***
     * Обработчик нажатий на какие-то элементы
     *
     * @param v вид на котором произошло нажатие
     */
    @Override
    public void onClick(View v) {
        String login = parameters.getString("input_login", "");

        switch (v.getId()) {
            case R.id.send_security_code_button:
                String token = parameters.getString("token", "");
                String code = codeEditText.getText().toString();
                if (buttonSendAction) {
                    new RestoreConfirmTask().execute(code, token);
                } else {
                    new SendCodeTask().execute(login);
                }
                break;
        }
    }

    /***
     * Сюда падают результаты с диалоговых окон
     *
     * @param dialog_id идентификатор диалогового окна
     * @param cmd       команда от диалогового окна
     */
    @Override
    public void DialogSendResult(int dialog_id, int cmd) {
        Toast.makeText(getActivity(), "Result: " + String.valueOf(dialog_id) + "/" + String.valueOf(cmd),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void DialogSendParamResult(int dialog_id, int cmd, JSONObject params) {

    }

    /***
     * Отправка кода подтверждения на сервер, при неудаче - диалоговое окно
     * при успехе переход на другой фрагмент
     */
    class RestoreConfirmTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            button.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            // code, token
            ServerTalk server = new ServerTalk();
            return server.restoreConfirm(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            progress.setVisibility(View.INVISIBLE);
            button.setVisibility(View.VISIBLE);
            button.setEnabled(false);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    StopTimer();
                    button.setEnabled(true);
                    chainStartNextFragment(FRAGMENT_ID, RestCompleteFragment.class, result, 0, false);

                } else {
                    ResultDialog.showError(getActivity(), "Ошибка восстановления пароля", errorCode, result.getErrorText());
                }
            } catch (Exception e) {
                Logger.Exception(e);
                ResultDialog.showError(getActivity(), "Ошибка восстановления пароля", ErrorResult.INTERNAL_ERROR);
            }

            button.setEnabled(true);
        }
    }

    /***
     * Повторная отправка кода подтверждения (регулировка назначения - параметрами)
     */
    class SendCodeTask extends AsyncTask<String, Void, JSONObject> {
        private String first;
        private String last;
        private String email;
        private String phone;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            button.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            // first, last, email, phone
            first = params[0];
            last = params[1];
            email = params[2];
            phone = params[3];

            ServerTalk server = new ServerTalk();
            return server.registerRequest(first, last, email, phone);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            progress.setVisibility(View.INVISIBLE);
            button.setVisibility(View.VISIBLE);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    String token = (String) result.get("token");
                    Long ttl = -1L;
                    if (result.has("codeTTL")) {
                        ttl = (Long) result.get("codeTTL");
                    }

                    parameters.put("token", token);
                    StopTimer();
                    timer = new Timer();
                    timerTask = new CountdownTask(ttl);
                    timer.schedule(timerTask, 0, 1000);
                    button.setText(getResources().getString(R.string.continue_enter));
                } else {
                    ResultDialog.showError(getActivity(), "Ошибка отправки кода", errorCode, result.getErrorText());
                }
            } catch (Exception e) {
                Logger.Exception(e);
                ResultDialog.showError(getActivity(), "Ошибка отправки кода", ErrorResult.INTERNAL_ERROR);
            }

            button.setEnabled(codeEditText.getText().length() >= 6);
        }
    }

    /***
     * Фоновая задача. Отсчет времени валидности кода подтверждения.
     * Если время истекло, кнопка "Продолжить" меняется на "Получить код повторно"
     */
    class CountdownTask extends TimerTask {
        int timerSeconds = Settings.getInt(Settings.CODE_LIVE_TIME);

        public CountdownTask(Long timerSeconds) {
            if ((timerSeconds != null) && (timerSeconds > 0))
                this.timerSeconds = (int) (timerSeconds / 1000);
        }

        @Override
        public void run() {
            timerSeconds--;
            if (fragmentActive)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (timerSeconds <= 0) {
                            StopTimer();
                            timerText.setVisibility(View.INVISIBLE);
                            button.setText(getResources().getString(R.string.send_code_again));
                            button.setEnabled(true);
                            buttonSendAction = false;
                        } else {
                            String text = stringTimerText.replace("null", String.valueOf(timerSeconds));
                            timerText.setText(text);
                            timerText.setVisibility(View.VISIBLE);
                            button.setText(getResources().getString(R.string.continue_enter));
                            buttonSendAction = true;
                        }
                    }
                });
        }

    }

    /***
     * Останавливает таймер, в случае проблем происто игнорируем их
     */
    public void StopTimer() {
        try {
            if (timer != null) timer.cancel();
        } catch (Exception e) {
            // pass
        }
        timer = null;
    }
}