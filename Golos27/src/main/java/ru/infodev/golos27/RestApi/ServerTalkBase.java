package ru.infodev.golos27.RestApi;

import android.util.Pair;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Profile;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONException;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;

import static ru.infodev.golos27.Common.Utils.Json.JSONObject.JSON_LONG_STRING;
import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

public class ServerTalkBase {
    protected Transport transport = null;
    protected static String rootPath = null;

    /***
     * Конструктор - инициализация транспорта
     */
    public ServerTalkBase() {
        transport = new Transport(
                Settings.getStr(Settings.SERVER_SCHEMA),
                Settings.getStr(Settings.SERVER_ADDRESS),
                Settings.getStr(Settings.SERVER_ROOT),
                Settings.getInt(Settings.SERVER_PORT));

        rootPath = transport.getBasePath();
    }

    /***
     * Возвращает базовый путь до сервера
     * @return строка с uri
     */
    public static String getBaseUri() {
        return rootPath;
    }

    /***
     * Возвращает токен для авторизации. Если токен истек, обновляет его
     *
     * @return строку с access маркером
     */
    public String getAccessToken() {
        long access_token_ttl = Settings.getInt(Settings.USER_TOKEN_EXPIRES_IN) * 1000;
        long access_token_start = Settings.getLong(Settings.USER_TOKEN_START_TIME);
        long current_time = System.currentTimeMillis();
        if (current_time - access_token_start < access_token_ttl) {
            return Settings.getStr(Settings.USER_ACCESS_TOKEN);
        } else {
            // Запрашиваем новый access_token по refresh_token
            return getUpdatedAuthToken(Settings.getStr(Settings.USER_REFRESH_TOKEN));
        }
    }

    /***
     * Получение авторизационного токена с сервера
     *
     * @param username имя пользователя
     * @param password пароль
     * @return Map с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject getAuthToken(String username, String password) {
        JSONObject response = new JSONObject();
        HashMap<String, Object> query = transport.getQuery();
        query.put("username", username);
        query.put("password", password);
        query.put("grant_type", "password");
        query.put("scope", "trust");
        Pair<ErrorResult, String> result = transport.post("oauth/token", query, null, null);

        try {
            if (result.first != null) {
                response.put("errorCode", result.first.getErrorCode());
                response.put("errorText", result.first.getErrorText());
            } else {
                response.put("errorCode", 0);
                response.put("errorText", "Операция выполнена успешно");
            }

            try {
                if (result.second != null) {
                    JSONObject jsonResponse = new JSONObject(result.second);
                    response = handleAuthResponse(jsonResponse, response);
                }
            } catch (Exception e) {
                Logger.Exception(e);
                response.put("errorText", "Некорректный ответ от сервера");
            }
        } catch (JSONException e) {
            Logger.Exception(e);
            try {
                response.put("errorText", "Некорректный ответ от сервера");
            } catch (JSONException e1) {
                Logger.Exception(e1);
            }
        }

        return response;
    }

    /***
     * Перевыпуск авторизационного токена
     *
     * @param refresh_token токен для перевыпуска
     * @return строку с access токеном
     */
    public String getUpdatedAuthToken(String refresh_token) {
        if (isEmpty(refresh_token)) return getNewAuthToken();

        boolean bad_token = false;
        HashMap<String, Object> query = transport.getQuery();
        query.put("refresh_token", refresh_token);
        query.put("grant_type", "refresh_token");
        Pair<ErrorResult, String> result = transport.post("oauth/token", query, null, null);

        try {
            if (result.second != null) {
                JSONObject jsonResponse = JSONObject.fromString(result.second, 1024);
                if (jsonResponse.has("access_token") && jsonResponse.has("expires_in")) {
                    Integer expires_in = (Integer) jsonResponse.get("expires_in");
                    String access_token = (String) jsonResponse.get("access_token");
                    Settings.setParameter(Settings.USER_ACCESS_TOKEN, access_token);
                    Settings.setParameter(Settings.USER_TOKEN_EXPIRES_IN, String.valueOf(expires_in));
                    Settings.setParameter(Settings.USER_TOKEN_START_TIME, String.valueOf(System.currentTimeMillis()));
                    return access_token;
                } else {
                    if (jsonResponse.has("error")) {
                        if (jsonResponse.has("error_description"))
                            Logger.Log(jsonResponse.getString("error_description", "Unknown OAUTH error"));

                        String error = jsonResponse.getString("error", "");
                        if (error.equals("invalid_token") || error.equals("invalid_grant"))
                            bad_token = true;
                    }
                }
            }
        } catch (Exception e) {
            Logger.Exception(e);
        }

        if (bad_token) return getNewAuthToken();
        Logger.Warn("Can't update access_token! " + result.first.getErrorText());
        return "";
    }

    private static String getNewAuthToken() {
        String username = Settings.Read(Settings.HASH1);
        String password = Settings.Read(Settings.HASH2);
        if (isEmpty(username) || isEmpty(password)) return Transport.INVALID_CREDENTIALS_TOKEN;
        Logger.Warn("Update refresh token called!");

        ServerTalkBase server = new ServerTalkBase();
        JSONObject result = server.getAuthToken(username, password);

        int errorCode = result.getInt("errorCode", ErrorResult.UNKNOWN_ERROR);
        if (errorCode == Transport.SUCCESS) {
            if (result.has("access_token") && result.has("refresh_token")) {
                String access_token = result.getString("access_token", "");
                String refresh_token = result.getString("refresh_token", "");
                Integer expires_in = result.getInt("expires_in", 300);

                long startTime = System.currentTimeMillis();
                Settings.setParameter(Settings.USER_ACCESS_TOKEN, access_token);
                Settings.setParameter(Settings.USER_REFRESH_TOKEN, refresh_token);
                Settings.setParameter(Settings.USER_TOKEN_EXPIRES_IN, String.valueOf(expires_in));
                Settings.setParameter(Settings.USER_TOKEN_START_TIME, String.valueOf(startTime));

                return access_token;
            }
        }

        if (errorCode == ErrorResult.INVALID_GRANT || errorCode == ErrorResult.UNSUPPORTED_GRANT_TYPE) {
            Logger.Log("Can't update token - invalid credentials. Clean profile called");
            Profile.RemoveAllData();
            return Transport.INVALID_CREDENTIALS_TOKEN;
        } else {
            Logger.Log("Background token update error: " + result.getString("errorText", "Unknown error"));
        }

        return "";
    }

    /***
     * Получение данных с сервера (методом GET, вариант без авторизации)
     *
     * @param path  относительный путь на сервере
     * @param query запрос (map c полями)
     * @return JSONObject с ответом сервера
     */
    @SuppressWarnings("unused")
    JSONObject getServerForm(String path, HashMap<String, Object> query) {
        return sendServerForm(path, query, null, null, Transport.REQUEST_GET);
    }

    /***
     * Получение данных с сервера (методом GET)
     *
     * @param path  относительный путь на сервере
     * @param query запрос (map c полями)
     * @param cred  акторизационный token
     * @return JSONObject с ответом сервера
     */
    @SuppressWarnings("unused")
    JSONObject getServerForm(String path, HashMap<String, Object> query, String cred) {
        return sendServerForm(path, query, null, cred, Transport.REQUEST_GET);
    }

    /***
     * Отправка формы на сервер и обработка ответа сервера (вариант без авторизации)
     *
     * @param path относительный путь на сервере
     * @param form Map с полями формы
     * @return JSONObject с ответами сервера (обязательные: ErrorCode, ErrorText)
     */
    JSONObject postServerForm(String path, HashMap<String, Object> form) {
        HashMap<String, Object> query = transport.getQuery();
        return sendServerForm(path, query, form, null, Transport.REQUEST_POST);
    }

    /***
     * Отправка формы на сервер и обработка ответа сервера (вариант с авторизацией)
     *
     * @param path относительный путь на сервере
     * @param form Map с полями формы
     * @param cred строка с access token для авторизации
     * @return JSONObject с ответами сервера (обязательные: ErrorCode, ErrorText)
     */
    @SuppressWarnings("unused")
    JSONObject postServerForm(String path, HashMap<String, Object> form, String cred) {
        HashMap<String, Object> query = transport.getQuery();
        return sendServerForm(path, query, form, cred, Transport.REQUEST_POST);
    }

    /***
     * Отправка формы на сервер (put) и обработка ответа сервера (вариант без авторизации)
     *
     * @param path относительный путь на сервере
     * @param form Map с полями формы
     * @return JSONObject с ответами сервера (обязательные: ErrorCode, ErrorText)
     */
    @SuppressWarnings("unused")
    JSONObject putServerForm(String path, HashMap<String, Object> form) {
        HashMap<String, Object> query = transport.getQuery();
        return sendServerForm(path, query, form, null, Transport.REQUEST_PUT);
    }

    /***
     * Отправка формы на сервер и обработка ответа сервера (вариант с авторизацией)
     *
     * @param path относительный путь на сервере
     * @param form Map с полями формы
     * @param cred строка с access token для авторизации
     * @return JSONObject с ответами сервера (обязательные: ErrorCode, ErrorText)
     */
    @SuppressWarnings("unused")
    JSONObject delServerForm(String path, HashMap<String, Object> form, String cred) {
        HashMap<String, Object> query = transport.getQuery();
        return sendServerForm(path, query, form, cred, Transport.REQUEST_DEL);
    }

    /***
     * Отправка формы на сервер (PUT) и обработка ответа сервера (вариант с авторизацией)
     *
     * @param path относительный путь на сервере
     * @param form Map с полями формы
     * @param cred строка с access token для авторизации
     * @return JSONObject с ответами сервера (обязательные: ErrorCode, ErrorText)
     */
    JSONObject putServerForm(String path, HashMap<String, Object> form, String cred) {
        HashMap<String, Object> query = transport.getQuery();
        return sendServerForm(path, query, form, cred, Transport.REQUEST_PUT);
    }

    /***
     * Отправка формы на сервер (с выбором метода) и обработка ответа сервера (вариант с авторизацией)
     *
     * @param path относительный путь на сервере
     * @param form Map с полями формы
     * @param cred строка с access token для авторизации
     * @return JSONObject с ответами сервера (обязательные: ErrorCode, ErrorText)
     */
    JSONObject sendServerForm(String path, HashMap<String, Object> query, HashMap<String, Object> form, String cred, int request_type) {
        JSONObject jsonForm = new JSONObject();
        Pair<ErrorResult, String> result;
        JSONObject response = new JSONObject();
        try {
            if (form != null) {
                jsonForm = new JSONObject().put("form", new JSONObject(form));
            }

            switch (request_type) {
                case Transport.REQUEST_GET:
                    result = transport.get(path, query, cred);
                    break;
                case Transport.REQUEST_PUT:
                    result = transport.put(path, query, jsonForm, cred);
                    break;
                case Transport.REQUEST_POST:
                    result = transport.post(path, query, jsonForm, cred);
                    break;
                case Transport.REQUEST_DEL:
                    result = transport.del(path, query, jsonForm, cred);
                    break;
                default:
                    result = transport.post(path, query, jsonForm, cred);
                    break;
            }

            if (result.first != null) {
                response.put("errorCode", result.first.getErrorCode());
                response.put("errorText", result.first.getErrorText());
            } else {
                response.put("errorCode", 0);
                response.put("errorText", "Операция выполнена успешно");
            }

            try {
                if (isNotEmpty(result.second)) {
                    JSONObject jsonResponse = JSONObject.fromString(result.second, 32768);
                    if (jsonResponse.has("meta")) {
                        response = handleServerResponse(jsonResponse);
                    } else if (jsonResponse.has(JSON_LONG_STRING)) {
                        // Очень большой ответ от сервера - требуется потоковый json парсер
                        // Ответ лежит в поле LONG_STRING и будет обработан позднее
                        response = jsonResponse;
                    } else {
                        response = handleAuthResponse(jsonResponse, response);
                    }
                    return response;
                }
            } catch (Exception e) {
                Logger.Exception(e);
                response.put("errorCode", ErrorResult.NETWORK_ERROR);
                response.put("errorText", ErrorResult.DEFAULT_NETWORK_ERROR);
            }
        } catch (JSONException e) {
            Logger.Exception(e);
            try {
                response.put("errorText", "Внутренняя ошибка приложения");
            } catch (JSONException e1) { /* pass */ }
        }

        return response;
    }

    /***
     * Отправка формы на сервер (с выбором метода) и обработка ответа сервера (вариант с авторизацией)
     *
     * @param path относительный путь на сервере
     * @param file файл для отправки на сервер
     * @param cred строка с access token для авторизации
     * @return JSONObject с ответами сервера (обязательные: ErrorCode, ErrorText)
     */
    JSONObject sendFile(String path, File file, String cred) {
        Pair<ErrorResult, String> result;
        JSONObject response = new JSONObject();
        try {
            result = transport.postFile(path, file, cred);
            if (result.first != null) {
                response.put("errorCode", result.first.getErrorCode());
                response.put("errorText", result.first.getErrorText());
            } else {
                response.put("errorCode", 0);
                response.put("errorText", "Операция выполнена успешно");
            }

            try {
                if (result.second != null) {
                    JSONObject jsonResponse = JSONObject.fromString(result.second, Integer.MAX_VALUE);
                    if (jsonResponse.has("meta")) {
                        response = handleServerResponse(jsonResponse);
                    } else {
                        response = handleAuthResponse(jsonResponse, response);
                    }
                    return response;
                }
            } catch (Exception e) {
                Logger.Exception(e);
                response.put("errorCode", ErrorResult.NETWORK_ERROR);
                response.put("errorText", ErrorResult.DEFAULT_NETWORK_ERROR);
            }
        } catch (JSONException e) {
            Logger.Exception(e);
            try {
                response.put("errorText", "Внутренняя ошибка приложения");
            } catch (JSONException e1) { /* pass */ }
        }
        return response;
    }

    /***
     * Обработка ответа сервера / обработка ошибок
     *
     * @param serverResponse ответ сервера в форме json
     * @return JSONObject с ответами сервера (обязательные: ErrorCode, ErrorText)
     */
    JSONObject handleServerResponse(JSONObject serverResponse) {
        // Сервер должен ответить следующим образом:
        // {"meta":{"status":"xxx"},"notifications":{"key1": "value1", "key2": "value2"},
        //  "response":{"key1": "value1", "key2": "value2"}}
        JSONObject reply = new JSONObject();
        try {
            if (serverResponse.has("meta")) {
                JSONObject meta = (JSONObject) serverResponse.get("meta");
                String status = meta.getString("status");
                if (status.equals("SUCCESS")) {
                    reply.put("errorCode", ErrorResult.SUCCESS);
                    reply.put("errorText", ErrorResult.getErrorText(ErrorResult.SUCCESS));
                    if (serverResponse.has("response")) {
                        Object responseObject = serverResponse.get("response");
                        if (responseObject instanceof JSONObject) {
                            JSONObject response = (JSONObject) responseObject;
                            Iterator<?> keys = response.keys();
                            while (keys.hasNext()) {
                                String key = (String) keys.next();
                                reply.put(key, response.get(key));
                            }
                        }

                        if (responseObject instanceof JSONArray) {
                            JSONArray response = (JSONArray) responseObject;
                            reply.put("replyArray", response);
                        }
                    }
                    // TODO: добавить парсинг оповещений сервера!
                } else {
                    Pair<Integer, String> serverError = ErrorResult.Get(status);
                    if (serverError.first == ErrorResult.INVALID_TOKEN) {
                        Logger.Log("Invalid token detected (asr). Timeout it early!");
                        Settings.setParameter(Settings.USER_TOKEN_START_TIME, "0");
                    }

                    reply.put("errorCode", serverError.first);
                    reply.put("errorText", serverError.second);
                }
            } else {
                Logger.Warn("No meta in server reply (register request)");
                reply.put("errorText", "Некорректный ответ сервера");
            }
        } catch (JSONException e) {
            Logger.Exception(e);
            try {
                reply.put("errorText", "Некорректный ответ сервера");
            } catch (JSONException e1) { /* pass */ }
        }
        return reply;
    }

    /***
     * Обработка ответа сервера при прохождении авторизации
     *
     * @param serverResponse ответ сервера в формате json
     * @param response       результат выполнения операции уровнем ниже (транспортный)
     * @return хэш с ответами сервера (обязательные: ErrorCode, ErrorText)
     */
    JSONObject handleAuthResponse(JSONObject serverResponse, JSONObject response) {
        JSONObject reply = new JSONObject();
        // Сервер должен ответить следующим образом:
        // {"key1": "value1", "key2": "value2"} в числе которых
        // могут быть error и error_description
        try {
            if (response.has("errorCode")) {
                reply.put("errorCode", response.get("errorCode"));
            } else {
                reply.put("errorCode", 0);
            }
            if (response.has("errorText")) {
                reply.put("errorText", response.get("errorText"));
            } else {
                reply.put("errorText", "");
            }
            try {
                if (serverResponse.has("error")) {
                    String error = serverResponse.getString("error");
                    Pair<Integer, String> serverError = ErrorResult.Get(error);
                    String errorText = serverError.second;
                    if (serverResponse.has("error_description") && !serverResponse.isNull("error_description"))
                        errorText = serverResponse.getString("error_description", serverError.second);

                    if (serverError.first == ErrorResult.INVALID_TOKEN) {
                        Logger.Log("Invalid token detected (hsr). Timeout it early!");
                        Settings.setParameter(Settings.USER_TOKEN_START_TIME, "0");
                    } else {
                        if (isNotEmpty(errorText))
                            Logger.Log("Auth error during request: " + errorText);
                    }

                    reply.put("errorCode", serverError.first);
                    reply.put("errorText", serverError.second);
                } else {
                    reply.put("errorCode", ErrorResult.SUCCESS);
                    reply.put("errorText", ErrorResult.getErrorText(ErrorResult.SUCCESS));
                }

                Iterator<?> keys = serverResponse.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    reply.put(key, serverResponse.get(key));
                }
            } catch (JSONException e) {
                Logger.Exception(e);
                reply.put("errorText", "Некорректный ответ сервера");
            }
        } catch (Exception e) {
            Logger.Exception(e);
        }
        return reply;
    }
}
