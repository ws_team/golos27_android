package ru.infodev.golos27.Fragments.AppealClaim;

import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Executor;
import ru.infodev.golos27.Common.Catalogs.NewAppeal;
import ru.infodev.golos27.Common.Catalogs.OKTMO;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.Adapters.ExecutorAdapter;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.Lists.NestedListView;
import ru.infodev.golos27.Widgets.MonitoredEditText;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.trimString;


public class SetExecutorFragment extends ParamFragment implements View.OnClickListener, TextWatcherAdapter.TextWatcherListener {
    public static final int FRAGMENT_ID = R.layout.set_executor;
    private ExecutorAdapter adapter;
    private NestedListView listView;
    private TextView notFoundTextView;
    private TextView totalFound;

    public SetExecutorFragment() {
        titleId = R.string.executor_area;
        setFragmentAllowMenu(false);
        setActionButtonIcon(ICON_CHECK);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        setTitle("Исполнитель");

        List<Executor> executors = Executor.All();
        TextView cityName = (TextView) v.findViewById(R.id.city_name);
        OKTMO settlement = NewAppeal.getOrSetExecutorFilterCity(null);
        if (settlement != null && isNotEmpty(settlement.getFullCity())) {
            cityName.setText(trimString(settlement.getFullCity(), 20));
            executors = Executor.Search(null, settlement.getCode(), Integer.MAX_VALUE);
        } else {
            cityName.setText(Application.AppContext.getString(R.string.not_set));
        }

        cityName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chainStartNextFragment(FRAGMENT_ID, SetExecutorCityFragment.class, null, false);
            }
        });

        totalFound = (TextView) v.findViewById(R.id.total_found);
        notFoundTextView = (TextView) v.findViewById(R.id.not_found_text);
        MonitoredEditText searchEditText = (MonitoredEditText) v.findViewById(R.id.search_edit_text);
        searchEditText.setOnTextChangedListener(this);

        listView = (NestedListView) v.findViewById(R.id.main_list);
        Executor checkedExecutor = NewAppeal.getInstance().Executor();
        if (checkedExecutor != null && executors != null) {
            for (Executor current : executors) {
                current.setIsChecked(current.Id().equals(checkedExecutor.Id()));
            }
        }

        if ((executors == null) || (executors.isEmpty())) {
            listView.setVisibility(View.GONE);
            notFoundTextView.setVisibility(View.VISIBLE);
        } else {
            listView.setVisibility(View.VISIBLE);
            notFoundTextView.setVisibility(View.GONE);
        }

        adapter = new ExecutorAdapter(Application.AppContext, executors);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.setChecked(position, true);
                setActionButtonVisibility(true);
            }
        });

        totalFound.setText(Application.AppContext.getString(R.string.total_found)
                .replace("null", String.valueOf(adapter.getCount())));

        return v;
    }

    /***
     * Выполняется при нажатии на кнопку ActionBar
     * @param item кнопка на которую нажали
     * @return обработано нажатие или нет
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_item) {
            NewAppeal.getInstance().setExecutor(adapter.getCheckedItem());
            chainPopFragments(FRAGMENT_ID, 1, false);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        ArrayList<Executor> executors;
        String code = null;
        OKTMO settlement = NewAppeal.getOrSetExecutorFilterCity(null);
        if (settlement != null) code = settlement.getCode();

        if (text.length() > 2) {
            executors = (ArrayList<Executor>) Executor.Search(text, code, Integer.MAX_VALUE);
        } else {
            executors = (ArrayList<Executor>) Executor.Search(null, code, Integer.MAX_VALUE);
        }

        if ((executors == null) || (executors.isEmpty())) {
            listView.setVisibility(View.GONE);
            notFoundTextView.setVisibility(View.VISIBLE);
        } else {
            listView.setVisibility(View.VISIBLE);
            notFoundTextView.setVisibility(View.GONE);
        }

        adapter.setData(executors);
        Executor checkedExecutor = NewAppeal.getInstance().Executor();
        if (checkedExecutor != null && executors != null) {
            for (Executor current : executors) {
                current.setIsChecked(current.Id().equals(checkedExecutor.Id()));
            }
        }

        totalFound.setText(Application.AppContext.getString(R.string.total_found)
                .replace("null", String.valueOf(adapter.getCount())));

        adapter.notifyDataSetChanged();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}
