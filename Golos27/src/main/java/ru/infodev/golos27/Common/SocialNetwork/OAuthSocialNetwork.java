package ru.infodev.golos27.Common.SocialNetwork;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.HashMap;
import java.util.Map;

import ru.infodev.golos27.Common.SocialNetwork.listener.base.SocialNetworkListener;

/**
 * Class for oauth connection to social networks
 *
 * @author Anton Krasov
 * @author Evgeny Gorbin (gorbin.e.o@gmail.com)
 */
public abstract class OAuthSocialNetwork extends SocialNetwork {

    protected Map<String, SocialNetworkAsyncTask> mRequests = new HashMap<>();

    protected OAuthSocialNetwork(Fragment fragment) {
        super(fragment);
    }

    protected void executeRequest(SocialNetworkAsyncTask request, Bundle params, String requestID) {
        checkRequestState(mRequests.get(requestID));
        mRequests.put(requestID, request);
        request.execute(params == null ? new Bundle() : params);
    }

    private void cancelRequest(String requestID) {
        SocialNetworkAsyncTask request = mRequests.get(requestID);

        if (request != null) {
            request.cancel(true);
        }

        mRequests.remove(requestID);
    }
    /**
     * Cancel login request
     */
    @Override
    public void cancelLoginRequest() {
        super.cancelLoginRequest();

        cancelRequest(REQUEST_LOGIN);
        cancelRequest(REQUEST_LOGIN2);
    }

    /**
     * Cancel request
     */
    @Override
    public void cancelAccessTokenRequest() {
        super.cancelAccessTokenRequest();

        cancelRequest(REQUEST_ACCESS_TOKEN);
    }

    /**
     * Cancel post message request
     */
    @Override
    public void cancelPostMessageRequest() {
        super.cancelPostMessageRequest();

        cancelRequest(REQUEST_POST_MESSAGE);
    }

    /**
     * Cancel post photo request
     */
    @Override
    public void cancelPostPhotoRequest() {
        super.cancelPostPhotoRequest();

        cancelRequest(REQUEST_POST_PHOTO);
    }

    /**
     * Cancel post link request
     */
    @Override
    public void cancelPostLinkRequest() {
        super.cancelPostLinkRequest();

        cancelRequest(REQUEST_POST_LINK);
    }

    /**
     * Cancel share dialog request
     */
    @Override
    public void cancelPostDialogRequest() {
        super.cancelPostDialogRequest();

        cancelRequest(REQUEST_POST_DIALOG);
    }

    protected boolean handleRequestResult(Bundle result, String requestID) {
        return handleRequestResult(result, requestID, null);
    }

    protected boolean handleRequestResult(Bundle result, String requestID, Object data) {
        mRequests.remove(requestID);

        SocialNetworkListener socialNetworkListener = mLocalListeners.get(requestID);

        // 1: user didn't set listener, or pass null, this doesn't have any sence
        // 2: request was canceled...
        if (socialNetworkListener == null) return false;
        String error = result.getString(SocialNetworkAsyncTask.RESULT_ERROR);

        if (error != null) {
            socialNetworkListener.onError(getID(), requestID, error, data);
            mLocalListeners.remove(requestID);
            return false;
        }

        return true;
    }
}
