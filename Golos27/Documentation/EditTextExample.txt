<RelativeLayout android:id="@+id/edit_block_!!!!!!!"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginTop="7dp"
    android:orientation="vertical">

    <LinearLayout android:id="@+id/inside_block_!!!!!!!"
        android:layout_width="match_parent"
        android:orientation="vertical"
        android:layout_height="wrap_content">
        <TextView android:id="@+id/name_text_!!!!!!!"
            android:visibility="invisible"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textColor="@color/colorPrimary"
            android:textSize="14sp"
            android:layout_marginBottom="2dp"
            android:text="Текст метки"/>
        <FrameLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content">
            <ru.infodev.golos27.widget.CustomEditText
                android:id="@+id/edit_!!!!!!!"
                android:clickable="true"
                android:cursorVisible="true"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_gravity="center"
                android:hint="Текст метки"
                app:empty_warning_id="@+id/warn_text_!!!!!!!"
                app:upper_name_id="@+id/name_text_!!!!!!!"
                app:bottom_help_id="@+id/help_text_!!!!!!!" />
            <TextView android:id="@+id/warn_text_!!!!!!!"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_vertical|end|right"
                android:gravity="end"
                android:text="@string/mandatory"
                android:textColor="@color/colorErrorText" />
        </FrameLayout>
        <TextView android:id="@+id/help_text_!!!!!!!"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textColor="@color/colorGrayText"
            android:textSize="16sp"
            android:layout_marginTop="2dp"
            android:text="Длиная подсказка под поле ввода"/>
    </LinearLayout>
</RelativeLayout>

