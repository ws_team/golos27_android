package ru.infodev.golos27.Common;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ThumbnailUtils;
import android.net.Uri;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Appeal.AppealAttachment;
import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Common.Utils.UriToPath;
import ru.infodev.golos27.Common.Utils.Utils;

import static ru.infodev.golos27.Common.Utils.Utils.decodeFromFile;
import static ru.infodev.golos27.Common.Utils.Utils.getOrientation;
import static ru.infodev.golos27.Common.Utils.Utils.recycleBitmap;

@SuppressWarnings("unused")
public class GalleryEntry {
    private Uri originalUri = null;
    private String imagePath = "";
    private String thumbnailPath = "";
    private String attachmentId = "";
    private boolean checked = false;
    //-----------------------------------------------------------

    private AppealAttachment attachment;

    public GalleryEntry() {
    }

    public GalleryEntry(Uri originalUri) {
        this.originalUri = originalUri;
    }

    public GalleryEntry(String imagePath, String thumbnailPath, boolean checked) {
        this.imagePath = imagePath;
        this.thumbnailPath = thumbnailPath;
        this.checked = checked;
    }

    public Uri OriginalUri() {
        return originalUri;
    }

    public void setOriginalUri(Uri originalUri) {
        this.originalUri = originalUri;
    }

    public String ImagePath() {
        return imagePath;
    }

    public GalleryEntry setImagePath(String imagePath) {
        this.imagePath = imagePath;
        return this;
    }

    public String ThumbnailPath() {
        return thumbnailPath;
    }

    public GalleryEntry setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
        return this;
    }

    public boolean isChecked() {
        return checked;
    }

    public GalleryEntry setChecked(boolean checked) {
        this.checked = checked;
        return this;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public GalleryEntry setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
        return this;
    }

    public JSONObject toJson() {
        JSONObject result = new JSONObject();
        result.putSafe("originalUri", originalUri.toString());
        result.putSafe("imagePath", imagePath);
        result.putSafe("thumbnailPath", thumbnailPath);
        result.putSafe("checked", checked);
        result.putSafe("attachmentId", attachmentId);
        return result;
    }

    public static List<GalleryEntry> fromJson(JSONArray source) {
        ArrayList<GalleryEntry> images = new ArrayList<>();
        if (source == null) return images;

        for (int i = 0; i < source.length(); i++) {
            GalleryEntry image = new GalleryEntry();
            try {
                JSONObject row = source.getJSONObject(i);
                image.setOriginalUri(Uri.parse(row.getString("originalUri", "")));
                image.setImagePath(row.getString("imagePath", ""));
                image.setThumbnailPath(row.getString("thumbnailPath", ""));
                image.setChecked(row.getBoolean("checked", false));
                image.setAttachmentId(row.getString("attachmentId", null));
                images.add(image);
            } catch (Exception e) {
                Logger.Exception(e);
            }
        }

        return images;
    }

    public AppealAttachment getAttachment() {
        return attachment;
    }

    public void setAttachment(AppealAttachment attachment) {
        this.attachment = attachment;
        if (attachment != null) this.attachmentId = attachment.Id();
    }

    /***
     * Создает новую запись в галерее с проверкой на существование записи
     * изображение копируется к каталог с кэшем и при необходимости уменьшается
     * Кроме того создается файл с preview изображением маленького размера
     *
     * @param imageUri       путь (Uri) до изображения
     * @param thumbnailWidth ширина/высота "плитки" галереи
     * @return новую запись галереи
     */
    @SuppressWarnings("SuspiciousNameCombination") // У нас квадратная плитка!
    public static GalleryEntry fromImage(Uri imageUri, int thumbnailWidth, int maxImageLength) {
        Bitmap photo;
        GalleryEntry newEntry = new GalleryEntry(imageUri);
        int orientation = getOrientation(Application.AppContext, imageUri);

        String path = UriToPath.getPath(Application.AppContext, imageUri);

        if (path == null) return null;

        String imageFullPath = Utils.getImageCachePath(path);
        String imageThumbPath = Utils.getImageThumbPath(path);
        if ((imageFullPath == null) || (imageThumbPath == null)) return null;
        File imageFile = new File(imageFullPath);
        File thumbFile = new File(imageThumbPath);

        File imageOrigFile = new File(path);
        long imageSize = imageOrigFile.length();
        if (imageSize > 0) {
            if (imageSize > maxImageLength) {
                photo = decodeFromFile(path, maxImageLength);
                if (photo != null) {
                    try {
                        photo.compress(Bitmap.CompressFormat.JPEG, 80, new FileOutputStream(imageFile));
                    } catch (FileNotFoundException e) {
                        Logger.Exception(e);
                        imageFullPath = null;
                    }
                } else imageFullPath = null;
            } else {
                photo = decodeFromFile(path, thumbnailWidth * 2, thumbnailWidth * 2);
                Utils.copyFile(path, imageFullPath);
            }

            // На данном этапе у нас уже должен быть битмап с бОльшими размерами чем thumbnail
            if (photo != null) {
                Bitmap thumbnail = ThumbnailUtils.extractThumbnail(photo, thumbnailWidth, thumbnailWidth);
                Logger.Log("Image orientation is " + String.valueOf(orientation));
                if (orientation > 0) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(orientation);
                    Bitmap tempBitmap = thumbnail;
                    thumbnail = Bitmap.createBitmap(tempBitmap, 0, 0, thumbnail.getWidth(),
                            thumbnail.getHeight(), matrix, true);
                    if (!tempBitmap.equals(thumbnail)) {
                        recycleBitmap(tempBitmap);
                    }
                }

                photo.recycle();
                if (thumbnail != null) {
                    try {
                        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(thumbFile));
                        thumbnail.recycle();
                    } catch (FileNotFoundException e) {
                        Logger.Exception(e);
                        imageThumbPath = null;
                    }
                } else imageThumbPath = null;
            }

            // Если все прошло хорошо - вернем результат
            if ((imageFullPath != null) || (imageThumbPath != null)) {
                newEntry.setImagePath(imageFullPath);
                newEntry.setThumbnailPath(imageThumbPath);
                return newEntry;
            }
        }
        return null;
    }
}
