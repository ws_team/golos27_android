package ru.infodev.golos27.Fragments.Profile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;

public class ChangeLoginFragment extends ParamFragment {
    public static final int FRAGMENT_ID = R.layout.profile_change_login_info;
    private TextView PhoneText;
    private TextView EmailText;
    private String emptyText;
    private String mPhone;
    private String mEmail;
    private LinearLayout loginConfirmBaloon;

    public ChangeLoginFragment() {
        titleId = R.string.account_data;
        setFragmentAllowMenu(false);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        setTitle(titleId);

        emptyText = getResources().getString(R.string.not_specified);
        PhoneText = (TextView) v.findViewById(R.id.textViewPhone);
        mPhone = parameters.getString("activePhone", "");
        if (isEmpty(mPhone)) {
            PhoneText.setText(emptyText);
            PhoneText.setTextColor(ContextCompat.getColor(Application.AppContext, R.color.colorGrayText));
        } else {
            PhoneText.setText(mPhone);
            PhoneText.setTextColor(ContextCompat.getColor(Application.AppContext, R.color.colorBlackBack));
        }

        EmailText = (TextView) v.findViewById(R.id.Text_View_Email);
        mEmail = parameters.getString("activeEmail", "");
        if (isEmpty(mEmail)) {
            EmailText.setText(emptyText);
            EmailText.setTextColor(ContextCompat.getColor(Application.AppContext, R.color.colorGrayText));
        } else {
            EmailText.setText(mEmail);
            EmailText.setTextColor(ContextCompat.getColor(Application.AppContext, R.color.colorBlackBack));
        }
        if (EmailText.length() >22){
            EmailText.setTextSize(17);
        }

        ImageView editPhoneButton = (ImageView) v.findViewById(R.id.button_edit_phone);
        editPhoneButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                JSONObject result = new JSONObject();
                if (!isEmpty(mPhone)) result.putSafe("login_phone", mPhone);
                chainStartNextFragment(FRAGMENT_ID, ChangeMobileFragment.class, result, 0, false);
            }
        });

        ImageView editEmailButton = (ImageView) v.findViewById(R.id.edit_email_ad);
        editEmailButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                JSONObject result = new JSONObject();
                if (!isEmpty(mEmail)) result.putSafe("login_email", mEmail);
                chainStartNextFragment(FRAGMENT_ID, ChangeEmailFragment.class, result, 0, false);
            }
        });

        loginConfirmBaloon = (LinearLayout) v.findViewById(R.id.loginConfirmBaloon);
        TextView baloonHideButton = (TextView) loginConfirmBaloon.findViewById(R.id.buttonHide);
        baloonHideButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loginConfirmBaloon.setVisibility(View.GONE);
                Settings.setParameter(Settings.PROFILE_HIDE_POPUP, "true");
            }
        });

        loginConfirmBaloon.setVisibility(View.GONE);

        // Фоновые задачи. Если успели - хорошо. Не успели и ладно.
        new ReceiveAccounts().execute();

        return v;
    }

    /***
     * Получает список логинов и показывает всплывающее
     * уведомление если один из них не подтвержден
     */
    class ReceiveAccounts extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected final JSONObject doInBackground(Void... params) {
            ServerTalk server = new ServerTalk();
            return server.getAccounts();
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            try {
                int errorCode = (Integer) result.get("errorCode");
                if ((errorCode == Transport.SUCCESS) && fragmentActive) {
                    // Если есть неподтвержденный логин - надо показать его
                    if ((result.has("replyArray")) && (result.get("replyArray") instanceof JSONArray)) {
                        JSONArray loginList = result.getJSONArray("replyArray");
                        for (int i = 0; i < loginList.length(); i++) {
                            if (loginList.get(i) instanceof JSONObject) {
                                JSONObject loginInfo = loginList.getJSONObject(i);
                                final String login_to_activate = loginInfo.getString("login", "");
                                String login = login_to_activate;
                                final boolean isPhone = loginInfo.getBoolean("isPhone");
                                if (isEmpty(login_to_activate)) login = emptyText;
                                if (isPhone) PhoneText.setText(login);

                                else EmailText.setText(login);

                                if (loginInfo.has("isActive") && loginInfo.get("isActive") instanceof Boolean) {
                                    if (!loginInfo.getBoolean("isActive")) {
                                        if (!isEmpty(login_to_activate) && !Settings.getBool(Settings.PROFILE_HIDE_POPUP)) {
                                            TextView loginText = (TextView) loginConfirmBaloon.findViewById(R.id.loginConfirmText);
                                            String text = getResources().getString(R.string.confirm_waiting);
                                            loginText.setText(Html.fromHtml(text.replace("null", "<b>" + login_to_activate + "</b>")));
                                            loginConfirmBaloon.setVisibility(View.VISIBLE);
                                            TextView baloonConfirm = (TextView) loginConfirmBaloon.findViewById(R.id.buttonConfirm);
                                            baloonConfirm.setOnClickListener(new OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    loginConfirmBaloon.setVisibility(View.GONE);
                                                    JSONObject result = new JSONObject();
                                                    result.putSafe("confirm_login", login_to_activate);

                                                    Class next_chain = ChangeMobileFragment.class;
                                                    if (!isPhone) next_chain = ChangeEmailFragment.class;
                                                    chainStartNextFragment(FRAGMENT_ID, next_chain, result, 0, false);
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Logger.Exception(e);
            }
        }
    }
}
