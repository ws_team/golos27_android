package ru.infodev.golos27.Widgets.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.infodev.golos27.Common.Catalogs.OKTMO;
import ru.infodev.golos27.R;

public class OKTMOAdapter extends BaseAdapter {
    private ArrayList<OKTMO> mData;
    private LayoutInflater inflater;

    public OKTMOAdapter(Context context, List<OKTMO> items) {
        mData = (ArrayList<OKTMO>) items;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public OKTMO getItem(int index) {
        return mData.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_executor, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.executor_TV);
            viewHolder.check = (ImageView) convertView.findViewById(R.id.check_name_executor);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        OKTMO item = getItem(position);
        viewHolder.name.setText(item.getFullCity());
        if (item.isChecked()) {
            viewHolder.check.setVisibility(View.VISIBLE);
        } else {
            viewHolder.check.setVisibility(View.INVISIBLE);
        }
        return convertView;
    }

    public ArrayList<OKTMO> getData() {
        return mData;
    }

    public void setData(ArrayList<OKTMO> mData) {
        this.mData = mData;
    }

    public OKTMO getCheckedItem() {
        if (mData == null) return null;
        for (OKTMO entry : mData)
            if (entry.isChecked()) return entry;
        return null;
    }

    static class ViewHolder {
        TextView name;
        ImageView check;
    }

    public void setChecked(int position, boolean checked) {
        if (mData == null) return;
        for (OKTMO entry : mData) entry.setIsChecked(false);
        OKTMO executor = mData.get(position);
        if (executor != null) executor.setIsChecked(checked);
        notifyDataSetChanged();
    }
}
