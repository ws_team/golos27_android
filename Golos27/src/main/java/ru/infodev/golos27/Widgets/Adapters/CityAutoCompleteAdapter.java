package ru.infodev.golos27.Widgets.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;

import ru.infodev.golos27.Common.Catalogs.OKTMO;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;

public class CityAutoCompleteAdapter extends BaseAdapter implements Filterable {
    private static final int MAX_RESULTS = 50;
    private static final int DB_MAX_RESULTS = 300;
    private Context mContext;
    private ArrayList<OKTMO> mData;

    public CityAutoCompleteAdapter(Context context) {
        mData = new ArrayList<>();
        mContext = context;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public OKTMO getItem(int index) {
        return mData.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_autocomplete_dropdown, parent, false);
        }
        if (convertView != null) {
            ((TextView) convertView.findViewById(R.id.autoCompleteCity)).setText(getItem(position).getName());
            ((TextView) convertView.findViewById(R.id.autoCompleteRegion)).setText(getItem(position).getFullRegionReversed());
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    try {
                        String search = constraint.toString().toLowerCase();
                        mData = (ArrayList<OKTMO>) OKTMO.SearchCities(search, DB_MAX_RESULTS);
                        // Теперь удалим все результаты в которых строка поиска не находится в начале слова
                        if (mData != null) {
                            Iterator<OKTMO> i = mData.iterator();
                            while (i.hasNext()) {
                                boolean found = false;
                                OKTMO city = i.next();
                                String cityName = city.getName();
                                if (cityName == null) continue;

                                String[] cityNameWords = cityName.toLowerCase().split("[\\s+\\-]");
                                for (String word : cityNameWords) {
                                    if (word.startsWith(search)) {
                                        found = true;
                                        break;
                                    }
                                }

                                if (!found) i.remove();
                            }
                        }

                    } catch (Exception e) {
                        Logger.Exception(e);
                    }
                    filterResults.values = mData;
                    if (mData != null) {
                        filterResults.count = Math.max(mData.size(), MAX_RESULTS);
                    } else {
                        filterResults.count = 0;
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }
}