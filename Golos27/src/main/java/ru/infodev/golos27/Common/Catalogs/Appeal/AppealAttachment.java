package ru.infodev.golos27.Common.Catalogs.Appeal;

import android.net.Uri;

import java.util.ArrayList;

import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

@SuppressWarnings("unused")
public class AppealAttachment {
    private String id;
    private String name;
    private String mimeType;
    private int index;
    private long length;
    private Uri file;
    private String cachePath;

    public String Id() {
        return id;
    }

    public AppealAttachment setId(String id) {
        this.id = id;
        return this;
    }

    public String Name() {
        return name;
    }

    public AppealAttachment setName(String name) {
        this.name = name;
        return this;
    }

    public String MimeType() {
        return mimeType;
    }

    public AppealAttachment setMimeType(String mimeType) {
        this.mimeType = mimeType;
        return this;
    }

    public int Index() {
        return index;
    }

    public AppealAttachment setIndex(int index) {
        this.index = index;
        return this;
    }

    public long Length() {
        return length;
    }

    public AppealAttachment setLength(long length) {
        this.length = length;
        return this;
    }

    public Uri File() {
        return file;
    }

    public AppealAttachment setFile(Uri file) {
        this.file = file;
        return this;
    }

    public String CachePath() {
        return cachePath;
    }

    public void setCachePath(String cache) {
        this.cachePath = cache;
    }

    public static ArrayList<AppealAttachment> fromJson(JSONObject documentsRoot) {
        ArrayList<AppealAttachment>  result = new ArrayList<>();
        if (documentsRoot == null) return result;
        JSONArray documents = documentsRoot.getJSONArray("items", null);
        if (documents == null) return result;

        for (int i = 0; i < documents.length(); i++) {
            AppealAttachment document = new AppealAttachment();
            try {
                JSONObject row = documents.getJSONObject(i);
                document.setId(row.getString("id"));
                document.setName(row.getString("name"));
                document.setMimeType(row.getString("mimeType", ""));
                document.setIndex(row.getInt("index"));

                if (!row.isNull("length"))
                    document.setLength(row.getLong("length"));
                else
                    document.setLength(0);

                String stringUri = row.getString("privateURI", "");
                if (isNotEmpty(stringUri)) document.setFile(Uri.parse(stringUri));
                document.setCachePath(row.getString("privateCache", ""));
                result.add(document);
            } catch (Exception e) {
                Logger.Exception(e);
            }
        }

        return result;
    }

    public JSONObject toJson() {
        JSONObject result = new JSONObject();
        result.putSafe("id", id);
        result.putSafe("name", name);
        result.putSafe("mimeType", mimeType);
        result.putSafe("index", index);
        result.putSafe("length", length);
        if (file != null) result.putSafe("privateURI", file.toString());
        result.putSafe("privateCache", cachePath);
        return result;
    }

    public String getThumbnailId() {
        return isEmpty(id) ? "" : id + "?view=THUMBNAIL";
    }

    public String getMobileId() {
        return isEmpty(id) ? "" : id + "?view=MOBILE";
    }
}
