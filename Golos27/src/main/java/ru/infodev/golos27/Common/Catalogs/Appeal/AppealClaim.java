package ru.infodev.golos27.Common.Catalogs.Appeal;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Address;
import ru.infodev.golos27.Common.Catalogs.Executor;
import ru.infodev.golos27.Common.Catalogs.OKTMO;
import ru.infodev.golos27.Common.Catalogs.Subject;
import ru.infodev.golos27.Common.DBHelper;
import ru.infodev.golos27.Common.GalleryEntry;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;

import static ru.infodev.golos27.Common.Utils.Utils.deleteFile;
import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

@SuppressWarnings("unused")
public class AppealClaim {

    private long mDate = 0;
    private Long mDatabaseId = null;
    private String mFailReason = "";
    private int mTries = 0;

    private Subject subject = new Subject();
    private String message = "";
    private String userId = "";
    private String userFirstName = "";
    private String userLastName = "";
    private String userMiddleName = "";
    private Address address = DefaultAddress();
    private Address zipAddress = new Address();
    private Executor executor = null;
    private boolean autoExecutor = true;
    private boolean isPublishAppeal = true;
    private boolean isPublishResponse = true;
    private boolean isPublishPersonalData = true;
    private boolean isGetMailResponse = false;
    private String documentCoverId = null;
    List<AppealAttachment> documents = new ArrayList<>();
    List<GalleryEntry> imagesList = new ArrayList<>();
    //----------------------------------------------------------------

    public long getDatabaseId() {
        return mDatabaseId;
    }

    public void setDatabaseId(long id) {
        this.mDatabaseId = id;
    }

    public int getTries() {
        return mTries;
    }

    public void incTries() {
        this.mTries++;
    }

    public void setTries(int tries) {
        this.mTries = tries;
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long mDate) {
        this.mDate = mDate;
    }

    public String getFailReason() {
        return mFailReason;
    }

    public AppealClaim setFailReason(String mFailReason) {
        this.mFailReason = mFailReason;
        return this;
    }

    public String Message() {
        return message;
    }

    public Subject Subject() {
        return subject;
    }

    public AppealClaim setSubject(Subject subject) {
        this.subject = subject;
        return this;
    }

    public AppealClaim setMessage(String message) {
        this.message = message;
        return this;
    }

    public String UserId() {
        return userId;
    }

    public AppealClaim setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public String UserFirstName() {
        return userFirstName;
    }

    public AppealClaim setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
        return this;
    }

    public String UserLastName() {
        return userLastName;
    }

    public AppealClaim setUserLastName(String userLastName) {
        this.userLastName = userLastName;
        return this;
    }

    public String UserMiddleName() {
        return userMiddleName;
    }

    public AppealClaim setUserMiddleName(String userMiddleName) {
        this.userMiddleName = userMiddleName;
        return this;
    }

    public Address Address() {
        return address;
    }

    public AppealClaim setAddress(Address address) {
        if (address != null)
            this.address = address;
        else
            this.address = DefaultAddress();
        return this;
    }

    public Address ZipAddress() {
        return zipAddress;
    }

    public AppealClaim setZipAddress(Address zipAddress) {
        if (zipAddress != null)
            this.zipAddress = zipAddress;
        return this;
    }

    public boolean IsPublishAppeal() {
        return isPublishAppeal;
    }

    public AppealClaim setIsPublishAppeal(boolean isPublishAppeal) {
        this.isPublishAppeal = isPublishAppeal;
        return this;
    }

    public boolean IsPublishResponse() {
        return isPublishResponse;
    }

    public AppealClaim setIsPublishResponse(boolean isPublishResponse) {
        this.isPublishResponse = isPublishResponse;
        return this;
    }

    public boolean IsPublishPersonalData() {
        return isPublishPersonalData;
    }

    public AppealClaim setIsPublishPersonalData(boolean isPublishPersonalData) {
        this.isPublishPersonalData = isPublishPersonalData;
        return this;
    }

    public Executor Executor() {
        return executor;
    }

    public AppealClaim setExecutor(Executor executor) {
        this.executor = executor;
        return this;
    }

    public boolean isAutoExecutor() {
        return autoExecutor;
    }

    public AppealClaim setAutoExecutor(boolean autoExecutor) {
        this.autoExecutor = autoExecutor;
        return this;
    }

    //------------------------------------------------------------------------------------------
    public List<GalleryEntry> ImagesList() {
        return imagesList;
    }

    public AppealClaim setImagesList(List<GalleryEntry> attachmentList) {
        this.imagesList = attachmentList;
        return this;
    }

    public AppealClaim AddImage(GalleryEntry attachmentPath) {
        this.imagesList.add(attachmentPath);
        return this;
    }

    public AppealClaim DelImage(GalleryEntry attachmentPath) {
        this.imagesList.remove(attachmentPath);
        return this;
    }

    public AppealClaim CleanImages() {
        this.imagesList.clear();
        return this;
    }

    public void ClearImagesSelection() {
        for (GalleryEntry entry: this.imagesList) entry.setChecked(false);
    }
    //------------------------------------------------------------------------------------------

    public List<AppealAttachment> Documents() {
        return documents;
    }

    public AppealClaim setDocuments(List<AppealAttachment> documents) {
        this.documents = documents;
        return this;
    }

    public AppealClaim AddDocument(AppealAttachment document) {
        this.documents.add(document);
        return this;
    }

    public AppealClaim DelDocument(AppealAttachment document) {
        this.documents.remove(document);
        return this;
    }

    public AppealClaim CleanDocuments() {
        this.documents.clear();
        return this;
    }

    public String DocumentCoverIdPlain() {
        return documentCoverId;
    }

    public String DocumentCoverId() {
        return isEmpty(documentCoverId) ? "" : documentCoverId + "?view=MOBILE";
    }

    public AppealClaim setDocumentCoverId(String documentCoverId) {
        this.documentCoverId = documentCoverId;
        return this;
    }

    public boolean IsGetMailResponse() {
        return this.isGetMailResponse;
    }

    public AppealClaim setIsGetMailResponse(boolean isGetMailResponse) {
        this.isGetMailResponse = isGetMailResponse;
        return this;
    }

    /***
     * Конструктор объекта из JSONObject
     */
    public static AppealClaim fromJson(JSONObject source) {
        AppealClaim newClaim = new AppealClaim();
        if (source == null) return null;
        try {
            newClaim.setMessage(source.getString("message", ""))
                    .setUserId(source.getString("userId", ""))
                    .setUserFirstName(source.getString("userFirstName", ""))
                    .setUserLastName(source.getString("userLastName", ""))
                    .setUserMiddleName(source.getString("userMiddleName", ""))
                    .setAddress(Address.fromJson(source.getJSONObject("address", null)))
                    .setZipAddress(Address.fromJson(source.getJSONObject("zipAddress", null)))
                    .setIsPublishAppeal(source.getBoolean("isPublishAppeal", true))
                    .setIsPublishResponse(source.getBoolean("isPublishResponse", true))
                    .setIsPublishPersonalData(source.getBoolean("isPublishPersonalData", true))
                    .setDocumentCoverId(source.getString("documentCoverId", ""))
                    .setDocuments(AppealAttachment.fromJson(source.getJSONObject("documents", null)))
                    .setFailReason(source.getString("privateFailReason", ""));

            newClaim.setAutoExecutor(source.getBoolean("privateAutoExecutor", true))
                    .setSubject(Subject.fromJson(source.getJSONObject("privateSubject", null)))
                    .setExecutor(Executor.fromJson(source.getJSONObject("privateExecutor", null)))
                    .setImagesList(GalleryEntry.fromJson(source.getJSONArray("privateImages", null)));

        } catch (Exception e) {
            Logger.Exception(e);
            return null;
        }

        return newClaim;
    }

    /***
     * Преобразует объект в JSONObject
     */
    public JSONObject toJson() {
        JSONObject result = new JSONObject();
        JSONArray documents = new JSONArray();
        JSONArray images = new JSONArray();
        JSONObject docRoot = new JSONObject();

        result.putSafe("message", message);
        result.putSafe("userId", userId);
        result.putSafe("userFirstName", userFirstName);
        result.putSafe("userLastName", userLastName);
        result.putSafe("userMiddleName", userMiddleName);
        result.putSafe("address", address.toJson());
        result.putSafe("zipAddress", zipAddress.toJson());
        result.putSafe("isPublishAppeal", isPublishAppeal);
        result.putSafe("isPublishResponse", isPublishResponse);
        result.putSafe("isPublishPersonalData", isPublishPersonalData);
        result.putSafe("isGetMailResponse", isGetMailResponse);
        result.putSafe("documentCoverId", documentCoverId);
        for (AppealAttachment document : this.documents) documents.put(document.toJson());
        docRoot.putSafe("count", documents.length());
        docRoot.putSafe("items", documents);
        result.putSafe("documents", docRoot);

        result.putSafe("privateSubject", subject.toJson());
        if (executor != null) {
            result.putSafe("privateExecutor", executor.toJson());
            result.putSafe("privateAutoExecutor", autoExecutor);
        }

        for (GalleryEntry entry : imagesList) images.put(entry.toJson());
        result.putSafe("privateImages", images);
        result.putSafe("privateFailReason", mFailReason);

        return result;
    }

    private static Address DefaultAddress() {
        Address result = new Address();
        result.setSettlement(new OKTMO("", "08000000", "Хабаровский край", "к", null, null, 0));
        return result;
    }

    /***
     * Полностью очищает заявку на обращение с установкой значений по умолчанию
     */
    public void Cleanup(boolean cleanFiles) {
        subject = new Subject();
        message = "";
        userId = "";
        userFirstName = "";
        userLastName = "";
        userMiddleName = "";
        address = DefaultAddress();
        zipAddress = new Address();
        executor = null;
        autoExecutor = true;
        isPublishAppeal = true;
        isPublishResponse = true;
        isPublishPersonalData = true;
        isGetMailResponse = false;
        documentCoverId = null;

        try {
            List<GalleryEntry> galleryEntries = ImagesList();
            if (galleryEntries != null) {
                for (Iterator<GalleryEntry> it = galleryEntries.iterator(); it.hasNext(); ) {
                    GalleryEntry entry = it.next();
                    if (cleanFiles) {
                        deleteFile(entry.ImagePath());
                        deleteFile(entry.ThumbnailPath());
                    }
                    it.remove();
                }
            }
        } catch (Exception e) {
            Logger.Exception(e);
        }

        documents = new ArrayList<>();
        imagesList = new ArrayList<>();
    }

    public String AuthorFIO() {
        String result = isNotEmpty(userLastName) ? userLastName : "";
        if (isNotEmpty(result)) result += " ";
        if (isNotEmpty(userFirstName))
            result += userFirstName + " ";
        if (isNotEmpty(userMiddleName))
            result += userMiddleName;
        return result;
    }

    /***
     * Возвращает кол-во документов в данном обращении
     */
    public int getDocumentsCount() {
        int result = 0;
        if (documents == null) return 0;
        for (AppealAttachment doc : documents)
            if (!doc.MimeType().contains("image")) result++;

        return result;
    }

    /***
     * Возвращает изображений в данном обращении
     */
    public int getImagesCount() {
        int result = 0;
        if (documents == null) return 0;
        for (AppealAttachment doc : documents)
            if (doc.MimeType().contains("image")) result++;

        return result;
    }

    /***
     * Возвращает список изображений в данном обращении
     */
    public List<AppealAttachment> getImagesList() {
        List<AppealAttachment> result = new ArrayList<>();
        if (documents == null) return result;
        for (AppealAttachment doc : documents)
            if (doc.MimeType().contains("image")) result.add(doc);
        return result;
    }

    /***
     * Возвращает список документов в данном обращении
     */
    public List<AppealAttachment> getDocsList() {
        List<AppealAttachment> result = new ArrayList<>();
        if (documents == null) return result;
        for (AppealAttachment doc : documents)
            if (!doc.MimeType().contains("image")) result.add(doc);
        return result;
    }

    /***
     * Сохраняет обращение в базе данных приложения для последующей отправки
     * return: идентификатор заявки в базе данных
     */
    private Long save(boolean commit) {
        DBHelper database = Settings.getDatabase();
        if (database == null) return null;

        SQLiteDatabase db = null;
        try {
            db = database.getWritableDatabase();
            ContentValues cv = new ContentValues();

            cv.put("date", mDate);
            cv.put("tries", mTries);
            cv.put("content", toJson().toString());

            long iNum;
            if (mDatabaseId == null) {
                iNum = db.insert(Application.AppContext.getString(R.string.appeals_table_name), null, cv);
                if (iNum <= 0) Logger.Log("Can't insert value for " + toString());
            } else {
                String[] whereArgs = { mDatabaseId.toString() };
                iNum = db.update(Application.AppContext.getString(R.string.appeals_table_name), cv, "id=?", whereArgs);
                if (iNum <= 0) Logger.Log("Can't update value for " + toString());
            }

            if ((db.inTransaction()) && commit) db.endTransaction();
            if (iNum >= 0) mDatabaseId = iNum;
            return iNum >= 0 ? iNum : null;

        } catch (Exception ex) {
            if ((db != null) && db.inTransaction()) {
                db.endTransaction();
            }
            Logger.Exception(ex);
        }

        return null;
    }

    public Long save() {
        return save(true);
    }

    /***
     * Удалим текущий объект из базы данных
     */
    public boolean remove(boolean commit) {
        if (mDatabaseId == null) return false;

        DBHelper database = Settings.getDatabase();
        if (database == null) return false;
        String[] itemId = { mDatabaseId.toString() };
        SQLiteDatabase db = null;
        try {
            db = database.getWritableDatabase();
            long iNum = db.delete(Application.AppContext.getString(R.string.appeals_table_name), "id=?", itemId);
            if (iNum <= 0) Logger.Log("Can't delete value of appeal " + mDatabaseId.toString());
            if (db.inTransaction() && commit) db.endTransaction();
        } catch (Exception ex) {
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        return false;
    }

    @SuppressWarnings("unused")
    public boolean remove() {
        return remove(true);
    }

    @Override
    public String toString() {
        return "Заявка на обращение " + subject.toString();
    }

    // Получает список сохраненных в базе и не отправленных обращений
    public static List<AppealClaim> getSavedAppealClaims() {
        Cursor c = null;
        List<AppealClaim> result = null;
        DBHelper database = Settings.getDatabase();
        if (database == null) return null;
        SQLiteDatabase db = null;
        try {
            db = database.getWritableDatabase();
            c = db.query(Application.AppContext.getString(R.string.appeals_table_name),
                    null, null, null, null, null, null);

            result = new ArrayList<>();
            if ((c.getCount() <= 0)) {
                return result;
            } else {
                if (c.moveToFirst()) {
                    do {
                        // Subject(String id, String name, String parentId, String resource, Long version) {
                        JSONObject appealJson = new JSONObject(c.getString(3));
                        AppealClaim appealClaim = AppealClaim.fromJson(appealJson);
                        if (appealClaim == null) {
                            Logger.Warn("Can't parse appeal claim from database!");
                            continue;
                        }

                        appealClaim.setTries(c.getInt(2));
                        appealClaim.setDatabaseId(c.getLong(0));
                        appealClaim.setDate(c.getLong(1));
                        result.add(appealClaim);
                    } while (c.moveToNext());

                    return result;
                }
            }

            c.close();
            if (db.inTransaction()) db.endTransaction();
        } catch (Exception ex) {
            if (c != null) c.close();
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        return result;
    }
}
