package ru.infodev.golos27.Fragments.Dialogs;

import android.app.Activity;
import android.content.Context;

import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.MainActivity;

/***
 * Вызывается из треда UI. Открывает диалог отмены обращения
 */
public class YesNoDialog {
    YesNoDialogFragment rDialog;

    public YesNoDialog(Context context, String shortText, String fullText, String buttonOKText,
                       String buttonCancelText, DialogResultInterface listener) {
        rDialog = new YesNoDialogFragment();
        rDialog.setContext(context);
        rDialog.setMessages(shortText, fullText, buttonOKText, buttonCancelText);
        if (listener != null) rDialog.setResultListener(listener);
    }

    public YesNoDialog(Context context, String shortText, String fullText, String buttonOKText,
                       String buttonCancelText, DialogResultInterface listener, boolean isOkButtonPrimary) {
        rDialog = new YesNoDialogFragment();
        rDialog.setContext(context);
        rDialog.setMessages(shortText, fullText, buttonOKText, buttonCancelText);
        if (listener != null) rDialog.setResultListener(listener);
        if(isOkButtonPrimary) rDialog.setOkButtonPrimary();
    }

    public YesNoDialog(Context context, int shortText, int fullText, int buttonOKText,
                       int buttonCancelText, DialogResultInterface listener) {
        rDialog = new YesNoDialogFragment();
        rDialog.setContext(context);
        rDialog.setMessages(shortText, fullText, buttonOKText, buttonCancelText);
        if (listener != null) rDialog.setResultListener(listener);
    }

    /***
     * Инициализация строками
     */
    public static YesNoDialog makeDialog(Context context, String shortText, String fullText, String buttonOKText,
                                         String buttonCancelText, DialogResultInterface listener) {
        return new YesNoDialog(context, shortText, fullText, buttonOKText, buttonCancelText, listener);
    }

    public static YesNoDialog makeDialog(Context context, int shortText, int fullText, int buttonOKText,
                                         int buttonCancelText, DialogResultInterface listener) {
        return new YesNoDialog(context, shortText, fullText, buttonOKText, buttonCancelText, listener);
    }

    public void show(Activity activity) {
        if (activity != null) {
            if (activity instanceof MainActivity) {
                if (((MainActivity) activity).isActive)
                    rDialog.show(activity.getFragmentManager(), "YesNoDialog");
            } else {
                rDialog.show(activity.getFragmentManager(), "YesNoDialog");
            }
        }
    }
}
