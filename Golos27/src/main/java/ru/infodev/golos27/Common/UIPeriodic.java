package ru.infodev.golos27.Common;

import android.os.Handler;
import android.os.Looper;

import ru.infodev.golos27.Common.Utils.Logger;

public class UIPeriodic {
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private Runnable mStatusChecker;
    private int updateInterval = 300000;

    /**
     * Запускает переданный объект Runnable с заданным интервалом
     */
    public UIPeriodic(final Runnable uiPeriodic, final int updateIntervalParam) {
        this.updateInterval = updateIntervalParam;

        mStatusChecker = new Runnable() {
            @Override
            public void run() {
                uiPeriodic.run();
                mHandler.postDelayed(this, updateInterval);
            }
        };
    }

    public synchronized void startUpdates(){
        Logger.Log("Start periodic task");
        mStatusChecker.run();
    }

    public synchronized void stopUpdates(){
        Logger.Log("Stop periodic task");
        mHandler.removeCallbacks(mStatusChecker);
    }
}
