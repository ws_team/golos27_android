package ru.infodev.golos27.Widgets;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.EditText;

import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;

public class MonitoredEditText extends AppCompatEditText implements TextWatcherAdapter.TextWatcherListener {
    private TextWatcherAdapter.TextWatcherListener textChangeListener;

    public MonitoredEditText(Context context) {
        super(context);
        init();
    }

    public MonitoredEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MonitoredEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setOnTextChangedListener(TextWatcherAdapter.TextWatcherListener l) {
        this.textChangeListener = l;
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        if (textChangeListener != null) {
            textChangeListener.onTextChanged(view, text);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        if (textChangeListener != null) {
            textChangeListener.beforeTextChanged(s, start, count, after);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (textChangeListener != null) {
            textChangeListener.afterTextChanged(s);
        }
    }

    private void init() {
        addTextChangedListener(new TextWatcherAdapter(this, this));
    }
}
