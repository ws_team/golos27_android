package ru.infodev.golos27.Fragments.AppealClaim;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Address;
import ru.infodev.golos27.Common.Catalogs.NewAppeal;
import ru.infodev.golos27.Common.Catalogs.OKTMO;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.Interfaces.LocationResultInterface;
import ru.infodev.golos27.Common.Interfaces.MapJavaScriptInterface;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;
import ru.infodev.golos27.Widgets.Layouts.NestedWebView;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

@SuppressWarnings("FieldCanBeLocal")
public class SetMapLocationFragment extends ParamFragment implements LocationResultInterface {
    public static final int FRAGMENT_ID = R.layout.set_map_location;
    public static final int INVALID_COORDINATE = 1000000;
    private double userLatitude = INVALID_COORDINATE;
    private double userLongitude = INVALID_COORDINATE;
    private String userAddress = "";
    private String userRegion = "";
    private String userArea = "";
    private String userSettlement = "";
    private int userZoomLevel = 6;

    private MapJavaScriptInterface mapJSInterface;
    private NestedWebView mapWebView;
    private TextView mapLoading;
    private boolean notChain = false;
    private Button skipButton;
    private FrameLayout mapСontainer;

    public SetMapLocationFragment() {
        titleId = R.string.map_address;
        setFragmentAllowMenu(false);
    }

    private boolean getPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{ Manifest.permission.ACCESS_FINE_LOCATION }, 0);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initMap();
        } else {
            initMap();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mapJSInterface != null) {
            mapJSInterface.stopLocationTracker();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mapJSInterface != null) {
            mapJSInterface.startLocationTracker();
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        notChain = parameters.getBoolean(ParamFragment.NOT_CHAIN, false);
        if (notChain)
            setActionButtonIcon(ParamFragment.ICON_CHECK);
        else
            setActionButtonIcon(ParamFragment.ICON_RIGHT);

        showSaveButtonIfCan();
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setRetainInstance(true);

        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        notChain = parameters.getBoolean(ParamFragment.NOT_CHAIN, false);

        skipButton = (Button) v.findViewById(R.id.button_skip);
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button_skip:
                        NewAppeal.getInstance().Address().setLatitude((double) INVALID_COORDINATE);
                        NewAppeal.getInstance().Address().setLongitude((double) INVALID_COORDINATE);
                        NewAppeal.getInstance().Address().setCustomAddress("");
                        NewAppeal.setSettlement(OKTMO.getDefaultSettlement());
                        if (notChain) {
                            chainPopFragments(FRAGMENT_ID, 1, false);
                        } else {
                            chainStartNextFragment(FRAGMENT_ID, SetPictureFragment.class, null, false);
                        }
                }
            }
        });

        mapСontainer = (FrameLayout) v.findViewById(R.id.map_container);
        mapLoading = (TextView) v.findViewById(R.id.map_loading);
        mapLoading.setTypeface(roboto_light);
        mapLoading.setText(getString(R.string.map_loading).replace("null", "0%"));

        mapWebView = (NestedWebView) v.findViewById(R.id.mapWebView);
        mapWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (fragmentActive)
                    mapLoading.setText(getString(R.string.map_loading)
                            .replace("null", String.valueOf(progress) + "%"));
            }
        });

        if(getPermission()) initMap();

        return v;
    }

    private void initMap() {
        mapJSInterface = new MapJavaScriptInterface(Application.AppContext);

        Address address = NewAppeal.getInstance().Address();
        if (address != null &&
                address.Latitude() != INVALID_COORDINATE &&
                address.Longitude() != INVALID_COORDINATE &&
                isNotEmpty(address.CustomAddress())) {

            userLatitude = address.Latitude();
            userLongitude = address.Longitude();
            userAddress = address.CustomAddress();
            userZoomLevel = address.ZoomLevel();
        }

        mapJSInterface.setLocationListener(this);
        mapWebView.addJavascriptInterface(mapJSInterface, "Golos27App");
        WebSettings webSettings = mapWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        try {
            InputStream is = getActivity().getAssets().open("map.html");
            byte[] buffer = new byte[is.available()];
            if (is.read(buffer) <= 0) Logger.Warn("Can't read embedded assets map.html file!");
            is.close();

            String htmlText = new String(buffer);
            mapWebView.loadDataWithBaseURL(Application.AppContext.getString(R.string.yandex_maps_url),
                    htmlText, "text/html", "UTF-8", null
            );
        } catch (IOException e) {
            Logger.Exception(e);
        }

        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
    }

    @Override
    public void OnLocationReceived(String region, String area, String settlement,
                                   String address, final double latitude, final double longitude, int iZoomLevel) {
        userRegion = region;
        userArea = area;
        userSettlement = settlement;
        userAddress = address;
        userLatitude = latitude;
        userLongitude = longitude;
        userZoomLevel = iZoomLevel;
        Logger.Log("Location data: region=\"" + region + "\", area=\"" + area + "\", setl=\"" + settlement + "\"");
        Logger.Log("Location data: addr=\"" + address + "\", lat=\'" + String.valueOf(latitude) +
                "\', long=\'" + String.valueOf(longitude) + "\'");

        if ((latitude != INVALID_COORDINATE) && (longitude != INVALID_COORDINATE)) {
            new RequestOKTMOCode().execute(userRegion, userArea, userSettlement);
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showSaveButtonIfCan();
            }
        });
    }

    public void openBaloonAtPoint(Double latitude, Double longitude, Integer zoomLevel) {
        mapWebView.loadUrl("javascript:openBaloonAtPoint(" + latitude.toString() + ", " +
                longitude.toString() + ", " + zoomLevel.toString() + ");");
    }

    public void moveMapToSettlement(String latitude, String longitude) {
        mapWebView.loadUrl("javascript:moveMapTo(" + latitude + ", " + longitude + ");");
    }

    @SuppressWarnings("unused")
    public void moveMapToPoint(String latitude, String longitude) {
        mapWebView.loadUrl("javascript:moveMapToPoint(" + latitude + ", " + longitude + ");");
    }

    @Override
    public void OnPageLoaded() {
        Activity activity = getActivity();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mapWebView.setVisibility(View.VISIBLE);
                    String[] location = mapJSInterface.getLocation();
                    Address address = NewAppeal.getInstance().Address();
                    if (address != null &&
                            address.Latitude() != INVALID_COORDINATE &&
                            address.Longitude() != INVALID_COORDINATE &&
                            isNotEmpty(address.CustomAddress())) {

                        userLatitude = address.Latitude();
                        userLongitude = address.Longitude();
                        userAddress = address.CustomAddress();
                        userZoomLevel = address.ZoomLevel();
                        openBaloonAtPoint(userLatitude, userLongitude, userZoomLevel);
                    } else {
                        if (location != null) moveMapToSettlement(location[0], location[1]);
                    }
                }
            });
        }
    }

    @Override
    public void OnGPSError(int code) {
    }

    private void showSaveButtonIfCan() {
        boolean dataOk = userAddress.length() > 0 &&
                userLatitude != INVALID_COORDINATE && userLongitude != INVALID_COORDINATE;

        setActionButtonVisibility(dataOk);
        if (dataOk) {
            skipButton.setVisibility(View.GONE);
            // Восстанавливаем weight=1 чтобы  контейнер забрал себе все освободившееся место
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mapСontainer.getLayoutParams();
            layoutParams.height = 0;
            layoutParams.weight = 1;
            mapСontainer.setLayoutParams(layoutParams);
            mapСontainer.getParent().requestLayout();
        } else {
            int visibility = skipButton.getVisibility();
            skipButton.setVisibility(View.VISIBLE);

            // Если кнопка была спрятана, то место для нее придется освобождать вручную потому
            // что на очень нелюбимом андроид 4 оно автоматически не освобождается.
            if (visibility == View.GONE) {
                int height = mapСontainer.getMeasuredHeight();
                ViewGroup.LayoutParams skipParams = skipButton.getLayoutParams();
                LinearLayout.LayoutParams containerParams = (LinearLayout.LayoutParams) mapСontainer.getLayoutParams();
                containerParams.height = height - skipParams.height;
                containerParams.weight = 0;
                mapСontainer.setLayoutParams(containerParams);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_item) {
            NewAppeal.getInstance().Address().setLatitude(userLatitude);
            NewAppeal.getInstance().Address().setLongitude(userLongitude);
            NewAppeal.getInstance().Address().setCustomAddress(userAddress);
            NewAppeal.getInstance().Address().setZoomLevel(userZoomLevel);

            if (notChain) {
                // Пользователь цепочку уже прошел - желает изменений (выходим с результатом)
                chainPopFragments(FRAGMENT_ID, 1, false);
            } else {
                // Идем по цепочке
                JSONObject params = new JSONObject();
                params.putSafe(ParamFragment.NOT_CHAIN, notChain);
                chainStartNextFragment(FRAGMENT_ID, SetPictureFragment.class, params, false);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /***
     * Преобразование результатов геокодинга яндекс-карт в OKTMO код
     * используется отдельный запрос на сервер
     */
    class RequestOKTMOCode extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected final JSONObject doInBackground(String[] params) {
            // Регион, Район, Поселение
            ServerTalk server = new ServerTalk();
            JSONObject result = server.getOKTMOByAddress(params[0], params[1], params[2]);
            result.putSafe("regionName", params[0]);
            result.putSafe("areaName", params[1]);
            result.putSafe("settlementName", params[2]);
            return result;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            OKTMO settlement;
            JSONObject response;
            String code = "invalid";

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    try {
                        response = result.getJSONObject("response");
                        code = response.getString("code");
                    } catch (Exception e) {
                        Logger.Exception(e);
                    }
                }

                settlement = OKTMO.Get(code);
                if (settlement == null) settlement = OKTMO.getDefaultSettlement();
                NewAppeal.setSettlement(settlement); // Там запускается получение исполнителя по умолчанию
            } catch (Exception e) {
                Logger.Exception(e);
            }
        }
    }
}
