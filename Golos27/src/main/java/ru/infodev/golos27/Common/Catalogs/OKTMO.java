package ru.infodev.golos27.Common.Catalogs;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.DBHelper;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

@SuppressWarnings("unused")
public class OKTMO {

    private String Id = "";
    private String Code = "";
    private String RegionCode = "";
    private String AreaCode = "";
    private String Name = "";
    private String Type = "";
    private String Data = "";
    private int level;
    private boolean isChecked = false;

    // Конструкторы класса
    public OKTMO(String id, String code, String name, String type, String areaCode, String regionCode, int level) {
        Id = id;
        Code = code;
        RegionCode = regionCode;
        AreaCode = areaCode;
        Name = name;
        Type = type;
        this.level = level;
    }

    public OKTMO(String code, String name) {
        Code = code;
        Name = name;
    }

    public OKTMO() {
    }

    // Методы работы с полями
    public String getId() {
        return Id != null ? Id : "";
    }

    public OKTMO setId(String id) {
        Id = id == null ? "" : id;
        return this;
    }

    public String getCode() {
        return Code != null ? Code : "";
    }

    public OKTMO setCode(String code) {
        Code = code == null ? "" : code;
        return this;
    }

    public String getRegionCode() {
        return RegionCode != null ? RegionCode : "";
    }

    public OKTMO setRegionCode(String regionCode) {
        RegionCode = regionCode == null ? "" : regionCode;
        return this;
    }

    public String getAreaCode() {
        return AreaCode != null ? AreaCode : "";
    }

    public OKTMO setAreaCode(String areaCode) {
        AreaCode = areaCode == null ? "" : areaCode;
        return this;
    }

    public String getName() {
        return Name != null ? Name : "";
    }

    public OKTMO setName(String name) {
        Name = name == null ? "" : name;
        return this;
    }

    public String getType() {
        return Type != null ? Type : "";
    }

    public OKTMO setType(String type) {
        Type = type == null ? "" : type;
        return this;
    }

    public int getLevel() {
        return level;
    }

    public OKTMO setLevel(int level) {
        this.level = level;
        return this;
    }

    public String getData() {
        return getFullSettlement();
        //return Data != null ? Data : "";
    }

    public OKTMO setData(String data) {
        Data = data;
        return this;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public OKTMO setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
        return this;
    }

    // Общие методы класса
    public String getFullCity() {
        String sep = "";
        if (isNotEmpty(getType()) && isNotEmpty(getName())) sep = " ";
        return getType() + sep + getName();
    }

    // Полное название региона
    public String getFullRegion() {
        String sep = "";
        if (isNotEmpty(getRegionCode()) && isNotEmpty(getAreaCode())) sep = ", ";
        return getRegionCode() + sep + getAreaCode();
    }

    // Полное название региона (обратный порядок)
    public String getFullRegionReversed() {
        String sep = "";
        if (isNotEmpty(getRegionCode()) && isNotEmpty(getAreaCode())) sep = ", ";
        return getAreaCode() + sep + getRegionCode();
    }

    // Полное название населенного пункта с регионом и типом
    public String getFullSettlement() {
        String sep = "";
        if (isNotEmpty(getFullRegion()) && isNotEmpty(getFullCity())) sep = ", ";
        return getFullRegion() + sep + getFullCity();
    }

    @Override
    public String toString() {
        return getName();
    }

    public String getCityFullName() {
        return getFullRegion() + " " + getFullCity();
    }

    /***
     * Сохраним текущий объект в базу данных
     */
    private boolean save(boolean commit) {
        DBHelper database = Settings.getDatabase();
        if (database == null) return false;

        SQLiteDatabase db = null;
        try {
            db = database.getWritableDatabase();
            ContentValues cv = new ContentValues();

            cv.put("guid", getId());
            cv.put("code", getCode());
            cv.put("name", getName());
            cv.put("type", getType());
            cv.put("area", getAreaCode());
            cv.put("region", getRegionCode());
            cv.put("level", getLevel());
            cv.put("search", getName().toLowerCase(Locale.getDefault()));

            long iNum = db.insert(Application.AppContext.getString(R.string.oktmos_table_name), null, cv);
            if (iNum <= 0) Logger.Log("Can't write value for " + toString());
            if ((db.inTransaction()) && commit) db.endTransaction();
            return iNum > 0;

        } catch (Exception ex) {
            if ((db != null) && db.inTransaction()) {
                db.endTransaction();
            }
            Logger.Exception(ex);
        }

        return false;
    }

    @SuppressWarnings("unused")
    public boolean save() {
        return save(true);
    }

    /***
     * Удалим текущий объект из базы данных
     */
    public boolean remove(boolean commit) {
        DBHelper database = Settings.getDatabase();
        if (database == null) return false;
        String[] code = {getCode()};
        SQLiteDatabase db = null;
        try {
            db = database.getWritableDatabase();
            long iNum = db.delete(Application.AppContext.getString(R.string.oktmos_table_name), "code=?", code);
            if (iNum <= 0) Logger.Log("Can't delete value of " + getCode());
            if (db.inTransaction() && commit) db.endTransaction();
        } catch (Exception ex) {
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        return false;
    }

    @SuppressWarnings("unused")
    public boolean remove() {
        return remove(true);
    }

    // Статические методы класса
    public static OKTMO fromJson(JSONObject oktmoUnit) {
        OKTMO result = new OKTMO();
        try {
            result.setCode((String) oktmoUnit.get("code"));
            result.setLevel(oktmoUnit.getInt("type"));

            if (!oktmoUnit.isNull("id"))
                result.setId(oktmoUnit.getString("id"));

            if (!oktmoUnit.isNull("name"))
                result.setName(oktmoUnit.getString("name"));

            if (!oktmoUnit.isNull("shortName"))
                result.setType(oktmoUnit.getString("shortName"));

            if (!oktmoUnit.isNull("area"))
                result.setAreaCode(oktmoUnit.getString("area"));

            if (!oktmoUnit.isNull("region"))
                result.setRegionCode(oktmoUnit.getString("region"));
        } catch (Exception e) {
            Logger.Exception(e);
            return null;
        }

        return result;
    }

    public static List<OKTMO> Search(String search) {
        return Search(search, 1024);
    }

    public static List<OKTMO> All() {
        Cursor c = null;
        String clause = null;
        String[] selectVal;
        List<OKTMO> result = null;
        DBHelper database = Settings.getDatabase();
        if (database == null) return null;
        SQLiteDatabase db = null;
        try {

            db = database.getWritableDatabase();
            c = db.query(Application.AppContext.getString(R.string.oktmos_table_name),
                    null, null, null, null, null, null, null);

            result = new ArrayList<>();
            if ((c.getCount() <= 0)) {
                return result;
            } else {
                // У нас есть результаты!
                if (c.moveToFirst()) {
                    do {
                        OKTMO oktmo = new OKTMO(c.getString(0), c.getString(1), c.getString(2),
                                c.getString(3), c.getString(4), c.getString(5), c.getInt(6));
                        result.add(oktmo);
                    } while (c.moveToNext());

                    return result;
                }
            }

            c.close();
            if (db.inTransaction()) db.endTransaction();
        } catch (Exception ex) {
            if (c != null) c.close();
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        return result;
    }

    public static List<OKTMO> Search(String search, Integer limit) {
        Cursor c = null;
        String clause = null;
        String[] selectVal;
        List<OKTMO> result = null;
        DBHelper database = Settings.getDatabase();
        if (database == null) return null;
        SQLiteDatabase db = null;
        try {
            if (search != null) {
                clause = "search LIKE ?";
                selectVal = new String[]{"%" + search + "%"};
            } else {
                selectVal = null;
            }

            db = database.getWritableDatabase();
            c = db.query(Application.AppContext.getString(R.string.oktmos_table_name),
                    null, clause, selectVal, null, null, null, limit.toString());

            result = new ArrayList<>();
            if ((c.getCount() <= 0)) {
                return result;
            } else {
                // У нас есть результаты!
                if (c.moveToFirst()) {
                    do {
                        OKTMO oktmo = new OKTMO(c.getString(0), c.getString(1), c.getString(2),
                                c.getString(3), c.getString(4), c.getString(5), c.getInt(6));
                        result.add(oktmo);
                    } while (c.moveToNext());

                    return result;
                }
            }

            c.close();
            if (db.inTransaction()) db.endTransaction();
        } catch (Exception ex) {
            if (c != null) c.close();
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        return result;
    }

    public static List<OKTMO> SearchCities(String search, Integer limit) {
        Cursor c = null;
        String clause = null;
        String[] selectVal;
        List<OKTMO> result = null;
        DBHelper database = Settings.getDatabase();
        if (database == null) return null;
        SQLiteDatabase db = null;
        try {
            if (search != null) {
//                clause = "search LIKE ? AND (level = 2 OR level = 4)"; //prior to DB v15
                clause = "search LIKE ? AND level = 5";
                selectVal = new String[]{"%" + search + "%"};
            } else {
                selectVal = null;
            }

            db = database.getWritableDatabase();
            c = db.query(Application.AppContext.getString(R.string.oktmos_table_name),
                    null, clause, selectVal, null, null, null, limit.toString());

            result = new ArrayList<>();
            if ((c.getCount() <= 0)) {
                return result;
            } else {
                // У нас есть результаты!
                if (c.moveToFirst()) {
                    do {
                        OKTMO oktmo = new OKTMO(c.getString(0), c.getString(1), c.getString(2),
                                c.getString(3), c.getString(4), c.getString(5), c.getInt(6));
                        result.add(oktmo);
                    } while (c.moveToNext());

                    return result;
                }
            }

            c.close();
            if (db.inTransaction()) db.endTransaction();
        } catch (Exception ex) {
            if (c != null) c.close();
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        return result;
    }

    public static OKTMO Get(String code) {
        Cursor c = null;
        String clause;
        String[] selectVal;
        DBHelper database = Settings.getDatabase();
        if (database == null) return null;
        SQLiteDatabase db = null;
        try {
            if (code == null) return null;
            clause = "code = ?";
            selectVal = new String[]{code};

            db = database.getWritableDatabase();
            c = db.query(Application.AppContext.getString(R.string.oktmos_table_name),
                    null, clause, selectVal, null, null, null);

            if ((c.getCount() <= 0)) {
                return null;
            } else {
                // У нас есть результат!
                if (c.moveToFirst()) {
                    return new OKTMO(c.getString(0), c.getString(1), c.getString(2),
                            c.getString(3), c.getString(4), c.getString(5), c.getInt(6));
                }
            }

            c.close();
            if (db.inTransaction()) db.endTransaction();
        } catch (Exception ex) {
            if (c != null) c.close();
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        return null;
    }

    public static boolean updateListFromServer(JSONObject serverReply) {
        boolean result = false;
        JSONArray jsonList = null;
        try {
            if (serverReply == null) return false;
            if (serverReply.has(JSONObject.JSON_LONG_STRING)) {
                JSONObject temp = new JSONObject(serverReply.getString(JSONObject.JSON_LONG_STRING));
                if (temp.has("response")) {
                    jsonList = temp.getJSONObject("response").getJSONArray("items");
                }
            } else {
                jsonList = serverReply.getJSONArray("items");
            }

            if (jsonList == null) return false;
            DBHelper database = Settings.getDatabase();
            if (database == null) return false;

            SQLiteDatabase db = database.getWritableDatabase();
            try {
                db.beginTransaction();
                db.execSQL("DELETE FROM oktmos");
                for (int i = 0; i < jsonList.length(); i++) {
                    JSONObject row = jsonList.getJSONObject(i);
                    OKTMO oktmos = OKTMO.fromJson(row);
                    if (oktmos != null)
                        oktmos.save(false);
                }
                db.setTransactionSuccessful();
                result = true;
            } catch (Exception e) {
                if ((db != null) && db.inTransaction()) db.endTransaction();
                Logger.Exception(e);
            } finally {
                if (db != null) db.endTransaction();
            }

        } catch (Exception e) {
            Logger.Exception(e);
        }

        return result;
    }

    /***
     * Населенный пункт по умолчанию. Используется тогда
     * когда пользователь пропускает выбор местоположения
     *
     * @return населенный пункт
     */
    public static OKTMO getDefaultSettlement() {
        return new OKTMO()
                .setCode("08000000")
                .setName("Хабаровский")
                .setType("край")
                .setRegionCode(null)
                .setAreaCode(null);
    }
}