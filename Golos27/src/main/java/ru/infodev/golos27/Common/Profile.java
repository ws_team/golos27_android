package ru.infodev.golos27.Common;

import java.util.Date;

import ru.infodev.golos27.Common.Catalogs.OKTMO;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;

public class Profile {
    public static final int MALE = 0;
    public static final int FEMALE = 1;

    public static String FirstName() {
        return Settings.getStr(Settings.PROFILE_FIRST_NAME);
    }

    public static void setFirstName(String value) {
        if (value != null) {
            Settings.setParameter(Settings.PROFILE_FIRST_NAME, value);
        } else {
            Settings.delDatabaseParameter(Settings.PROFILE_FIRST_NAME);
        }
    }

    public static String LastName() {
        return Settings.getStr(Settings.PROFILE_LAST_NAME);
    }

    public static void setLastName(String value) {
        if (value != null) {
            Settings.setParameter(Settings.PROFILE_LAST_NAME, value);
        } else {
            Settings.delDatabaseParameter(Settings.PROFILE_LAST_NAME);
        }
    }

    public static String MiddleName() {
        return Settings.getStr(Settings.PROFILE_MIDDLE_NAME);
    }

    public static void setMiddleName(String value) {
        if (value != null) {
            Settings.setParameter(Settings.PROFILE_MIDDLE_NAME, value);
        } else {
            Settings.delDatabaseParameter(Settings.PROFILE_MIDDLE_NAME);
        }
    }

    public static String OKTMOCode() {
        return Settings.getStr(Settings.PROFILE_OKTMO_CODE);
    }

    public static void setOKTMOCode(String value) {
        if (value != null) {
            Settings.setParameter(Settings.PROFILE_OKTMO_CODE, value);
        } else {
            Settings.delDatabaseParameter(Settings.PROFILE_OKTMO_CODE);
        }
    }

    public static String OKTMOData() {
        return Settings.getStr(Settings.PROFILE_OKTMO_DATA);
    }

    public static void setOKTMOData(String value) {
        if (value != null) {
            Settings.setParameter(Settings.PROFILE_OKTMO_DATA, value);
        } else {
            Settings.delDatabaseParameter(Settings.PROFILE_OKTMO_DATA);
        }
    }

    public static String Address() {
        return Settings.getStr(Settings.PROFILE_CUSTOM_ADDRESS);
    }

    public static void setAddress(String value) {
        if (value != null) {
            Settings.setParameter(Settings.PROFILE_CUSTOM_ADDRESS, value);
        } else {
            Settings.delDatabaseParameter(Settings.PROFILE_CUSTOM_ADDRESS);
        }
    }

    public static double Latitude() {
        String strValue = Settings.getStr(Settings.PROFILE_LATITUDE);
        try {
            return Double.valueOf(strValue);
        } catch (Exception e) { /* pass */ }
        return 0d;
    }

    public static void setLatitude(Double value) {
        if (value != null) {
            String strValue = value.toString();
            Settings.setParameter(Settings.PROFILE_LATITUDE, strValue);
        } else {
            Settings.delDatabaseParameter(Settings.PROFILE_LATITUDE);
        }
    }

    public static double Longitude() {
        String strValue = Settings.getStr(Settings.PROFILE_LONGITUDE);
        try {
            return Double.valueOf(strValue);
        } catch (Exception e) { /* pass */ }
        return 0d;
    }

    public static void setLongitude(Double value) {
        if (value != null) {
            String strValue = value.toString();
            Settings.setParameter(Settings.PROFILE_LONGITUDE, strValue);
        } else {
            Settings.delDatabaseParameter(Settings.PROFILE_LONGITUDE);
        }
    }

    public static Date Birth() {
        String strValue = Settings.getStr(Settings.PROFILE_BIRTH_DATE);
        Long tempValue = null;
        try {
            tempValue = Long.valueOf(strValue);
        } catch (NumberFormatException e) { /* pass */ }

        if (tempValue != null)
            return new Date(tempValue);
        else
            return null;
    }

    public static void setBirth(Date value) {
        if (value != null) {
            String strValue = String.valueOf(value.getTime());
            Settings.setParameter(Settings.PROFILE_BIRTH_DATE, strValue);
        } else {
            Settings.delDatabaseParameter(Settings.PROFILE_BIRTH_DATE);
        }
    }

    public static int Gender() {
        return Settings.getInt(Settings.PROFILE_GENDER);
    }

    public static void setGender(Integer value) {
        if (value != null) {
            Settings.setParameter(Settings.PROFILE_GENDER, value.toString());
        } else {
            Settings.delDatabaseParameter(Settings.PROFILE_GENDER);
        }
    }

    public static void RemoveProfileData() {
        Settings.delDatabaseParameter(Settings.PROFILE_FIRST_NAME);
        Settings.delDatabaseParameter(Settings.PROFILE_LAST_NAME);
        Settings.delDatabaseParameter(Settings.PROFILE_MIDDLE_NAME);
        Settings.delDatabaseParameter(Settings.PROFILE_GENDER);
        Settings.delDatabaseParameter(Settings.PROFILE_BIRTH_DATE);
        Settings.delDatabaseParameter(Settings.PROFILE_CUSTOM_ADDRESS);
        Settings.delDatabaseParameter(Settings.PROFILE_OKTMO_DATA);
        Settings.delDatabaseParameter(Settings.PROFILE_OKTMO_CODE);
        Settings.delDatabaseParameter(Settings.PROFILE_LATITUDE);
        Settings.delDatabaseParameter(Settings.PROFILE_LONGITUDE);
        Settings.delDatabaseParameter(Settings.PROFILE_HIDE_POPUP);
    }

    public static void RemoveAllData() {
        RemoveProfileData();

        Settings.delDatabaseParameter(Settings.USER_ACCESS_TOKEN);
        Settings.delDatabaseParameter(Settings.USER_REFRESH_TOKEN);
        Settings.delDatabaseParameter(Settings.USER_TOKEN_EXPIRES_IN);
        Settings.delDatabaseParameter(Settings.USER_TOKEN_START_TIME);
        Settings.delDatabaseParameter(Settings.PROVIDER_HASH_DATA);
        Settings.delDatabaseParameter(Settings.HASH1);
        Settings.delDatabaseParameter(Settings.HASH2);
    }

    public static String FullName() {
        String first = FirstName();
        String last = LastName();
        String middle = MiddleName();
        String result = "";

        if (!isEmpty(last)) result += last;
        if (!isEmpty(result) && !isEmpty(first)) result += ' ';
        if (!isEmpty(first)) result += first;
        if (!isEmpty(result) && !isEmpty(middle)) result += ' ';
        if (!isEmpty(middle)) result += middle;

        return result;
    }

    /***
     * Результат получения профиля с сервера. Метод должен положить все
     * имеющиеся в ответе сервера поля в локальную базу
     * @param data Map с результатом работы ServerTalk.getProfile()
     */
    public static void ParseServerResponse(JSONObject data) {
        RemoveProfileData();

        try {
            if (data.has("firstName")) setFirstName(data.getString("firstName", ""));
            if (data.has("lastName")) setLastName(data.getString("lastName", ""));
            if (data.has("middleName")) setMiddleName(data.getString("middleName", ""));

            if (data.has("gender")) {
                String gender = data.getString("gender", "");
                if (gender.equals("MALE")) setGender(MALE);
                if (gender.equals("FEMALE")) setGender(FEMALE);
            }

            if (data.has("birthDate") && !data.isNull("birthDate")) {
                Long longBirth = data.getLong("birthDate", null);
                setBirth(new Date(longBirth));
            }
        } catch (Exception e) {
            Logger.Exception(e);
        }

        try {
            if ((data.has("address") && (data.get("address") instanceof JSONObject))) {
                @SuppressWarnings("unchecked")
                JSONObject address = (JSONObject) data.get("address");
                if (address.has("oktmoCode")) setOKTMOCode(address.getString("oktmoCode", ""));
                if (address.has("oktmoData")) setOKTMOData(address.getString("oktmoData", ""));
                if (address.has("customAddress")) setAddress(address.getString("customAddress", ""));

                if (address.has("longitude") && (address.get("longitude") instanceof String)) {
                    String strCoord = address.getString("longitude", "");
                    @SuppressWarnings("UnusedAssignment")
                    Double longCoord = 10000000D; // Невозможное значение для координаты
                    try {
                        longCoord = Double.valueOf(strCoord);
                        if (longCoord != 10000000) {
                            setLongitude(longCoord);
                        }
                    } catch (NumberFormatException e) {
                        Logger.Exception(e);
                    }
                }

                if (address.has("latitude") && (address.get("latitude") instanceof String)) {
                    String strCoord = address.getString("latitude", "");
                    //noinspection UnusedAssignment
                    Double longCoord = 10000000D; // Невозможное значение для координаты
                    try {
                        longCoord = Double.valueOf(strCoord);
                        if (longCoord != 10000000) {
                            setLatitude(longCoord);
                        }
                    } catch (NumberFormatException e) {
                        Logger.Exception(e);
                    }
                }
            }
        } catch (Exception e) {
            Logger.Exception(e);
        }
    }

    public static OKTMO Settlement() {
        if (OKTMOCode() != null) {
            return OKTMO.Get(OKTMOCode());
        }

        return null;
    }
}
