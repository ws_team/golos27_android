package ru.infodev.golos27.Fragments;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Interfaces.FragmentChainInterface;
import ru.infodev.golos27.Common.Interfaces.FragmentResultInterface;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.MaterialMenuIcon;

public class ParamFragment extends Fragment {

    protected static final int ICON_CHECK = 0;
    protected static final int ICON_RIGHT = 1;
    protected static final int ICON_UNKNOWN = 2;
    protected static final int ICON_ADD_PROGRESS = 3;
    protected static final int ICON_DEL_PROGRESS = 4;
    protected static final int ICON_PLUS = 5;

    protected JSONObject parameters;
    public static final String BUNDLE_CONTENT = "bundle_parameters";
    public static final String NOT_CHAIN = "NotChain";

    protected View RootView;
    protected View SingleTitleView;
    protected View DualTitleView;
    protected boolean fragmentActive;
    private boolean fragmentAllowMenu = true;
    private boolean fragmentAllowScroll = true;
    private boolean fragmentIconCross = false;
    private boolean fragmentAuthNeeded = false;
    protected String fragment_title = "";
    protected String fragment_subtitle = "";
    protected int titleId;
    private MaterialMenuIcon actionIcon;
    private View progressView;
    protected MenuItem actionButton;

    protected FragmentResultInterface fragmentResultListener;
    protected FragmentChainInterface fragmentChainInterface;
    protected Class nextFragment;
    protected JSONObject nextParams;

    public int getTitleId() {
        return titleId;
    }

    public boolean isFragmentAllowMenu() {
        return fragmentAllowMenu;
    }

    public void setFragmentAllowMenu(boolean allow) {
        fragmentAllowMenu = allow;
    }

    public boolean isFragmentAllowScroll() {
        return fragmentAllowScroll;
    }

    public void setFragmentAllowScroll(boolean allow) {
        fragmentAllowScroll = allow;
    }

    public boolean isFragmentIconCross() {
        return fragmentIconCross;
    }

    public void setFragmentIconCross(boolean fragmentIconCross) {
        this.fragmentIconCross = fragmentIconCross;
    }

    public boolean isFragmentAuthNeeded() {
        return fragmentAuthNeeded;
    }

    public void setFragmentAuthNeeded(boolean fragmentAuthNeeded) {
        this.fragmentAuthNeeded = fragmentAuthNeeded;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (getArguments() != null && getArguments().containsKey(BUNDLE_CONTENT)) {
            try {
                String strParam = getArguments().getString(BUNDLE_CONTENT);
                parameters = new JSONObject(strParam);
            } catch (Exception e) {
                Logger.Exception(e);
            }
        } else {
            throw new IllegalArgumentException("Must be created through newInstance(...)");
        }
    }

    /***
     * Создает новый фрагмент и передает ему параметры через bundle
     *
     * @param child   название класса фрагмента
     * @param content параметры передаваемые фрагменту
     * @return новый экземпляр фрагмента
     */
    @SuppressWarnings("unused")
    public static Fragment newInstance(Class child, JSONObject content) {
        try {
            final Fragment fragment = (Fragment) child.newInstance();
            final Bundle arguments = new Bundle();
            if (content != null) {
                arguments.putString(BUNDLE_CONTENT, content.toString());
            } else {
                arguments.putString(BUNDLE_CONTENT, "{}");
            }
            fragment.setArguments(arguments);
            return fragment;
        } catch (Exception e) {
            Logger.Exception(e);
        }

        return new ParamFragment();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_action_button, menu);
        actionButton = menu.findItem(R.id.action_item);
        if (actionIcon != null) actionButton.setIcon(actionIcon);

        actionButton.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();
        fragmentActive = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        fragmentActive = false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        setTitle(titleId);
        progressView = inflater.inflate(R.layout.action_progress_bar, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setContentView(view);
    }

    /***
     * Вызывается потомком, для установки обработчика, убирающего фокус
     *
     * @param v главный вид потомка
     */
    public void setContentView(View v) {
        if (v != null) {
            View base = v.findViewById(R.id.base_layout);
            if (base != null) {
                base.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clearFocus();
                    }
                });
            }
        }
    }

    /***
     * Возвращает название фрагмента
     *
     * @return строка с названием
     */
    public String getTitle() {
        return fragment_title;
    }

    public void setTitle(String title, String subtitle) {
        fragment_title = title;
        fragment_subtitle = subtitle;
        setTitleDual(fragment_title, fragment_subtitle);
    }

    public void setTitle(int id, int subId) {
        fragment_title = getResources().getString(id);
        fragment_subtitle = getResources().getString(subId);
        setTitleDual(fragment_title, fragment_subtitle);
    }

    /***
     * Задает название фрамента
     *
     * @param title название фрагмента
     */
    public void setTitle(String title) {
        fragment_title = title;
        setTitleSingle(fragment_title);
    }

    public void setTitle(int id) {
        fragment_title = getResources().getString(id);
        setTitleSingle(fragment_title);
    }

    public void setTitleSingle(String fragment_title) {
        if (RootView != null && DualTitleView != null && SingleTitleView != null) {
            DualTitleView.setVisibility(View.INVISIBLE);
            SingleTitleView.setVisibility(View.VISIBLE);
            TextView toolbar_text = (TextView) RootView.findViewById(R.id.toolbar_text);
            if (toolbar_text != null) {
                toolbar_text.setText(fragment_title);
            } else {
                Logger.Warn("Warning: can't find toolbar at root window!!!");
            }
        }
    }

    public void setTitleDual(String fragment_title, String fragment_subtitle) {
        if (RootView != null && DualTitleView != null && SingleTitleView != null) {
            DualTitleView.setVisibility(View.VISIBLE);
            SingleTitleView.setVisibility(View.INVISIBLE);
            TextView toolbar_text_l1 = (TextView) RootView.findViewById(R.id.toolbar_text_l1);
            TextView toolbar_text_l2 = (TextView) RootView.findViewById(R.id.toolbar_text_l2);
            if (toolbar_text_l1 != null && toolbar_text_l2 != null) {
                toolbar_text_l1.setText(fragment_title);
                toolbar_text_l2.setText(fragment_subtitle);
            } else {
                Logger.Warn("Warning: can't find toolbar at root window!!!");
            }
        }
    }

    @SuppressWarnings("unused")
    protected MaterialMenuIcon getActionIcon() {
        return actionIcon;
    }

    protected void setActionButtonIconDrawable(int iconId, int title) {
        Drawable icon = ContextCompat.getDrawable(Application.AppContext, iconId);
        if (icon != null) {
            actionButton.setIcon(icon);
            actionButton.setTitle(title);
        }
    }

    protected void setActionButtonIcon(int type) {
        if (actionIcon != null) {
            actionButton.setIcon(actionIcon);
            switch (type) {
                case ICON_CHECK:
                    actionButton.setTitle(R.string.save_hint);
                    actionIcon.setIconState(MaterialMenuIcon.IconState.CHECK);
                    actionIcon.setRTLEnabled(false);
                    break;
                case ICON_PLUS:
                    actionButton.setTitle(R.string.create_appeal_hint);
                    actionIcon.setIconState(MaterialMenuIcon.IconState.PLUS);
                    actionIcon.setRTLEnabled(false);
                    break;
                case ICON_RIGHT:
                    actionButton.setTitle(R.string.next_hint);
                    actionIcon.setIconState(MaterialMenuIcon.IconState.ARROW);
                    actionIcon.setRTLEnabled(true);
                    break;
                case ICON_ADD_PROGRESS:
                    actionButton.setActionView(progressView);
                    break;
                case ICON_DEL_PROGRESS:
                    actionButton.setActionView(null);
                    break;
            }
        }
    }

    protected void setActionButtonVisibility(boolean visibility) {
        if (actionButton != null)
            actionButton.setVisible(visibility);
    }

    protected void setActionButtonEnabled(boolean enabled) {
        actionButton.setEnabled(enabled);
    }

    @SuppressWarnings("unused")
    public int getActionIconType() {
        if (actionIcon != null) {
            switch (actionIcon.getIconState()) {
                case ARROW:
                    return ICON_RIGHT;
                case CHECK:
                    return ICON_CHECK;
                default:
                    return ICON_UNKNOWN;
            }
        }

        return ICON_UNKNOWN;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity != null) {
            RootView = activity.getWindow().getDecorView();
            if (RootView != null) {
                SingleTitleView = RootView.findViewById(R.id.navBarData);
                DualTitleView = RootView.findViewById(R.id.navBarDataDual);
            }

            setTitle(fragment_title);
            actionIcon = new MaterialMenuIcon(getActivity(), Color.WHITE, Color.YELLOW, MaterialMenuIcon.Stroke.THIN);
            actionIcon.setIconState(MaterialMenuIcon.IconState.CHECK);
        }
        try {
            fragmentResultListener = (FragmentResultInterface) activity;
            fragmentChainInterface = (FragmentChainInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement fragmentResult/Chain interfaces");
        }
    }

    protected void chainStartNextFragment(int sender, Class nextFragment,
                                          JSONObject params, int popCount, boolean lockWindow, boolean authNeeded) {
        fragmentChainInterface.FragmentChain(sender, nextFragment, params, popCount, lockWindow, authNeeded);
    }

    protected void chainStartNextFragment(int sender, Class nextFragment,
                                          JSONObject params, int popCount, boolean lockWindow) {
        fragmentChainInterface.FragmentChain(sender, nextFragment, params, popCount, lockWindow, false);
    }

    protected void chainStartNextFragment(int sender, Class nextFragment, JSONObject params, boolean popMe) {
        chainStartNextFragment(sender, nextFragment, params, popMe ? 1 : 0, false);
    }

    protected void chainStartNextFragment(int sender, Class nextFragment, JSONObject params,
                                          boolean popMe, boolean authNeeded) {
        chainStartNextFragment(sender, nextFragment, params, popMe ? 1 : 0, false, authNeeded);
    }

    protected void chainPopFragments(int sender, int popCount, boolean lockWindow) {
        fragmentChainInterface.FragmentPop(sender, popCount, lockWindow);
    }

    public void clearFocus() {
        fragmentChainInterface.FragmentClearFocus();
    }

    /***
     * Приложение с помощью этого метода запрашивает у
     * фрагмента возможность его закрытия.
     *
     * @return true если фрагмент разрешает закрытие
     */
    public boolean ExitRequest() {
        return true;
    }

    public void setNextFragmentClass(Class nextFragment, JSONObject nextParams) {
        this.nextFragment = nextFragment;
        this.nextParams = nextParams;
    }
}
