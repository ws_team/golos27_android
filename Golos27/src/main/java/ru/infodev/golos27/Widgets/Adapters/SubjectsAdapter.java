package ru.infodev.golos27.Widgets.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Subject;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.R;

public class SubjectsAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Subject> mData;

    public SubjectsAdapter(Context context, List<Subject> items) {
        mData = (ArrayList<Subject>) items;
        mContext = context;
    }

    @Override
    public boolean isEnabled(int position) {
        return !(getItem(position).isGroup());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Subject getItem(int index) {
        return mData.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_subject, parent, false);
            if (convertView != null) {
                ((TextView) convertView.findViewById(R.id.subject_text))
                        .setTypeface(CustomFontLoader.getTypeface(Application.AppContext, 0));
            }
        }
        if (convertView != null) {
            Subject item = getItem(position);

            TextView entry = (TextView) convertView.findViewById(R.id.subject_text);
            TextView group = (TextView) convertView.findViewById(R.id.group_text);
            if (!item.isGroup()) {
                entry.setText(item.Name());
                group.setVisibility(View.GONE);
                entry.setVisibility(View.VISIBLE);
            } else {
                group.setText(item.Name());
                entry.setVisibility(View.GONE);
                group.setVisibility(View.VISIBLE);
            }
        }
        return convertView;
    }
}