package ru.infodev.golos27.Fragments.AppealList;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.shehabic.droppy.DroppyClickCallbackInterface;
import com.shehabic.droppy.DroppyMenuPopup;
import com.shehabic.droppy.animations.DroppyFadeInAnimation;
import com.vk.sdk.VKScope;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Address;
import ru.infodev.golos27.Common.Catalogs.Appeal.Appeal;
import ru.infodev.golos27.Common.Catalogs.Appeal.AppealAttachment;
import ru.infodev.golos27.Common.Catalogs.Appeal.AppealListSingleton;
import ru.infodev.golos27.Common.Catalogs.NotificationListSingleton;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Interfaces.RecycleViewClickInterface;
import ru.infodev.golos27.Common.SocialNetwork.SocialNetwork;
import ru.infodev.golos27.Common.SocialNetwork.SocialNetworkManager;
import ru.infodev.golos27.Common.SocialNetwork.facebook.FacebookSocialNetwork;
import ru.infodev.golos27.Common.SocialNetwork.listener.OnLoginCompleteListener;
import ru.infodev.golos27.Common.SocialNetwork.listener.OnPostingCompleteListener;
import ru.infodev.golos27.Common.SocialNetwork.vkontakte.VkSocialNetwork;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.MainActivity;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.Widgets.Adapters.DocListAdapter;
import ru.infodev.golos27.Widgets.Adapters.ImageListAdapter;
import ru.infodev.golos27.Widgets.GridSpacingItemDecoration;
import ru.infodev.golos27.Widgets.HelpPopup;
import ru.infodev.golos27.Widgets.ResponseRatingBlock;
import ru.infodev.golos27.Widgets.SwipeToLoad.OnRefreshListener;
import ru.infodev.golos27.Widgets.SwipeToLoad.SwipeToLoadLayout;

import static android.support.v4.content.ContextCompat.getColor;
import static android.support.v4.content.ContextCompat.getDrawable;
import static ru.infodev.golos27.Common.Utils.Utils.getNumEnding;
import static ru.infodev.golos27.Common.Utils.Utils.hasImage;
import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

public class AppealCardFragment extends ParamFragment implements View.OnClickListener, OnRefreshListener,
        SocialNetworkManager.OnInitializationCompleteListener, OnLoginCompleteListener {
    public static final int FRAGMENT_ID = R.layout.appeal_card;
    public static final int MAX_MESSAGE_LINES = 3;
    public int grey_color;
    public int blue_color;
    public Drawable grey_clock;
    public Drawable blue_clock;
    public Drawable grey_hide;
    public Drawable blue_hide;
    public Drawable blue_rdr;
    public Drawable grey_rdr;
    public Drawable blue_resp;
    public Drawable grey_resp;
    private SwitchCompat hideAppealSwitch;
    private SwitchCompat hideResponseSwitch;
    private int appeal_message_lines = Integer.MAX_VALUE;
    private Integer reply_line_count = 0;
    private View fragmentView;

    private TextView reply_message_text;
    private LinearLayout reply_documents_section;
    private LinearLayout reply_images_section;
    private TextView reply_hide_all_img;
    private TextView reply_hide_all_doc;
    private TextView reply_show_all;
    private View appeal_reply_gradient;
    private int replyDocsCount;
    private int replyImageCount;
    private boolean reply_collapsed;
    private HelpPopup popupWindow;
    private Appeal appeal;
    private TextView subscribed_text;
    private TextView liked_text;
    private ImageView answer_hidden_icon;
    private FrameLayout ratedLayout;
    private ImageView ratedIcon;
    private TextView ratedText;
    private LinearLayout hideAppealSwitchSection;
    private LinearLayout hideResponseSwitchSection;
    private View menuAnchorView;
    private int windowWidth;
    private SwipeToLoadLayout swipeToLoadLayout;
    private boolean isLoading = false;
    private String coverId = "";
    private View emptyHint;
    private View cardContent;
    private DroppyMenuPopup menu;
    private SocialNetworkManager mSocialNetworkManager;
    private ImageView coverImage;
    private List<SocialNetwork> mSocialNetworkPostQueue = new ArrayList<>();

    public AppealCardFragment() {
        titleId = R.string.empty_string;
        setFragmentAllowMenu(false);
        setFragmentAllowScroll(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        Logger.Log("AppealCard fragment paused. Closing popups");
        if (popupWindow != null) popupWindow.dismiss();
        if (menu != null) menu.dismiss(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.Log("Resume called in AppealCardFragment");
        if (mSocialNetworkPostQueue != null && mSocialNetworkPostQueue.size() > 0) {
            postSocialNetwork(mSocialNetworkPostQueue.get(0));
            mSocialNetworkPostQueue.remove(0);
        }

        // При возврате из другого фрагмента картинка из вида пропадает,
        // сбрасываем поле чтобы она смогла загрузиться повторно
        coverImage = (ImageView) fragmentView.findViewById(R.id.cover_image);
        if (isNotEmpty(coverId) && !hasImage(coverImage)) coverId = "";
    }

    protected void fillCardView() {
        if (appeal == null || appeal.AppealClaim() == null || !fragmentActive) return;
        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        menuAnchorView = fragmentView.findViewById(R.id.menu_anchor);

        // Заголовок фрагмента
        setTitle(getString(R.string.appeal_number).replace("null", String.valueOf(appeal.Number())));

        // Заранее загрузим иконки
        grey_color = getColor(Application.AppContext, R.color.colorGrayText);
        blue_color = getColor(Application.AppContext, R.color.colorPrimary);
        grey_clock = getDrawable(Application.AppContext, R.drawable.grey_time_plus);
        blue_clock = getDrawable(Application.AppContext, R.drawable.blue_time_plus);
        grey_hide = getDrawable(Application.AppContext, R.drawable.grey_crossed_eye);
        blue_hide = getDrawable(Application.AppContext, R.drawable.blue_crossed_eye);
        grey_rdr = getDrawable(Application.AppContext, R.drawable.grey_direction);
        blue_rdr = getDrawable(Application.AppContext, R.drawable.blue_direction);
        grey_resp = getDrawable(Application.AppContext, R.drawable.grey_list);
        blue_resp = getDrawable(Application.AppContext, R.drawable.blue_list);

        // ------[ Обращение ]-----------------------------------------------------------------------------------------
        // Обложка обращения. Соотношение сторон 16:9, фоновая загрузка изображения
        int width = windowWidth;
        int height = (int) ((float) width / 1.77f);
        coverImage = (ImageView) fragmentView.findViewById(R.id.cover_image);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) coverImage.getLayoutParams();
        params.width = width;
        params.height = height;

        if (appeal.AppealClaim() == null || isEmpty(appeal.AppealClaim().DocumentCoverId()) ||
                !coverId.equals(appeal.AppealClaim().DocumentCoverId())) {
            coverImage.setLayoutParams(params);
            coverImage.setImageDrawable(null);
            coverId = "";
            if (appeal.AppealClaim() != null && isNotEmpty(appeal.AppealClaim().DocumentCoverId())) {
                try {
                    String imageUri = ServerTalk.getDocumentUri(appeal.AppealClaim().DocumentCoverId());
                    ImageLoader.getInstance().displayImage(imageUri, coverImage);
                    coverId = appeal.AppealClaim().DocumentCoverId();
                } catch (Exception e) {
                    Logger.Exception(e);
                }
            }
        }

        // Лайки
        liked_text = (TextView) fragmentView.findViewById(R.id.liked_text);
        if (!appeal.isAllowAction(Appeal.ACTION_LIKE) && !appeal.isAllowAction(Appeal.ACTION_DISLIKE))
            liked_text.setEnabled(false);
        else
            liked_text.setEnabled(true);
        liked_text.setOnClickListener(this);
        setupLikedText();

        // Подписки
        subscribed_text = (TextView) fragmentView.findViewById(R.id.subscribed_text);
        if (!appeal.isAllowAction(Appeal.ACTION_SUBSCRIBE) && !appeal.isAllowAction(Appeal.ACTION_UNSUBSCRIBE))
            subscribed_text.setEnabled(false);
        else
            subscribed_text.setEnabled(true);

        subscribed_text.setOnClickListener(this);
        setupSubscribedText();

        // Иконки
        ImageView appeal_answer = (ImageView) fragmentView.findViewById(R.id.appeal_answer);
        ImageView time_expanded = (ImageView) fragmentView.findViewById(R.id.time_expanded);
        answer_hidden_icon = (ImageView) fragmentView.findViewById(R.id.answer_hidden_icon);
        ImageView forwarded_icon = (ImageView) fragmentView.findViewById(R.id.forwarded_icon);
        time_expanded.setImageDrawable(appeal.IsAppealProlonged() ? blue_clock : grey_clock);
        answer_hidden_icon.setImageDrawable(appeal.IsPublishResponse() ? grey_hide : blue_hide);
        appeal_answer.setImageDrawable(appeal.IsHasResponse() ? blue_resp : grey_resp);
        forwarded_icon.setImageDrawable(appeal.IsForwarded() ? blue_rdr : grey_rdr);

        // Оценка ответа
        ratedLayout = (FrameLayout) fragmentView.findViewById(R.id.rated_icon_layout);
        ratedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null) popupWindow.dismiss();
                popupWindow = new HelpPopup(Application.AppContext, getString(R.string.appeal_rated));
                popupWindow.show(ratedLayout);
            }
        });
        ratedIcon = (ImageView) fragmentView.findViewById(R.id.rated_icon);
        ratedText = (TextView) fragmentView.findViewById(R.id.rated_text);
        if(appeal.ResponseRating() != null) {
            ratedIcon.setImageResource(R.drawable.greystar_active);
        } else {
            ratedIcon.setImageResource(R.drawable.greystar);
        }
        String ratingText;
        if(appeal.ResponseRating() == null) {
            ratingText = "";
        } else {
            ratingText = String.valueOf(appeal.ResponseRating());
        }
        ratedText.setText(ratingText);

        appeal_answer = (ImageView) fragmentView.findViewById(R.id.appeal_answer);
        final ImageView finalAppeal_answer = appeal_answer;
        appeal_answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null) popupWindow.dismiss();
                popupWindow = new HelpPopup(Application.AppContext, getString(R.string.has_reply));
                popupWindow.show(finalAppeal_answer);
            }
        });

        time_expanded = (ImageView) fragmentView.findViewById(R.id.time_expanded);
        final ImageView finalTime_expanded = time_expanded;
        time_expanded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null) popupWindow.dismiss();
                popupWindow = new HelpPopup(Application.AppContext, getString(R.string.time_expanded));
                popupWindow.show(finalTime_expanded);
            }
        });

        answer_hidden_icon = (ImageView) fragmentView.findViewById(R.id.answer_hidden_icon);
        final ImageView finalAnswer_hidden_icon = answer_hidden_icon;
        answer_hidden_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null) popupWindow.dismiss();
                popupWindow = new HelpPopup(Application.AppContext, getString(R.string.hidden_reply));
                popupWindow.show(finalAnswer_hidden_icon);
            }
        });

        forwarded_icon = (ImageView) fragmentView.findViewById(R.id.forwarded_icon);
        final ImageView finalForwarded_icon = forwarded_icon;
        forwarded_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null) popupWindow.dismiss();
                popupWindow = new HelpPopup(Application.AppContext, getString(R.string.appeal_redirected));
                popupWindow.show(finalForwarded_icon);
            }
        });

        // Дата
        TextView date_add = (TextView) fragmentView.findViewById(R.id.date_add);
        Calendar added = Calendar.getInstance();
        added.setTimeInMillis(appeal.CreatingDate());
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy в hh:mm", Locale.getDefault());
        date_add.setText(formatter.format(added.getTime()));
        date_add.setTypeface(roboto_light);

        // Основное наполнение
        final TextView message_text = (TextView) fragmentView.findViewById(R.id.message_text);
        message_text.setTypeface(CustomFontLoader.getTypeface(Application.AppContext, 0));
        TextView subject_text = (TextView) fragmentView.findViewById(R.id.subject_text);
        TextView author_text = (TextView) fragmentView.findViewById(R.id.author_fio);
        subject_text.setText(appeal.Subject().Name());
        String authorFio = appeal.AppealClaim().AuthorFIO();
        if (isNotEmpty(authorFio)) author_text.setText(authorFio);

        // Адрес проблемы
        TextView address_text = (TextView) fragmentView.findViewById(R.id.full_card_address);
        String address = getString(R.string.address_not_set);
        int address_color = grey_color;
        if ((appeal.AppealClaim() != null) && (appeal.AppealClaim().Address() != null)) {
            String appeal_address = appeal.AppealClaim().Address().CustomAddress();
            if (isNotEmpty(appeal_address)) {
                address = appeal_address;
                address_color = blue_color;
            }
        }

        address_text.setText(address);
        address_text.setTextColor(address_color);
        address_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject params = new JSONObject();
                if (appeal == null || appeal.AppealClaim() == null || appeal.AppealClaim().Address() == null ||
                        appeal.AppealClaim().Address().Latitude() == Address.INVALID_COORDINATE ||
                        appeal.AppealClaim().Address().Longitude() == Address.INVALID_COORDINATE) return;

                String address = appeal.AppealClaim().Address().FullAddress();
                if (isEmpty(address)) return;
                params.putSafe("Address", address);
                params.putSafe("Latitude", appeal.AppealClaim().Address().Latitude());
                params.putSafe("Longitude", appeal.AppealClaim().Address().Longitude());
                chainStartNextFragment(FRAGMENT_ID, AppealMapFragment.class, params, false);
            }
        });

        // Текст статуса обращения
        TextView statusText = (TextView) fragmentView.findViewById(R.id.appeal_status_text);
        statusText.setText(appeal.getStatusText());
        statusText.setTextColor(getColor(getContext(), appeal.getStatusColorId()));

        // Логика показа-сокрытия текстового описания обращения
        message_text.setText(appeal.AppealClaim().Message());
        final TextView show_all_label = (TextView) fragmentView.findViewById(R.id.show_all);
        final View collapse_gradient = fragmentView.findViewById(R.id.appeal_message_gradient);
        message_text.post(new Runnable() {
            @Override
            public void run() {
                int lineCount = message_text.getLineCount();
                if (lineCount > MAX_MESSAGE_LINES) {
                    // По умолчанию свернем текст до 3 строк
                    message_text.setMaxLines(MAX_MESSAGE_LINES);
                    appeal_message_lines = MAX_MESSAGE_LINES;
                    show_all_label.setText(R.string.show_all);
                    collapse_gradient.setVisibility(View.VISIBLE);

                    show_all_label.setVisibility(View.VISIBLE);
                    show_all_label.setClickable(true);
                    show_all_label.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (appeal_message_lines != MAX_MESSAGE_LINES) {
                                message_text.setMaxLines(MAX_MESSAGE_LINES);
                                appeal_message_lines = MAX_MESSAGE_LINES;
                                show_all_label.setText(R.string.show_all);
                                collapse_gradient.setVisibility(View.VISIBLE);
                            } else {
                                message_text.setMaxLines(Integer.MAX_VALUE);
                                appeal_message_lines = Integer.MAX_VALUE;
                                show_all_label.setText(R.string.collapse);
                                collapse_gradient.setVisibility(View.INVISIBLE);
                            }
                        }
                    });
                } else {
                    show_all_label.setVisibility(View.GONE);
                    collapse_gradient.setVisibility(View.INVISIBLE);
                }
            }
        });

        // Блок документов карточки обращения
        final LinearLayout documents_section = (LinearLayout) fragmentView.findViewById(R.id.documents_section);
        final TextView documents_text = (TextView) fragmentView.findViewById(R.id.documents_count_text);
        final LinearLayout images_section = (LinearLayout) fragmentView.findViewById(R.id.images_section);
        final RecyclerView images_grid = (RecyclerView) fragmentView.findViewById(R.id.photo_grid);
        final RecyclerView docsList = (RecyclerView) fragmentView.findViewById(R.id.documents_list);
        final TextView images_text = (TextView) fragmentView.findViewById(R.id.photo_count_text);

        int documentsCount = appeal.AppealClaim().getDocumentsCount();
        if (documentsCount > 0) {
            String[] imageCountText = getResources().getStringArray(R.array.documents_array);
            String docText = String.valueOf(documentsCount) + " " + getNumEnding(documentsCount, imageCountText);
            documents_text.setText(docText);

            String[] units = getResources().getStringArray(R.array.size_units);
            List<AppealAttachment> docList = appeal.AppealClaim().getDocsList();
            final DocListAdapter docListAdapter = new DocListAdapter(getContext(), docList, units);
            LinearLayoutManager docListManager = new LinearLayoutManager(getContext(),
                    LinearLayoutManager.VERTICAL, false);
            docListManager.setAutoMeasureEnabled(true);
            docsList.setAdapter(docListAdapter);
            docListAdapter.setOnClickListener(new RecycleViewClickInterface() {
                @Override
                public void onItemClick(int position) {
                    AppealAttachment attachment = docListAdapter.getItem(position);
                    downloadAttachmentAttempt(attachment);
                }

                @Override
                public void onItemClick2(int position) {
                }
            });
            docsList.setLayoutManager(docListManager);
            docsList.setNestedScrollingEnabled(false);
            documents_section.setVisibility(View.VISIBLE);
        } else {
            documents_section.setVisibility(View.GONE);
        }

        // Блок фотографий карточки обращения
        int imagesCount = appeal.AppealClaim().getImagesCount();
        if (imagesCount > 0) {
            List<AppealAttachment> imagesList = appeal.AppealClaim().getImagesList();
            final ImageListAdapter imageListAdapter = new ImageListAdapter(getContext(), imagesList);
            GridLayoutManager imageGridManager = new GridLayoutManager(getContext(), 3);
            images_grid.setAdapter(imageListAdapter);
            imageListAdapter.setOnClickListener(new RecycleViewClickInterface() {
                @Override
                public void onItemClick(int position) {
                    try {
                        AppealAttachment attachment = imageListAdapter.getItem(position);
                        if (attachment != null && isNotEmpty(attachment.Id())) {
                            String documentUri = ServerTalk.getDocumentUri(attachment.Id());
                            if (isNotEmpty(documentUri)) {
                                final Intent viewerIntent = new Intent(Intent.ACTION_VIEW);
                                viewerIntent.setDataAndType(Uri.parse(documentUri), "image/*");
                                startActivity(viewerIntent);
                            }
                        }
                    } catch (Exception e) {
                        Logger.Exception(e);
                    }
                }

                @Override
                public void onItemClick2(int position) {
                }
            });

            imageGridManager.setAutoMeasureEnabled(true);
            images_grid.setLayoutManager(imageGridManager);
            images_grid.setNestedScrollingEnabled(false);
            images_text.setText(getString(R.string.photo).replace("null", String.valueOf(imagesCount)));
            images_section.setVisibility(View.VISIBLE);
        } else {
            images_section.setVisibility(View.GONE);
        }

        // Блок "исполнитель"
        TextView executor_text = (TextView) fragmentView.findViewById(R.id.executor_name);
        executor_text.setTypeface(roboto_light);
        String executorName = getString(R.string.not_set);
        if (appeal.Executor() != null) executorName = appeal.Executor().Name();
        if (isNotEmpty(executorName)) executor_text.setText(executorName);

        // ------[ Ответ на обращение ]--------------------------------------------------------------------------------
        LinearLayout responseSection = (LinearLayout) fragmentView.findViewById(R.id.reply_section);
        if (appeal.Response() != null) {
            // Дата ответа
            TextView date_reply = (TextView) fragmentView.findViewById(R.id.response_date_text);
            Calendar replied = Calendar.getInstance();
            replied.setTimeInMillis(appeal.Response().Date());
            date_reply.setText(formatter.format(replied.getTime()));
            date_reply.setTypeface(roboto_light);

            // Автор ответа
            TextView response_author_name = (TextView) fragmentView.findViewById(R.id.response_author_name);
            String authorText = appeal.Response().Author().trim();
            String authorPosition = appeal.Response().Post();
            if (isNotEmpty(authorPosition)) authorText += " (" + authorPosition.trim() + ")";
            response_author_name.setText(authorText);

            // Текст ответа
            reply_message_text = (TextView) fragmentView.findViewById(R.id.response_message_text);
            reply_message_text.setText(appeal.Response().Text());
            reply_message_text.setTypeface(roboto_light);

            // Блок документов ответа на обращение
            reply_documents_section = (LinearLayout) fragmentView.findViewById(R.id.reply_docs_section);
            reply_images_section = (LinearLayout) fragmentView.findViewById(R.id.reply_images_section);
            reply_hide_all_img = (TextView) fragmentView.findViewById(R.id.reply_hide_all_img);
            reply_hide_all_doc = (TextView) fragmentView.findViewById(R.id.reply_hide_all_doc);
            reply_show_all = (TextView) fragmentView.findViewById(R.id.reply_show_all);
            appeal_reply_gradient = fragmentView.findViewById(R.id.appeal_reply_gradient);
            replyDocsCount = appeal.Response().getDocumentsCount();
            replyImageCount = appeal.Response().getImagesCount();

            reply_show_all.setOnClickListener(this);
            reply_hide_all_img.setOnClickListener(this);
            reply_hide_all_doc.setOnClickListener(this);

            // Блок фотографий ответа на обращение
            final RecyclerView reply_images_grid = (RecyclerView) fragmentView.findViewById(R.id.reply_photo_grid);
            final TextView reply_images_text = (TextView) fragmentView.findViewById(R.id.reply_photo_count_text);
            if (replyImageCount > 0) {
                List<AppealAttachment> imagesList = appeal.Response().getImagesList();
                final ImageListAdapter imageListAdapter = new ImageListAdapter(getContext(), imagesList);
                GridLayoutManager imageGridManager = new GridLayoutManager(getContext(), 3);
                reply_images_grid.setAdapter(imageListAdapter);
                imageListAdapter.setOnClickListener(new RecycleViewClickInterface() {
                    @Override
                    public void onItemClick(int position) {
                        try {
                            AppealAttachment attachment = imageListAdapter.getItem(position);
                            if (attachment != null && isNotEmpty(attachment.Id())) {

                                String documentUri = ServerTalk.getDocumentUri(attachment.Id());
                                if (isNotEmpty(documentUri)) {
                                    final Intent viewerIntent = new Intent(Intent.ACTION_VIEW);
                                    viewerIntent.setDataAndType(Uri.parse(documentUri), "image/*");
                                    startActivity(viewerIntent);
                                }
                            }
                        } catch (Exception e) {
                            Logger.Exception(e);
                        }
                    }

                    @Override
                    public void onItemClick2(int position) {
                    }
                });
                reply_images_grid.setLayoutManager(imageGridManager);
                reply_images_grid.setNestedScrollingEnabled(false);
                reply_images_text.setText(getString(R.string.photo).replace("null", String.valueOf(replyImageCount)));
            }

            // Блок документов ответа на обращение
            if (replyDocsCount > 0) {
                final TextView replyDocsCountText = (TextView) fragmentView.findViewById(R.id.reply_docs_count_text);
                final RecyclerView replyDocsList = (RecyclerView) fragmentView.findViewById(R.id.reply_documents_list);
                String[] imageCountText = getResources().getStringArray(R.array.documents_array);
                String docText = String.valueOf(replyDocsCount) + " " + getNumEnding(replyDocsCount, imageCountText);
                replyDocsCountText.setText(docText);

                String[] units = getResources().getStringArray(R.array.size_units);
                List<AppealAttachment> docList = appeal.Response().getDocsList();
                final DocListAdapter docListAdapter = new DocListAdapter(getContext(), docList, units);
                docListAdapter.setOnClickListener(new RecycleViewClickInterface() {
                    @Override
                    public void onItemClick(int position) {
                        AppealAttachment attachment = docListAdapter.getItem(position);
                        downloadAttachmentAttempt(attachment);
                    }

                    @Override
                    public void onItemClick2(int position) {
                    }
                });

                LinearLayoutManager docListManager = new LinearLayoutManager(getContext(),
                        LinearLayoutManager.VERTICAL, false);
                replyDocsList.setAdapter(docListAdapter);
                replyDocsList.setLayoutManager(docListManager);
                replyDocsList.setNestedScrollingEnabled(false);
            }

            // Свернем ответ для первоначального показа
            reply_message_text.post(new Runnable() {
                @Override
                public void run() {
                    reply_line_count = reply_message_text.getLineCount();
                    if (reply_line_count <= MAX_MESSAGE_LINES &&
                            reply_hide_all_img.getVisibility() == View.GONE &&
                            reply_hide_all_doc.getVisibility() == View.GONE) {
                        // Ничего прятать не надо. Убираем все "свернуть/развернуть"
                        reply_show_all.setVisibility(View.GONE);
                    } else {
                        // Что-то прятать все-таки надо
                        collapse_reply();
                    }
                }
            });

            responseSection.setVisibility(View.VISIBLE);
        } else {
            responseSection.setVisibility(View.GONE);
        }

        // оценка ответа на обращение
        ResponseRatingBlock responseRatingBlock = (ResponseRatingBlock) fragmentView.findViewById(R.id.response_rating_block);
        responseRatingBlock.setState(appeal.Id(), appeal.ResponseRating(), appeal.isAllowAction(Appeal.ACTION_RATE_RESPONSE));

        // ------[ Управление видимостью ]-----------------------------------------------------------------------------
        // Управление отображением обращения на сайте (переключатель)
        hideAppealSwitchSection = (LinearLayout) fragmentView.findViewById(R.id.portal_hide_block);
        hideAppealSwitch = (SwitchCompat) fragmentView.findViewById(R.id.portal_hide_switch);
        hideAppealSwitchSection.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (v.isClickable()) hideAppealSwitch.toggle();
                    }
                }

        );
        hideAppealSwitch.setChecked(!appeal.IsPublishAppeal());
        hideAppealSwitch.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            if (appeal.isAllowAction(Appeal.ACTION_HIDE))
                                new ChangeAppealVisibility(appeal, ServerTalk.ACTION_HIDE).execute();
                        } else {
                            Toast.makeText(getContext(), R.string.unhide_warning, Toast.LENGTH_LONG).show();
                            if (appeal.isAllowAction(Appeal.ACTION_UNHIDE))
                                new ChangeAppealVisibility(appeal, ServerTalk.ACTION_UNHIDE).execute();
                        }
                    }
                }

        );

        if ((appeal.isAllowAction(Appeal.ACTION_HIDE) && appeal.IsPublishAppeal()) ||
                (appeal.isAllowAction(Appeal.ACTION_UNHIDE) && !appeal.IsPublishAppeal())) {
            hideAppealSwitchSection.setVisibility(View.VISIBLE);
        } else {
            hideAppealSwitchSection.setVisibility(View.GONE);
        }

        // Управление отображением ответа на обращение на сайте (переключатель)
        hideResponseSwitchSection = (LinearLayout) fragmentView.findViewById(R.id.reply_hide_block);
        hideResponseSwitch = (SwitchCompat) fragmentView.findViewById(R.id.hide_response_switch);
        hideResponseSwitchSection.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (v.isClickable()) hideResponseSwitch.toggle();
                    }
                }

        );

        hideResponseSwitch.setChecked(!appeal.IsPublishResponse());
        hideResponseSwitch.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            if (appeal.isAllowAction(Appeal.ACTION_HIDE_ANSWER))
                                new ChangeAppealVisibility(appeal, ServerTalk.ACTION_HIDE_ANSWER).execute();
                        } else {
                            Toast.makeText(getContext(), R.string.unhide_response_warning, Toast.LENGTH_LONG).show();
                            if (appeal.isAllowAction(Appeal.ACTION_UNHIDE_ANSWER))
                                new ChangeAppealVisibility(appeal, ServerTalk.ACTION_UNHIDE_ANSWER).execute();
                        }
                    }
                }
        );

        if ((appeal.isAllowAction(Appeal.ACTION_HIDE_ANSWER) && appeal.IsPublishResponse()) ||
                (appeal.isAllowAction(Appeal.ACTION_UNHIDE_ANSWER) && !appeal.IsPublishResponse())) {
            hideResponseSwitchSection.setVisibility(View.VISIBLE);
        } else {
            hideResponseSwitchSection.setVisibility(View.GONE);
        }

        if (fragmentActive) {
            final NestedScrollView scrollView = (NestedScrollView) fragmentView.findViewById(R.id.swiped_list);
            if (scrollView != null) fragmentView.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.scrollTo(0, 0);
                }
            });
        }

        emptyHint.setVisibility(View.INVISIBLE);
        cardContent.setVisibility(View.VISIBLE);
    }

    private void downloadAttachmentAttempt(AppealAttachment attachment) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    5346);

        } else {
            downloadAttachment(attachment);
        }
    }

    private void downloadAttachment(AppealAttachment attachment) {
        try {
            if (attachment != null && isNotEmpty(attachment.Id())) {
                String documentUri = ServerTalk.getDocumentUri(attachment.Id());
                if (isNotEmpty(documentUri)) {
                    DownloadManager.Request downloadReq = new DownloadManager.Request(Uri.parse(documentUri));
                    downloadReq.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, attachment.Name());
                    downloadReq.allowScanningByMediaScanner();
                    downloadReq.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    DownloadManager dm = (DownloadManager) Application.AppContext.getSystemService(Context.DOWNLOAD_SERVICE);

                    dm.enqueue(downloadReq);
                    Toast.makeText(getActivity(), R.string.download_attachment_starts, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Logger.Exception(e);
            Toast.makeText(getActivity(), R.string.download_attachment_failure, Toast.LENGTH_LONG).show();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        windowWidth = container.getMeasuredWidth();
        fragmentView = v;

        final RecyclerView images_grid = (RecyclerView) fragmentView.findViewById(R.id.photo_grid);
        final RecyclerView reply_images_grid = (RecyclerView) fragmentView.findViewById(R.id.reply_photo_grid);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.appeal_card_grid_spacing);
        images_grid.addItemDecoration(new GridSpacingItemDecoration(3, spacingInPixels, false));
        reply_images_grid.addItemDecoration(new GridSpacingItemDecoration(3, spacingInPixels, false));

        cardContent = v.findViewById(R.id.card_content);
        emptyHint = v.findViewById(R.id.empty_content_hint);
        emptyHint.setVisibility(View.VISIBLE);
        cardContent.setVisibility(View.INVISIBLE);

        swipeToLoadLayout = (SwipeToLoadLayout) v.findViewById(R.id.swipeToLoadLayout);
        swipeToLoadLayout = (SwipeToLoadLayout) v.findViewById(R.id.swipeToLoadLayout);
        swipeToLoadLayout.setOnRefreshListener(this);

        if (parameters.has("NotificationID") && (parameters.has("NotificationObjectID"))) {
            // Карточка открыта из списка уведомлений, надо сначала ее загрузить
            appeal = new Appeal();
            appeal.setId(parameters.getString("NotificationObjectID", ""));
            appeal.setAppealClaim(null);
            swipeToLoadLayout.post(new Runnable() {
                @Override
                public void run() {
                    setOrCheckLoading(true);
                    swipeToLoadLayout.setRefreshing(true);
                }
            });

            new GetAppealFromServer(appeal.Id()).execute();
        } else {
            // Карточка открыта из ленты, загруженное обращение уже есть, но чтобы
            // не вводить пользователя в заблуждение - покажем индикатор загрузки и загрузим обращение
            appeal = Appeal.fromJson(parameters);

            if (appeal != null) {
                appeal.setAppealClaim(null);
                swipeToLoadLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        setOrCheckLoading(true);
                        swipeToLoadLayout.setRefreshing(true);
                    }
                });
                new GetAppealFromServer(appeal.Id()).execute();
            }
            //showOrHideActionButton();
        }

        fragmentActive = true;
        fillCardView();
        return v;
    }

    synchronized public boolean setOrCheckLoading(Boolean loading) {
        if (loading != null) this.isLoading = loading;
        return this.isLoading;
    }

    // Загружаем заново первую страницу
    @Override
    public void onRefresh() {
        if (setOrCheckLoading(null)) {
            swipeToLoadLayout.setRefreshing(false);
        } else {
            new GetAppealFromServer(appeal.Id()).execute();
        }
    }

    private void setupSubscribedText() {
        int si = appeal.isSubscribed() ? R.drawable.blue_pin : R.drawable.grey_pin;
        subscribed_text.setCompoundDrawablesWithIntrinsicBounds(si, 0, 0, 0);
        subscribed_text.setText(String.valueOf(appeal.SubscribersCount()));
    }

    private void setupLikedText() {
        int li = appeal.isLiked() ? R.drawable.green_man : R.drawable.grey_man;
        liked_text.setCompoundDrawablesWithIntrinsicBounds(li, 0, 0, 0);
        liked_text.setText(String.valueOf(appeal.LikesCount()));
    }

    /***
     * Сворачивает ответ на обращение
     */
    private void collapse_reply() {
        reply_show_all.setVisibility(View.VISIBLE);
        reply_show_all.setText(R.string.show_all);
        reply_documents_section.setVisibility(View.GONE);
        reply_images_section.setVisibility(View.GONE);

        if (reply_line_count > MAX_MESSAGE_LINES) {
            reply_message_text.setMaxLines(MAX_MESSAGE_LINES);
            appeal_reply_gradient.setVisibility(View.VISIBLE);
        } else {
            appeal_reply_gradient.setVisibility(View.GONE);
            if (replyDocsCount <= 0 && replyImageCount <= 0) {
                reply_show_all.setVisibility(View.GONE);
            }
        }
        reply_collapsed = true;
        Logger.Log("Collapse reply finished");
    }

    /***
     * Разворачивает ответ на обращение
     */
    private void expand_reply() {
        // Развернем ответ
        reply_message_text.setMaxLines(Integer.MAX_VALUE);
        appeal_reply_gradient.setVisibility(View.INVISIBLE);
        reply_show_all.setVisibility(View.VISIBLE);
        reply_show_all.setText(R.string.collapse);

        // Блок фотографий ответа на обращение
        if (replyImageCount > 0) {
            reply_hide_all_img.setVisibility(View.VISIBLE);
            reply_hide_all_img.setText(R.string.collapse);
            reply_show_all.setVisibility(View.GONE);
            reply_images_section.setVisibility(View.VISIBLE);
        } else {
            reply_hide_all_img.setVisibility(View.GONE);
            reply_images_section.setVisibility(View.GONE);
        }

        // Блок документов ответа на обращение
        if (replyDocsCount > 0) {
            reply_hide_all_doc.setVisibility(View.VISIBLE);
            reply_hide_all_doc.setText(R.string.collapse);
            reply_hide_all_img.setVisibility(View.GONE);
            reply_show_all.setVisibility(View.GONE);
            reply_documents_section.setVisibility(View.VISIBLE);
        } else {
            reply_hide_all_doc.setVisibility(View.GONE);
            reply_documents_section.setVisibility(View.GONE);
        }

        reply_collapsed = false;
        Logger.Log("Expand reply finished");
    }

    /***
     * Создание и настройка меню actionbar
     */
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        setNormalActionButton();
        showOrHideActionButton();
    }

    /***
     * Прячет или показывает кнопку на тулбаре в зависимости от статуса обращения
     * если обращение не загружено или на модерации кнопка не показывается
     */
    public void showOrHideActionButton() {
        setActionButtonVisibility(!(
                (appeal == null) ||
                        (appeal.Status() == null) ||
                        (appeal.Status().equals(Appeal.STATUS_MODERATION))));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_item) {
            View anchor = null;
            try {
                Window window = getActivity().getWindow();
                View v = window.getDecorView();
                anchor = v.findViewById(R.id.action_item);
            } catch (Exception ignored) {
            }

            if (anchor == null) anchor = menuAnchorView;
            DroppyMenuPopup.Builder menuBuilder = new DroppyMenuPopup.Builder(getContext(), anchor);
            menu = menuBuilder.fromMenu(R.menu.social)
                    .triggerOnAnchorClick(false)
                    .setPopupAnimation(new DroppyFadeInAnimation())
                    .setOnClick(new DroppyClickCallbackInterface() {
                        @Override
                        public void call(View v, int id) {
                            switch (id) {
                                case R.id.vk_menu:
                                    menu.dismiss(true);
                                    postVKSocial();
                                    break;
                                case R.id.fb_menu:
                                    menu.dismiss(true);
                                    postFBSocial();
                                    break;
//                                case R.id.ok_menu:
//                                    menu.dismiss(true);
//                                    postOKSocial();
//                                    break;
                            }
                        }
                    })
                    .setXOffset(0)
                    .setYOffset(0)
                    .build();
            menu.show();
        }
        return super.onOptionsItemSelected(item);
    }

    private OnPostingCompleteListener postingComplete = new OnPostingCompleteListener() {
        @Override
        public void onPostSuccessfully(int socialNetworkID) {
//            Toast.makeText(getActivity(), R.string.post_sent, Toast.LENGTH_LONG).show();
            setNormalActionButton();
        }

        @Override
        public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {
//            Toast.makeText(getActivity(), R.string.cant_post, Toast.LENGTH_LONG).show();
            Logger.Log("Can't post to social network! " + errorMessage);
            setNormalActionButton();
        }
    };

    public void setNormalActionButton() {
        setActionButtonIcon(ParamFragment.ICON_DEL_PROGRESS);
        setActionButtonIconDrawable(R.drawable.share, R.string.share_hint);
        setActionButtonEnabled(true);
    }

    /***
     * Постинг с соцсеть Facebook
     */
    private void postFBSocial() {
//        setActionButtonIcon(ParamFragment.ICON_ADD_PROGRESS);
//        setActionButtonEnabled(false);

        initSocialNetworkManager();
        Logger.Log("Posting to FB social network requested");

        SocialNetwork socialNetwork = mSocialNetworkManager.getSocialNetwork(FacebookSocialNetwork.ID);
        if (socialNetwork != null) {
            if (!socialNetwork.isConnected()) {
                socialNetwork.requestLogin(this);
            } else {
                postSocialNetwork(socialNetwork);
            }
        }
    }

    private void postVKSocial() {
//        setActionButtonIcon(ParamFragment.ICON_ADD_PROGRESS);
//        setActionButtonEnabled(false);

        initSocialNetworkManager();
        Logger.Log("Posting to VK social network");

        SocialNetwork socialNetwork = mSocialNetworkManager.getSocialNetwork(VkSocialNetwork.ID);
        if (socialNetwork != null) {
            if (!socialNetwork.isConnected()) {
                socialNetwork.requestLogin(this);
            } else {
                postSocialNetwork(socialNetwork);
            }
        }
    }

    @Override
    public void onLoginSuccess(int socialNetworkID) {
        if (mSocialNetworkManager == null) return;
        SocialNetwork socialNetwork = mSocialNetworkManager.getSocialNetwork(socialNetworkID);
        Logger.Log("Login success. Sharing appeal...");
        if (fragmentActive)
            postSocialNetwork(socialNetwork);
        else
            mSocialNetworkPostQueue.add(socialNetwork);
    }

    @Override
    public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {
        if (errorMessage == null) errorMessage = "unknown error";
        Logger.Log("Login error. Can't share appeal (" + errorMessage + ")");
        setNormalActionButton();
    }

    public void postSocialNetwork(SocialNetwork socialNetwork) {
        Bundle postParams = new Bundle();
        postParams.putString(SocialNetwork.BUNDLE_NAME, appeal.Subject().Name());
        postParams.putString(SocialNetwork.BUNDLE_LINK, appeal.getUrl());
        postParams.putString(SocialNetwork.BUNDLE_MESSAGE, appeal.AppealClaim().Message());
        if (isNotEmpty(appeal.AppealClaim().DocumentCoverId())) {
            String imageUri = ServerTalk.getDocumentUri(appeal.AppealClaim().DocumentCoverId());
            postParams.putString(SocialNetwork.BUNDLE_PICTURE, imageUri);
        }

        Bitmap bitmap = null;
        try {
            bitmap = ((BitmapDrawable) coverImage.getDrawable()).getBitmap();
        } catch (Exception ignored) {
        }

        if (bitmap != null) postParams.putParcelable(SocialNetwork.BUNDLE_BITMAP, bitmap);

        if (socialNetwork.getID() == FacebookSocialNetwork.ID) {
            socialNetwork.requestPostDialog(postParams, postingComplete);
        } else if (socialNetwork.getID() == VkSocialNetwork.ID) {
            socialNetwork.requestPostDialog(postParams, postingComplete);
        } else {
            socialNetwork.requestPostLink(postParams, appeal.AppealClaim().Message(), postingComplete);
        }
    }

    private void initSocialNetworkManager() {
        if (mSocialNetworkManager != null) return;
        mSocialNetworkManager = (SocialNetworkManager) getActivity().getSupportFragmentManager()
                .findFragmentByTag(MainActivity.SOCIAL_NETWORK_TAG);

        if (mSocialNetworkManager == null) {

            // Социальная сеть "Facebook"
            ArrayList<String> fbScope = new ArrayList<>();
            fbScope.addAll(Collections.singletonList("public_profile"));
            FacebookSocialNetwork fbNetwork = new FacebookSocialNetwork(this, fbScope);

            // Социальная сеть "Вконтакте"
            String[] vkScope = new String[]{VKScope.WALL, VKScope.PHOTOS};
            String VK_APP_ID = getActivity().getString(R.string.vk_app_id);
            VkSocialNetwork vkNetwork = new VkSocialNetwork(this, VK_APP_ID, vkScope);

            mSocialNetworkManager = new SocialNetworkManager();
            mSocialNetworkManager.setOnInitializationCompleteListener(this);
            mSocialNetworkManager.addSocialNetwork(fbNetwork);
            mSocialNetworkManager.addSocialNetwork(vkNetwork);

            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .add(mSocialNetworkManager, MainActivity.SOCIAL_NETWORK_TAG)
                    .commitAllowingStateLoss();
        }

        mSocialNetworkManager.setOnInitializationCompleteListener(this);
    }

    @Override
    public void onSocialNetworkManagerInitialized() {
        if (mSocialNetworkManager == null) return;
        if (!mSocialNetworkManager.getInitializedSocialNetworks().isEmpty()) {
            List<SocialNetwork> socialNetworks = mSocialNetworkManager.getInitializedSocialNetworks();
            for (SocialNetwork socialNetwork : socialNetworks) {
                socialNetwork.setOnLoginCompleteListener(this);
            }
        }

        Logger.Log("Social network manager initialized");
    }

    /***
     * Обработка нажатий на различные элементы
     */
    @Override
    public void onClick(View v) {
        if (!v.isClickable()) return;

        switch (v.getId()) {
            case R.id.reply_hide_all_img:
            case R.id.reply_hide_all_doc:
            case R.id.reply_show_all:
                if (reply_collapsed) {
                    expand_reply();
                } else {
                    collapse_reply();
                }
                break;

            case R.id.liked_text:
                if (appeal.isLiked()) {
                    if (appeal.isAllowAction(Appeal.ACTION_DISLIKE))
                        new ChangeLikeTask(appeal, ServerTalk.ACTION_UNLIKE).execute();
                } else {
                    if (appeal.isAllowAction(Appeal.ACTION_LIKE))
                        new ChangeLikeTask(appeal, ServerTalk.ACTION_LIKE).execute();
                }
                break;

            case R.id.subscribed_text:
                if (appeal.isSubscribed()) {
                    if (appeal.isAllowAction(Appeal.ACTION_UNSUBSCRIBE))
                        new ChangeSubscribeTask(appeal, ServerTalk.ACTION_UNSUBSCRIBE).execute();
                } else {
                    if (appeal.isAllowAction(Appeal.ACTION_SUBSCRIBE))
                        new ChangeSubscribeTask(appeal, ServerTalk.ACTION_SUBSCRIBE).execute();
                }
                break;
        }
    }

    /***
     * Поддержать или отозвать поддержку для обращения
     */
    class ChangeLikeTask extends AsyncTask<String, Void, JSONObject> {
        private Appeal appeal;
        private String action;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            liked_text.setClickable(false);
        }

        public ChangeLikeTask(Appeal appeal, String action) {
            super();
            this.appeal = appeal;
            this.action = action;
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            return server.appealAction(appeal.Id(), action);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            liked_text.setClickable(true);
            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == ErrorResult.SUCCESS) {
                    if (action.equals(ServerTalk.ACTION_LIKE)) {
                        appeal.setIsLiked(true);
                        appeal.setLikesCount(appeal.LikesCount() + 1);
                        AppealListSingleton.setChangedAppeal(appeal);
                        Logger.Log("Appeal like success!");
                    }

                    if (action.equals(ServerTalk.ACTION_UNLIKE)) {
                        appeal.setIsLiked(false);
                        appeal.setLikesCount(Math.max(0, appeal.LikesCount() - 1));
                        AppealListSingleton.setChangedAppeal(appeal);
                        Logger.Log("Appeal unlike success!");
                    }

                    setupLikedText();
                    new GetAppealFromServer(appeal.Id()).execute();
                    return;
                } else {
                    Logger.Warn("Can't like appeal: " + result.getString("errorText"));
                }
            } catch (Exception e) {
                Logger.Exception(e);
                Logger.Warn("Save address error: Can't parse server reply!");
            }

            // Уведомим пользователя о том, что действие не удалось
            if (appeal.isLiked())
                Toast.makeText(Application.AppContext, R.string.cant_unlike, Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(Application.AppContext, R.string.cant_like, Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * Поддержать или отозвать поддержку для обращения
     */
    class ChangeSubscribeTask extends AsyncTask<String, Void, JSONObject> {
        private Appeal appeal;
        private String action;

        public ChangeSubscribeTask(Appeal appeal, String action) {
            super();
            this.appeal = appeal;
            this.action = action;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            subscribed_text.setClickable(false);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            // 1 параметр - GUID обращения, вторая - действие (like/unlike)
            return server.appealAction(appeal.Id(), action);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            subscribed_text.setClickable(true);
            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == ErrorResult.SUCCESS) {
                    if (action.equals(ServerTalk.ACTION_SUBSCRIBE)) {
                        appeal.setIsSubscribed(true);
                        appeal.setSubscribersCount(appeal.SubscribersCount() + 1);
                        AppealListSingleton.setChangedAppeal(appeal);
                        Logger.Log("Appeal subscribe success!");
                    }

                    if (action.equals(ServerTalk.ACTION_UNSUBSCRIBE)) {
                        appeal.setIsSubscribed(false);
                        appeal.setSubscribersCount(Math.max(0, appeal.SubscribersCount() - 1));
                        AppealListSingleton.setChangedAppeal(appeal);
                        Logger.Log("Appeal unsubscribe success!");
                    }

                    setupSubscribedText();
                    new GetAppealFromServer(appeal.Id()).execute();
                    return;
                } else {
                    Logger.Warn("Can't subscribe/unsubscribe appeal: " + result.getString("errorText"));
                }
            } catch (Exception e) {
                Logger.Exception(e);
                Logger.Warn("Save address error: Can't parse server reply!");
            }

            // Уведомим пользователя о том, что действие не удалось
            if (appeal.isSubscribed())
                Toast.makeText(Application.AppContext, R.string.cant_unsubscribe, Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(Application.AppContext, R.string.cant_subscribe, Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * Изменить видимость обращения или ответа на него
     */
    class ChangeAppealVisibility extends AsyncTask<String, Void, JSONObject> {
        private Appeal appeal;
        private String action;

        public ChangeAppealVisibility(Appeal appeal, String action) {
            super();
            this.appeal = appeal;
            this.action = action;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            hideAppealSwitch.setEnabled(false);
            hideResponseSwitch.setEnabled(false);
            hideAppealSwitchSection.setClickable(false);
            hideResponseSwitchSection.setClickable(false);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            return server.appealAction(appeal.Id(), action);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            hideAppealSwitch.setEnabled(true);
            hideResponseSwitch.setEnabled(true);
            hideAppealSwitchSection.setClickable(true);
            hideResponseSwitchSection.setClickable(true);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == ErrorResult.SUCCESS) {
                    Logger.Log("Appeal visibility success! (" + action + ")");
                    answer_hidden_icon.setImageDrawable(action.equals(ServerTalk.ACTION_HIDE_ANSWER) ? blue_hide : grey_hide);
                    new GetAppealFromServer(appeal.Id()).execute();
                    return;
                } else {
                    Logger.Warn("Can't change appeal visibility: " + result.getString("errorText"));
                }
            } catch (Exception e) {
                Logger.Exception(e);
                Logger.Warn("Save address error: Can't parse server reply!");
            }

            // Уведомим пользователя о том, что действие не удалось
            int action_failed = R.string.cant_action;
            switch (action) {
                case ServerTalk.ACTION_HIDE:
                    action_failed = R.string.cant_hide;
                    break;
                case ServerTalk.ACTION_UNHIDE:
                    action_failed = R.string.cant_unhide;
                    break;
                case ServerTalk.ACTION_HIDE_ANSWER:
                    action_failed = R.string.cant_hide_answer;
                    break;
                case ServerTalk.ACTION_UNHIDE_ANSWER:
                    action_failed = R.string.cant_unhide_answer;
                    break;
            }
            Toast.makeText(Application.AppContext, action_failed, Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * Установить "прочитано" для уведомления
     */
    class SetNotificationRead extends AsyncTask<String, Void, JSONObject> {
        private String notificationId;

        public SetNotificationRead(String notificationId) {
            super();
            this.notificationId = notificationId;
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            return server.setNotificationRead(notificationId);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == ErrorResult.SUCCESS) {
                    Logger.Log("Notification has been read!");
                    NotificationListSingleton.setNeedRefresh(true);
                } else {
                    Logger.Warn("Can't set notification read: " + result.getString("errorText"));
                }
            } catch (Exception e) {
                Logger.Exception(e);
                Logger.Warn("Read notification error: Can't parse server reply!");
            }
        }
    }

    /***
     * Обновить обращение с сервера
     */
    class GetAppealFromServer extends AsyncTask<String, Void, JSONObject> implements DialogResultInterface {
        private String id;

        public GetAppealFromServer(String id) {
            super();
            this.id = id;
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            return server.getAppeal(id);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            String errorText = ErrorResult.getErrorText(ErrorResult.APPLICATION_ERROR);
            int errorCode = result.getInt("errorCode", ErrorResult.SUCCESS);
            boolean success = false;
            try {
                errorText = result.getErrorText();
                if (errorCode == ErrorResult.SUCCESS) {
                    appeal = Appeal.fromJson(result);
                    showOrHideActionButton();
                    AppealListSingleton.setChangedAppeal(appeal);
                    fillCardView();
                    success = true;
                    SetNotificationRead();
                } else if ((errorText.contains(" 404 (")) || (errorCode == ErrorResult.NOT_FOUND)) {
                    SetNotificationRead();
                } else {
                    Logger.Warn("Can't load appeal: " + errorText);
                }
            } catch (Exception e) {
                Logger.Exception(e);
                Logger.Warn("Can't load appeal: Can't parse server reply!");
            }

            if (!success && fragmentActive) {
                if ((!errorText.contains(" 404 (") &&  (errorCode != ErrorResult.NOT_FOUND))) {
                    ResultDialog.makeDialog(Application.AppContext, R.drawable.error_grey,
                            getString(R.string.error_loading_appeal), errorText,
                            getString(R.string.repeat), this).show(getActivity());
                } else {
                    ResultDialog.makeDialog(Application.AppContext, R.drawable.error_grey,
                            getString(R.string.error_loading_appeal), getString(R.string.was_hidden),
                            getString(R.string.close), new DialogResultInterface() {
                                @Override
                                public void DialogSendResult(int dialog_id, int cmd) {
                                    chainPopFragments(FRAGMENT_ID, 1, false);
                                }

                                @Override
                                public void DialogSendParamResult(int dialog_id, int cmd, JSONObject params) {
                                }
                            }).show(getActivity());
                }
            }

            StopLoading();
        }

        @Override
        public void DialogSendResult(int dialog_id, int cmd) {
            if (cmd == Activity.RESULT_OK) new GetAppealFromServer(id).execute();
        }

        @Override
        public void DialogSendParamResult(int dialog_id, int cmd, JSONObject params) {
        }
    }

    private void SetNotificationRead() {
        // Если карточка открыта из уведомления, то надо установить уведомление прочитанным
        if (parameters.has("NotificationID")) {
            NotificationListSingleton.setNeedRefresh(true);
            new SetNotificationRead(parameters.getString("NotificationID", "")).execute();
        }
    }

    public void StopLoading() {
        swipeToLoadLayout.setRefreshEnabled(true);
        swipeToLoadLayout.setRefreshing(false);
        setOrCheckLoading(false);
        showOrHideActionButton();
    }
}
