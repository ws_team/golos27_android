package ru.infodev.golos27.Widgets.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Subject;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Common.Utils.Utils;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.Lists.NestedGridView;

public class CategoryAdapter extends BaseAdapter {
    private ArrayList<Subject> mData;
    private LayoutInflater inflater;

    public CategoryAdapter(Context context, List<Subject> items) {
        mData = (ArrayList<Subject>)items;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public Subject getItem(int index) {
        return mData.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        float h = (float)((NestedGridView)parent).getColumnWidthCompat();

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_category, parent, false);
            Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
            holder = new ViewHolder();
            holder.image = (ImageView) convertView.findViewById(R.id.category_icon);
            holder.text = (TextView) convertView.findViewById(R.id.category_name);
            holder.text.setTypeface(roboto_light);
            View root_view = convertView.findViewById(R.id.root_view);
            convertView.setTag(holder);

            // Надо задать размеры image в процентах от реального размера ячейки
            int imageSize = (int)(h / 2.28f);
            int topPadding = (int)(h / 6.4f);
            int sidePadding = (int)(h / 6.4f);
            int textTop = (int)(h / 10.66f);
            LinearLayout.LayoutParams imageParam = (LinearLayout.LayoutParams) holder.image.getLayoutParams();
            imageParam.height = imageSize;
            imageParam.width = imageSize;
            imageParam.weight = 0;

            root_view.setPadding(sidePadding, topPadding, sidePadding, 0);
            holder.text.setPadding(0, textTop, 0, 0);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(getItem(position).Name());
        holder.image.setImageDrawable(null);
        String imageUri = getItem(position).Resource();

        if (Utils.isNotEmpty(imageUri)) {
            try {
                // Асинхронная загрузка изображений с помощью библиотеки UIL
                ImageLoader.getInstance().loadImage(imageUri, new ImageSize((int)h, (int)h), new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        Drawable topImage = new BitmapDrawable(Application.AppContext.getResources(), loadedImage);
                        holder.image.setImageDrawable(topImage);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        Logger.Warn("Can't load image into gallery grid (" + failReason.getType().toString() + ")");
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        Logger.Warn("Can't load image into gallery grid (canceled)");
                    }
                });
            } catch (Exception e) {
                Logger.Exception(e);
            }
        }

        return convertView;
    }

    static class ViewHolder {
        ImageView image;
        TextView text;
    }
}