package ru.infodev.golos27.Common;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;

import java.util.ArrayList;
import java.util.HashMap;

import ru.infodev.golos27.Common.Utils.Crypto;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;

import static ru.infodev.golos27.ApiConfig.SERVER_ADDRESS_VALUE;
import static ru.infodev.golos27.ApiConfig.SERVER_PORT_VALUE;
import static ru.infodev.golos27.ApiConfig.SERVER_SCHEMA_VALUE;
import static ru.infodev.golos27.Common.Utils.Crypto.getSecretKey;
import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

/* Manage database application settings */
public class Settings {

    public static final String HASH1 = "ConfigHash1";
    public static final String HASH2 = "ConfigHash2";
    public static final int MAX_NOTIFICATIONS = 3;

    private static Context lContext = null;
    private static DBHelper database = null;
    private static HashMap<String, String> Cache = null;
    private static boolean Cached = false;
    private static HashMap<String, String> defValues;
    private static ArrayList<String> uriValues = new ArrayList<>();

    public static final String SERVER_SCHEMA = "ServerSchema";
    public static final String SERVER_SCHEMA_DVAL = SERVER_SCHEMA_VALUE;

    public static final String SERVER_ADDRESS = "ServerAddress";
    private static final String SERVER_ADDRESS_DVAL = SERVER_ADDRESS_VALUE;

    public static final String SERVER_PORT = "ServerPort";
    public static final String SERVER_PORT_DVAL = SERVER_PORT_VALUE;

    public static final String SERVER_ROOT = "ServerRootPath";
    public static final String SERVER_ROOT_DVAL = "/opengolos";

    public static final String SERVER_READ_TIMEOUT = "ServerReadTimeout";
    public static final String SERVER_READ_TIMEOUT_DVAL = "30000";

    public static final String SERVER_CONNECT_TIMEOUT = "ServerConnectTimeout";
    public static final String SERVER_CONNECT_TIMEOUT_DVAL = "20000";

    public static final String CLIENT_ID = "ClientId";
    public static final String CLIENT_ID_DVAL = "android-client";

    public static final String CLIENT_SECRET = "ClientSecret";
    public static final String CLIENT_SECRET_DVAL = "android-secret";

    public static final String USER_ACCESS_TOKEN = "UserAccessToken";
    public static final String USER_ACCESS_TOKEN_DVAL = "";

    public static final String USER_REFRESH_TOKEN = "UserRefreshToken";
    public static final String USER_REFRESH_TOKEN_DVAL = "";

    public static final String USER_TOKEN_EXPIRES_IN = "UserTokenExpiresIn";
    public static final String USER_TOKEN_EXPIRES_IN_DVAL = "";

    public static final String USER_TOKEN_START_TIME = "UserTokenStartTime";
    public static final String USER_TOKEN_START_TIME_DVAL = "0";

    public static final String CODE_LIVE_TIME = "CodeLiveTime";
    public static final String CODE_LIVE_TIME_DVAL = "300";

    public static final String EXECUTORS_VERSION = "ExecutorVersion";
    public static final String EXECUTORS_VERSION_DVAL = "0";

    public static final String SUBJECTS_VERSION = "SubjectsVersion";
    public static final String SUBJECTS_VERSION_DVAL = "0";

    public static final String TOPIC_TOKEN = "TopicToken";
    public static final String TOPIC_TOKEN_DVAL = "";

    public static final String PUSH_TOKEN = "PushToken";
    public static final String PUSH_TOKEN_DVAL = "";

    public static final String NOTIFICATION_CURRENT_ID = "NotificationCurrentId";
    public static final String NOTIFICATION_CURRENT_ID_DVAL = "0";

    // Данные профиля
    public static final String PROFILE_FIRST_NAME = "ProfileFirstName";
    public static final String PROFILE_FIRST_NAME_DVAL = "";

    public static final String PROFILE_LAST_NAME = "ProfileLastName";
    public static final String PROFILE_LAST_NAME_DVAL = "";

    public static final String PROFILE_MIDDLE_NAME = "ProfileMiddleName";
    public static final String PROFILE_MIDDLE_NAME_DVAL = "";

    public static final String PROFILE_OKTMO_CODE = "ProfileOktmoCode";
    public static final String PROFILE_OKTMO_CODE_DVAL = "";

    public static final String PROFILE_OKTMO_DATA = "ProfileOktmoData";
    public static final String PROFILE_OKTMO_DATA_DVAL = "";

    public static final String PROFILE_CUSTOM_ADDRESS = "ProfileCustomAddress";
    public static final String PROFILE_CUSTOM_ADDRESS_DVAL = "";

    public static final String PROFILE_LATITUDE = "ProfileLatitude";
    public static final String PROFILE_LATITUDE_DVAL = "";

    public static final String PROFILE_LONGITUDE = "ProfileLongitude";
    public static final String PROFILE_LONGITUDE_DVAL = "";

    public static final String PROFILE_BIRTH_DATE = "ProfileBirthDate";
    public static final String PROFILE_BIRTH_DATE_DVAL = "";

    public static final String PROFILE_GENDER = "ProfileGender";
    public static final String PROFILE_GENDER_DVAL = "-1";

    public static final String PROFILE_HIDE_POPUP = "ProfileHidePopup";
    public static final String PROFILE_HIDE_POPUP_DVAL = "false";

    public static final String AUTHORIZED_FLAG = "AuthorizedFlag";
    public static final String AUTHORIZED_FLAG_DVAL = "false";

    public static final String PROVIDER_HASH_DATA = "ProviderHashData";

    public static ArrayList<String> getUriValues() {
        return uriValues;
    }

    public static void Init(Context context) {
        lContext = context;
        database = new DBHelper(lContext);

        // Дефолтовые значения настроек
        defValues = new HashMap<>();
        defValues.put(SERVER_SCHEMA, SERVER_SCHEMA_DVAL);
        defValues.put(SERVER_ADDRESS, SERVER_ADDRESS_DVAL);
        defValues.put(SERVER_PORT, SERVER_PORT_DVAL);
        defValues.put(SERVER_ROOT, SERVER_ROOT_DVAL);
        defValues.put(SERVER_READ_TIMEOUT, SERVER_READ_TIMEOUT_DVAL);
        defValues.put(SERVER_CONNECT_TIMEOUT, SERVER_CONNECT_TIMEOUT_DVAL);
        defValues.put(CLIENT_ID, CLIENT_ID_DVAL);
        defValues.put(CLIENT_SECRET, CLIENT_SECRET_DVAL);
        defValues.put(USER_ACCESS_TOKEN, USER_ACCESS_TOKEN_DVAL);
        defValues.put(USER_REFRESH_TOKEN, USER_REFRESH_TOKEN_DVAL);
        defValues.put(USER_TOKEN_EXPIRES_IN, USER_TOKEN_EXPIRES_IN_DVAL);
        defValues.put(USER_TOKEN_START_TIME, USER_TOKEN_START_TIME_DVAL);
        defValues.put(CODE_LIVE_TIME, CODE_LIVE_TIME_DVAL);
        defValues.put(EXECUTORS_VERSION, EXECUTORS_VERSION_DVAL);
        defValues.put(SUBJECTS_VERSION, SUBJECTS_VERSION_DVAL);
        defValues.put(TOPIC_TOKEN, TOPIC_TOKEN_DVAL);
        defValues.put(PUSH_TOKEN, PUSH_TOKEN_DVAL);
        defValues.put(NOTIFICATION_CURRENT_ID, NOTIFICATION_CURRENT_ID_DVAL);

        // Дефолтовые значения профиля
        defValues.put(PROFILE_FIRST_NAME, PROFILE_FIRST_NAME_DVAL);
        defValues.put(PROFILE_LAST_NAME, PROFILE_LAST_NAME_DVAL);
        defValues.put(PROFILE_MIDDLE_NAME, PROFILE_MIDDLE_NAME_DVAL);
        defValues.put(PROFILE_GENDER, PROFILE_GENDER_DVAL);
        defValues.put(PROFILE_BIRTH_DATE, PROFILE_BIRTH_DATE_DVAL);
        defValues.put(PROFILE_OKTMO_CODE, PROFILE_OKTMO_CODE_DVAL);
        defValues.put(PROFILE_OKTMO_DATA, PROFILE_OKTMO_DATA_DVAL);
        defValues.put(PROFILE_CUSTOM_ADDRESS, PROFILE_CUSTOM_ADDRESS_DVAL);
        defValues.put(PROFILE_LATITUDE, PROFILE_LATITUDE_DVAL);
        defValues.put(PROFILE_LONGITUDE, PROFILE_LONGITUDE_DVAL);
        defValues.put(PROFILE_HIDE_POPUP, PROFILE_HIDE_POPUP_DVAL);
        defValues.put(AUTHORIZED_FLAG, AUTHORIZED_FLAG_DVAL);

        // В новой версии наконец сделали отдельный профиль для андроид клиента
        setParameter(CLIENT_ID, CLIENT_ID_DVAL);
        setParameter(CLIENT_SECRET, CLIENT_SECRET_DVAL);
        setParameter(SERVER_ADDRESS, SERVER_ADDRESS_DVAL);
        setParameter(SERVER_PORT, SERVER_PORT_DVAL);
        setParameter(SERVER_SCHEMA, SERVER_SCHEMA_DVAL);
    }

    public static DBHelper getDatabase() {
        return database;
    }

    public static boolean getBool(String name) {
        return getBool(name, true);
    }

    public static boolean getBool(String name, boolean useCache) {
        String text = getDatabaseParameter(name, useCache);
        return text != null && text.toLowerCase().equals("true");
    }

    public static int getInt(String name) {
        return getInt(name, true);
    }

    public static int getInt(String name, boolean useCache) {
        String text = getDatabaseParameter(name, useCache);
        if (text != null) {
            try {
                return Integer.valueOf(text);
            } catch (Exception ex) {
                Logger.Exception(ex);
            }
        }

        return Integer.valueOf("0");
    }

    public static long getLong(String name) {
        return getLong(name, true);
    }

    public static long getLong(String name,boolean useCache) {
        String text = getDatabaseParameter(name, useCache);
        if (text != null) {
            try {
                return Long.valueOf(text);
            } catch (Exception ex) {
                Logger.Exception(ex);
            }
        }

        return Long.valueOf("0");
    }

    public static int getColor(String name) {
        return getColor(name, true);
    }

    public static int getColor(String name, boolean useCache) {
        String text = getDatabaseParameter(name, useCache);
        try {
            return Color.parseColor(text);
        } catch (Exception ex) {
            Logger.Exception(ex);
        }
        return 0;
    }

    public static String getStr(String name, boolean useCache) {
        return getDatabaseParameter(name, useCache);
    }

    public static String getStr(String name) {
        return getDatabaseParameter(name);
    }

    public static void setParameter(String name, String value) {
        setDatabaseParameter(name, value);
    }

    private static String getDatabaseParameter(String name) {
        return getDatabaseParameter(name, true);
    }

    private static String getDatabaseParameter(String name, boolean useCache) {
        Cursor c = null;
        String[] lName = {name};
        SQLiteDatabase db = null;
        String retVal = null;
        String defVal = defValues.get(name);

        // Try to get from cache
        if (useCache && Cached && (Cache != null) && (Cache.containsKey(name)))
            return Cache.get(name);

        try {
            if (database != null) {
                db = database.getReadableDatabase();
                c = db.query(true, lContext.getString(R.string.settings_table_name),
                        null, "name = ?", lName, null, null, null, null);

                if ((c.getCount() == 0)) {
                    if ((defVal != null) && (!defVal.isEmpty())) {
                        // Init variable (not found in database)
                        setDatabaseParameter(name, defVal);
                        retVal = defVal;
                    }
                } else {
                    // Return variable
                    c.moveToFirst();
                    retVal = c.getString(1);
                }

                c.close();
                if (db.inTransaction()) {
                    db.endTransaction();
                }
                return retVal;
            }
        } catch (Exception ex) {
            if (c != null) c.close();
            if ((db != null) && db.inTransaction()) {
                db.endTransaction();
            }
            Logger.Exception(ex);
        }

        return null;
    }

    private static void setDatabaseParameter(String name, String value) {
        SQLiteDatabase db = null;
        String[] lName = {name};
        try {
            if (database != null) {
                db = database.getWritableDatabase();
                ContentValues cv = new ContentValues();

                cv.put("name", lName[0]);
                cv.put("value", value);

                long iNum = db.insert(lContext.getString(R.string.settings_table_name), null, cv);
                if (iNum <= 0) Logger.Log("Can't write value for " + name);
                if (db.inTransaction()) {
                    db.endTransaction();
                }
            }
        } catch (Exception ex) {
            if ((db != null) && db.inTransaction()) {
                db.endTransaction();
            }
            Logger.Exception(ex);
        }

        // Reload Cache after change
        LoadCache();
    }

    // Delete parameter from database and cache
    public static void delDatabaseParameter(String name) {
        SQLiteDatabase db = null;
        String[] lName = {name};
        try {
            if (database != null) {
                db = database.getWritableDatabase();
                long iNum = db.delete(lContext.getString(R.string.settings_table_name), "name=?", lName);
                if (db.inTransaction()) {
                    db.endTransaction();
                }

                // Remove from cache
                if ((iNum > 0) && Cached && (Cache != null) && (Cache.containsKey(name)))
                    Cache.remove(name);
            }
        } catch (Exception ex) {
            if ((db != null) && db.inTransaction()) {
                db.endTransaction();
            }
            Logger.Exception(ex);
        }

        // Reload cache after deletion
        LoadCache();
    }

    // Dump all app settings into log
    public static void LogAllSettings() {
        SQLiteDatabase db = null;
        try {
            if (database != null) {
                db = database.getReadableDatabase();
                Cursor c = db.query(lContext.getString(R.string.settings_table_name), null, null, null, null, null, null);
                c.moveToFirst();
                do {
                    String name = c.getString(0);
                    String value = c.getString(1);
                    Logger.Log("s> " + name + " = " + value);
                } while (!c.moveToNext());
                c.close();
                if (db.inTransaction()) {
                    db.endTransaction();
                }
            }
        } catch (Exception ex) {
            if ((db != null) && db.inTransaction()) {
                db.endTransaction();
            }
            Logger.Exception(ex);
        }
    }

    // Load settings into the cache (all get/set methods will use it)
    public static void LoadCache() {
        SQLiteDatabase db = null;
        Cache = new HashMap<>();
        try {
            if (database != null) {
                db = database.getReadableDatabase();
                Cursor c = db.query(lContext.getString(R.string.settings_table_name), null, null, null, null, null, null);
                if (c.moveToFirst()) {
                    while (!c.isAfterLast()) {
                        String name = c.getString(0);
                        String value = c.getString(1);
                        Cache.put(name, value);
                        c.moveToNext();
                    }
                }
                c.close();
                if (db.inTransaction()) {
                    db.endTransaction();
                }
            }
        } catch (Exception ex) {
            if ((db != null) && db.inTransaction()) {
                db.endTransaction();
            }
            Logger.Exception(ex);
            Cached = false;
            Cache = null;
        }

        if ((Cache != null) && (Cache.size() > 0)) {
            Cached = true;
        }
    }

    public static void Store(String alias, String value) {
        value = Crypto.encrypt(value, getSecretKey());
        if (value != null) Settings.setParameter(alias, value);
    }

    public static String Read(String alias) {
        String str = Settings.getStr(alias, false);
        return isNotEmpty(str) ? Crypto.decrypt(str, getSecretKey()) : null;
    }

    public static synchronized int getOrSetNotificationId(Integer id) {
        Integer notificationId = getInt(NOTIFICATION_CURRENT_ID);
        if (id != null) {
            setParameter(NOTIFICATION_CURRENT_ID, id.toString());
            return id;
        } else {
            notificationId++;
            if (notificationId > MAX_NOTIFICATIONS) notificationId = 1;
            setParameter(NOTIFICATION_CURRENT_ID, notificationId.toString());
        }
        Logger.Log("Got notification id: " + notificationId.toString());
        return notificationId;
    }
}
