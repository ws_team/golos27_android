package ru.infodev.golos27;

/**
 * Created by Maxim Ostrovidov on 10.08.17.
 * (c) White Soft
 */
public class ApiConfig {
    public static final String SERVER_SCHEMA_VALUE = "http";
    public static final String SERVER_ADDRESS_VALUE = "78.157.253.22";
    public static final String SERVER_PORT_VALUE = "6080";

    public static final String FRONT_ADDRESS = "http://vmeste.pkgo.ru";
}

//public class ApiConfig {
//    public static final String SERVER_SCHEMA_VALUE = "https";
//    public static final String SERVER_ADDRESS_VALUE = "vmeste.pkgo.ru";
//    public static final String SERVER_PORT_VALUE = "443";
//
//    public static final String FRONT_ADDRESS = "http://vmeste.pkgo.ru";
//}