# Add project specific ProGuard rules here.
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

-keepclassmembers class ru.infodev.golos27.Common.Interfaces.MapJavaScriptInterface {
   public *;
}

-keep class ru.infodev.golos27.Fragments.ParamFragment { *; }

-keepclassmembers class * extends ru.infodev.golos27.Fragments.ParamFragment {
   public static android.support.v4.app.Fragment newInstance(java.lang.Class, ru.infodev.golos27.Common.Utils.Json.JSONObject);
}

-dontwarn org.apache.commons.**
-keep class org.apache.http.** { *; }
-dontwarn org.apache.http.**

-keep class com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**
