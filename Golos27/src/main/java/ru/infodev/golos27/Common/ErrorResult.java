package ru.infodev.golos27.Common;

import android.util.Pair;

import java.util.HashMap;
import java.util.Map;

import ru.infodev.golos27.RestApi.Transport;

public class ErrorResult {
    private Integer errorCode = 0;
    private String errorText = null;

    public ErrorResult(Integer errorCode, String errorText) {
        this.errorCode = errorCode;
        this.errorText = errorText;
    }

    public ErrorResult(Integer errorCode) {
        this.errorCode = errorCode;
        this.errorText = ErrorResult.getErrorText(errorCode);
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    // Ошибки сервера
    public static final int SUCCESS = Transport.SUCCESS;
    public static final int USER_ALREADY_EXISTS = -1;
    public static final int INVALID_CONFIRMATION_CODE = -2;
    public static final int INVALID_ACTION_TOKEN = -3;
    public static final int INVALID_LOGIN_FORMAT = -4;
    public static final int INVALID_PASSWORD_LEN = -5;
    public static final int INVALID_PASSWORD_FORMAT = -6;
    public static final int INVALID_OLD_PASSWORD_FORMAT = -7;
    public static final int INVALID_PASSWORD = -8;
    public static final int USER_NOT_FOUND = -10;
    public static final int INVALID_SUBJECT = -20;
    public static final int INVALID_EXECUTOR = -21;
    public static final int DOCUMENT_NOT_FOUND = -9;
    public static final int INVALID_PARAM = 400;
    public static final int INVALID_OAUTH = 401;
    public static final int NOT_AUTHORIZED = 403;
    public static final int NOT_FOUND = 404;
    public static final int INTERNAL_SERVER_ERROR = 500;
    public static final int INVALID_GRANT = 100;
    public static final int INVALID_CLIENT = 101;
    public static final int UNSUPPORTED_GRANT_TYPE = 102;
    public static final int INVALID_TOKEN = 103;
    public static final int UNAUTHORIZED = 104;

    // Ошибки транспорта
    public static final int NETWORK_ERROR = 20999;

    public static final int TRANSPORT_NOT_INIT = 20001;
    public static final int URI_BUILD_ERROR = 20002;
    public static final int CANT_CONNECT = 20003;
    public static final int BAD_REQUEST = 20004;
    public static final int INTERNAL_ERROR = 20009;

    // Ошибки приложения
    public static final int DATABASE_ERROR = 30001;
    public static final int APPLICATION_ERROR = 30002;
    public static final int INVALID_SAVED_CRED = 30003;
    public static final int UNKNOWN_ERROR = -99999;

    // Дефолтовая ошибка транспорта
    public static final String DEFAULT_NETWORK_ERROR = "Во время выполнения запроса произошла ошибка. " +
            "Возможно, отсутствует подключение к сети Интернет или связь с сервером.";

    private static Map<String, Pair<Integer, String>> ERROR_CODES = new HashMap<String, Pair<Integer, String>>() {{

        // Ошибки сервиса
        put("SUCCESS", new Pair<>(SUCCESS, "Операция выполнена успешно"));
        put("USER_ALREADY_EXISTS", new Pair<>(USER_ALREADY_EXISTS, "Пользователь с указанным телефонным номером или адресом электронной почты уже существует в системе. Повторная регистрация невозможна"));
        put("INVALID_CONFIRMATION_CODE", new Pair<>(INVALID_CONFIRMATION_CODE, "Указанный код подтверждения не является верным или устарел. Запросите новый код подтверждения или укажите верный код"));
        put("INVALID_ACTION_TOKEN", new Pair<>(INVALID_ACTION_TOKEN, "Токен действия не найден"));
        put("INVALID_LOGIN_FORMAT", new Pair<>(INVALID_LOGIN_FORMAT, "Указанный телефонный номер или адрес электронной почты имеют неверный формат. Укажите данные в правильном формате"));
        put("INVALID_PASSWORD_LEN", new Pair<>(INVALID_PASSWORD_LEN, "Указанный Вами пароль не соответствует требованиям безопасности. Укажите другой пароль"));
        put("INVALID_PASSWORD_FORMAT", new Pair<>(INVALID_PASSWORD_FORMAT, "Указанный Вами пароль не соответствует требованиям безопасности. Укажите другой пароль"));
        put("INVALID_OLD_PASSWORD_FORMAT", new Pair<>(INVALID_OLD_PASSWORD_FORMAT, "Указан неверный старый пароль"));
        put("INVALID_PASSWORD", new Pair<>(INVALID_PASSWORD, "Указан неверный пароль"));
        put("USER_NOT_FOUND", new Pair<>(USER_NOT_FOUND, "Пользователь с указанным телефонным номером или адресом электронной почты не зарегистрирован в системе"));
        put("INVALID_SUBJECT", new Pair<>(INVALID_SUBJECT, "Тематика не найдена"));
        put("INVALID_EXECUTOR", new Pair<>(INVALID_EXECUTOR, "Исполнитель не найден"));
        put("DOCUMENT_NOT_FOUND", new Pair<>(DOCUMENT_NOT_FOUND, "Вложение не найдено"));
        put("INVALID_PARAM", new Pair<>(INVALID_PARAM, "Отсутствует обязательный параметр"));
        put("INVALID_OAUTH", new Pair<>(INVALID_OAUTH, "Переданный OAuth токен не действителен"));
        put("NOT_AUTHORIZED", new Pair<>(NOT_AUTHORIZED, "Пользователь не авторизован"));
        put("NOT_FOUND", new Pair<>(NOT_FOUND, "Операция не выполнена. Запрашиваемый ресурс не найден"));
        put("INTERNAL_SERVER_ERROR", new Pair<>(INTERNAL_SERVER_ERROR, "Во время выполнения запроса произошла непредвиденная ошибка"));

        // Ошибки авторизации
        put("invalid_grant", new Pair<>(INVALID_GRANT, "Указан неверный логин или пароль"));
        put("invalid_client", new Pair<>(INVALID_CLIENT, "Ваше приложение не поддерживается. Попробуйте повторить попытку позже"));
        put("unsupported_grant_type", new Pair<>(UNSUPPORTED_GRANT_TYPE, "Возникла неизвестная ошибка приложения. Повторите попытку позже"));
        put("invalid_token", new Pair<>(INVALID_TOKEN, "Неверный параметр доступа"));
        put("unauthorized", new Pair<>(UNAUTHORIZED, "Ошибка аутентификации пользователя. Пожалуйста авторизуйтесь снова"));
        put("DATABASE_ERROR", new Pair<>(DATABASE_ERROR, "Возникла неизвестная ошибка приложения. Повторите попытку позже"));

        // Ошибки приложения
        put("APPLICATION_ERROR", new Pair<>(APPLICATION_ERROR, "Возникла неизвестная ошибка приложения. Повторите попытку позже"));
        put("UNKNOWN_ERROR", new Pair<>(UNKNOWN_ERROR, "Возникла неизвестная ошибка приложения. Повторите попытку позже"));

        // Ошибки транспорта
        put("TRANSPORT_NOT_INIT", new Pair<>(NETWORK_ERROR, DEFAULT_NETWORK_ERROR));
        put("URI_BUILD_ERROR", new Pair<>(NETWORK_ERROR, DEFAULT_NETWORK_ERROR));
        put("CANT_CONNECT", new Pair<>(NETWORK_ERROR, DEFAULT_NETWORK_ERROR));
        put("BAD_REQUEST", new Pair<>(NETWORK_ERROR, DEFAULT_NETWORK_ERROR));
        put("AUTH_ERROR", new Pair<>(NETWORK_ERROR, DEFAULT_NETWORK_ERROR));
        put("HTTP_ERROR", new Pair<>(NETWORK_ERROR, DEFAULT_NETWORK_ERROR));
        put("SOCKET_TIMEOUT", new Pair<>(NETWORK_ERROR, DEFAULT_NETWORK_ERROR));
        put("UNKNOWN_REQUEST_ERROR", new Pair<>(NETWORK_ERROR, DEFAULT_NETWORK_ERROR));
        put("INTERNAL_ERROR", new Pair<>(NETWORK_ERROR, DEFAULT_NETWORK_ERROR));
        put("CANT_READ_SERVER", new Pair<>(NETWORK_ERROR, DEFAULT_NETWORK_ERROR));

    }};

    public static Pair<Integer, String> Get(String errorCode) {
        if (ERROR_CODES.containsKey(errorCode)) return ERROR_CODES.get(errorCode);
        return ERROR_CODES.get("UNKNOWN_ERROR");
    }

    public static String getErrorText(int errorCode) {
        for (Map.Entry<String, Pair<Integer, String>> error : ERROR_CODES.entrySet()) {
            if (error.getValue().first == errorCode) return error.getValue().second;
        }
        return ERROR_CODES.get("UNKNOWN_ERROR").second;
    }
}
