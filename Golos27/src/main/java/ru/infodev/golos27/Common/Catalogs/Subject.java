package ru.infodev.golos27.Common.Catalogs;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.DBHelper;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

@SuppressWarnings("unused")
public class Subject {
    private String id;
    private String name;
    private String parentId = "";
    private String resource = "";
    private String uriPrefix = "";
    private String uriSuffix = "";
    private Integer priority = null;
    private Long version = 0L;
    private boolean isDeleted = false;
    private List<Subject> groupList = null;
    //------------------------------------------------------------------

    // Конструкторы
    public Subject() {
    }

    public Subject(String id, String name, String parentId, String resource, Long version, Integer priority) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.resource = resource;
        this.version = version;
        this.priority = priority;
    }

    // Методы для работы с полями
    public String Id() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String Name() {
        return name;
    }

    public void setName(String name) {
        this.name = name.trim();
    }

    public String ParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String Resource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public boolean IsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long Version() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public void setGroupList(List<Subject> list) {
        groupList = list;
    }

    public List<Subject> getGroupList() {
        return groupList;
    }

    public boolean isGroup() {
        return ((groupList != null) && (groupList.size() > 0));
    }

    public String UriPrefix() {
        return uriPrefix;
    }

    public void setUriPrefix(String uriPrefix) {
        this.uriPrefix = uriPrefix;
    }

    public String UriSuffix() {
        return uriSuffix;
    }

    public void setUriSuffix(String uriSuffix) {
        this.uriSuffix = uriSuffix;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    // Общие методы класса
    public String toString() {
        return Name();
    }

    /***
     * Сохраним текущий объект в базу данных
     */
    private boolean save(boolean commit) {
        if (IsDeleted()) return remove(commit);

        DBHelper database = Settings.getDatabase();
        if (database == null) return false;

        SQLiteDatabase db = null;
        try {
            db = database.getWritableDatabase();
            ContentValues cv = new ContentValues();

            cv.put("guid", Id());
            cv.put("name", Name());
            cv.put("parent", ParentId());
            cv.put("resource", Resource() != null ? Resource() : "");
            cv.put("version", Version());
            cv.put("priority", priority);

            long iNum = db.insertOrThrow(Application.AppContext.getString(R.string.subjects_table_name), null, cv);
            if (iNum <= 0) Logger.Log("Can't write value for " + toString());
            if ((db.inTransaction()) && commit) db.endTransaction();
            return iNum > 0;

        } catch (Exception ex) {
            if ((db != null) && db.inTransaction()) {
                db.endTransaction();
            }
            Logger.Exception(ex);
        }

        return false;
    }

    @SuppressWarnings("unused")
    public boolean save() {
        return save(true);
    }

    /***
     * Удалим текущий объект из базы данных
     */
    public boolean remove(boolean commit) {
        DBHelper database = Settings.getDatabase();
        if (database == null) return false;
        String[] code = {Id()};
        SQLiteDatabase db = null;
        try {
            db = database.getWritableDatabase();
            long iNum = db.delete(Application.AppContext.getString(R.string.subjects_table_name), "guid=?", code);
            if (iNum <= 0) Logger.Log("Can't delete value of " + Id());
            if (db.inTransaction() && commit) db.endTransaction();
        } catch (Exception ex) {
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        return false;
    }

    @SuppressWarnings("unused")
    public boolean remove() {
        return remove(true);
    }

    // Статические методы класса (импорт/экспорт)
    public static boolean updateListFromServer(JSONObject serverReply) {
        boolean result = false;
        JSONArray jsonList = null;
        try {
            if (serverReply == null) return false;
            if (serverReply.has(JSONObject.JSON_LONG_STRING)) {
                JSONObject temp = new JSONObject(serverReply.getString(JSONObject.JSON_LONG_STRING));
                if (temp.has("response")) {
                    jsonList = temp.getJSONArray("response");
                }
            } else {
                jsonList = serverReply.getJSONArray("replyArray");
            }

            if (jsonList == null) return false;
            DBHelper database = Settings.getDatabase();
            if (database == null) return false;

            SQLiteDatabase db = database.getWritableDatabase();
            Long maxVersion = 0L;
            try {
                db.beginTransaction();
                for (int i = 0; i < jsonList.length(); i++) {
                    JSONObject row = jsonList.getJSONObject(i);
                    Subject subject = Subject.fromJson(row);
                    if (subject != null) {
                        long version = subject.Version();
                        if (version > maxVersion) maxVersion = version;
                        subject.save(false);
                    }
                }
                db.setTransactionSuccessful();
                result = true;
            } catch (Exception e) {
                if ((db != null) && db.inTransaction()) db.endTransaction();
                Logger.Exception(e);
            } finally {
                if (db != null) db.endTransaction();
                Settings.setParameter(Settings.SUBJECTS_VERSION, maxVersion.toString());
            }
        } catch (Exception e) {
            Logger.Exception(e);
        }

        return result;
    }

    public static Subject fromJson(JSONObject subject) {
        Subject result = new Subject();
        if (subject == null) return result;
        try {
            // Идентификатора может и не быть если это загруженное обращение
            result.setId((String) subject.get("id", ""));
            result.setVersion(subject.getLong("version", 0L));

            if (subject.has("name") && !subject.isNull("name"))
                result.setName(subject.getString("name"));

            if (subject.has("priority") && !subject.isNull("priority"))
                result.setPriority(subject.getInt("priority"));

            if (subject.has("parentId") && !subject.isNull("parentId"))
                result.setParentId(subject.getString("parentId"));

            if (subject.has("resource") && !subject.isNull("resource"))
                result.setResource(getSubjectUri(subject.getJSONObject("resource", null)));

            if (subject.has("rootResource") && !subject.isNull("rootResource")) {
                JSONObject resource = subject.getJSONObject("rootResource", null);
                result.setResource(getSubjectUri(resource));
                if (resource != null) {
                    result.setUriPrefix(resource.getString("prefix", null));
                    result.setUriSuffix(resource.getString("suffix", null));
                }
            }

            if (subject.has("isDeleted") && !subject.isNull("isDeleted"))
                result.setIsDeleted(subject.getBoolean("isDeleted"));

            if (subject.has("privateGroups") && !subject.isNull("privateGroups")) {
                List<Subject> list = new ArrayList<>();
                JSONArray childs = subject.getJSONArray("privateGroups");
                for (int i = 0; i < childs.length(); i++) {
                    try {
                        JSONObject row = (JSONObject) childs.get(i);
                        Subject child = Subject.fromJson(row);
                        list.add(child);
                    } catch (Exception e) {
                        Logger.Exception(e);
                    }
                }
                result.setGroupList(list.size() > 0 ? list : null);
            }

        } catch (Exception e) {
            Logger.Exception(e);
            return null;
        }

        return result;
    }


    public JSONObject toJson() {
        JSONObject result = new JSONObject();
        JSONObject resource = new JSONObject();
        JSONArray childs = new JSONArray();

        result.putSafe("id", id);
        result.putSafe("name", name);
        result.putSafe("parentId", parentId);
        result.putSafe("resource", this.resource);
        result.putSafe("version", version);
        result.putSafe("isDeleted", isDeleted);

        if (isNotEmpty(uriPrefix) && isNotEmpty(uriSuffix)) {
            resource.putSafe("prefix", uriPrefix);
            resource.putSafe("suffix", uriSuffix);
            result.putSafe("rootResource", resource);
        }

        if (groupList != null)
            for (Subject child : groupList)
                childs.put(child.toJson());

        result.putSafe("privateGroups", groupList != null ? childs : null);
        return result;
    }

    public static String getSubjectUri(JSONObject resource) {
        String result;
        if (resource == null) return null;
        if (resource.has("prefix") && !resource.isNull("prefix") &&
                resource.has("suffix") && !resource.isNull("suffix")) {

            result = ServerTalk.getBaseUri();
            if (isNotEmpty(result))
                return result + resource.getString("prefix", "") + '/' + resource.getString("suffix", "");
        }

        return null;
    }

    public static Subject Get(String id) {
        List<Subject> list = GetSubjectsByClause("guid = ?", new String[]{id});
        if ((list != null) && (list.size() > 0)) {
            return list.get(0);
        }

        return null;
    }

    public static List<Subject> GetCategories() {
        return GetSubjectsByClause("parent == ?", new String[]{""});
    }

    public static List<Subject> GetAllSubjects() {
        return GetSubjectsByClause("parent <> ?", new String[]{""});
    }

    public static List<Subject> GetSubjectsByParent(String categoryId) {
        return GetSubjectsByClause("parent = ?", new String[]{categoryId});
    }

    public static List<Subject> GetAll() {
        return GetSubjectsByClause(null, null);
    }

    public static List<Subject> GetSubjectsByClause(String clause, String[] selectVal) {
        Cursor c = null;
        List<Subject> result = null;
        DBHelper database = Settings.getDatabase();
        if (database == null) return null;
        SQLiteDatabase db = null;
        try {
            db = database.getWritableDatabase();
            c = db.query(Application.AppContext.getString(R.string.subjects_table_name),
                    null, clause, selectVal, null, null, null);

            result = new ArrayList<>();
            if ((c.getCount() <= 0)) {
                return result;
            } else {
                if (c.moveToFirst()) {
                    do {
                        // Subject(String id, String name, String parentId, String resource, Long version, Integer priority) {
                        Subject subject = new Subject(c.getString(0), c.getString(1),
                                c.getString(2), c.getString(3), c.getLong(4), c.getInt(5));

                        result.add(subject);
                    } while (c.moveToNext());

                    //return result;
                }
            }

            c.close();
            if (db.inTransaction()) db.endTransaction();
        } catch (Exception ex) {
            if (c != null) c.close();
            if ((db != null) && db.inTransaction()) db.endTransaction();
            Logger.Exception(ex);
        }

        if (result != null) {
            Collections.sort(result, new Comparator<Subject>() {
                @Override
                public int compare(Subject subject1, Subject subject2) {
                    if (subject1.priority == 0 || subject2.priority == 0) {
                        return 1;
                    }

                    return subject1.priority - subject2.priority;
                }
            });
        }

        return result;
    }
}
