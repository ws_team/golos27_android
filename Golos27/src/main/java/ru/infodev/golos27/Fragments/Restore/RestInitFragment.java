package ru.infodev.golos27.Fragments.Restore;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.CustomEditText;

public class RestInitFragment extends ParamFragment
        implements OnClickListener, TextWatcherAdapter.TextWatcherListener {

    public static final int FRAGMENT_ID = R.layout.profile_password_restore;

    private CustomEditText loginEdit;
    private ProgressBar progressBar;
    private boolean loginGood = false;
    private Button nextButton;

    public RestInitFragment() {
        titleId = R.string.forgot_password;
        setFragmentAllowMenu(false);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        setTitle(titleId);

        // Навешивание обработчиков на поля ввода и кнопки
        nextButton = (Button) v.findViewById(R.id.send_security_code);
        nextButton.setOnClickListener(this);
        loginEdit = (CustomEditText) v.findViewById(R.id.restore_login_edit);
        loginEdit.setOnTextChangedListener(this);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        return v;
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        switch (view.getId()) {
            case R.id.restore_login_edit:
                loginGood = text.length() > 5;
                break;
        }

        if (loginGood) {
            nextButton.setEnabled(true);
        } else {
            nextButton.setEnabled(false);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // pass
    }

    @Override
    public void afterTextChanged(Editable s) {
        // pass
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_security_code:
                String login = loginEdit.getText().toString();
                new InitRestorePasswordTask().execute(login);
                break;
        }
    }

    class InitRestorePasswordTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nextButton.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            JSONObject response = server.initRestorePassword(params[0]);
            try {
                response.put("input_login", params[0]);
            } catch (Exception e) {
                Logger.Exception(e);
            }
            return response;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            progressBar.setVisibility(View.INVISIBLE);
            nextButton.setVisibility(View.VISIBLE);
            nextButton.setEnabled(false);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    nextButton.setEnabled(true);
                    try {
                        JSONObject response = result.getJSONObject("response");
                        String token = response.getString("token");
                        int ttl = response.getInt("codeTTL");
                        result.put("token", token);
                        result.put("codeTTL", ttl);
                    } catch (Exception e) {
                        Logger.Exception(e);
                    }

                    chainStartNextFragment(FRAGMENT_ID, RestConfirmFragment.class, result, 0, false);
                } else {
                    ResultDialog.showError(getActivity(), "Ошибка отправки кода", errorCode, result.getErrorText());
                }
            } catch (Exception e) {
                Logger.Exception(e);
                ResultDialog.showError(getActivity(), "Ошибка отправки кода", ErrorResult.INTERNAL_ERROR);
            }

            nextButton.setEnabled(true);
        }
    }

}
