package ru.infodev.golos27;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Logger;

public class PushIdListenerService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Logger.Log("Push service got refreshed token: " + refreshedToken);
        Settings.setParameter(Settings.PUSH_TOKEN, refreshedToken);
    }
}