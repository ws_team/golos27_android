package ru.infodev.golos27.Fragments.AppealList;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shehabic.droppy.DroppyClickCallbackInterface;
import com.shehabic.droppy.DroppyMenuPopup;
import com.shehabic.droppy.animations.DroppyFadeInAnimation;

import java.util.ArrayList;
import java.util.List;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Address;
import ru.infodev.golos27.Common.Catalogs.Appeal.Appeal;
import ru.infodev.golos27.Common.Catalogs.Appeal.AppealListSingleton;
import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Interfaces.RecycleViewClickInterface;
import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.AppealClaim.SetCategoryFragment;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.Widgets.Adapters.AppealListAdapter;
import ru.infodev.golos27.Widgets.CheckableMenuItem;
import ru.infodev.golos27.Widgets.Lists.NestedRecyclerView;
import ru.infodev.golos27.Widgets.RecyclerViewScrollListener;
import ru.infodev.golos27.Widgets.SwipeToLoad.GoogleHookRefreshHeaderView;
import ru.infodev.golos27.Widgets.SwipeToLoad.OnLoadMoreListener;
import ru.infodev.golos27.Widgets.SwipeToLoad.OnRefreshListener;
import ru.infodev.golos27.Widgets.SwipeToLoad.SwipeToLoadLayout;
import ru.infodev.golos27.Widgets.SwipeToLoad.SwipeTrigger;

import static ru.infodev.golos27.Common.Utils.Utils.getNumEnding;
import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;

public class AppealListFragment extends ParamFragment implements OnRefreshListener,
        OnLoadMoreListener, RecycleViewClickInterface, SwipeTrigger {

    public static final int FRAGMENT_ID = R.layout.appeal_list;
    public static final int FIRST_PAGE = 1;
    private List<Appeal> appealList = new ArrayList<>();
    private static final int page_size = 20;
    private int current_page = FIRST_PAGE;
    private boolean isLoading = false;
    private AppealListAdapter adapter;
    private LinearLayout progress;
    private RecyclerViewScrollListener scrollListener;
    private SwipeToLoadLayout swipeToLoadLayout;
    private TextView emptyContentHint;
    private int mOpenAppealCardPosition = -1;
    private int current_filter = R.string.menu_all;
    private String[] filter_statuses = null;
    private boolean filter_isSubscribed = false;
    private boolean filter_onlyMine = false;
    private View menuAnchorView;
    private View baloonView;
    private TextView baloonTextView;
    private DroppyMenuPopup menu;
    private String[] baloonTextStrings;
    private NestedRecyclerView listView;
    private int totalServerAppeals = 0;
    private boolean listEndReached = false;

    public static String getAuthTitle() {
        return Application.AppContext.getString(R.string.auth_list_title);
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (parameters != null && parameters.getBoolean("MyAppeals", false)) {
            titleId = R.string.my_appeals_line;
        }
    }

    public AppealListFragment() {
        titleId = R.string.appeal_list;
        setFragmentAllowMenu(true);
        setFragmentAllowScroll(false);
    }

    synchronized public boolean setOrCheckLoading(Boolean loading) {
        boolean temp = this.isLoading;
        if (loading != null) this.isLoading = loading;
        return temp;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.Log("AppealList fragment destroyed. Clear list!");
        AppealListSingleton.setAppealList(new ArrayList<Appeal>());
        AppealListSingleton.setAppealsCurrentPage(FIRST_PAGE);
    }

    @Override
    public void onPause() {
        super.onPause();
        List<Appeal> list = adapter.getData();
        Logger.Log("AppealList fragment paused. Saving list! (count=" + String.valueOf(list.size()) + ")");
        AppealListSingleton.setAppealList(list);
        AppealListSingleton.setAppealsCurrentPage(current_page);
        adapter.closePopup();
        if (menu != null) menu.dismiss(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (parameters.getBoolean("MyAppeals", false))
            setActionButtonIconDrawable(R.drawable.filter, R.string.filter_hint);
        else
            setActionButtonIcon(ICON_PLUS);
        setActionButtonVisibility(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_item) {
            if (parameters.getBoolean("MyAppeals", false)) {
                View anchor = null;
                try {
                    Window window = getActivity().getWindow();
                    View v = window.getDecorView();
                    anchor = v.findViewById(R.id.action_item);
                } catch (Exception ignored) {
                }
                if (anchor == null) anchor = menuAnchorView;


                DroppyMenuPopup.Builder menuBuilder = new DroppyMenuPopup.Builder(getContext(), anchor);
                menuBuilder.addMenuItem(new CheckableMenuItem(R.string.menu_all, current_filter)).addSeparator()
                        .addMenuItem(new CheckableMenuItem(R.string.menu_moderated, current_filter)).addSeparator()
                        .addMenuItem(new CheckableMenuItem(R.string.menu_inwork, current_filter)).addSeparator()
                        .addMenuItem(new CheckableMenuItem(R.string.menu_solved, current_filter)).addSeparator()
                        .addMenuItem(new CheckableMenuItem(R.string.menu_rejected, current_filter)).addSeparator()
                        .addMenuItem(new CheckableMenuItem(R.string.menu_subscribed, current_filter));

                menu = menuBuilder
                        .triggerOnAnchorClick(false)
                        .setPopupAnimation(new DroppyFadeInAnimation())
                        .setOnClick(new DroppyClickCallbackInterface() {
                            @Override
                            public void call(View v, int id) {
                                current_filter = id;
                                current_page = 1;
                                String[] statuses = new String[1];
                                filter_onlyMine = parameters.getBoolean("MyAppeals", false);
                                filter_isSubscribed = false;
                                switch (id) {
                                    case R.string.menu_all:
                                        statuses = null;
                                        Logger.Log("All my appeals selected");
                                        setTitle(R.string.my_appeals_line);
                                        break;
                                    case R.string.menu_moderated:
                                        statuses[0] = Appeal.STATUS_MODERATION;
                                        Logger.Log("Moderated appeals selected");
                                        setTitle(R.string.my_appeals_line, R.string.menu_moderated);
                                        break;
                                    case R.string.menu_inwork:
                                        statuses[0] = Appeal.STATUS_INWORK;
                                        Logger.Log("Inwork appeals selected");
                                        setTitle(R.string.my_appeals_line, R.string.menu_inwork);
                                        break;
                                    case R.string.menu_solved:
                                        statuses[0] = Appeal.STATUS_SOLVED;
                                        Logger.Log("Solved appeals selected");
                                        setTitle(R.string.my_appeals_line, R.string.menu_solved);
                                        break;
                                    case R.string.menu_rejected:
                                        statuses[0] = Appeal.STATUS_REJECTED;
                                        Logger.Log("Rejected appeals selected");
                                        setTitle(R.string.my_appeals_line, R.string.menu_rejected);
                                        break;
                                    case R.string.menu_subscribed:
                                        statuses = null;
                                        filter_onlyMine = false;
                                        filter_isSubscribed = true;
                                        Logger.Log("Subscribed appeals selected");
                                        setTitle(R.string.my_appeals_line, R.string.menu_subscribed);
                                        break;
                                }

                                filter_statuses = statuses;
                                if (!swipeToLoadLayout.isLoadingMore() && !swipeToLoadLayout.isRefreshing()) {
                                    showRefreshingProgress();
                                }
                                new GetNextAppeals().execute();
                            }
                        })
                        .build();

                menu.show();
            } else {
                // Пользователь кликнул на + - открываем создание обращения
                chainStartNextFragment(FRAGMENT_ID, SetCategoryFragment.class, null, false, true);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setRetainInstance(true);
        filter_onlyMine = parameters.getBoolean("MyAppeals", false);
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        menuAnchorView = v.findViewById(R.id.menu_anchor);
        setTitle(titleId);

        listView = (NestedRecyclerView) v.findViewById(R.id.swiped_list);
        baloonView = v.findViewById(R.id.baloonButton);
        baloonTextView = (TextView) v.findViewById(R.id.baloonText);
        baloonTextStrings = getResources().getStringArray(R.array.new_appeals_array);
        baloonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRefreshingProgress();
                listView.post(new Runnable() {
                    @Override
                    public void run() {
                        listView.scrollToPosition(0);
                    }
                });

                onRefresh();
            }
        });

        appealList = AppealListSingleton.getAppealList();
        if (appealList == null) appealList = new ArrayList<>();
        if (appealList.size() > 0) current_page = AppealListSingleton.getAppealsCurrentPage();

        emptyContentHint = (TextView) v.findViewById(R.id.empty_content_hint);
        progress = (LinearLayout) v.findViewById(R.id.loading_progress);
        adapter = new AppealListAdapter(Application.AppContext, appealList);
        adapter.setOnClickListener(this);
        GoogleHookRefreshHeaderView headerView = (GoogleHookRefreshHeaderView) v.findViewById(R.id.swipe_refresh_header);
        swipeToLoadLayout = (SwipeToLoadLayout) v.findViewById(R.id.swipeToLoadLayout);

        ((SimpleItemAnimator) listView.getItemAnimator()).setSupportsChangeAnimations(false);
        listView.setHorizontalScrollBarEnabled(false);
        listView.setVerticalScrollBarEnabled(true);
        listView.setNestedScrollingEnabled(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(Application.AppContext);
        layoutManager.setAutoMeasureEnabled(false);
        scrollListener = new RecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (listEndReached) return;
                Logger.Log("Loading next page: " + String.valueOf(page));
                if (!setOrCheckLoading(true)) {
                    if (!swipeToLoadLayout.isLoadingMore() && !swipeToLoadLayout.isRefreshing()) {
                        showLoadingProgress();
                    }
                    new GetNextAppeals().execute();
                }
            }

            @Override
            public void onScrolled(boolean first_item, boolean last_item) {
            }
        };

        swipeToLoadLayout = (SwipeToLoadLayout) v.findViewById(R.id.swipeToLoadLayout);
        swipeToLoadLayout.setOnLoadMoreListener(this);
        swipeToLoadLayout.setOnRefreshListener(this);
        headerView.setOnSwipeListener(this);

        listView.setLayoutManager(layoutManager);
        listView.addOnScrollListener(scrollListener);
        listView.setAdapter(adapter);
        if (appealList.size() <= 0) {
            if (!setOrCheckLoading(true)) {
                if (!swipeToLoadLayout.isLoadingMore() && !swipeToLoadLayout.isRefreshing()) {
                    showRefreshingProgress();
                }
                new GetNextAppeals().execute();
            }
        } else {
            if (mOpenAppealCardPosition >= 0) {
                // Возврат из карточки обращения надо обновить конкретное обращение в списке
                Appeal appeal = adapter.getItem(mOpenAppealCardPosition);
                if (appeal != null) {
                    Appeal changedAppeal = AppealListSingleton.getChangedAppeal();
                    if (changedAppeal != null) {
                        if (appeal.Id().equals(changedAppeal.Id()))
                            adapter.setItem(mOpenAppealCardPosition, changedAppeal);
                        adapter.notifyItemChanged(mOpenAppealCardPosition);
                    }
                    AppealListSingleton.setChangedAppeal(null);
                    mOpenAppealCardPosition = -1;
                }
            }
            emptyContentHint.setVisibility(View.INVISIBLE);
        }
        return v;
    }

    // Догружаем следующую страницу
    @Override
    public void onLoadMore() {
        if (!setOrCheckLoading(true)) {
            new GetNextAppeals().execute();
        }
    }

    // Загружаем заново первую страницу
    @Override
    public void onRefresh() {
        if (!setOrCheckLoading(true)) {
            current_page = FIRST_PAGE;
            new GetNextAppeals().execute();
        }
    }

    private void showLoadingProgress() {
        swipeToLoadLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeToLoadLayout.setLoadMoreEnabled(true);
                swipeToLoadLayout.setLoadingMore(true);
                swipeToLoadLayout.setRefreshEnabled(false);
            }
        });
    }

    private void showRefreshingProgress() {
        swipeToLoadLayout.post(new Runnable() {
            @Override
            public void run() {
                showMoreBaloon(0);
                swipeToLoadLayout.setRefreshEnabled(true);
                swipeToLoadLayout.setRefreshing(true);
                swipeToLoadLayout.setLoadMoreEnabled(false);
            }
        });
    }

    private void showMoreBaloon(int count) {
        if (count > 0) {
            if (!swipeToLoadLayout.isRefreshing()) {
                baloonView.setVisibility(View.VISIBLE);
                String baloonText = String.valueOf(count) + " " + getNumEnding(count, baloonTextStrings);
                baloonTextView.setText(baloonText);
                return;
            }
        }
        baloonView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onItemClick(int position) {
        Appeal appeal = adapter.getData().get(position);
        mOpenAppealCardPosition = position;
        chainStartNextFragment(FRAGMENT_ID, AppealCardFragment.class, appeal.toJson(), 0, false);
    }

    @Override
    public void onItemClick2(int position) {
        JSONObject params = new JSONObject();
        Appeal appeal = adapter.getData().get(position);
        mOpenAppealCardPosition = position;
        if (appeal == null || appeal.AppealClaim() == null || appeal.AppealClaim().Address() == null ||
                appeal.AppealClaim().Address().Latitude() == Address.INVALID_COORDINATE ||
                appeal.AppealClaim().Address().Longitude() == Address.INVALID_COORDINATE) return;

        String address = appeal.AppealClaim().Address().FullAddress();
        if (isEmpty(address)) return;
        params.putSafe("Address", address);
        params.putSafe("Latitude", appeal.AppealClaim().Address().Latitude());
        params.putSafe("Longitude", appeal.AppealClaim().Address().Longitude());
        chainStartNextFragment(FRAGMENT_ID, AppealMapFragment.class, params, false);
    }

    @Override
    public void onPrepare() {
    }

    @Override
    public void onSwipe(int y, boolean isComplete) {
        showMoreBaloon(0);
    }

    @Override
    public void onRelease() {
    }

    @Override
    public void complete() {
    }

    @Override
    public void onReset() {
    }

    private class GetNextAppeals extends AsyncTask<String, Void, JSONObject> implements DialogResultInterface {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setOrCheckLoading(true);
        }

        @Override
        protected JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            String[] statuses = null;
            if (filter_statuses != null && filter_statuses.length > 0) {
                statuses = new String[1];
                statuses[0] = filter_statuses[0];
            }
            return server.getAppealsList(page_size, current_page, filter_onlyMine,
                    filter_isSubscribed, statuses, null);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            int current_total = -1;
            int current_pages = -1;
            JSONArray jsonList = new JSONArray();

            if (result != null) {
                if (result.has("errorCode") && !result.isNull("errorCode")) {
                    ErrorResult retVal = new ErrorResult(result.getInt("errorCode", ErrorResult.UNKNOWN_ERROR),
                            result.getString("errorText", ErrorResult.getErrorText(ErrorResult.UNKNOWN_ERROR)));
                    if (retVal.getErrorCode() != ErrorResult.SUCCESS) {
                        StopLoading();
                        if (fragmentActive) {
                            ResultDialog.makeDialog(Application.AppContext, R.drawable.error_grey,
                                    "Ошибка загрузки\r\nобращений", retVal.getErrorText(),
                                    "Повторить", this).show(getActivity());
                        }
                        return;
                    }

                    try {
                        if (result.has(JSONObject.JSON_LONG_STRING)) {
                            JSONObject temp = new JSONObject(result.getString(JSONObject.JSON_LONG_STRING, ""));
                            if (temp.has("response")) {
                                jsonList = temp.getJSONObject("response").getJSONArray("items");
                                current_total = temp.getJSONObject("response").getInt("totalSize", 0);
                                current_pages = temp.getJSONObject("response").getInt("totalPages", 0);
                            }
                        } else {
                            jsonList = result.getJSONArray("items");
                            current_total = result.getInt("totalSize", 0);
                            current_pages = result.getInt("totalPages", 0);
                        }
                    } catch (Exception e) {
                        Logger.Exception(e);
                    }

                    if (current_page == FIRST_PAGE) {
                        appealList = new ArrayList<>();
                        showMoreBaloon(0);
                    }

                    adapter.setData(appealList);
                    int duplicateCount = 0;
                    for (int i = 0; i < jsonList.length(); i++) {
                        try {
                            JSONObject row = jsonList.getJSONObject(i);
                            Appeal ap = Appeal.fromJson(row);

                            if (ap != null) {
                                boolean newAppeal = true;
                                int appealListCount = appealList.size();
                                //int startFromIndex = Math.max(0, appealListCount - page_size);
                                int startFromIndex = 0;

                                try {
                                    for (int j = startFromIndex; j < appealListCount; j++)
                                        if (appealList.get(j).Id().equals(ap.Id()))
                                            newAppeal = false;

                                    Logger.Log("Adding ap " + ap.Id() + ". Check from " +
                                            String.valueOf(startFromIndex) + " to " + String.valueOf(appealListCount) +
                                            "(" + Boolean.toString(newAppeal) + ")");
                                } catch (Exception e) {
                                    Logger.Exception(e);
                                }

                                if (newAppeal)
                                    appealList.add(ap);
                                else
                                    duplicateCount++;
                            }
                        } catch (Exception e) {
                            Logger.Exception(e);
                        }
                    }

                    // Если количество увеличилось - покажем это балуном
                    if (current_total > totalServerAppeals) {
                        showMoreBaloon(current_total - totalServerAppeals);
                    }

                    Logger.Log("Duplicate appeals found: " + String.valueOf(duplicateCount));
                    if (jsonList.length() == page_size)
                        current_page++;
                    else
                        listEndReached = true;

                    if (current_pages >= 0 && current_total >= 0) {
                        totalServerAppeals = current_total;
                    }

                    adapter.setData(appealList);
                    adapter.notifyDataSetChanged();
                    StopLoading();
                    return;
                }
            }
            StopLoading();
        }

        @Override
        public void DialogSendResult(int dialog_id, int cmd) {
            if (cmd == Activity.RESULT_OK) new GetNextAppeals().execute();
        }

        @Override
        public void DialogSendParamResult(int dialog_id, int cmd, JSONObject params) {
        }
    }

    public void StopLoading() {
        if (appealList.size() <= 0) {
            swipeToLoadLayout.setLoadMoreEnabled(false);
            emptyContentHint.setVisibility(View.VISIBLE);
        } else {
            swipeToLoadLayout.setLoadMoreEnabled(true);
            emptyContentHint.setVisibility(View.INVISIBLE);
        }

        scrollListener.OnEndLoading();
        progress.setVisibility(View.INVISIBLE);
        swipeToLoadLayout.setRefreshEnabled(true);
        swipeToLoadLayout.setRefreshing(false);
        swipeToLoadLayout.setLoadingMore(false);
        setOrCheckLoading(false);
    }
}
