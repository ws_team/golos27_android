package ru.infodev.golos27.Widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Pattern;

import ru.infodev.golos27.Common.Catalogs.OKTMO;
import ru.infodev.golos27.Common.Utils.PhoneUtils;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

@SuppressWarnings("unused")
public class CityAutoCompleteEditText extends AutoCompleteTextView
        implements View.OnTouchListener, View.OnFocusChangeListener, TextWatcherAdapter.TextWatcherListener {

    int resource_blue;
    int resource_grey;
    int resource_black;
    int resource_red;

    private static final int MESSAGE_TEXT_CHANGED = 100;
    private static final int DEFAULT_AUTOCOMPLETE_DELAY = 50;
    private int mAutoCompleteDelay = DEFAULT_AUTOCOMPLETE_DELAY;

    private OnTouchListener touchListener;
    private OnFocusChangeListener focusChangeListener;
    private TextWatcherAdapter.TextWatcherListener textChangeListener;
    private static final int[] STATE_MISTAKE = {R.attr.state_mistake};
    private static final int[] STATE_EMPTY = {R.attr.state_empty};
    private boolean mIsEmpty = true;
    private Drawable xD;
    private Listener listener;
    private OKTMO cityData;

    private View empty_warning_view = null;
    private View upper_name_view = null;
    private View bottom_help_view = null;
    boolean hide_bottom_help = false;

    private boolean format_phone = false;
    private boolean mIsMistake = false;
    private int empty_warning_id;
    private int upper_name_id;
    private int bottom_help_id;

    public CityAutoCompleteEditText(Context context) {
        super(context);
        init(context, null);
    }

    public CityAutoCompleteEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CityAutoCompleteEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void setCity(OKTMO city) {
        cityData = city;
        setText(city.toString());
        setTextColor(resource_black);
        cityData = city;
    }

    public OKTMO getCity() {
        return cityData;
    }

    @SuppressWarnings("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            CityAutoCompleteEditText.super.performFiltering((CharSequence) msg.obj, msg.arg1);
        }
    };

    @Override
    protected void performFiltering(CharSequence text, int keyCode) {
//        if (mLoadingIndicator != null) {
//            mLoadingIndicator.setVisibility(View.VISIBLE);
//        }
        mHandler.removeMessages(MESSAGE_TEXT_CHANGED);
        mHandler.sendMessageDelayed(mHandler.obtainMessage(MESSAGE_TEXT_CHANGED, text), mAutoCompleteDelay);
    }

    @Override
    public void onFilterComplete(int count) {
//        if (mLoadingIndicator != null) {
//            mLoadingIndicator.setVisibility(View.GONE);
//        }
        super.onFilterComplete(count);
    }

    public void setAutoCompleteDelay(int autoCompleteDelay) {
        mAutoCompleteDelay = autoCompleteDelay;
    }

    @SuppressWarnings("unused")
    public void setEmpty(boolean isEmpty) {
        mIsEmpty = isEmpty;
        refreshDrawableState();
    }

    public void setMistake(boolean isMistake) {
        mIsMistake = isMistake;
        refreshDrawableState();
    }

    @SuppressWarnings("unused")
    public int getEmpty_warning_id() {
        return empty_warning_id;
    }

    @SuppressWarnings("unused")
    public int getUpper_name_id() {
        return upper_name_id;
    }

    @SuppressWarnings("unused")
    public int getBottom_help_id() {
        return bottom_help_id;
    }

    public interface Listener {
        void didClearText();
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);

        if (mIsMistake) {
            mergeDrawableStates(drawableState, STATE_MISTAKE);
        }

        if (bottom_help_view != null) {
            if (mIsMistake) {
                ((TextView) bottom_help_view).setTextColor(resource_red);
            } else {
                ((TextView) bottom_help_view).setTextColor(resource_grey);
            }
        }

        if (upper_name_view != null) {
            int upper_name_color = resource_grey;

            if (mIsMistake) {
                upper_name_color = resource_red;
            } else {
                if (isFocused()) {
                    upper_name_color = resource_blue;
                }
            }

            ((TextView) upper_name_view).setTextColor(upper_name_color);
        }

        if (mIsEmpty) {
            mergeDrawableStates(drawableState, STATE_EMPTY);
        }
        return drawableState;
    }

    @SuppressWarnings("unused")
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        this.touchListener = l;
    }

    public void setOnTextChangedListener(TextWatcherAdapter.TextWatcherListener l) {
        this.textChangeListener = l;
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener f) {
        this.focusChangeListener = f;
    }

    @Override
    public void onSelectionChanged(int start, int end) {
        if (format_phone) {
            CharSequence text = getText();
            if (text != null) {
                if (start != text.length() || end != text.length()) {
                    setSelection(text.length());
                    return;
                }
            }
        }
        super.onSelectionChanged(start, end);
    }

    private boolean mFormatting;

    @Override
    public void onTextChanged(EditText view, String text) {
        if ((format_phone) && text.equals(PhoneUtils.getNumberPrefix())) {
            if (!hasFocus()) {
                text = "";
                if (!mFormatting) {
                    mFormatting = true;
                    view.setText("");
                    mFormatting = false;
                }
            }
            setClearIconVisible(false);
        } else {
            if (hasFocus()) {
                setClearIconVisible(isNotEmpty(text));
            } else {
                setClearIconVisible(false);
            }
        }
        if ((upper_name_view != null) && (text.length() > 0))
            upper_name_view.setVisibility(VISIBLE);
        if (textChangeListener != null) {
            textChangeListener.onTextChanged(view, text);
        }

        cityData = null;
        setTextColor(resource_grey);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        if (textChangeListener != null) {
            textChangeListener.beforeTextChanged(s, start, count, after);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (textChangeListener != null) {
            textChangeListener.afterTextChanged(s);
        }

        if (format_phone) {
            if (!mFormatting) {
                mFormatting = true;
                if (s.length() > 0) PhoneUtils.formatNumber(s);
                mFormatting = false;
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (getCompoundDrawables()[2] != null) {
            int right = getWidth();
            int left = right - (xD.getIntrinsicWidth() * 2) - getPaddingRight();
            int bottom = getHeight();
            int top = 0;
            boolean tappedX = new Rect(left, top, right, bottom).contains((int) event.getX(), (int) event.getY());
            if (tappedX) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (format_phone) {
                        setText(PhoneUtils.getNumberPrefix());
                    } else {
                        cityData = null;
                        setText("");
                        cityData = null;
                    }

                    setClearIconVisible(false);
                    requestFocus();
                    if (listener != null) {
                        listener.didClearText();
                    }
                }
                return true;
            }
        }

        return touchListener != null && touchListener.onTouch(v, event);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (cityData == null) {
            setText("");
        }

        if (focusChangeListener != null) {
            focusChangeListener.onFocusChange(v, hasFocus);
        }

        if (format_phone) {
            String prefix = PhoneUtils.getNumberPrefix();
            if (hasFocus) {
                if (getText().length() <= 0) setText(prefix);
            } else {
                if (getText().toString().equals(prefix)) setText("");
            }
            setSelection(getText().length());
        }

        // Теперь обработаем видимость названия поля (если поле пустое - прятать)
        findSatellites();
        hide_label_by_focus(v, hasFocus);
        if ((hide_bottom_help) && (bottom_help_view != null)) {
            if (hasFocus) {
                bottom_help_view.setVisibility(VISIBLE);
                setClearIconVisible(isNotEmpty(getText().toString()));
            } else {
                bottom_help_view.setVisibility(INVISIBLE);
                setClearIconVisible(false);
            }
        }

        if (hasFocus) {
            setClearIconVisible(isNotEmpty(getText().toString()));
        } else {
            setClearIconVisible(false);
        }
    }

    private void hide_label_by_focus(View v, boolean hasFocus) {
        if (upper_name_view != null) {
            if (hasFocus) {
                upper_name_view.setVisibility(VISIBLE);
            } else {
                String text = ((EditText) v).getText().toString();
                if (text.isEmpty()) {
                    upper_name_view.setVisibility(INVISIBLE);
                } else {
                    upper_name_view.setVisibility(VISIBLE);
                }
            }
        }
    }

    private void init(Context context, AttributeSet attrs) {

        // Тупорылая AndroidStudio на предварительном просмотре сыпется при получении цвета
        // так как пытается загрузить цвет как drawable. Вот собственно костыли ибо дико раздражает!
        resource_blue = ContextCompat.getColor(context, R.color.colorPrimary);
        resource_grey = ContextCompat.getColor(context, R.color.colorGrayText);
        resource_black = ContextCompat.getColor(context, R.color.colorBlackText);
        resource_red = ContextCompat.getColor(context, R.color.colorErrorText);

        empty_warning_id = -1;
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText, 0, 0);
            try {
                empty_warning_id = a.getResourceId(R.styleable.CustomEditText_empty_warning_id, 0);
                upper_name_id = a.getResourceId(R.styleable.CustomEditText_upper_name_id, 0);
                bottom_help_id = a.getResourceId(R.styleable.CustomEditText_bottom_help_id, 0);
                hide_bottom_help = a.getBoolean(R.styleable.CustomEditText_hide_bottom_help, false);
                format_phone = a.getBoolean(R.styleable.CustomEditText_format_phone, false);
            } finally {
                a.recycle();
            }
        }

        xD = getCompoundDrawables()[2];
        if (xD == null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                xD = getResources().getDrawable(R.drawable.text_clear, null);
            } else {
                //noinspection deprecation
                xD = getResources().getDrawable(R.drawable.text_clear);
            }
        }
        assert xD != null;
        xD.setBounds(0, 0, xD.getIntrinsicWidth(), xD.getIntrinsicHeight());
        setClearIconVisible(false);
        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
        addTextChangedListener(new TextWatcherAdapter(this, this));
        findSatellites();
        if ((empty_warning_view != null) && (getText().length() > 0)) {
            empty_warning_view.setVisibility(INVISIBLE);
        }

        super.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // setText очищает cityData, для обработчиков приходится ставить ее дважды
                cityData = (OKTMO) getAdapter().getItem(position);
                setText(getText());
                cityData = (OKTMO) getAdapter().getItem(position);
                setTextColor(resource_black);
            }
        });

        setTextColor(resource_grey);
    }

    @Override
    public void setOnItemClickListener(AdapterView.OnItemClickListener l) {
        // Запрет изменения обработчика. Клик обрабатывается внутри.
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (!enabled) {
            setCompoundDrawables(getCompoundDrawables()[0],
                    getCompoundDrawables()[1], null, getCompoundDrawables()[3]);
        }
    }

    protected void setClearIconVisible(boolean visible) {
        boolean wasVisible = (getCompoundDrawables()[2] != null);
        if (visible != wasVisible) {
            Drawable x = visible ? xD : null;
            if (!isEnabled()) x = null;
            setCompoundDrawables(getCompoundDrawables()[0],
                    getCompoundDrawables()[1], x, getCompoundDrawables()[3]);

        }
        // Проверка на предмет дополнительных связанных текстовых полей
        findSatellites();

        // Обновляем связанное поле с предупреждением
        if (empty_warning_view != null) {
            if (visible) {
                empty_warning_view.setVisibility(INVISIBLE);
            } else {
                empty_warning_view.setVisibility(VISIBLE);
            }
        }
    }

    protected void findSatellites() {
        // Поиск текстового поля с предупреждением
        if (empty_warning_view == null) {
            try {
                if (empty_warning_id >= 0) {
                    View parent = (View) getParent();
                    if (parent != null) {
                        empty_warning_view = parent.findViewById(empty_warning_id);
                    }
                }
            } catch (Exception e) {
                //
            }
        }

        // Поиск текстового поля с предупреждением
        if (upper_name_view == null) {
            try {
                if (upper_name_id >= 0) {
                    View parent = (View) getParent();
                    if (parent != null) {
                        View p_parent = (View) parent.getParent();
                        if (p_parent != null) {
                            upper_name_view = p_parent.findViewById(upper_name_id);
                        }
                    }
                }
            } catch (Exception e) {
                //
            }
        }

        // Поиск нижней подсказки
        if (bottom_help_view == null) {
            try {
                if (bottom_help_id >= 0) {
                    View parent = (View) getParent();
                    if (parent != null) {
                        View p_parent = (View) parent.getParent();
                        if (p_parent != null) {
                            bottom_help_view = p_parent.findViewById(bottom_help_id);
                        }
                    }
                }
            } catch (Exception e) {
                //
            }
        }
    }

    /***
     * Проверяет поле с помощью прилагаемого регулярного выражения
     *
     * @param pattern         шаблон регулярного выражения для проверки
     * @param checkEmpty      проверять поле на пустое содержимое
     * @param emptySetMistake устанавливать ошибку если поле пустое
     * @return true если поле валидное
     */
    public boolean checkValue(Pattern pattern, boolean checkEmpty, boolean emptySetMistake) {
        return checkValue(pattern, checkEmpty, emptySetMistake, -1, -1);
    }

    /***
     * Проверяет поле с помощью прилагаемого регулярного выражения
     *
     * @param pattern         шаблон регулярного выражения для проверки
     * @param checkEmpty      проверять поле на пустое содержимое
     * @param emptySetMistake устанавливать ошибку если поле пустое
     * @param min_length      минимальная длина введенной строки
     * @param max_length      максимальная длина введенной строки
     * @return true если поле валидное
     */
    public boolean checkValue(Pattern pattern, boolean checkEmpty, boolean emptySetMistake, int min_length, int max_length) {
        boolean result = true;
        boolean mistake = false;

        if (!pattern.matcher(getText()).matches()) {
            result = false;
            mistake = true;
        }

        if ((min_length > 0) && (getText().toString().length() < min_length)) {
            mistake = true;
            result = false;
        }

        if ((max_length > 0) && (getText().toString().length() > max_length)) {
            mistake = true;
            result = false;
        }

        if (getText().toString().equals("")) {
            result = !checkEmpty;
            mistake = emptySetMistake;
        }

        setMistake(mistake);
        return result;
    }

}
