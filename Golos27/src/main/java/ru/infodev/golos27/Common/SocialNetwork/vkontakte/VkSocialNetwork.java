package ru.infodev.golos27.Common.SocialNetwork.vkontakte;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiLink;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKAttachments;
import com.vk.sdk.api.model.VKPhotoArray;
import com.vk.sdk.api.model.VKWallPostResult;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;
import com.vk.sdk.dialogs.VKShareDialogBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import ru.infodev.golos27.Common.SocialNetwork.AccessToken;
import ru.infodev.golos27.Common.SocialNetwork.SocialNetwork;
import ru.infodev.golos27.Common.SocialNetwork.SocialNetworkException;
import ru.infodev.golos27.Common.SocialNetwork.listener.OnLoginCompleteListener;
import ru.infodev.golos27.Common.SocialNetwork.listener.OnPostingCompleteListener;
import ru.infodev.golos27.Common.SocialNetwork.listener.OnRequestAccessTokenCompleteListener;
import ru.infodev.golos27.Common.Utils.Logger;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

/**
 * Class for VK social network integration
 */
public class VkSocialNetwork extends SocialNetwork {
    /*** Social network ID in asne modules, should be unique*/
    public static final int ID = 5;
    private static final String SAVE_STATE_KEY_OAUTH_TOKEN = "VkSocialNetwork.SAVE_STATE_KEY_OAUTH_TOKEN";
    private static final String SAVE_STATE_KEY_OAUTH_SECRET = "VkSocialNetwork.SAVE_STATE_KEY_OAUTH_SECRET";
    private static final String SAVE_STATE_KEY_USER_ID = "VkSocialNetwork.SAVE_STATE_KEY_USER_ID";
    /*** Developer activity*/
    private Activity mActivity;
    /*** VK app id*/
    private String mKey;
    /*** VK access token*/
    private VKAccessToken mAccessToken;
    /*** Id of current user*/
    private String mUserId;
    /*** Permissions array*/
    private String[] mPermissions;
    /*** VK SDK listener to catch authorization @see <a href="http://vkcom.github.io/vk-android-sdk/com/vk/sdk/VKSdkListener.html">VKSdkListener</a>*/
//    private final VKSdkListener mVkSdkListener = new VKSdkListener() {
//        @Override
//        public void onCaptchaError(VKError captchaError) {
//            new VKCaptchaDialog(captchaError).show();
//        }
//
//        @Override
//        public void onTokenExpired(VKAccessToken expiredToken) {
//            VKSdk.authorize(mPermissions, true, false);
//        }
//
//        @Override
//        public void onAccessDenied(VKError authorizationError) {
//            mLocalListeners.get(REQUEST_LOGIN).onError(getID(), REQUEST_LOGIN,
//                    authorizationError.toString(), null);
//        }
//
//        @Override
//        public void onReceiveNewToken(VKAccessToken accessToken) {
//            mAccessToken = accessToken;
//            mSharedPreferences.edit()
//                    .putString(SAVE_STATE_KEY_OAUTH_TOKEN, accessToken.accessToken)
//                    .putString(SAVE_STATE_KEY_OAUTH_SECRET, accessToken.secret)
//                    .putString(SAVE_STATE_KEY_USER_ID, accessToken.userId)
//                    .apply();
//            if (mLocalListeners.get(REQUEST_LOGIN) != null) {
//                ((OnLoginCompleteListener) mLocalListeners.get(REQUEST_LOGIN)).onLoginSuccess(getID());
//                mLocalListeners.remove(REQUEST_LOGIN);
//            }
//            mUserId = accessToken.userId;
//        }
//
//        @Override
//        public void onAcceptUserToken(VKAccessToken accessToken) {
//            mAccessToken = accessToken;
//            mSharedPreferences.edit()
//                    .putString(SAVE_STATE_KEY_OAUTH_TOKEN, accessToken.accessToken)
//                    .putString(SAVE_STATE_KEY_OAUTH_SECRET, accessToken.secret)
//                    .putString(SAVE_STATE_KEY_USER_ID, accessToken.userId)
//                    .apply();
//            mUserId = accessToken.userId;
//        }
//    };
    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken != null) {
                mAccessToken = newToken;
                mSharedPreferences.edit()
                        .putString(SAVE_STATE_KEY_OAUTH_TOKEN, newToken.accessToken)
                        .putString(SAVE_STATE_KEY_OAUTH_SECRET, newToken.secret)
                        .putString(SAVE_STATE_KEY_USER_ID, newToken.userId)
                        .apply();
                if (mLocalListeners.get(REQUEST_LOGIN) != null) {
                    ((OnLoginCompleteListener) mLocalListeners.get(REQUEST_LOGIN)).onLoginSuccess(getID());
                    mLocalListeners.remove(REQUEST_LOGIN);
                }
                mUserId = newToken.userId;
            }
        }
    };

    public VkSocialNetwork(Fragment fragment, String key, String[] permissions) {
        super(fragment);
        this.mKey = key;
        this.mPermissions = permissions;
        this.mActivity = fragment.getActivity();
        int wat = Integer.parseInt(mKey);
        vkAccessTokenTracker.startTracking();
        VKSdk.customInitialize(mActivity.getApplicationContext(),wat,null);// initialize(mActivity.getApplicationContext());
    }

//    public VkSocialNetwork(Fragment fragment, Context context, String key, String[] permissions) {
//        super(fragment, context);
//        this.key = key;
//        this.permissions = permissions;
//    }

    private static boolean stringToBool(String s) {
        if (s.equals("1"))
            return true;
        if (s.equals("0"))
            return false;
        throw new IllegalArgumentException(s+" is not a bool. Only 1 and 0 are.");
    }

    /*** Get current user id after authorization for inner use*/
    private void requestIdPerson() {
        VKRequest request = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS,"id"));
        request.secure = false;
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                try {
                    JSONObject jsonResponse = response.json.getJSONArray("response").getJSONObject(0);
                    mUserId = jsonResponse.getString("id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(VKError error) {
                throw new SocialNetworkException("Error in id request! " + error);

            }
            @Override
            public void onProgress(VKRequest.VKProgressType progressType,
                                   long bytesLoaded,
                                   long bytesTotal) {
            }
            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                throw new SocialNetworkException("Fail in id request!");
            }
        });
    }

    /**
     * Check is social network connected
     * @return true if connected to VK social network and false if not
     */
    @Override
    public boolean isConnected() {
        return VKSdk.isLoggedIn();
    }

    /**
     * Make login request - authorize in VK social network
     * @param onLoginCompleteListener listener to trigger when Login complete
     */
    @Override
    public void requestLogin(OnLoginCompleteListener onLoginCompleteListener) {
        super.requestLogin(onLoginCompleteListener);
        VKSdk.login(mActivity, mPermissions);
    }

    /**
     * Logout from VK social network
     */
    @Override
    public void logout() {
        VKSdk.logout();
    }

    /**
     * Get id of VK social network
     * @return Social network id for VK = 5
     */
    @Override
    public int getID() {
        return ID;
    }

    /**
     * Method to get AccessToken of VK social network
     * @return AccessToken
     */
    @Override
    public AccessToken getAccessToken() {
        return new AccessToken(mSharedPreferences.getString(SAVE_STATE_KEY_OAUTH_TOKEN, null),
                mSharedPreferences.getString(SAVE_STATE_KEY_OAUTH_SECRET, null));
    }

    /**
     * Request AccessToken of VK social network that you can get from onRequestAccessTokenCompleteListener
     * @param onRequestAccessTokenCompleteListener listener for AccessToken request
     */
    @Override
    public void requestAccessToken(OnRequestAccessTokenCompleteListener onRequestAccessTokenCompleteListener) {
        super.requestAccessToken(onRequestAccessTokenCompleteListener);
        ((OnRequestAccessTokenCompleteListener) mLocalListeners.get(REQUEST_ACCESS_TOKEN))
                .onRequestAccessTokenComplete(getID(),
                        new AccessToken(mSharedPreferences.getString(SAVE_STATE_KEY_OAUTH_TOKEN, null),
                                mSharedPreferences.getString(SAVE_STATE_KEY_OAUTH_SECRET, null)));
    }

    /**
     * Post message to social network
     * @param message message that should be shared
     * @param onPostingCompleteListener listener for posting request
     */
    @Override
    public void requestPostMessage(String message, OnPostingCompleteListener onPostingCompleteListener) {
        super.requestPostMessage(message, onPostingCompleteListener);
        makePost(null, message, REQUEST_POST_MESSAGE);
    }

    /**
     * Post photo to social network
     * @param photo photo that should be shared
     * @param message message that should be shared with photo
     * @param onPostingCompleteListener listener for posting request
     */
    @Override
    public void requestPostPhoto(File photo, final String message, OnPostingCompleteListener onPostingCompleteListener) {
        super.requestPostPhoto(photo, message, onPostingCompleteListener);
        final Bitmap vkPhoto = getPhoto(photo);
        VKRequest request = VKApi.uploadWallPhotoRequest(new VKUploadImage(vkPhoto, VKImageParameters.pngImage()), 0, Integer.parseInt(mUserId));
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                VKApiPhoto photoModel = ((VKPhotoArray) response.parsedModel).get(0);
                makePost(new VKAttachments(photoModel), message, REQUEST_POST_PHOTO);
                vkPhoto.recycle();
            }
            @Override
            public void onError(VKError error) {
                mLocalListeners.get(REQUEST_POST_PHOTO).onError(getID(), REQUEST_POST_PHOTO,
                        error.toString(), null);
            }
        });
    }

    private Bitmap getPhoto(File photo) {
        Bitmap b = null;
        try {
            b = BitmapFactory.decodeStream(new FileInputStream(photo));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return b;
    }

    /**
     * Post link with comment to social network
     * @param bundle bundle containing information that should be shared(Bundle constants in SocialNetwork)
     * @param message message that should be shared with bundle
     * @param onPostingCompleteListener listener for posting request
     */
    @Override
    public void requestPostLink(Bundle bundle, String message, OnPostingCompleteListener onPostingCompleteListener) {
        super.requestPostLink(bundle, message, onPostingCompleteListener);
        VKApiLink vkLink = new VKApiLink();
        String link = bundle.getString(BUNDLE_LINK);
        if((link != null) && (link.length() != 0)) vkLink.url = link;
        String name = bundle.getString(BUNDLE_NAME);
        if((name != null) && (name.length() != 0)) vkLink.title = name;
        String description = bundle.getString(BUNDLE_MESSAGE);
        if((description != null) && (description.length() != 0)) vkLink.description = description;
        String picture = bundle.getString(BUNDLE_PICTURE);
        if((picture != null) && (picture.length() != 0)) vkLink.image_src = picture;
        VKAttachments attachments = new VKAttachments();
        attachments.add(vkLink);
        makePost(attachments, message, REQUEST_POST_LINK);
    }

    /**
     * Not supported via vk sdk - in development
     * @throws SocialNetworkException
     * @param bundle bundle containing information that should be shared(Bundle constants in SocialNetwork)
     * @param onPostingCompleteListener listener for posting request
     */
    @Override
    public void requestPostDialog(Bundle bundle, final OnPostingCompleteListener onPostingCompleteListener) {
        super.requestPostDialog(bundle, onPostingCompleteListener);
        String link = bundle.getString(BUNDLE_LINK);
        String name = bundle.getString(BUNDLE_NAME);
        String description = bundle.getString(BUNDLE_MESSAGE);
        Bitmap bitmap = bundle.getParcelable(BUNDLE_BITMAP);

        VKShareDialogBuilder vkShareDialogBuilder = new VKShareDialogBuilder();
        if (isNotEmpty(description)) vkShareDialogBuilder.setText(name + "\r\n\r\n" + description);
        if (isNotEmpty(link) && isNotEmpty(name)) vkShareDialogBuilder.setAttachmentLink("", link);
        if (bitmap != null) vkShareDialogBuilder.setAttachmentImages(new VKUploadImage[] {
                new VKUploadImage(bitmap, VKImageParameters.pngImage())
        });

        vkShareDialogBuilder.setShareDialogListener(new VKShareDialogBuilder.VKShareDialogListener() {
            @Override
            public void onVkShareComplete(int postId) {
                Logger.Log("Posting complete (success)");
                if (onPostingCompleteListener != null)
                    onPostingCompleteListener.onPostSuccessfully(getID());
            }

            @Override
            public void onVkShareCancel() {
                Logger.Log("Posting canceled");
                if (onPostingCompleteListener != null)
                    onPostingCompleteListener.onError(getID(), REQUEST_POST_DIALOG, "User cancel posting", null);
            }

            @Override
            public void onVkShareError(VKError error) {
                Logger.Log("Posting error (" + error.toString() + ")");
                if (onPostingCompleteListener != null)
                    onPostingCompleteListener.onError(getID(), REQUEST_POST_DIALOG, error.errorMessage, null);
            }
        });

        vkShareDialogBuilder.show(mActivity.getFragmentManager(), "vkShareDialogBuilder.TAG");
        //throw new SocialNetworkException("requestPostDialog isn't allowed for VKSocialNetwork");
    }

    private void makePost(VKAttachments attachments, final String message, final String requestID) {
        VKRequest post = VKApi.wall().post(VKParameters.from(VKApiConst.ATTACHMENTS, attachments, VKApiConst.MESSAGE, message));
        post.setModelClass(VKWallPostResult.class);
        post.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                ((OnPostingCompleteListener) mLocalListeners.get(requestID)).onPostSuccessfully(getID());
            }

            @Override
            public void onError(VKError error) {
                mLocalListeners.get(requestID).onError(getID(), requestID,
                        error.toString(), null);
            }
        });
    }

    /**
     * Overrided for connect vk to activity
     * @param savedInstanceState If the activity is being re-initialized after previously being shut down then this
     *                           Bundle contains the data it most recently supplied in
     *                           onSaveInstanceState(Bundle). Note: Otherwise it is null.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        this.mActivity = mSocialNetworkManager.getActivity();
//        VKSdk.initialize(mActivity);
//        VKUIHelper.onCreate(mActivity);
//        VKSdk.initialize(mVkSdkListener, mKey);
//        VKSdk.wakeUpSession();

        if(isConnected()) {
            mUserId = mSharedPreferences.getString(SAVE_STATE_KEY_USER_ID, null);
            if (mUserId == null){
                requestIdPerson();
            }
        }
    }

    /**
     * Overrided for VK support
     */
    @Override
    public void onResume() {
        super.onResume();
//        VKUIHelper.onResume(mActivity);
    }

    /**
     * Overrided for VK support
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
//        VKUIHelper.onDestroy(mActivity);
    }

    /**
     * Overrided for VK support
     * @param requestCode The integer request code originally supplied to startActivityForResult(), allowing you to identify who this result came from.
     * @param resultCode The integer result code returned by the child activity through its setResult().
     * @param data An Intent, which can return result data to the caller (various data can be attached to Intent "extras").
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        int sanitizedRequestCode = requestCode & 0xFFFF;
        // VKUIHelper.onActivityResult(sanitizedRequestCode, resultCode, data);
        if (!VKSdk.onActivityResult(sanitizedRequestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                Log.e("VKSocial", "Login");
            }
            @Override
            public void onError(VKError error) {
                mLocalListeners.get(REQUEST_LOGIN).onError(getID(), REQUEST_LOGIN, error.toString(), null);
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
