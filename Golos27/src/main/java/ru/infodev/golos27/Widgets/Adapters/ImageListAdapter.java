package ru.infodev.golos27.Widgets.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

import ru.infodev.golos27.Common.Catalogs.Appeal.AppealAttachment;
import ru.infodev.golos27.Common.Interfaces.RecycleViewClickInterface;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

/**
 * Класс адаптера наследуется от RecyclerView.Adapter с указанием класса, который будет хранить
 * ссылки на виджеты элемента списка, т.е. класса, имплементирующего ViewHolder. В нашем
 * случае класс объявлен внутри класса адаптера.
 */
public class ImageListAdapter extends RecyclerView.Adapter {

    private List<AppealAttachment> data;
    private LayoutInflater inflater;
    private RecycleViewClickInterface clickInterface;

    /***
     * При создании адаптера загружаются все иконки для ускорения отрисовки
     * и инициализируется список с обращениями.
     *
     * @param context    контекст
     * @param appealList список обращений
     */
    public ImageListAdapter(Context context, List<AppealAttachment> appealList) {
        this.data = appealList;
        inflater = LayoutInflater.from(context);
    }

    public void setOnClickListener(RecycleViewClickInterface listener) {
        clickInterface = listener;
    }

    /***
     * Устанавливает данные в адаптер
     *
     * @param appealList список обращений
     */
    public void setData(List<AppealAttachment> appealList) {
        this.data = appealList;
    }

    /***
     * Возвращает список с данными привязанный к адаптеру
     *
     * @return список обращений
     */
    public List<AppealAttachment> getData() {
        return this.data;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые
     * впоследствии могут переиспользоваться.
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = inflater.inflate(R.layout.item_image_grid, viewGroup, false);
        return new ItemViewHolder(v);
    }

    /**
     * Заполнение элементов данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int i) {
        AppealAttachment entry = getData().get(i);
        final ItemViewHolder viewHolder = (ItemViewHolder) holder;

        // Асинхронная загрузка изображений с помощью библиотеки UIL
        viewHolder.selectSign.setVisibility(View.INVISIBLE);
        viewHolder.imageView.setImageDrawable(null);
        String documentThumbId = entry.getThumbnailId();

        if (isNotEmpty(documentThumbId)) {
            try {
                String imageUri = ServerTalk.getDocumentUri(documentThumbId);
                ImageLoader.getInstance().displayImage(imageUri, viewHolder.imageView, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        //noinspection ThrowableResultOfMethodCallIgnored
                        Throwable cause = failReason.getCause();
                        String reason = "unknown";
                        if (cause != null) reason = cause.toString();
                        Logger.Log("Loading " + imageUri + " failed. Reason: " + reason);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        Logger.Log("Loading " + imageUri + " failed. Reason: canceled");
                    }
                });
            } catch (Exception e) {
                Logger.Exception(e);
            }
        }
    }

    @Override
    public int getItemCount() {
        return getData().size();
    }

    public AppealAttachment getItem(int position) {
        return data.get(position);
    }

    /**
     * Реализация класса ViewHolder, хранящего ссылки на виджеты
     * Чтобы их не надо было каждый раз искать с помощью findViewById
     * При инициализации заполняет все ссылки
     */
    class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private FrameLayout selectSign;

        // Конструктор - здесь ищем все заполняемые элементы
        public ItemViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image_thumb);
            selectSign = (FrameLayout) itemView.findViewById(R.id.selected_sign);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickInterface != null) clickInterface.onItemClick(getAdapterPosition());
                }
            });

        }
    }
}
