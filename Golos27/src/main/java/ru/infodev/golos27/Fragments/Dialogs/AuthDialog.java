package ru.infodev.golos27.Fragments.Dialogs;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.MainActivity;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

/***
 * Вызывается из треда UI. Открывает диалог с указанными параметрами
 */
@SuppressWarnings("unused")
public class AuthDialog {
    AuthDialogFragment rDialog;

    public AuthDialog(Context context, DialogResultInterface listener, JSONObject content, String title) {
        try {
            Bundle arguments = new Bundle();
            rDialog = new AuthDialogFragment();
            rDialog.setResultListener(listener);

            if (isNotEmpty(title)) {
                if (content == null) content = new JSONObject();
                content.putSafe("title", title);
            }

            arguments.putString(AuthDialogFragment.BUNDLE_CONTENT, content != null ? content.toString() : "{}");
            rDialog.setArguments(arguments);
            rDialog.setStyle(DialogFragment.STYLE_NO_FRAME, 0);
        } catch (Exception e) {
            Logger.Exception(e);
        }
    }

    public void setDismiss(boolean cancelable) {
        rDialog.setCancelable(cancelable);
    }

    public static AuthDialog makeDialog(Context context) {
        return new AuthDialog(context, null, null, null);
    }

    public static AuthDialog makeDialog(Context context, DialogResultInterface listener) {
        return new AuthDialog(context, listener, null, null);
    }

    public static AuthDialog makeDialog(Context context, DialogResultInterface listener, JSONObject content, String title) {
        return new AuthDialog(context, listener, content, title);

    }

    public void show(Activity activity, FragmentManager fragmentManager) {
        if (activity != null) {
            if (activity instanceof MainActivity) {
                if (((MainActivity) activity).isActive)
                    rDialog.show(fragmentManager, "AuthDialog");
            } else {
                rDialog.show(fragmentManager, "AuthDialog");
            }
        }
    }
}
