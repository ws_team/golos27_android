package ru.infodev.golos27.Fragments.Registration;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.CustomEditText;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

public class RegConfirmFragment extends ParamFragment implements
        View.OnClickListener, TextWatcherAdapter.TextWatcherListener, DialogResultInterface {
    public static final int FRAGMENT_ID = R.layout.registration_confirm;
    private Button button;
    private CustomEditText codeEditText;
    private ProgressBar progress;
    private Timer timer;
    private TextView timerText;
    private CountdownTask timerTask;
    private String stringTimerText;
    private TextView changeNotificationView;
    private TextView screenHeaderView;
    private String stringHeaderPhone;
    private String stringHeaderEmail;
    private int minCodeLength = 0;
    boolean buttonSendAction = true;

    public RegConfirmFragment() {
        titleId = R.string.confirmation;
        setFragmentAllowMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        setTitle(titleId);

        button = (Button) v.findViewById(R.id.send_security_code_button);
        button.setOnClickListener(this);

        stringTimerText = getResources().getString(R.string.send_the_code_again);
        progress = (ProgressBar) v.findViewById(R.id.progressBar);
        codeEditText = (CustomEditText) v.findViewById(R.id.edit_code);
        codeEditText.setOnTextChangedListener(this);

        changeNotificationView = (TextView) v.findViewById(R.id.notification_by_email);
        switchChangeNotificationView(false);
        changeNotificationView.setOnClickListener(this);

        timerText = (TextView) v.findViewById(R.id.send_the_code_again);
        StopTimer();
        timer = new Timer();
        timerTask = new CountdownTask();
        timer.schedule(timerTask, 1000, 1000);

        stringHeaderPhone = getResources().getString(R.string.confirmation_entry_up_phone);
        stringHeaderEmail = getResources().getString(R.string.confirmation_entry_up_email);
        screenHeaderView = (TextView) v.findViewById(R.id.confirmation_of_entry_text);

        String phone = (String) parameters.get("input_phone", "");
        String email = (String) parameters.get("input_email", "");
        setScreenHeader(phone, email);
        setupEditTextIme(phone, email);

        return v;
    }

    @SuppressWarnings("UnusedParameters")
    private void setupEditTextIme(String phone, String email) {
        // Телефон - 6 цифр, email - 10 букв и цифр
        minCodeLength = 10;
        if (isNotEmpty(phone)) {
            minCodeLength = 6;
            codeEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
            codeEditText.setMaxLines(1);
            codeEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(minCodeLength)});
        } else {
            codeEditText.setInputType(InputType.TYPE_CLASS_TEXT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                codeEditText.setImeOptions(EditorInfo.IME_FLAG_FORCE_ASCII);
            }
            codeEditText.setMaxLines(1);
            codeEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(minCodeLength)});
        }
    }

    /***
     * Устанавливает заголовок окна соответственно  переданных параметров
     */
    public void setScreenHeader(String phone, String email) {
        if (isNotEmpty(phone)) {
            screenHeaderView.setText(Html.fromHtml(stringHeaderPhone.replace("null", "<b>" + phone + "</b>")));
        } else {
            screenHeaderView.setText(Html.fromHtml(stringHeaderEmail.replace("null", "<b>" + email + "</b>")));
        }
    }

    public void switchChangeNotificationView(boolean show) {
        if (show &&
                (parameters.get("input_email", "") instanceof String) &&
                (!parameters.get("input_email", "").equals("")) &&
                (parameters.get("input_phone", "") instanceof String) &&
                (!parameters.get("input_phone", "").equals(""))) {
            changeNotificationView.setVisibility(View.VISIBLE);
            changeNotificationView.setText(getResources().getString(R.string.send_code_to_email));
        } else {
            changeNotificationView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        button.setEnabled(text.length() >= minCodeLength);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // pass
    }

    @Override
    public void afterTextChanged(Editable s) {
        // pass
    }

    /***
     * Обработчик нажатий на какие-то элементы
     *
     * @param v вид на котором произошло нажатие
     */
    @Override
    public void onClick(View v) {
        String first = parameters.getString("input_first", "");
        String last = parameters.getString("input_last", "");
        String email = parameters.getString("input_email", "");

        switch (v.getId()) {
            case R.id.send_security_code_button:
                String token = parameters.getString("token", "");
                String code = codeEditText.getText().toString();
                if (buttonSendAction) {
                    new RegistrationConfirmTask().execute(code, token);
                } else {
                    String phone = parameters.getString("input_phone", "");
                    new SendCodeTask().execute(first, last, email, phone);
                }
                break;

            case R.id.notification_by_email:
                new SendCodeTask().execute(first, last, email, "");
                break;
        }
    }

    /***
     * Сюда падают результаты с диалоговых окон
     *
     * @param dialog_id идентификатор диалогового окна
     * @param cmd       команда от диалогового окна
     */
    @Override
    public void DialogSendResult(int dialog_id, int cmd) {
        Toast.makeText(getActivity(), "Result: " + String.valueOf(dialog_id) + "/" + String.valueOf(cmd),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void DialogSendParamResult(int dialog_id, int cmd, JSONObject params) {

    }

    /***
     * Отправка кода подтверждения на сервер, при неудаче - диалоговое окно
     * при успехе переход на другой фрагмент
     */
    class RegistrationConfirmTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            button.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            // code, token
            ServerTalk server = new ServerTalk();
            return server.registerConfirm(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            progress.setVisibility(View.INVISIBLE);
            button.setVisibility(View.VISIBLE);
            button.setEnabled(false);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    StopTimer();
                    button.setEnabled(true);
                    chainStartNextFragment(FRAGMENT_ID, RegCompleteFragment.class, result, 0, false);
                } else {
                    ResultDialog.showError(getActivity(), "Ошибка регистрации", errorCode, result.getErrorText());
                }
            } catch (Exception e) {
                Logger.Exception(e);
                ResultDialog.showError(getActivity(), "Ошибка регистрации", ErrorResult.INTERNAL_ERROR);
            }

            button.setEnabled(true);
        }
    }

    /***
     * Повторная отправка кода подтверждения (регулировка назначения - параметрами)
     */
    class SendCodeTask extends AsyncTask<String, Void, JSONObject> {
        private String first;
        private String last;
        private String email;
        private String phone;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            button.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            // first, last, email, phone
            first = params[0];
            last = params[1];
            email = params[2];
            phone = params[3];

            ServerTalk server = new ServerTalk();
            return server.registerRequest(first, last, email, phone);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            progress.setVisibility(View.INVISIBLE);
            button.setVisibility(View.VISIBLE);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    String token = (String) result.get("token");
                    parameters.put("token", token);
                    StopTimer();
                    timer = new Timer();
                    timerTask = new CountdownTask();
                    timer.schedule(timerTask, 0, 1000);
                    button.setText(getResources().getString(R.string.continue_enter));
                    setScreenHeader(phone, email);
                    setupEditTextIme(phone, email);
                } else {
                    ResultDialog.showError(getActivity(), "Ошибка регистрации", errorCode, result.getErrorText());
                }
            } catch (Exception e) {
                Logger.Exception(e);
                ResultDialog.showError(getActivity(), "Ошибка регистрации", ErrorResult.INTERNAL_ERROR);
            }

            button.setEnabled(codeEditText.getText().length() >= 6);
        }
    }

    /***
     * Фоновая задача. Отсчет времени валидности кода подтверждения.
     * Если время истекло, кнопка "Продолжить" меняется на "Получить код повторно"
     */
    class CountdownTask extends TimerTask {
        int timerSeconds = Settings.getInt(Settings.CODE_LIVE_TIME);

        @Override
        public void run() {
            timerSeconds--;
            if (fragmentActive)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (timerSeconds <= 0) {
                            StopTimer();
                            timerText.setVisibility(View.INVISIBLE);
                            button.setText(getResources().getString(R.string.send_code_again));
                            button.setEnabled(true);
                            buttonSendAction = false;
                            switchChangeNotificationView(true);
                        } else {
                            String text = stringTimerText.replace("null", String.valueOf(timerSeconds));
                            timerText.setText(text);
                            timerText.setVisibility(View.VISIBLE);
                            button.setText(getResources().getString(R.string.continue_enter));
                            buttonSendAction = true;
                            switchChangeNotificationView(false);
                        }
                    }
                });
        }

    }

    /***
     * Останавливает таймер, в случае проблем происто игнорируем их
     */
    public void StopTimer() {
        try {
            if (timer != null) timer.cancel();
        } catch (Exception e) {
            // pass
        }
        timer = null;
    }
}