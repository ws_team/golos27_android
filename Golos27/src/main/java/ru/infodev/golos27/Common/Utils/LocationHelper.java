package ru.infodev.golos27.Common.Utils;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.widget.Toast;

import ru.infodev.golos27.Common.Interfaces.LocationTracker;

public class LocationHelper implements LocationTracker, LocationTracker.LocationUpdateListener {
    private static final long STALE_TIME = 5 * 60 * 1000;

    private boolean isRunning;
    private ProviderLocationTracker gps;
    private ProviderLocationTracker net;
    private LocationUpdateListener listener;
    private LocationManager mLocationManager;
    private Context context;
    Location lastLoc;
    long lastTime;

    public LocationHelper(Context context) {
        this.context = context;
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        gps = new ProviderLocationTracker(context, ProviderLocationTracker.ProviderType.GPS);
        net = new ProviderLocationTracker(context, ProviderLocationTracker.ProviderType.NETWORK);
    }

    public static void openSettings(final Context context) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public boolean hasLocationEnabled() {
        boolean hasNetwork = hasLocationEnabled(LocationManager.NETWORK_PROVIDER);
        boolean hasGPS = hasLocationEnabled(LocationManager.GPS_PROVIDER);
        return hasNetwork || hasGPS;
    }

    private boolean hasLocationEnabled(final String providerName) {
        try {
            return mLocationManager.isProviderEnabled(providerName);
        } catch (Exception e) {
            return false;
        }
    }

    public void start(){
        if(isRunning) return;
        Logger.Log("Starting fallback location provider");
        gps.start(this);
        net.start(this);
        isRunning = true;
    }

    public void start(LocationUpdateListener update) {
        start();
        listener = update;
    }

    public void stop(){
        if(isRunning){
            Logger.Log("Stopping fallback location provider");
            gps.stop();
            net.stop();
            isRunning = false;
            listener = null;
        }
    }

    public boolean hasLocation(){
        return gps.hasLocation() || net.hasLocation();
    }

    public boolean hasPossiblyStaleLocation(){
        return gps.hasPossiblyStaleLocation() || net.hasPossiblyStaleLocation();
    }

    public Location getLocation(){
        Location ret = gps.getLocation();
        if(ret == null) ret = net.getLocation();

        if (ret == null) {
            long now = System.currentTimeMillis();
            if (now - lastTime <= STALE_TIME) {
                ret = lastLoc;
            }
        }

        return ret;
    }

    public Location getPossiblyStaleLocation(){
        Location ret = gps.getPossiblyStaleLocation();
        if(ret == null){
            ret = net.getPossiblyStaleLocation();
        }
        return ret;
    }

    public void toastCoordinates(String provider) {
        if (provider.equals(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(context, "Местоположение определено с помощью спутниковой системы",
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Местоположение определено по координатам сети",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void onUpdate(Location oldLoc, long oldTime, Location newLoc, long newTime) {
        Logger.Log("Location update called");
        boolean update = false;

        if(lastLoc == null){
            toastCoordinates(newLoc.getProvider());
            update = true;
        }
        else if(lastLoc.getProvider().equals(newLoc.getProvider())) {
            update = true;
        }
        else if(newLoc.getProvider().equals(LocationManager.GPS_PROVIDER)) {
            if (newLoc.getAccuracy() > lastLoc.getAccuracy()) {
                toastCoordinates(newLoc.getProvider());
                update = true;
            } else
                Logger.Log("Rejecting GPS location with low accuracy");
        }
        else if (newTime - lastTime > STALE_TIME) {
            toastCoordinates(newLoc.getProvider());
            update = true;
        }

        if (update) {
            if(listener != null) listener.onUpdate(lastLoc, lastTime, newLoc, newTime);
            lastLoc = newLoc;
            lastTime = newTime;
        }
    }
}
