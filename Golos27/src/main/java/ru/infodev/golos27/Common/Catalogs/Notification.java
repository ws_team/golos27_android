package ru.infodev.golos27.Common.Catalogs;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.R;

/***
 * Хранение уведомлений
 */
public class Notification {
    public static final String CREATED = "CREATED";
    public static final String ACCEPTED = "ACCEPTED";
    public static final String VIOLATED_REVIEWING = "VIOLATED_REVIEWING";
    public static final String ACCEPTED_AND_HIDED = "ACCEPTED_AND_HIDED";
    public static final String GRADED = "GRADED";
    public static final String HIDED = "HIDED";
    public static final String HIDED_BY_OPERATOR = "HIDED_BY_OPERATOR";
    public static final String SUBJECT_CHANGED = "SUBJECT_CHANGED";
    public static final String RESPONSE_HIDED = "RESPONSE_HIDED";
    public static final String RESPONSE_HIDED_BY_OPERATOR = "RESPONSE_HIDED_BY_OPERATOR";
    public static final String FORWARDED = "FORWARDED";
    public static final String PROLONGED = "PROLONGED";
    public static final String SOLVED = "SOLVED";
    public static final String REJECTED = "REJECTED";
    public static final String UNKNOWN = "PRIVATE_UNKNOWN";

    private String id = "";
    private long date = -1;
    private boolean isRead = true;
    private String eventText = "";
    private String eventType = "";

    private String objectId = "";
    private long objectNumber = -1;
    private long objectDate = -1;
    private String objectName = "";
    //---------------------------------------------------------------

    public String Id() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long Date() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        this.isRead = read;
    }

    public String EventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String EventText() {
        return eventText;
    }

    public void setEventText(String eventText) {
        this.eventText = eventText;
    }

    public String Title() {
        switch (eventType) {
            case CREATED: return getString(R.string.notification_created);
            case ACCEPTED: return getString(R.string.notification_accepted);
            case ACCEPTED_AND_HIDED: return getString(R.string.notification_accepted_hided);
            case REJECTED: return getString(R.string.notification_rejected);
            case VIOLATED_REVIEWING: return getString(R.string.notification_violated);
            case SOLVED: return getString(R.string.notification_solved);
            case PROLONGED: return getString(R.string.notification_prolonged);
            case GRADED: return getString(R.string.notification_graded);
            case FORWARDED: return getString(R.string.notification_forwarded);
            case HIDED: return getString(R.string.notification_hided);
            case HIDED_BY_OPERATOR: return getString(R.string.notification_hided_by_operator);
            case RESPONSE_HIDED: return getString(R.string.notification_response_hided);
            case RESPONSE_HIDED_BY_OPERATOR: return getString(R.string.notification_response_hided_by_operator);
            case UNKNOWN: return getString(R.string.notification_unknown);
        }

        return getString(R.string.notification_unknown);
    }

    public long ObjectNumber() {
        return objectNumber;
    }

    public void setObjectNumber(long objectNumber) {
        this.objectNumber = objectNumber;
    }

    public String ObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public long ObjectDate() {
        return objectDate;
    }

    public void setObjectDate(long objectDate) {
        this.objectDate = objectDate;
    }

    public String ObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public static Notification fromJson(JSONObject source) {
        Notification notification = new Notification();
        notification.setId(source.getString("id", ""));
        notification.setDate(source.getLong("date", -1L));
        notification.setRead(source.getBoolean("isRead", false));
        notification.setEventType(source.getString("eventType", UNKNOWN));

        JSONObject object = source.getJSONObject("object", null);
        if (object != null) {
            notification.setObjectId(object.getString("id", ""));
            notification.setObjectNumber(object.getLong("number", -1L));
            notification.setObjectDate(object.getLong("date", -1L));
            notification.setObjectName(object.getString("name", ""));
        }

        return notification;
    }

    public String getString(int resId) {
        return Application.AppContext.getString(resId);
    }
}
