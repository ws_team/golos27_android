package ru.infodev.golos27.Common.SocialNetwork;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import ru.infodev.golos27.Common.SocialNetwork.listener.OnLoginCompleteListener;
import ru.infodev.golos27.Common.SocialNetwork.listener.OnPostingCompleteListener;
import ru.infodev.golos27.Common.SocialNetwork.listener.OnRequestAccessTokenCompleteListener;
import ru.infodev.golos27.Common.SocialNetwork.listener.base.SocialNetworkListener;
import ru.infodev.golos27.Common.Utils.Logger;

/**
 * Base class for social networks. Containing base methods of social networks, listener registrations and some utility methods.<br>
 * Social network ids:<br>
 * 1 - Twitter<br>
 * 2 - LinkedIn<br>
 * 3 - Google Plus<br>
 * 4 - Facebook<br>
 * 5 - Vkontakte<br>
 * 6 - Odnoklassniki<br>
 * 7 - Instagram<br>
 *
 * @author Anton Krasov
 * @author Evgeny Gorbin (gorbin.e.o@gmail.com)
 */

public abstract class SocialNetwork {

    /*** Used to check is login request in progress*/
    public static final String REQUEST_LOGIN = "SocialNetwork.REQUEST_LOGIN";
    /*** Used to check is login request in progress for social networks with OAuth*/
    public static final String REQUEST_LOGIN2 = "SocialNetwork.REQUEST_LOGIN2";
    /*** Used to check is access token request in progress*/
    public static final String REQUEST_ACCESS_TOKEN = "SocialNetwork.REQUEST_ACCESS_TOKEN";
    /*** Used to check is post message request in progress*/
    public static final String REQUEST_POST_MESSAGE = "SocialNetwork.REQUEST_POST_MESSAGE";
    /*** Used to check is post photo request in progress*/
    public static final String REQUEST_POST_PHOTO = "SocialNetwork.REQUEST_POST_PHOTO";
    /*** Used to check is post link request in progress*/
    public static final String REQUEST_POST_LINK = "SocialNetwork.REQUEST_POST_LINK";
    /*** Used to check is post dialog request in progress*/
    public static final String REQUEST_POST_DIALOG = "SocialNetwork.REQUEST_POST_DIALOG";

    /*** Share bundle constant for message*/
    public static final String BUNDLE_MESSAGE = "message";
    /*** Share bundle constant for link*/
    public static final String BUNDLE_LINK = "link";
    /*** Share bundle constant for title*/
    public static final String BUNDLE_NAME = "name";
    /*** Share bundle constant for application name*/
    public static final String BUNDLE_APP_NAME = "app_name";
    /*** Share bundle constant for caption*/
    public static final String BUNDLE_CAPTION = "caption";
    /*** Share bundle constant for picture*/
    public static final String BUNDLE_PICTURE = "picture";
    /*** Share bundle constant for bitmap */
    public static final String BUNDLE_BITMAP = "bitmap";

    /*** Shared preferences name */
    private static final String SHARED_PREFERENCES_NAME = "social_networks";
    protected Fragment mSocialNetworkManager;
    protected SharedPreferences mSharedPreferences;
    protected Map<String, SocialNetworkListener> mGlobalListeners = new HashMap<>();
    protected Map<String, SocialNetworkListener> mLocalListeners = new HashMap<>();

    /**
     * @param fragment ant not activity or context, as we will need to call startActivityForResult,
     *                 we will want to receice on onActivityResult in out SocialNetworkManager
     *                 fragment
     */
    protected SocialNetwork(Fragment fragment) {
        mSocialNetworkManager = fragment;
        mSharedPreferences = mSocialNetworkManager.getActivity().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    /**
     * @param socialMediaManager the SocialMediaManager fragment.
     * @param context ant Activity or Application if not being called from a fragment
     */
    protected SocialNetwork(Fragment socialMediaManager, Context context) {
        //we keep the fragment in case it is needed in future. it also minimises the changes required.
        mSocialNetworkManager = socialMediaManager;
        mSharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }
    //////////////////// LIFECYCLE ////////////////////

    /**
     * Called when the Social Network activity is starting. Overrided in chosen social network
     * @param savedInstanceState If the activity is being re-initialized after previously being shut down then this Bundle contains the data it most recently supplied in onSaveInstanceState(Bundle). Note: Otherwise it is null.
     */
    public void onCreate(Bundle savedInstanceState) {

    }

    /**
     * Called after onCreate(Bundle) — or after onRestart() when the activity had been stopped, but is now again being displayed to the user. Overrided in chosen social network.
     */
    public void onStart() {

    }

    /**
     * Called after onRestoreInstanceState(Bundle), onRestart(), or onPause(), for your activity to start interacting with the user. Overrided in chosen social network.
     */
    public void onResume() {

    }

    /**
     * Called as part of the activity lifecycle when an activity is going into the background, but has not (yet) been killed. Overrided in chosen social network.
     */
    public void onPause() {

    }

    /**
     * Called when you are no longer visible to the user. Overrided in chosen social network.
     */
    public void onStop() {

    }

    /**
     * Perform any final cleanup: cancel all request before activity destroyed.
     */
    public void onDestroy() {
        cancelAll();
    }

    /**
     * Called to retrieve per-instance state from an activity before being killed so that the state can be restored in onCreate(Bundle) or onRestoreInstanceState(Bundle) (the Bundle populated by this method will be passed to both). Overrided in chosen social network.
     * @param outState Bundle in which to place your saved state.
     */
    public void onSaveInstanceState(Bundle outState) {

    }

    /**
     * Called when an activity you launched exits, giving you the requestCode you started it with, the resultCode it returned, and any additional data from it. Overrided in chosen social network
     * @param requestCode The integer request code originally supplied to startActivityForResult(), allowing you to identify who this result came from.
     * @param resultCode The integer result code returned by the child activity through its setResult().
     * @param data An Intent, which can return result data to the caller (various data can be attached to Intent "extras").
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    //////////////////// API ////////////////////

    /**
     * Check if selected social network connected: true or false
     * @return true if connected, else false
     */
    public abstract boolean isConnected();

    /**
     * Login to social network using global listener
     */
    public void requestLogin() {
        requestLogin(null);
    }

    /**
     * Login to social network using local listener
     * @param onLoginCompleteListener listener for login complete
     */
    public void requestLogin(OnLoginCompleteListener onLoginCompleteListener) {
        if (isConnected()) {
            throw new SocialNetworkException("Already connected, please check isConnected() method");
        }

        registerListener(REQUEST_LOGIN, onLoginCompleteListener);
    }

    /**
     * Logout from social network
     */
    public abstract void logout();

    /**
     * Get id of social network
     * @return Social network ids:<br>
     * 1 - Twitter<br>
     * 2 - LinkedIn<br>
     * 3 - Google Plus<br>
     * 4 - Facebook<br>
     * 5 - Vkontakte<br>
     * 6 - Odnoklassniki<br>
     * 7 - Instagram<br>
     */
    public abstract int getID();

    /**
     * Return (except GooglePlus)
     */
    public abstract AccessToken getAccessToken();

    /**
     * Get using global listener
     */
    public void requestAccessToken(){
        requestAccessToken(null);
    }

    /**
     * @param onRequestAccessTokenCompleteListener listener for request complete
     */
    public void requestAccessToken(OnRequestAccessTokenCompleteListener onRequestAccessTokenCompleteListener) {
        registerListener(REQUEST_ACCESS_TOKEN, onRequestAccessTokenCompleteListener);
    }

    /**
     * Post message to social network using global listener
     * @param message message that should be shared
     */
    public void requestPostMessage(String message) {
        requestPostMessage(message, null);
    }

    /**
     * Post message to social network using local listener
     * @param message  message that should be shared
     * @param onPostingCompleteListener listener for posting request
     */
    public void requestPostMessage(String message, OnPostingCompleteListener onPostingCompleteListener) {
        registerListener(REQUEST_POST_MESSAGE, onPostingCompleteListener);
    }

    /**
     * Post photo to social network using global listener
     * @param photo photo that should be shared
     * @param message message that should be shared with photo
     */
    public void requestPostPhoto(File photo, String message) {
        requestPostPhoto(photo, message, null);
    }

    /**
     * Post photo to social network using local listener
     * @param photo photo that should be shared
     * @param message message that should be shared with photo
     * @param onPostingCompleteListener listener for posting request
     */
    public void requestPostPhoto(File photo, String message, OnPostingCompleteListener onPostingCompleteListener) {
        registerListener(REQUEST_POST_PHOTO, onPostingCompleteListener);
    }

    /**
     * Get Share dialog of social network using global listener
     * @param bundle bundle containing information that should be shared(Bundle constants in SocialNetwork)
     */
    public void requestPostDialog(Bundle bundle) {
        requestPostDialog(bundle, null);
    }

    /**
     * Get Share dialog of social network using global listener
     * @param bundle bundle containing information that should be shared(Bundle constants in SocialNetwork)
     * @param onPostingCompleteListener listener for posting request
     */
    public void requestPostDialog(Bundle bundle, OnPostingCompleteListener onPostingCompleteListener) {
        registerListener(REQUEST_POST_DIALOG, onPostingCompleteListener);
    }

    /**
     * Post link with comment to social network using global listener
     * @param bundle bundle containing information that should be shared(Bundle constants in SocialNetwork)
     * @param message message that should be shared with bundle
     */
    public void requestPostLink(Bundle bundle, String message) {
        requestPostLink(bundle, message, null);
    }

    /**
     * Post link with comment to social network using local listener
     * @param bundle bundle containing information that should be shared(Bundle constants in SocialNetwork)
     * @param message message that should be shared with bundle
     * @param onPostingCompleteListener listener for posting request
     */
    public void requestPostLink(Bundle bundle, String message, OnPostingCompleteListener onPostingCompleteListener) {
        registerListener(REQUEST_POST_LINK, onPostingCompleteListener);
    }

    /**
     * Cancel login request
     */
    public void cancelLoginRequest() {
        mLocalListeners.remove(REQUEST_LOGIN);
    }

    /**
     * Cancel AccessToken request
     */
    public void cancelAccessTokenRequest() {
        mLocalListeners.remove(REQUEST_ACCESS_TOKEN);
    }

    /**
     * Cancel post message request
     */
    public void cancelPostMessageRequest() {
        mLocalListeners.remove(REQUEST_POST_MESSAGE);
    }

    /**
     * Cancel post photo request
     */
    public void cancelPostPhotoRequest() {
        mLocalListeners.remove(REQUEST_POST_PHOTO);
    }

    /**
     * Cancel post link request
     */
    public void cancelPostLinkRequest() {
        mLocalListeners.remove(REQUEST_POST_LINK);
    }

    /**
     * Cancel share dialog request
     */
    public void cancelPostDialogRequest() {
        mLocalListeners.remove(REQUEST_POST_DIALOG);
    }

    /**
     * Cancel all requests
     */
    public void cancelAll() {
        Logger.Log(".");

        // we need to call all, because in implementations we can possible do aditional work in specific methods
        cancelLoginRequest();
        cancelAccessTokenRequest();
        cancelPostMessageRequest();
        cancelPostPhotoRequest();
        cancelPostLinkRequest();
        cancelPostDialogRequest();

        // remove all local listeners
        mLocalListeners = new HashMap<>();
    }

    //////////////////// UTIL METHODS ////////////////////

    protected void checkRequestState(AsyncTask request) throws SocialNetworkException {
        if (request != null) {
            throw new SocialNetworkException(request.toString() + "Request is already running");
        }
    }

    private void registerListener(String listenerID, SocialNetworkListener socialNetworkListener) {
        if (socialNetworkListener != null) {
            mLocalListeners.put(listenerID, socialNetworkListener);
        } else {
            mLocalListeners.put(listenerID, mGlobalListeners.get(listenerID));
        }
    }

    //////////////////// SETTERS FOR GLOBAL LISTENERS ////////////////////

    /**
     * Register a callback to be invoked when login complete.
     * @param onLoginCompleteListener the callback that will run
     */
    public void setOnLoginCompleteListener(OnLoginCompleteListener onLoginCompleteListener) {
        mGlobalListeners.put(REQUEST_LOGIN, onLoginCompleteListener);
    }

    /**
     * Register a callback to be invoked when AccessToken request complete.
     * @param onRequestAccessTokenCompleteListener the callback that will run
     */
    public void setOnRequestAccessTokenCompleteListener(OnRequestAccessTokenCompleteListener onRequestAccessTokenCompleteListener) {
        mGlobalListeners.put(REQUEST_ACCESS_TOKEN, onRequestAccessTokenCompleteListener);
    }

    /**
     * Register a callback to be invoked when post message request complete.
     * @param onPostingCompleteListener the callback that will run
     */
    public void setOnPostingMessageCompleteListener(OnPostingCompleteListener onPostingCompleteListener) {
        mGlobalListeners.put(REQUEST_POST_MESSAGE, onPostingCompleteListener);
    }

    /**
     * Register a callback to be invoked when post photo request complete.
     * @param onPostingCompleteListener the callback that will run
     */
    public void setOnPostingPhotoCompleteListener(OnPostingCompleteListener onPostingCompleteListener) {
        mGlobalListeners.put(REQUEST_POST_PHOTO, onPostingCompleteListener);
    }

    /**
     * Register a callback to be invoked when post link request complete.
     * @param onPostingCompleteListener the callback that will run
     */
    public void setOnPostingLinkCompleteListener(OnPostingCompleteListener onPostingCompleteListener) {
        mGlobalListeners.put(REQUEST_POST_LINK, onPostingCompleteListener);
    }

    /**
     * Register a callback to be invoked when share dialog request complete.
     * @param onPostingCompleteListener the callback that will run
     */
    public void setOnPostingDialogCompleteListener(OnPostingCompleteListener onPostingCompleteListener) {
        mGlobalListeners.put(REQUEST_POST_DIALOG, onPostingCompleteListener);
    }
}
