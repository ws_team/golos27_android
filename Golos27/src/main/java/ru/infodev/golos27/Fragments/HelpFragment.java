package ru.infodev.golos27.Fragments;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.Registration.TermsOfUse;
import ru.infodev.golos27.R;

public class HelpFragment extends ParamFragment {
    public static final int FRAGMENT_ID = R.layout.help;
    public static final int COMMAND_PRIVACY = 16;

    public HelpFragment() {
        titleId = R.string.help_page;
        setFragmentAllowMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        setTitle(Application.AppContext.getString(R.string.help_page));

        // Получим версию приложения
        String app_version = null;
        Context app = Application.AppContext;
        try {
            PackageManager manager = app.getPackageManager();
            PackageInfo info = manager.getPackageInfo(app.getPackageName(), 0);
            app_version = info.versionName;
        } catch (Exception e) {
            Logger.Exception(e);
        }

        TextView version = (TextView) v.findViewById(R.id.version_and_date);
        version.setTypeface(roboto_light);
        if (app_version != null) {
            String versionDate = getString(R.string.version_text) + " " + app_version;
            version.setText(versionDate);
        }

        TextView company = (TextView) v.findViewById(R.id.company);
        company.setText(R.string.company_name);
        company.setTypeface(roboto_light);

        TextView terms_of_use_app_info = (TextView) v.findViewById(R.id.terms_of_use);
        terms_of_use_app_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentResultListener.FragmentSendResult(FRAGMENT_ID, COMMAND_PRIVACY, null);
                chainStartNextFragment(FRAGMENT_ID, TermsOfUse.class, null, false);
            }
        });

        return v;
    }
}