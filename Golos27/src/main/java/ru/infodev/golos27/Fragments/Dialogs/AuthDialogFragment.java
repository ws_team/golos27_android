package ru.infodev.golos27.Fragments.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.MaterialMenuIcon;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

@SuppressWarnings("unused")
public class AuthDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final String BUNDLE_CONTENT = "bundle_parameters";
    public static final int DIALOG_ID = R.layout.dialog_login;
    private DialogResultInterface resultInterface;
    private JSONObject parameters = null;
    private String title = "";

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getArguments() != null && getArguments().containsKey(BUNDLE_CONTENT)) {
            try {
                String strParam = getArguments().getString(BUNDLE_CONTENT);
                parameters = new JSONObject(strParam);
                if (parameters.has("title") && !parameters.isNull("title"))
                    title = parameters.getString("title", null);
            } catch (Exception e) {
                Logger.Exception(e);
            }
        } else {
            throw new IllegalArgumentException("Must be created through newInstance(...)");
        }

        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Logger.Log("Creating AuthDialog");
        View currentView = inflater.inflate(DIALOG_ID, container, false);

        try {
            Window window = getDialog().getWindow();
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.requestFeature(Window.FEATURE_NO_TITLE);
            WindowManager.LayoutParams windowParams = window.getAttributes();
            windowParams.dimAmount = 0.20f;
            windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            windowParams.windowAnimations = android.R.style.Animation_Dialog;
            window.setAttributes(windowParams);
        } catch (Exception e) {
            Logger.Exception(e);
        }

        currentView.findViewById(R.id.have_account).setOnClickListener(this);
        currentView.findViewById(R.id.new_user).setOnClickListener(this);
        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);

        TextView resultText = (TextView) currentView.findViewById(R.id.resultText);
        if (isNotEmpty(title)) resultText.setText(title);
        resultText.setTypeface(roboto_light);
        ImageView closeImage = (ImageView) currentView.findViewById(R.id.close_button);
        closeImage.setOnClickListener(this);
        int iconColor = ContextCompat.getColor(Application.AppContext, R.color.colorGrayText);
        MaterialMenuIcon icon = new MaterialMenuIcon(Application.AppContext, iconColor, iconColor,
                MaterialMenuIcon.Stroke.THIN);

        icon.setIconState(MaterialMenuIcon.IconState.X);
        closeImage.setImageDrawable(icon);
        return currentView;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public void show(FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null) {
            super.show(manager, tag);
        }
    }

    @Override
    public void onClick(View lv) {
        switch (lv.getId()) {
            case R.id.have_account:
                if (resultInterface != null)
                    resultInterface.DialogSendParamResult(DIALOG_ID, Activity.RESULT_OK, parameters);
                Logger.Log("User selected LOGIN button...");
                dismiss();
                break;
            case R.id.new_user:
                if (resultInterface != null)
                    resultInterface.DialogSendParamResult(DIALOG_ID, Activity.RESULT_CANCELED, parameters);
                Logger.Log("User selected REGISTER button...");
                dismiss();
                break;
            case R.id.close_button:
                Logger.Log("User selected CLOSE button");
                dismiss();
                break;
            default:
                Logger.Log("Unknown click from " + lv.toString());
                break;
        }
    }

    public void setResultListener(DialogResultInterface listener) {
        if (listener != null) resultInterface = listener;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}
