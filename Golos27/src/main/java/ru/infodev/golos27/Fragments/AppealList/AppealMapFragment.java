package ru.infodev.golos27.Fragments.AppealList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.Interfaces.LocationResultInterface;
import ru.infodev.golos27.Common.Interfaces.MapJavaScriptInterface;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.Layouts.NestedWebView;

@SuppressWarnings("FieldCanBeLocal")
public class AppealMapFragment extends ParamFragment implements LocationResultInterface {
    public static final int FRAGMENT_ID = R.layout.map_location;
    private MapJavaScriptInterface mapJSInterface;
    private NestedWebView mapWebView;
    private TextView mapLoading;

    public AppealMapFragment() {
        titleId = R.string.map_address;
        mapJSInterface = new MapJavaScriptInterface(Application.AppContext, false);
        setFragmentAllowMenu(false);
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        View v = inflater.inflate(FRAGMENT_ID, container, false);

        if (!parameters.has("Address") || !parameters.has("Latitude") || !parameters.has("Longitude")) {
            Toast.makeText(getContext(), R.string.cant_show_address, Toast.LENGTH_SHORT).show();
            chainPopFragments(FRAGMENT_ID, 1, false);
        }

        String address = parameters.getString("Address", "");
        double latitude = parameters.getDouble("Latitude", 48.47655);
        double longitude = parameters.getDouble("Longitude", 135.064308);
        mapJSInterface.setInitialBalloon(address, latitude, longitude);

        mapLoading = (TextView) v.findViewById(R.id.map_loading);
        mapLoading.setTypeface(roboto_light);
        mapLoading.setText(getString(R.string.map_loading).replace("null", "0%"));
        mapWebView = (NestedWebView) v.findViewById(R.id.mapWebView);
        mapWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (fragmentActive)
                    mapLoading.setText(getString(R.string.map_loading).replace("null", String.valueOf(progress) + "%"));
            }
        });

        mapJSInterface.setLocationListener(this);
        mapWebView.addJavascriptInterface(mapJSInterface, "Golos27App");
        String userAgent = mapWebView.getSettings().getUserAgentString();
        Logger.Log("UserAgent: " + userAgent);
        WebSettings webSettings = mapWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

        try {
            InputStream is = getActivity().getAssets().open("map_simple.html");
            byte[] buffer = new byte[is.available()];
            if (is.read(buffer) <= 0) Logger.Warn("Can't read embedded assets map_simple.html file!");
            is.close();

            String htmlText = new String(buffer);
            mapWebView.loadDataWithBaseURL(getString(R.string.yandex_maps_url), htmlText, "text/html", "UTF-8", null);
        } catch (IOException e) {
            Logger.Exception(e);
        }

        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        return v;
    }

    @Override
    public void OnPageLoaded() {
        Activity activity = getActivity();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mapWebView.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    @Override
    public void OnGPSError(int code) {
    }

    @Override
    public void OnLocationReceived(String region, String area, String settlement, String address,
                                   double latitude, double longitude, int iZoomLevel) {
    }
}
