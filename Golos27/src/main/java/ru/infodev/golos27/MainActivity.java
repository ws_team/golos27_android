package ru.infodev.golos27;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.vk.sdk.util.VKUtil;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.infodev.golos27.Common.Catalogs.Catalogs;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Interfaces.FragmentChainInterface;
import ru.infodev.golos27.Common.Interfaces.FragmentResultInterface;
import ru.infodev.golos27.Common.Interfaces.LoadingResultInterface;
import ru.infodev.golos27.Common.Interfaces.RecycleViewClickInterface;
import ru.infodev.golos27.Common.OfflineAppealSender;
import ru.infodev.golos27.Common.Profile;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.UIPeriodic;
import ru.infodev.golos27.Common.Utils.DeviceName;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.LocationHelper;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.AppealClaim.SetCategoryFragment;
import ru.infodev.golos27.Fragments.AppealList.AppealCardFragment;
import ru.infodev.golos27.Fragments.AppealList.AppealListFragment;
import ru.infodev.golos27.Fragments.Dialogs.AuthDialog;
import ru.infodev.golos27.Fragments.Dialogs.AuthDialogFragment;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.HelpFragment;
import ru.infodev.golos27.Fragments.LoginFragment;
import ru.infodev.golos27.Fragments.NotificationsFragment;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.Fragments.Profile.ChangeAddressFragment;
import ru.infodev.golos27.Fragments.Profile.ChangeGeneralFragment;
import ru.infodev.golos27.Fragments.Profile.ChangeListFragment;
import ru.infodev.golos27.Fragments.Profile.ChangePasswordFragment;
import ru.infodev.golos27.Fragments.Registration.RegRequestFragment;
import ru.infodev.golos27.RestApi.PushNotifications;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;
import ru.infodev.golos27.Widgets.Adapters.CustomDrawerAdapter;
import ru.infodev.golos27.Widgets.DrawerItem;
import ru.infodev.golos27.Widgets.Layouts.SwitchableScrollView;
import ru.infodev.golos27.Widgets.MaterialMenuIcon;

import static ru.infodev.golos27.ApiConfig.FRONT_ADDRESS;
import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

@SuppressWarnings("unused")
public class MainActivity extends AppCompatActivity
        implements FragmentResultInterface, FragmentChainInterface, LoadingResultInterface, RecycleViewClickInterface, DialogResultInterface {

    // Команды от фрагментов с событием login/logout
    public static final int AUTH_COMMAND_ID = R.id.item_auth_command;
    public static final int AUTH_COMMAND_HAVE = 1;
    public static final int AUTH_COMMAND_NOT = 0;
    public static final int UPDATE_NOTIFICATIONS = 2;

    public static final int DRAWER_CLOSE_DELAY = 310; // миллисекунды
    public static final String SOCIAL_NETWORK_TAG = "SocialIntegrationMain.SOCIAL_NETWORK_TAG";
    public static final String OPEN_APPEAL_CARD = "OpenAppealCardCommand";

    private int shadowOffsetTop;
    private int shadowOffsetLeft;
    public boolean isActive = false;
    private UIPeriodic mPeriodicTask;

    private CoordinatorLayout layout_content;
    private NavigationView layout_drawer;
    private FrameLayout animated_view;
    private RecyclerView drawerMenuView;
    private LinearLayout nav_header_block;
    private ScrollView main_screen_content;
    private TextView item_text_full_name;
    private Menu optionsMenu;
    private DrawerLayout drawerView;
    private View content_view;
    private View content_root;
    private LinearLayout drawer_menu_view;
    private CustomDrawerAdapter menuAdapter;
    private LinearLayout loadingProgressView;
    private MaterialMenuIcon materialMenu;
    private SwitchableScrollView mainScrollView;

    @SuppressWarnings("FieldCanBeLocal")
    private int drawerSetWidth = -1;
    @SuppressWarnings("FieldCanBeLocal")
    private int drawerSetHeaderWidth = -1;

    private final Object lock = new Object();
    private int mClickedMenuItem = 0;
    private boolean mIsMenuItemClicked = false;
    private boolean mIsDrawerWidthSet = false;
    private Class nextAuthFragment;
    private JSONObject nextAuthParams;
    private FragmentManager fragManager;
    private int colorTransparent;
    private boolean lockWindow = false;
    private boolean lockToolbar = false;
    private boolean menuAvailable = true;
    private final Context context = this;
    private int toolBarHeight = 0;
    private final int contentElevation = 40;
    private final float fadeAnimationStart = 0.75f;
    private float lastMoveX = 0.0f;
    private float lastMoveY = 0.0f;
    private float lastScaleX = 0.0f;
    private float lastScaleY = 0.0f;

    private float lastMoveIntX = 0.0f;
    private float lastMoveIntY = 0.0f;
    private float lastScaleIntX = 0.0f;
    private float lastScaleIntY = 0.0f;

    private float lastAlpha = 0.0f;
    private float maxScale = 0.2f;
    private float maxIntScale = 0.05f;
    private boolean mIsDrawerWidthChanged = false;

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
        if (mPeriodicTask != null) mPeriodicTask.startUpdates();
        Logger.Log("MainActivity onResume() called. Trying to check auth status!");
        checkAuthorizationTasks();
        PushNotifications.Subscribe(false);
    }

    @Override
    public void onNewIntent(Intent intent) {
        Logger.Log("OnNewIntent() called");
        startAppealCardOnNotification(intent);
    }

    // Проверим авторизацию и если есть токен, то его надо проверить
    private void checkAuthorizationTasks() {
        String token = Settings.getStr(Settings.USER_ACCESS_TOKEN);
        if (!isEmpty(token)) {
            FragmentSendResult(AUTH_COMMAND_ID, AUTH_COMMAND_HAVE, null);
        } else {
            FragmentSendResult(AUTH_COMMAND_ID, AUTH_COMMAND_NOT, null);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPeriodicTask != null) mPeriodicTask.stopUpdates();
        isActive = false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogDeviceDetails();

        mPeriodicTask = new UIPeriodic(new Runnable() {
            @Override
            public void run() {
                try {
                    new RunAuthorizationEvents().execute();
                    OfflineAppealSender.executeJobs();
                } catch (Exception e) {
                    Logger.Exception(e);
                }
            }
        }, 60000);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);

        // Шрифт для заголовка на главном экране
//        TextView appName = (TextView) findViewById(R.id.app_name);
//        if (appName != null) {
//            appName.setTypeface(CustomFontLoader.getTypeface(Application.AppContext, 0));
//        }

        View testButton = findViewById(R.id.test_button);
        content_view = findViewById(R.id.content_view);
        content_root = findViewById(R.id.content_root);
        layout_content = (CoordinatorLayout) findViewById(R.id.parent_content);
        animated_view = (FrameLayout) findViewById(R.id.animated_view);
        layout_drawer = (NavigationView) findViewById(R.id.nav_view);
        nav_header_block = (LinearLayout) findViewById(R.id.nav_header_block);
        main_screen_content = (ScrollView) findViewById(R.id.main_screen_content);
        item_text_full_name = (TextView) findViewById(R.id.item_text_full_name);
        drawer_menu_view = (LinearLayout) findViewById(R.id.drawer_menu_layout);

        toolBarHeight = (int) getResources().getDimension(R.dimen.menu_scale_height);
        final LocationHelper fallbackLocationTracker = new LocationHelper(context);
        loadingProgressView = (LinearLayout) findViewById(R.id.loading_progress);

        TextView loading_text = (TextView) findViewById(R.id.loading_text);
        if (loading_text != null) loading_text.setTypeface(roboto_light);

        mainScrollView = (SwitchableScrollView) findViewById(R.id.main_scroll_view);
        if (mainScrollView != null) mainScrollView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        colorTransparent = ContextCompat.getColor(this, R.color.colorTransparent);
        int colorNotification = ContextCompat.getColor(this, R.color.colorNotification);
        materialMenu = new MaterialMenuIcon(this, Color.WHITE, colorNotification, MaterialMenuIcon.Stroke.THIN);
        materialMenu.animateIconState(MaterialMenuIcon.IconState.BURGER);
        final Drawable shadow = ContextCompat.getDrawable(this, R.drawable.shadow_big);
        Rect padding = new Rect();
        shadow.getPadding(padding);
        shadowOffsetTop = padding.top;
        shadowOffsetLeft = padding.left;
        mIsDrawerWidthSet = false;
        Logger.Log("Shadow paddingTop: " + String.valueOf(shadowOffsetTop));

        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setDisplayShowHomeEnabled(false);
        }

        if (toolbar != null) toolbar.setNavigationIcon(materialMenu);
        drawerView = (DrawerLayout) findViewById(R.id.drawer_layout);
        setupDrawerMenu();
        setupDrawerAnimation();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerView, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float localSlideOffset = slideOffset;
                if (localSlideOffset <= 0) localSlideOffset = 0f;
                // Настроим ширину шторки меню (не менее 0.6 экрана и не более ширины экрана - высота тулбара)
                // При этом пункты меню по возможности должны влезть в одну строку
                setupDrawerWidth(false);

                AnimationSet animationSet = new AnimationSet(true);
                final AnimationSet animationIntSet = new AnimationSet(true);

                float moveX = (layout_drawer.getMeasuredWidth() * localSlideOffset);
                float moveY = ((toolBarHeight - shadowOffsetTop) * localSlideOffset);
                float scaleFactor = 1 - (maxScale * localSlideOffset);

                if (moveX <= toolBarHeight) {
                    // Ставим прозрачный фон без отступов
                    animated_view.setBackgroundColor(colorTransparent);
                    animated_view.setPadding(0, 0, 0, 0);
                } else {
                    // Ставим фон с тенью. Тень показывается в анимации
                    if (content_view.getVisibility() == View.VISIBLE) {
                        animated_view.setBackgroundResource(R.drawable.shadow_big);
                        animated_view.setPadding(0, 0, 0, 0);
                    }
                }

                // Появление меню (fade анимация)
                float fadeOffset = 0;
                if (localSlideOffset > fadeAnimationStart) {
                    fadeOffset = (localSlideOffset - fadeAnimationStart) / (1 - fadeAnimationStart);
                }
                AlphaAnimation fadeAnim = new AlphaAnimation(lastAlpha, fadeOffset);
                fadeAnim.setDuration(0);
                fadeAnim.setFillAfter(true);

                // Сдвиг основного вида вправо вместе с движением шторки меню
                TranslateAnimation moveAnim = new TranslateAnimation(lastMoveX, moveX, lastMoveY, moveY);
                moveAnim.setDuration(0);

                // Масштабирование вида (уменьшение) вместе с движением вида вправо
                ScaleAnimation scaleAnim = new ScaleAnimation(lastScaleX, scaleFactor, lastScaleY, scaleFactor);
                scaleAnim.setDuration(0);

                // Добавляем анимации в набор анимаций
                animationSet.addAnimation(scaleAnim);
                animationSet.addAnimation(moveAnim);
                animationSet.setDuration(0);
                animationSet.setFillAfter(true);

                // Набор анимаций для отображения тени под карточкой
                float moveIntX = (shadowOffsetLeft * localSlideOffset);
                float moveIntY = (shadowOffsetTop * localSlideOffset);
                float scaleFactorInt = 1 - (maxIntScale * localSlideOffset);
                TranslateAnimation moveIntAnim = new TranslateAnimation(lastMoveIntX, moveIntX, lastMoveIntY, moveIntY);
                ScaleAnimation scaleIntAnim = new ScaleAnimation(lastScaleIntX, scaleFactorInt, lastScaleIntY, scaleFactorInt);
                moveIntAnim.setDuration(0);
                scaleIntAnim.setDuration(0);
                animationIntSet.addAnimation(scaleIntAnim);
                animationIntSet.addAnimation(moveIntAnim);
                animationIntSet.setDuration(0);
                animationIntSet.setFillAfter(true);

                content_root.startAnimation(animationIntSet);
                animated_view.startAnimation(animationSet);
                layout_drawer.startAnimation(fadeAnim);
                lastMoveX = moveX;
                lastMoveY = moveY;
                lastScaleX = scaleFactor;
                lastScaleY = scaleFactor;

                lastMoveIntX = moveIntX;
                lastMoveIntY = moveIntY;
                lastScaleIntX = scaleFactorInt;
                lastScaleIntY = scaleFactorInt;

                materialMenu.setTransformationOffset(MaterialMenuIcon.AnimationState.BURGER_X, localSlideOffset);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                Logger.Log("Drawer state changed " + String.valueOf(newState));
                hideKeyboard();
                if ((!isDrawerOpened()) && (newState == DrawerLayout.STATE_IDLE)) {
                    invalidateOptionsMenu();
                    main_screen_content.setVisibility(View.VISIBLE);
                } else {
                    if (optionsMenu != null) optionsMenu.clear();
                }

                if (newState == DrawerLayout.STATE_IDLE) {
                    if (isDrawerOpened()) {
                        materialMenu.setIconState(MaterialMenuIcon.IconState.X);
                    } else {
                        materialMenu.setIconState(MaterialMenuIcon.IconState.BURGER);
                    }
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Logger.Log("Drawer opened!");
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Logger.Log("Drawer closed!");
                invalidateOptionsMenu();

                if (mIsDrawerWidthChanged) RelayoutDrawer();

                // Если перед закрытием меню было нажатие на пункт меню
                // то обработаем это нажатие здесь
                if (mIsMenuItemClicked) {
                    switch (mClickedMenuItem) {
                        case R.id.item_new_appeal:
                            AddFragment(SetCategoryFragment.class, null, true, 0, true);
                            break;
                        case R.id.enter_or_register:
                            AddFragment(LoginFragment.class, null, true, 0, false);
                            break;
                        case R.id.item_own_complaints:
                            JSONObject params = new JSONObject();
                            params.putSafe("MyAppeals", true);
                            AddFragment(AppealListFragment.class, params, true, 0, true, AppealListFragment.getAuthTitle());
                            break;
                        case R.id.item_appeals:
                            AddFragment(AppealListFragment.class, null, true, 0, false);
                            break;
                        case R.id.item_own_profile:
                            AddFragment(ChangeListFragment.class, null, true, 0, true);
                            break;
                        case R.id.item_logout:
                            new AsyncTask<Void, Void, String>() {
                                @Override
                                protected String doInBackground(Void... params) {
                                    try {
                                        ServerTalk server = new ServerTalk();
                                        return server.getAccessToken();
                                    } catch (Exception e) {
                                        Logger.Exception(e);
                                    }
                                    return "---unknownToken---";
                                }

                                @Override
                                protected void onPostExecute(String s) {
                                    super.onPostExecute(s);
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FRONT_ADDRESS + "/bridge/mc/esialogout?access_token=" + s)));
                                    FragmentSendResult(AUTH_COMMAND_ID, AUTH_COMMAND_NOT, null);
                                    fragManager.popBackStack(); //remove top fragment if present (profile, help etc)
                                }
                            }.execute();
                            break;
                        case R.id.item_notifications:
                            AddFragment(NotificationsFragment.class, null, true, 0, true);
                            break;
                        case R.id.item_help:
                            AddFragment(HelpFragment.class, null, true, 0, false);
                            break;
                    }
                }
                mIsMenuItemClicked = false;
            }
        };

        // Обработка нажатия на кнопку навигации
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (menuAvailable) {
                        toggleDrawer();
                    } else {
                        onBackPressed();
                    }
                }
            });
        }

        drawerView.addDrawerListener(toggle);
        drawerView.setScrimColor(Color.TRANSPARENT);

        fragManager = getSupportFragmentManager();
        fragManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                hideKeyboard();
                DrawerMenuInvalidate();
                int count = fragManager.getBackStackEntryCount();

                // Если фрагментов не осталось снимаем блокировку тулбара
                if (count <= 0) lockToolbar = false;

                if ((count <= 0) && (!lockWindow)) {
                    content_view.setVisibility(View.INVISIBLE);
                    animated_view.setBackgroundColor(colorTransparent);
                    animated_view.setPadding(0, 0, 0, 0);
                    nextAuthParams = null;
                    nextAuthFragment = null;
                    setActionBarBack(false, false);
                } else {
                    if (count <= 0) lockWindow = false;
                    content_view.setVisibility(View.VISIBLE);
                    animated_view.setBackgroundResource(R.drawable.shadow_big);
                    animated_view.setPadding(0, 0, 0, 0);

                    // Спросим у фрагмента позволяет ли он открывать меню
                    ParamFragment topFragment = getTopFragment();
                    if (topFragment != null) {
                        if (!lockToolbar)
                            setActionBarBack(!topFragment.isFragmentAllowMenu(), topFragment.isFragmentIconCross());
                        mainScrollView.setAllowScroll(topFragment.isFragmentAllowScroll());
                    }
                }
            }
        });

        //noinspection ConstantConditions
        findViewById(R.id.new_appeal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddFragment(SetCategoryFragment.class, null, true, 0, true);
            }
        });

        //noinspection ConstantConditions
        findViewById(R.id.swiped_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddFragment(AppealListFragment.class, null, true, 0, false);
            }
        });

        //noinspection ConstantConditions
        findViewById(R.id.my_appeals).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject params = new JSONObject();
                params.putSafe("MyAppeals", true);
                AddFragment(AppealListFragment.class, params, true, 0, true, AppealListFragment.getAuthTitle());
            }
        });

        // Отключим оверскролл в меню, а то на некоторых китайцах выглядит ужасно
        try {
            for (int i = 0; i < layout_drawer.getChildCount(); i++) {
                layout_drawer.getChildAt(i).setOverScrollMode(View.OVER_SCROLL_NEVER);
            }
        } catch (Exception e) {
            Logger.Exception(e);
        }

        // Тестовая кнопка в углу главного экрана. Выводит в лог список
        // неотправленных обращений
        if (testButton != null) {
            testButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    PushMsgListenerService psh = new PushMsgListenerService();
//                    Notification notification = new Notification();
//                    notId++;
//                    notification.setId("notification1" + notId.toString());
//                    notification.setEventType(Notification.FORWARDED);
//                    notification.setObjectNumber(244+notId);
//                    notification.setObjectId("notification1oid" + notId.toString());
//                    notification.setRead(false);
//                    psh.sendNotification(notification);
                    OfflineAppealSender.executeJobs();
                }
            });
        }

        startAppealCardOnNotification(getIntent());
        new RunCatalogsUpdate(this).execute();
    }

    private void startAppealCardOnNotification(Intent intent) {
        Boolean startAppealCard = intent.getBooleanExtra(OPEN_APPEAL_CARD, false);
        Logger.Log("Check for start appeal card on notification: " + startAppealCard.toString());
        if (startAppealCard) {
            JSONObject params = new JSONObject();
            String notificationId = intent.getStringExtra("NotificationID");
            String notificationObjectId = intent.getStringExtra("NotificationObjectID");
            if (isNotEmpty(notificationId) && isNotEmpty(notificationObjectId)) {
                params.putSafe("NotificationID", notificationId);
                params.putSafe("NotificationObjectID", notificationObjectId);

                intent.removeExtra("NotificationID");
                intent.removeExtra("NotificationObjectID");
                intent.removeExtra(OPEN_APPEAL_CARD);

                AddFragment(AppealCardFragment.class, params, true, 0, true);
            }
        }
    }

    private void setupDrawerWidth(boolean restore) {
        if (restore) return;
        if (!mIsDrawerWidthSet) {
            TypedValue outValue = new TypedValue();
            getResources().getValue(R.dimen.drawerWidthPercent, outValue, true);
            float drawerWidthPercent = outValue.getFloat();
            int minWidth = (int) ((getResources().getDisplayMetrics().widthPixels) * drawerWidthPercent);
            int listWidth = drawerMenuView.getMeasuredWidth();
            int sidePadding = drawer_menu_view.getPaddingLeft() + drawer_menu_view.getPaddingRight();
            int maxWidth = listWidth + sidePadding;
            drawerSetWidth = Math.max(maxWidth, minWidth);
            drawerSetHeaderWidth = drawerSetWidth - sidePadding;

            if (maxWidth <= minWidth) {
                // Места на экране - вагон. Поэтому делаем шторку чуть меньше максимальной (65%)
                DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) layout_drawer.getLayoutParams();
                params.width = drawerSetWidth;
                layout_drawer.setLayoutParams(params);

                LinearLayout.LayoutParams header_params = (LinearLayout.LayoutParams) nav_header_block.getLayoutParams();
                header_params.width = drawerSetHeaderWidth;
                nav_header_block.setLayoutParams(header_params);

                FrameLayout.LayoutParams list_params = (FrameLayout.LayoutParams) drawer_menu_view.getLayoutParams();
                list_params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
                drawer_menu_view.setLayoutParams(list_params);
            } else {
                drawerSetWidth = -1;
                drawerSetHeaderWidth = -1;
                // Места мало, шторка = ширина экрана - высота тулбара
                DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) layout_drawer.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                layout_drawer.setLayoutParams(params);

                LinearLayout.LayoutParams header_params = (LinearLayout.LayoutParams) nav_header_block.getLayoutParams();
                header_params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                nav_header_block.setLayoutParams(header_params);

                FrameLayout.LayoutParams list_params = (FrameLayout.LayoutParams) drawer_menu_view.getLayoutParams();
                list_params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                drawer_menu_view.setLayoutParams(list_params);
            }

            mIsDrawerWidthSet = true;
        }
    }

    private void LogDeviceDetails() {
        Logger.Log("GOLOS27. MainActivity activity launched");
        Settings.Init(getApplicationContext());
        Logger.Log("Android version: API" + String.valueOf(android.os.Build.VERSION.SDK_INT));
        Logger.Log("Device model (manufacturer): " + DeviceName.getDeviceName());
        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
        Logger.Log("VK Sign fingerprint: " + Arrays.toString(fingerprints));

        try {
            @SuppressLint("PackageManagerGetSignatures")
            PackageInfo info = getPackageManager().getPackageInfo(this.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Logger.Log("Fb Sign fingerprint: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception ignored) {
        }

        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            Logger.Log("Application version: " + version);
        } catch (Exception ignored) {
        }

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float logicalDensity = metrics.density;
        Logger.Log("Density xdpi = " + String.valueOf(metrics.xdpi));
        Logger.Log("Density ydpi = " + String.valueOf(metrics.ydpi));
        Logger.Log("Size X = " + String.valueOf(metrics.widthPixels));
        Logger.Log("Size Y = " + String.valueOf(metrics.heightPixels));
        Logger.Log("Logical density = " + String.valueOf(logicalDensity));

        int screenSize = getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        String screenSizeValue;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                screenSizeValue = "XLarge";
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                screenSizeValue = "Large";
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                screenSizeValue = "Normal";
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                screenSizeValue = "Small";
                break;
            default:
                screenSizeValue = "Undefined! (" + screenSize + ")";

        }
        Logger.Log("Screen size: " + screenSizeValue);
    }

    private void toggleDrawer() {
        if (drawerView.isDrawerOpen(layout_drawer)) {
            drawerView.closeDrawer(layout_drawer);
        } else {
            drawerView.openDrawer(layout_drawer);
        }
    }

    @Override
    public void DialogSendResult(int dialog_id, int cmd) {

    }

    @Override
    public void DialogSendParamResult(int dialog_id, int cmd, JSONObject params) {
        switch (dialog_id) {
            case AuthDialogFragment.DIALOG_ID:
                if (cmd == Activity.RESULT_OK) {
                    AddFragment(LoginFragment.class, null, true, 0, false);
                }
                if (cmd == Activity.RESULT_CANCELED) {
                    AddFragment(RegRequestFragment.class, null, true, 0, false);
                }
                break;
        }
    }

    synchronized private boolean isDrawerOpened() {
        return drawerView.isDrawerOpen(GravityCompat.START);
    }

    private void setupDrawerMenu() {
        List<DrawerItem> items = new ArrayList<DrawerItem>() {{
            add(new DrawerItem(getString(R.string.enter_or_register), R.id.enter_or_register, true, DrawerItem.SHOW_NON_AUTHORIZED));
            add(new DrawerItem(getString(R.string.item_new_complain), R.id.item_new_appeal, true, DrawerItem.SHOW_ALWAYS));
            add(new DrawerItem(getString(R.string.item_own_complaints), R.id.item_own_complaints, true, DrawerItem.SHOW_AUTHORIZED));
            add(new DrawerItem(getString(R.string.item_complaints), R.id.item_appeals, true, DrawerItem.SHOW_ALWAYS));
            add(new DrawerItem(getString(R.string.item_notifications), R.id.item_notifications, true, DrawerItem.SHOW_AUTHORIZED));
            add(new DrawerItem(getString(R.string.item_own_profile), R.id.item_own_profile, true, DrawerItem.SHOW_AUTHORIZED));
            add(new DrawerItem(getString(R.string.item_help), R.id.item_help, true, DrawerItem.SHOW_ALWAYS));
            add(new DrawerItem(getString(R.string.item_logout), R.id.item_logout, true, DrawerItem.SHOW_AUTHORIZED));
        }};

        drawerMenuView = (RecyclerView) findViewById(R.id.lst_menu_items);
        menuAdapter = new CustomDrawerAdapter(this, R.layout.item_drawer_menu, items);
        drawerMenuView.setAdapter(menuAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setAutoMeasureEnabled(true);
        drawerMenuView.setLayoutManager(layoutManager);
        DrawerMenuInvalidate();
        menuAdapter.setOnItemClickListener(this);
        drawerView.closeDrawer(layout_drawer);
    }

    @Override
    public void onItemClick(int itemId) {
        mClickedMenuItem = itemId;
        mIsMenuItemClicked = true;
        if (drawerView != null) drawerView.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onItemClick2(int position) {
    }

    // Посчитаем коэффициент для маштабирования вида при движении шторки меню
    private void setupDrawerAnimation() {
        float pixelsTotal;
        float pixelsToScale = (toolBarHeight - shadowOffsetTop) * 2;
        float pixelsIntToScale = shadowOffsetTop * 2;
        Rect windowRect = new Rect();
        layout_content.getWindowVisibleDisplayFrame(windowRect);
        pixelsTotal = windowRect.bottom - windowRect.top;
        maxScale = 1f - ((pixelsTotal - pixelsToScale) / pixelsTotal);
        maxIntScale = 1f - ((pixelsTotal - pixelsIntToScale) / pixelsTotal);
    }

    @Override
    public void onBackPressed() {
        if (drawerView != null) {
            if (drawerView.isDrawerOpen(GravityCompat.START)) {
                drawerView.closeDrawer(GravityCompat.START);
            } else {
                if (fragManager.getBackStackEntryCount() > 0) {
                    ParamFragment topFragment = getTopFragment();
                    if (topFragment != null) {
                        if (!topFragment.ExitRequest()) return;
                    }
                }

                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        optionsMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Отсюда все переехало в OnDrawerClosed - т.е. выполняться
        // все будет после закрытия меню для обеспечения плавности
        // анимации и корректной смены иконки тулбара
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    /***
     * Меняет кнопку навигации на соответствующий состоянию
     *
     * @param set               true - кнопка Back, false - кнопка Hamburger
     * @param fragmentIconCross если true - иконка тулбара - крестик
     */
    public void setActionBarBack(boolean set, boolean fragmentIconCross) {
        synchronized (lock) {
            if (set) {
                menuAvailable = false;
                drawerView.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                if (fragmentIconCross) {
                    materialMenu.animateIconState(MaterialMenuIcon.IconState.X);
                } else {
                    materialMenu.animateIconState(MaterialMenuIcon.IconState.ARROW);
                }
            } else {
                menuAvailable = true;
                drawerView.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                materialMenu.animateIconState(MaterialMenuIcon.IconState.BURGER);
            }
        }
    }

    @Override
    public void FragmentSendResult(int fragment_id, int cmd, JSONObject result) {
        switch (fragment_id) {
            // Обновим кол-во непрочитанных уведомлений в главном меню
            case UPDATE_NOTIFICATIONS:
                new CheckForNotifications().execute();
                break;

            case AUTH_COMMAND_ID:
                // Это событие авторизации - нам надо загрузить профиль, поменять меню
                // и вывести в меню полное имя пользователя
                if (cmd == AUTH_COMMAND_HAVE) {
                    new RunAuthorizationEvents().execute();
                } else {
                    SetupAuthorization(false);
                }
                break;

            // Данные пользователя сохранены. Возвращаемся к списку
            case ChangeGeneralFragment.FRAGMENT_ID:
                new RunAuthorizationEvents().execute();
                PopFragment(false);
                break;

            // Адрес пользователя сохранен. Возврашаемся к списку
            case ChangeAddressFragment.FRAGMENT_ID:
                new RunAuthorizationEvents().execute();
                PopFragment(false);
                break;

            // Пароль изменен. Возвращаемся к списку
            case ChangePasswordFragment.FRAGMENT_ID:
                new RunAuthorizationEvents().execute();
                PopFragment(false);
                break;
        }
    }

    /***
     * Фрагмент попросил нас запустить следующий фрагмент цепочки
     *
     * @param fragment_id идентификатор отправителя
     * @param chainNext   класс следующего фрагмента в цепочке
     * @param params      параметры, передаваемые фрагменту
     */
    @Override
    public void FragmentChain(int fragment_id, Class chainNext, JSONObject params,
                              int popCount, boolean lockWindow, boolean authNeeded) {
        if (popCount > 0) {
            lockToolbar = true;
            try {
                for (int i = 0; i < popCount; i++) PopFragment(lockWindow);
            } catch (Exception ignored) {
            }
        }

        if (chainNext != null) AddFragment(chainNext, params, authNeeded);
    }

    /***
     * Фрагмент попросил нас выгрузить несколько фрагментов из стека
     *
     * @param fragment_id идентификатор отправителя
     * @param popCount    количество выгружаемых фрагментов
     * @param lockWindow  блокировать ли сокрытие основного окна
     */
    @Override
    public void FragmentPop(int fragment_id, int popCount, boolean lockWindow) {
        try {
            for (int i = 0; i < popCount; i++) {
                PopFragment(lockWindow);
            }
        } catch (Exception ignored) {
        }
    }

    /***
     * Вставляет фрагмент в вид по умолчанию. Меняет также заголовок окна.
     * Передает параметры новому фрагменту
     *
     * @param fragment класс фрагмента для добавления
     */
    public void AddFragment(Class fragment, JSONObject param, boolean authNeeded) {
        AddFragment(R.id.main_content, fragment, param, true, 0, null, authNeeded, null);
    }

    public void AddFragment(Class fragment, JSONObject param, boolean addBackStack, int delay, boolean authNeeded) {
        AddFragment(R.id.main_content, fragment, param, addBackStack, delay, null, authNeeded, null);
    }

    public void AddFragment(Class fragment, JSONObject param, boolean addBackStack, int delay, boolean authNeeded, String title) {
        AddFragment(R.id.main_content, fragment, param, addBackStack, delay, null, authNeeded, title);
    }

    @SuppressWarnings("unchecked")
    public void AddFragment(final int id, final Class fragment, final JSONObject param,
                            final boolean addBackStack, final int delay, final String name,
                            final boolean authNeeded, String title) {

        // Если нужна авторизация, а ее нету - запускаем диалог
        if (authNeeded && !Settings.getBool(Settings.AUTHORIZED_FLAG)) {
            nextAuthParams = param;
            nextAuthFragment = fragment;
            if (isActive) AuthDialog.makeDialog(context, this, null, title).show(this, getSupportFragmentManager());
            return;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    ParamFragment fragObject = (ParamFragment) fragment
                            .getMethod("newInstance", Class.class, JSONObject.class)
                            .invoke(null, fragment, param);

                    String title = getResources().getString(fragObject.getTitleId());
                    TextView toolbar_text = (TextView) findViewById(R.id.toolbar_text);
                    if (toolbar_text != null) toolbar_text.setText(title);

                    // Фрагменты первого уровня (с доступным меню) могут быть только в 1 экземпляре
                    // перед их добавлением удаляем все остальные, имеющиеся в стеке
                    if (fragObject.isFragmentAllowMenu()) {
                        if (fragManager.getBackStackEntryCount() > 0) lockWindow = true;
                        while (fragManager.getBackStackEntryCount() > 0) {
                            if (fragObject.ExitRequest()) {
                                fragManager.popBackStackImmediate();
                            } else {
                                // Возможно надо запомнить новый фрагмент чтобы потом его
                                // закинуть после выхода текущего фрагмента
                                return; // Фрагмент не позволил себя заменить
                            }
                        }
                    }

                    // Отдельная проверка на LoginFragment.class - ему возможно требуется
                    // передать параметром следующий запускаемый фрагмент
                    if (fragObject instanceof LoginFragment)
                        fragObject.setNextFragmentClass(nextAuthFragment, nextAuthParams);

                    // Проверим фрагмент
                    FragmentTransaction fTrans = fragManager.beginTransaction();
                    fTrans.replace(id, fragObject);
                    if (addBackStack) fTrans.addToBackStack(name);
                    fTrans.commit();
                    hideKeyboard();
                } catch (Exception e) {
                    Logger.Exception(e);
                }
            }
        }, delay);
    }

    /***
     * Удаляет из стека фрагментов самый верхний
     */
    public void PopFragment(boolean lock) {
        lockWindow = lock;
        getSupportFragmentManager().popBackStack();
        hideKeyboard();
    }

    public void PopFragments(String name) {
        getSupportFragmentManager().popBackStack(name, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        hideKeyboard();
    }

    public ParamFragment getTopFragment() {
        List<Fragment> fragList = fragManager.getFragments();
        if(fragList==null) return null;
        ParamFragment top;
        for (int i = fragList.size() - 1; i >= 0; i--) {
            if (fragList.get(i) instanceof ParamFragment) {
                top = (ParamFragment) fragList.get(i);
                if (top != null) {
                    return top;
                }
            }
        }
        return null;
    }

    /***
     * Прячет софт-клавиатуру
     */
    public void hideKeyboard() {
        View cf = getCurrentFocus();
        if (cf != null) {
            IBinder wt = cf.getWindowToken();
            if (wt != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(wt, 0);
            }
        }
    }

    /***
     * Вызывается по событию авторизации либо при запуске приложения
     * Пытается загрузить профиль пользователя и если все успешно
     * меняет интерфейс на более так скажем "расширенный"
     */
    private class RunAuthorizationEvents extends AsyncTask<Void, Void, String> {

        @Override
        protected final String doInBackground(Void... params) {
            try {
                ServerTalk server = new ServerTalk();
                return server.getAccessToken();
            } catch (Exception e) {
                Logger.Exception(e);
            }

            return "---unknownToken---";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equals(Transport.INVALID_CREDENTIALS_TOKEN)) {
                if (isNotEmpty(Settings.getStr(Settings.USER_REFRESH_TOKEN)))
                    Logger.Log("Auth check failed. Bad credentials! Need user interaction");
                SetupAuthorization(false);
            } else {
                if (isEmpty(result))
                    Logger.Log("Auth check failed. Network error! Trying to start auth events!");
                else {
                    Logger.Log("Auth check success!. Starting auth events");
                }
                SetupAuthorization(true);
            }
        }
    }

    /***
     * Выполняет изменения в интерфейсе для авторизованного
     * и неавторизованного пользователя
     *
     * @param authorize true если пользователь авторизирован
     */
    public void SetupAuthorization(boolean authorize) {
        CustomDrawerAdapter adapter = (CustomDrawerAdapter) drawerMenuView.getAdapter();

        // Меню меняется, надо заново вычислить его ширину (если меню открыто, его ширину менять
        // нельзя иначе начнутся проблемы с отрицательными значениями смещения и неполного его закрытия
        if (!isDrawerOpened()) {
            RelayoutDrawer();
        } else {
            mIsDrawerWidthChanged = true;
        }

        if (authorize) {
            adapter.setUserHasAuthorized(true);
            Settings.setParameter(Settings.AUTHORIZED_FLAG, "true");
            new CheckForProfile().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            new CheckForNotifications().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            //new CheckForMyAppeals().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            // Удаляет подписку на push уведомления, а также чистит авторизацию после выполнения запроса
            PushNotifications.Unsubscribe();
            nav_header_block.setVisibility(View.GONE);
            materialMenu.setShowNewNotifications(false);
            adapter.setUserHasAuthorized(false);
            View cntView = findViewById(R.id.myAppealsCountLayout);
            if (cntView != null) cntView.setVisibility(View.INVISIBLE);
        }

        DrawerMenuInvalidate();
    }

    private void RelayoutDrawer() {
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) layout_drawer.getLayoutParams();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        layout_drawer.setLayoutParams(params);
        layout_drawer.requestLayout();
        mIsDrawerWidthChanged = false;
        mIsDrawerWidthSet = false;
    }

    public void DrawerMenuInvalidate() {
        mIsDrawerWidthSet = false;
        CustomDrawerAdapter adapter = (CustomDrawerAdapter) drawerMenuView.getAdapter();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void FragmentClearFocus() {
        View v = getCurrentFocus();
        if (v instanceof EditText) {
            v.clearFocus();
            hideKeyboard();
        }
    }

    /***
     * Вызывается при старте приложения для загрузки справочников.
     * Интерфейс до этого отключен путем скрывания вида.
     * После успешного завершения показывает вид с интерфейсом.
     * При неуспешном завершении показывает диалог с ошибкой
     * и через интерфейс отправляет сигнал приложению повторить загрузку
     */
    private class RunCatalogsUpdate extends AsyncTask<Void, Void, ErrorResult>
            implements DialogResultInterface {

        private Activity activity;
        private ErrorResult resultCode = new ErrorResult(ErrorResult.SUCCESS);
        LoadingResultInterface loadingListener;

        public RunCatalogsUpdate(Activity activity) {
            this.activity = activity;
            loadingListener = (LoadingResultInterface) activity;
        }

        @Override
        protected final ErrorResult doInBackground(Void... params) {
            //noinspection EmptyTryBlock
            try {
                ErrorResult result = Catalogs.UpdateOKTMOCatalog();
                if (result.getErrorCode() != ErrorResult.SUCCESS) return result;

                result = Catalogs.UpdateSubjectsCatalog();
                if (result.getErrorCode() != ErrorResult.SUCCESS) return result;

                result = Catalogs.UpdateExecutorsCatalog();
                if (result.getErrorCode() != ErrorResult.SUCCESS) return result;

            } catch (Exception e) {
                Logger.Exception(e);
                return new ErrorResult(ErrorResult.APPLICATION_ERROR);
            }

            return new ErrorResult(ErrorResult.SUCCESS);
        }

        @Override
        protected void onPostExecute(ErrorResult result) {
            super.onPostExecute(result);
            resultCode = result;
            if (result.getErrorCode() == ErrorResult.SUCCESS) {
                loadingProgressView.setVisibility(View.INVISIBLE);
                drawerView.setVisibility(View.VISIBLE);
                loadingListener.onLoadingResult(result);
            } else {
                ResultDialog.makeDialog(Application.AppContext, R.drawable.error_grey,
                        "Ошибка загрузки\r\nсправочников", result.getErrorText(),
                        "Повторить", this).show(activity);
            }
        }

        @Override
        public void DialogSendResult(int dialog_id, int cmd) {
            if (cmd == RESULT_OK) {
                // Пользователь нажал кнопку "Повторить". Отправляем команду
                // в activity где обновление будет запущено снова
                loadingListener.onLoadingResult(resultCode);
            }

            // Если пользователь нажал отмену - выходим из приложения.
            // Мы не можем продолжать работу без справочников!
            if (cmd == RESULT_CANCELED) {
                try {
                    activity.finish();
                    System.exit(0);
                } catch (Exception e) {
                    Logger.Exception(e);
                }
            }
        }

        @Override
        public void DialogSendParamResult(int dialog_id, int cmd, JSONObject params) {
        }
    }

    @Override
    public void onLoadingResult(ErrorResult errorCode) {
        if (errorCode.getErrorCode() != ErrorResult.SUCCESS)
            new RunCatalogsUpdate(this).execute();
    }


    /***
     * Вызывается по событию авторизации либо при запуске приложения
     * Пытается загрузить профиль пользователя и если все успешно
     * меняет интерфейс на более так скажем "расширенный"
     */
    private class CheckForProfile extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected final JSONObject doInBackground(Void... params) {
            ServerTalk server = new ServerTalk();
            return server.getProfile();
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            try {
                if (result.getInt("errorCode", ErrorResult.UNKNOWN_ERROR) == ErrorResult.SUCCESS) {
                    Profile.ParseServerResponse(result);
                    item_text_full_name.setText(Profile.FullName());
                    nav_header_block.setVisibility(View.VISIBLE);
                    Logger.Log("GetProfile success. Time to change drawerView look&fill");
                } else {
                    Logger.Warn("GetProfile returned error (" + result.get("errorCode") + ")");
                }
            } catch (Exception e) {
                Logger.Exception(e);
            }

            DrawerMenuInvalidate();
        }
    }

    /***
     * Вызывается по событию авторизации
     * Пытается загрузить количество непрочитанных уведомлений
     * и если кол-во > 0 добавляет изменения в интерфейс
     */
    private class CheckForNotifications extends AsyncTask<Void, Void, Integer> {
        @Override
        protected final Integer doInBackground(Void... params) {
            return new ServerTalk().getUnreadNotificationsCount();
        }

        @Override
        protected void onPostExecute(Integer notificationsCount) {
            super.onPostExecute(notificationsCount);
            if (notificationsCount == null) return;
            List<DrawerItem> data = menuAdapter.getData();
            Logger.Log("Get unread notification count returned " + String.valueOf(notificationsCount));
            materialMenu.setShowNewNotifications(notificationsCount > 0);
            for (DrawerItem entry : data)
                if (entry.getItemId() == R.id.item_notifications) {
                    entry.setNewItemsCount(notificationsCount);
                    DrawerMenuInvalidate();
                }
            DrawerMenuInvalidate();
        }
    }

    /***
     * Вызывается по событию авторизации
     * Пытается загрузить количество собственных обращений
     * и если кол-во > 0 добавляет изменения в интерфейс
     */
    private class CheckForMyAppeals extends AsyncTask<Void, Void, Integer> {
        @Override
        protected final Integer doInBackground(Void... params) {
            return new ServerTalk().getMyAppealsCount();
        }

        @Override
        protected void onPostExecute(Integer myAppealsCount) {
            super.onPostExecute(myAppealsCount);
            View cntView = findViewById(R.id.myAppealsCountLayout);
            if (myAppealsCount == null || myAppealsCount <= 0) {
                if (cntView != null) cntView.setVisibility(View.INVISIBLE);
                return;
            }
            TextView cntTextView = (TextView) findViewById(R.id.myAppealsCount);
            String countText = String.valueOf(myAppealsCount);
            if (myAppealsCount > 99) countText = "99+";
            if (cntTextView != null) cntTextView.setText(countText);
            if (cntView != null) cntView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MainActivity.SOCIAL_NETWORK_TAG);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}

