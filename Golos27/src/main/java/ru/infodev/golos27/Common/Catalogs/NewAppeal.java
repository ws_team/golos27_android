package ru.infodev.golos27.Common.Catalogs;

import android.os.AsyncTask;

import ru.infodev.golos27.Common.Catalogs.Appeal.AppealClaim;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;

public class NewAppeal {
    private static final AppealClaim instance = new AppealClaim();

    private static OKTMO executorFilterCity = null;
    synchronized public static OKTMO getOrSetExecutorFilterCity(OKTMO city) {
        if (city != null) executorFilterCity = city;
        return executorFilterCity;
    }

    public static AppealClaim getInstance() {
        return instance;
    }

    /***
     * Ручное задание исполнителя для обращения
     *
     * @param executor исполнитель
     * @return объект с новым обращением
     */
    @SuppressWarnings("unused")
    synchronized public static AppealClaim setExecutor(Executor executor) {
        instance.setExecutor(executor);
        instance.setAutoExecutor(false);
        return instance;
    }

    /***
     * При задании нового населенного пункта запускает задачу получения информации об
     * исполнителе для данного обращения (если исполнитель не был задан вручную)
     *
     * @param settlement населенный пункт в котором имеется проблема
     * @return объект с новым обращением
     */
    synchronized public static AppealClaim setSettlement(OKTMO settlement) {
        getOrSetExecutorFilterCity(settlement);
        instance.Address().setSettlement(settlement);
        if (instance.isAutoExecutor())
            new RequestDefaultExecutor().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
        return instance;
    }

    /***
     * При изменении тематики приложения запускается задача получения информации об
     * исполнителе для данного обращения (если исполнитель не был задан вручную)
     *
     * @param subject тематика обращения
     * @return объект с новым обращением
     */
    synchronized public static AppealClaim setSubject(Subject subject) {
        instance.setSubject(subject);
        if (instance.isAutoExecutor())
            new RequestDefaultExecutor().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
        return instance;
    }

    /***
     * Запрашивает с сервера исполнителя по умолчанию используя выбранную
     * пользователем тематику и определенный по координатам oktmo код
     */
    static class RequestDefaultExecutor extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... params) {
            ServerTalk server = new ServerTalk();
            OKTMO settlement = instance.Address().Settlement();
            Subject subject = instance.Subject();
            if (settlement != null && subject != null) {
                return server.getDefaultExecutor(settlement.getCode(), instance.Subject().Id());
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            String name = null;
            String executorId = null;
            super.onPostExecute(result);
            if (result == null) return;

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    try {
                        name = result.getString("name");
                        executorId = result.getString("executorId");
                    } catch (Exception e) {
                        Logger.Exception(e);
                    }

                    if (name != null && executorId != null) {
                        Executor executor = Executor.Get(executorId);
                        if (executor != null) instance.setExecutor(executor);
                    }
                }
            } catch (Exception e) {
                Logger.Exception(e);
            }
        }
    }
}
