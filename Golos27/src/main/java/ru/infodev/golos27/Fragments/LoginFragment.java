package ru.infodev.golos27.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telecom.Call;
import android.text.Editable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.CallbackActivity;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Profile;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.Registration.RegRequestFragment;
import ru.infodev.golos27.Fragments.Restore.RestInitFragment;
import ru.infodev.golos27.MainActivity;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.PushNotifications;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.CustomEditText;

import static ru.infodev.golos27.ApiConfig.FRONT_ADDRESS;

public class LoginFragment extends ParamFragment
        implements OnClickListener, TextWatcherAdapter.TextWatcherListener {

    public static final int FRAGMENT_ID = R.layout.login;

    private CustomEditText passwordEditText;
    private CustomEditText usernameEditText;
    private Button loginButton;
    private ProgressBar loginProgress;
    private TextView showPasswordLabel;
    private LinearLayout esiaLoginButton;

    boolean usernameOK = false;
    boolean passwordOK = false;
    private boolean showPassword = false;

    private BroadcastReceiver broadcastReceiver;

    public LoginFragment() {
        titleId = R.string.login;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        setTitle(titleId);

        showPasswordLabel = (TextView) v.findViewById(R.id.show_password);
        showPasswordLabel.setOnClickListener(this);

        passwordEditText = (CustomEditText) v.findViewById(R.id.edit_password);
        passwordEditText.setTypeface(roboto_light);
        passwordEditText.setOnTextChangedListener(this);

        usernameEditText = (CustomEditText) v.findViewById(R.id.edit_username);
        usernameEditText.setTypeface(roboto_light);
        usernameEditText.setOnTextChangedListener(this);

        TextView forgotPasswordLabel = (TextView) v.findViewById(R.id.forgot_password);
        forgotPasswordLabel.setOnClickListener(this);

        loginButton = (Button) v.findViewById(R.id.enter_login);
        loginButton.setOnClickListener(this);

        Button registerButton = (Button) v.findViewById(R.id.register_login);
        registerButton.setOnClickListener(this);

        loginProgress = (ProgressBar) v.findViewById(R.id.progressBar);
        passwordEditText.setTypeface(usernameEditText.getTypeface());

        esiaLoginButton = (LinearLayout) v.findViewById(R.id.login_esia);
        esiaLoginButton.setOnClickListener(this);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String accessToken = intent.getStringExtra(CallbackActivity.ACCESS_TOKEN);
                String refreshToken = intent.getStringExtra(CallbackActivity.REFRESH_TOKEN);
                String expiresIn = intent.getStringExtra(CallbackActivity.EXPIRES_IN);
                handleSuccessfulLogin(accessToken, refreshToken, expiresIn);
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CallbackActivity.ESIA_LOGIN_SUCCESS);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(Settings.getStr(Settings.USER_ACCESS_TOKEN)!= null && !Settings.getStr(Settings.USER_ACCESS_TOKEN).isEmpty()) {
            fragmentResultListener.FragmentSendResult(MainActivity.AUTH_COMMAND_ID,
                    MainActivity.AUTH_COMMAND_HAVE, null);

            if (nextFragment == null) {
                chainStartNextFragment(FRAGMENT_ID, null, null, true);
            } else {
                chainStartNextFragment(FRAGMENT_ID, nextFragment, nextParams, 1, true);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_password:
                if (showPassword) {
                    showPassword = false;
                    String label_text = getResources().getString(R.string.show_password);
                    showPasswordLabel.setText(label_text);
                    passwordEditText.setTypeface(usernameEditText.getTypeface());
                    passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwordEditText.setSelection(passwordEditText.getText().length());
                } else {
                    showPassword = true;
                    String label_text = getResources().getString(R.string.hide_password);
                    showPasswordLabel.setText(label_text);
                    passwordEditText.setTypeface(usernameEditText.getTypeface());
                    passwordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwordEditText.setSelection(passwordEditText.getText().length());
                }
                break;

            case R.id.forgot_password:
                chainStartNextFragment(FRAGMENT_ID, RestInitFragment.class, null, false);
                break;

            case R.id.register_login:
                // Пользователь не имеет учетной записи и хочет зарегистрироваться
                chainPopFragments(FRAGMENT_ID, 1, true);
                chainStartNextFragment(FRAGMENT_ID, RegRequestFragment.class, null, false);
                break;

            case R.id.enter_login:
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                new LoginTask(username, password).execute();
                break;

            case R.id.login_esia:
                getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FRONT_ADDRESS + "/bridge/mc/esialogin")));
                break;
        }
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        switch (view.getId()) {
            case R.id.edit_username:
                usernameOK = text.length() > 0;
                break;

            case R.id.edit_password:
                passwordOK = text.length() > 0;
                break;

        }
        loginButton.setEnabled(usernameOK & passwordOK);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // pass
    }

    @Override
    public void afterTextChanged(Editable s) {
        // pass
    }

    class LoginTask extends AsyncTask<Void, Void, JSONObject> {
        String username;
        String password;

        public LoginTask(String username, String password) {
            this.username = username;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loginButton.setVisibility(View.INVISIBLE);
            loginProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected final JSONObject doInBackground(Void[] params) {
            ServerTalk server = new ServerTalk();
            return server.getAuthToken(username, password);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            loginButton.setEnabled(false);
            loginButton.setVisibility(View.VISIBLE);
            loginProgress.setVisibility(View.INVISIBLE);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == ErrorResult.SUCCESS) {
                    Settings.Store(Settings.HASH1, username);
                    Settings.Store(Settings.HASH2, password);

                    if (result.has("access_token") && result.has("refresh_token")) {
                        String access_token = (String) result.get("access_token");
                        String refresh_token = (String) result.get("refresh_token");
                        Integer expires_in = (Integer) result.get("expires_in");

                        handleSuccessfulLogin(access_token, refresh_token, String.valueOf(expires_in));
                    }
                } else {
                    Profile.RemoveAllData();
                    fragmentResultListener.FragmentSendResult(MainActivity.AUTH_COMMAND_ID,
                            MainActivity.AUTH_COMMAND_NOT, result);

                    ResultDialog.showError(getActivity(), "Ошибка входа", errorCode, result.getErrorText());
                }
            } catch (Exception e) {
                Logger.Exception(e);
                ResultDialog.showError(getActivity(), "Ошибка входа", ErrorResult.INTERNAL_ERROR);
            }
            loginButton.setEnabled(true);
        }
    }

    private void handleSuccessfulLogin(String accessToken, String refreshToken, String expiresIn) {
        Settings.setParameter(Settings.USER_ACCESS_TOKEN, accessToken);
        Settings.setParameter(Settings.USER_REFRESH_TOKEN, refreshToken);
        Settings.setParameter(Settings.USER_TOKEN_EXPIRES_IN, expiresIn);
        Settings.setParameter(Settings.USER_TOKEN_START_TIME, String.valueOf(System.currentTimeMillis()));

        PushNotifications.Subscribe(true);
        fragmentResultListener.FragmentSendResult(MainActivity.AUTH_COMMAND_ID,
                MainActivity.AUTH_COMMAND_HAVE, null);

        if (nextFragment == null) {
            chainStartNextFragment(FRAGMENT_ID, null, null, true);
        } else {
            chainStartNextFragment(FRAGMENT_ID, nextFragment, nextParams, 1, true);
        }
    }
}
