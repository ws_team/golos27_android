package ru.infodev.golos27.Fragments.Dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;

import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.MainActivity;
import ru.infodev.golos27.R;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;

/***
 * Вызывается из треда UI. Открывает диалог с указанными параметрами
 */
public class ResultDialog {
    ResultDialogFragment rDialog;

    public ResultDialog(Context context, int iconId, String resultText,
                        String resultFullText, String buttonText, DialogResultInterface listener) {

        rDialog = new ResultDialogFragment();
        rDialog.setContext(context);
        rDialog.setIcon(iconId);
        rDialog.setMessages(resultText, resultFullText, buttonText);
        if (listener != null) {
            rDialog.setResultListener(listener);
        }
    }

    @SuppressWarnings("unused")
    public void setDismiss(boolean cancelable) {
        rDialog.setCancelable(cancelable);
    }

    public static ResultDialog makeDialog(Context context, int iconId, String resultText, String resultFullText, String buttonText) {
        return new ResultDialog(context, iconId, resultText, resultFullText, buttonText, null);
    }

    public static ResultDialog makeDialog(Context context, int iconId, String resultText,
                                          String resultFullText, String buttonText,
                                          DialogResultInterface listener) {
        return new ResultDialog(context, iconId, resultText, resultFullText, buttonText, listener);
    }

    public void show(Activity activity) {
        if (activity != null) {
            if (activity instanceof MainActivity) {
                if (((MainActivity) activity).isActive)
                    rDialog.show(activity.getFragmentManager(), "ResultDialog");
            } else {
                rDialog.show(activity.getFragmentManager(), "ResultDialog");
            }
        }
    }

    public static void showError(FragmentActivity activity, String title, int errorCode) {
        showError(activity, title, errorCode, null);
    }

    public static void showError(FragmentActivity activity, String title, int errorCode, String errorText) {
        if (activity == null) return;
        if (isEmpty(errorText)) errorText = ErrorResult.getErrorText(ErrorResult.APPLICATION_ERROR);
        if (errorCode == ErrorResult.NETWORK_ERROR || isEmpty(title)) title = "Ошибка связи";
        makeDialog(activity, R.drawable.error_grey, title, errorText, null).show(activity);
    }
}
