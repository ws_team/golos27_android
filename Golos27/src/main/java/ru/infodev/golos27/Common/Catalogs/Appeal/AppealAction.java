package ru.infodev.golos27.Common.Catalogs.Appeal;


public class AppealAction {
    String actionId = "";
    String actionName = "";
    //---------------------------------------------------------------

    public String ActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String ActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String toJson() {
        return actionName;
    }

    public static AppealAction fromJson(String source) {
        AppealAction result = new AppealAction();
        result.setActionId("-1");
        result.setActionName(source);
        return result;
    }
}
