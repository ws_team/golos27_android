package ru.infodev.golos27.Common.Interfaces;

import ru.infodev.golos27.Common.ErrorResult;

public interface LoadingResultInterface {
    void onLoadingResult(ErrorResult errorCode);
}
