package ru.infodev.golos27.Fragments;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Notification;
import ru.infodev.golos27.Common.Catalogs.NotificationListSingleton;
import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Interfaces.RecycleViewClickInterface;
import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.AppealList.AppealCardFragment;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.MainActivity;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.Widgets.Adapters.NotificationListAdapter;
import ru.infodev.golos27.Widgets.Lists.NestedRecyclerView;
import ru.infodev.golos27.Widgets.RecyclerViewScrollListener;
import ru.infodev.golos27.Widgets.SwipeToLoad.OnLoadMoreListener;
import ru.infodev.golos27.Widgets.SwipeToLoad.OnRefreshListener;
import ru.infodev.golos27.Widgets.SwipeToLoad.SwipeToLoadLayout;

public class NotificationsFragment extends ParamFragment implements OnRefreshListener, OnLoadMoreListener, RecycleViewClickInterface {
    public static final int FRAGMENT_ID = R.layout.notification_list;
    public static final int FIRST_PAGE = 1;
    private List<Notification> notificationList = new ArrayList<>();
    private NotificationListAdapter adapter;
    private static final int page_size = 20;
    private int current_page = FIRST_PAGE;
    private boolean isLoading = false;
    private boolean scrollLoad = true;
    private RecyclerViewScrollListener scrollListener;
    private SwipeToLoadLayout swipeToLoadLayout;
    private TextView emptyContentHint;
    private LinearLayout progress;

    public NotificationsFragment() {
        titleId = R.string.notifications;
        setFragmentAllowMenu(true);
        setFragmentAllowScroll(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.Log("AppealList fragment destroyed. Clear list!");
        NotificationListSingleton.setNotificationList(new ArrayList<Notification>());
        NotificationListSingleton.setNotificationsCurrentPage(FIRST_PAGE);
    }

    @Override
    public void onPause() {
        super.onPause();
        List<Notification> list = adapter.getData();
        Logger.Log("AppealList fragment paused. Saving list! (count=" + String.valueOf(list.size()) + ")");
        NotificationListSingleton.setNotificationList(list);
        NotificationListSingleton.setNotificationsCurrentPage(current_page);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (NotificationListSingleton.isNeedRefresh()) {
            Logger.Log("AppealList fragment resumed. Was changed - updating!");
            new GetNextNotifications().execute();
        } else {
            Logger.Log("AppealList fragment resumed. No changes!");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        setActionButtonVisibility(false);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        setRetainInstance(true);
        setTitle(titleId);

        NotificationManager mNotificationManager = (NotificationManager)
                Application.AppContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();

        notificationList = NotificationListSingleton.getNotificationList();
        if (notificationList == null) notificationList = new ArrayList<>();
        if (notificationList.size() > 0) current_page = NotificationListSingleton.getNotificationsCurrentPage();

        adapter = new NotificationListAdapter(Application.AppContext, notificationList);
        adapter.setOnClickListener(this);

        emptyContentHint = (TextView) v.findViewById(R.id.empty_content_hint);
        progress = (LinearLayout) v.findViewById(R.id.loading_progress);
        NestedRecyclerView listView = (NestedRecyclerView) v.findViewById(R.id.swiped_list);
        listView.setHorizontalScrollBarEnabled(false);
        listView.setVerticalScrollBarEnabled(true);
        listView.setNestedScrollingEnabled(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(Application.AppContext);
        layoutManager.setAutoMeasureEnabled(false);
        scrollListener = new RecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (scrollLoad) new GetNextNotifications().execute();
            }

            @Override
            public void onScrolled(boolean first_item, boolean last_item) {
            }
        };

        swipeToLoadLayout = (SwipeToLoadLayout) v.findViewById(R.id.swipeToLoadLayout);
        swipeToLoadLayout.setOnLoadMoreListener(this);
        swipeToLoadLayout.setOnRefreshListener(this);

        ItemTouchHelper touchHelper = new ItemTouchHelper(new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int dragFlags = 0;
                int swipeFlags = ItemTouchHelper.END;
                return makeMovementFlags(dragFlags, swipeFlags);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                    float dX, float dY, int actionState, boolean isCurrentlyActive) {
                viewHolder.itemView.setTranslationX(0);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                try {
                    int pos = viewHolder.getAdapterPosition();
                    Notification notification = adapter.getData().get(pos);
                    if (!notification.isRead()) new SetNotificationRead(notification.Id()).execute();
                } catch (Exception ignored) {
                }
            }
        });

        touchHelper.attachToRecyclerView(listView);

        listView.setLayoutManager(layoutManager);
        listView.addOnScrollListener(scrollListener);
        listView.setAdapter(adapter);
        if (notificationList.size() <=0) {
            new GetNextNotifications().execute();
        } else {
            emptyContentHint.setVisibility(View.INVISIBLE);
        }
        return v;
    }

    @Override
    public void onItemClick(int position) {
        Notification notification = adapter.getData().get(position);
        JSONObject params = new JSONObject();
        params.putSafe("NotificationID", notification.Id());
        params.putSafe("NotificationObjectID", notification.ObjectId());
        chainStartNextFragment(FRAGMENT_ID, AppealCardFragment.class, params, 0, false);
    }

    @Override
    public void onItemClick2(int position) {
    }

    // Догружаем следующую страницу
    @Override
    public void onLoadMore() {
        if (setOrCheckLoading(null))
            swipeToLoadLayout.setLoadingMore(false);
        else
            new GetNextNotifications().execute();
    }

    // Загружаем заново первую страницу. Все уже
    // загруженное - удаляем
    @Override
    public void onRefresh() {
        if (setOrCheckLoading(null)) {
            swipeToLoadLayout.setRefreshing(false);
        } else {
            current_page = FIRST_PAGE;
            new GetNextNotifications().execute();
        }
    }

    synchronized public boolean setOrCheckLoading(Boolean loading) {
        if (loading != null) this.isLoading = loading;
        return this.isLoading;
    }

    private class GetNextNotifications extends AsyncTask<Integer, Void, JSONObject> implements DialogResultInterface {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setOrCheckLoading(true);

            swipeToLoadLayout.post(new Runnable() {
                @Override
                public void run() {
                    setOrCheckLoading(true);
                    swipeToLoadLayout.setRefreshing(true);
                }
            });

//            if (!swipeToLoadLayout.isLoadingMore() && !swipeToLoadLayout.isRefreshing())
//                progress.setVisibility(View.VISIBLE);

            swipeToLoadLayout.setLoadMoreEnabled(false);
            swipeToLoadLayout.setRefreshEnabled(false);

        }

        @Override
        protected JSONObject doInBackground(Integer... params) {
            ServerTalk server = new ServerTalk();
            Logger.Log("Loading next notification page: " + String.valueOf(current_page));
            return server.getNotificationsList(page_size, current_page);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            JSONArray jsonList = new JSONArray();

            if (result != null) {
                if (result.has("errorCode") && !result.isNull("errorCode")) {
                    ErrorResult retVal = new ErrorResult(result.getInt("errorCode", 0), result.getString("errorText", ""));
                    if (retVal.getErrorCode() != ErrorResult.SUCCESS) {
                        StopLoading();
                        if (fragmentActive) {
                            ResultDialog.makeDialog(Application.AppContext, R.drawable.error_grey,
                                    "Ошибка загрузки\r\nуведомлений", retVal.getErrorText(),
                                    "Повторить", this).show(getActivity());
                        }
                        return;
                    }

                    if (fragmentResultListener != null)
                        fragmentResultListener.FragmentSendResult(MainActivity.UPDATE_NOTIFICATIONS, 0, null);

                    try {
                        if (result.has(JSONObject.JSON_LONG_STRING)) {
                            JSONObject temp = new JSONObject(result.getString(JSONObject.JSON_LONG_STRING, ""));
                            if (temp.has("response")) {
                                jsonList = temp.getJSONObject("response").getJSONArray("items");
                            }
                        } else {
                            jsonList = result.getJSONArray("items");
                        }
                    } catch (Exception e) {
                        Logger.Exception(e);
                    }

                    if (current_page == FIRST_PAGE) {
                        adapter.setData(new ArrayList<Notification>());
                        NotificationListSingleton.setNeedRefresh(false);
                    }

                    notificationList = adapter.getData();
                    for (int i = 0; i < jsonList.length(); i++) {
                        try {
                            JSONObject row = jsonList.getJSONObject(i);
                            Notification notification = Notification.fromJson(row);
                            if (notification != null) {
                                boolean newNotification = true;
                                int notifyListCount = notificationList.size();
                                //int startFromIndex = Math.max(0, notifyListCount - page_size);
                                int startFromIndex = 0;

                                try {
                                    Logger.Log("Adding " + notification.Id() + " and checking from " +
                                            String.valueOf(startFromIndex) + " to " + String.valueOf(notifyListCount));

                                    for (int j = startFromIndex; j < notifyListCount; j++)
                                        if (notificationList.get(j).Id().equals(notification.Id()))
                                            newNotification = false;
                                } catch (Exception ignored) {
                                }

                                if (newNotification) notificationList.add(notification);
                                else Logger.Log("Notification already loaded!");
                            }
                        } catch (Exception e) {
                            Logger.Exception(e);
                        }
                    }

                    if (jsonList.length() == page_size) {
                        current_page++;
                        scrollLoad = true;
                    } else {
                        scrollLoad = false;
                    }
                    adapter.notifyDataSetChanged();
                    StopLoading();
                    return;
                }
            }

            StopLoading();
        }

        @Override
        public void DialogSendResult(int dialog_id, int cmd) {
            if (cmd == Activity.RESULT_OK) new GetNextNotifications().execute();
        }

        @Override
        public void DialogSendParamResult(int dialog_id, int cmd, JSONObject params) {
        }
    }

    /***
     * Установить "прочитано" для уведомления
     */
    class SetNotificationRead extends AsyncTask<String, Void, JSONObject> {
        private String notificationId;

        public SetNotificationRead(String notificationId) {
            super();
            this.notificationId = notificationId;
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            return server.setNotificationRead(notificationId);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == ErrorResult.SUCCESS) {
                    Logger.Log("Notification has been read!");
                    NotificationListSingleton.setNeedRefresh(true);
                    current_page = FIRST_PAGE;
                    new GetNextNotifications().execute();
                } else {
                    Logger.Warn("Can't set notification read: " + result.getString("errorText"));
                }
            } catch (Exception e) {
                Logger.Exception(e);
                Logger.Warn("Read notification error: Can't parse server reply!");
            }
        }
    }

    public void StopLoading() {
        if (notificationList.size() <= 0) {
            swipeToLoadLayout.setLoadMoreEnabled(false);
            emptyContentHint.setVisibility(View.VISIBLE);
        } else {
            swipeToLoadLayout.setLoadMoreEnabled(true);
            emptyContentHint.setVisibility(View.INVISIBLE);
        }

        scrollListener.OnEndLoading();
        progress.setVisibility(View.INVISIBLE);
        swipeToLoadLayout.setRefreshEnabled(true);
        swipeToLoadLayout.setRefreshing(false);
        swipeToLoadLayout.setLoadingMore(false);
        setOrCheckLoading(false);
    }
}
