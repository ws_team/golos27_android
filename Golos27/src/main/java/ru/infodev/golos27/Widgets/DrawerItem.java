package ru.infodev.golos27.Widgets;

@SuppressWarnings("unused")
public class DrawerItem {
    public static final int SHOW_ALWAYS = 0;
    public static final int SHOW_AUTHORIZED = 1;
    public static final int SHOW_NON_AUTHORIZED = 2;

    private String ItemName;
    private int Id;
    private boolean Visibility = true;
    private int ShowCondition = SHOW_ALWAYS;
    private int newItemsCount = 0;

    public DrawerItem(String itemName, int id, boolean visibility, int showCondition) {
        super();
        this.ItemName = itemName;
        this.Id = id;
        this.Visibility = visibility;
        this.ShowCondition = showCondition;
    }

    public DrawerItem(String itemName, int id, boolean visibility) {
        super();
        this.ItemName = itemName;
        this.Id = id;
        this.Visibility = visibility;
        this.ShowCondition = SHOW_ALWAYS;
    }

    public DrawerItem(String itemName, int id) {
        super();
        ItemName = itemName;
        Id = id;
        Visibility = true;
        ShowCondition = SHOW_ALWAYS;
    }

    public int getItemId() {
        return Id;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }

    public void setVisibility(boolean visibility) {
        Visibility = visibility;
    }

    public boolean getVisibility() {
        return Visibility;
    }

    public void setShowCondition(int condition) {
        this.ShowCondition = condition;
    }

    public int getShowCondition() {
        return ShowCondition;
    }

    public boolean isVisible(boolean hasAuth) {
        if (Visibility) {
            switch (ShowCondition) {
                case SHOW_ALWAYS: return true;
                case SHOW_AUTHORIZED:  if (hasAuth) return true; break;
                case SHOW_NON_AUTHORIZED: if (!hasAuth) return true; break;
            }
        }

        return false;
    }

    public int NewItemsCount() {
        return newItemsCount;
    }

    public void setNewItemsCount(int newItemsCount) {
        this.newItemsCount = newItemsCount;
    }
}
