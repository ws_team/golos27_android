package ru.infodev.golos27.Common.Catalogs.Appeal;

import java.util.ArrayList;
import java.util.List;

/***
 * Singleton класс для хранения списка загруженных обращений
 * для отображения в ленте обращений
 */
public class AppealListSingleton {
    private static List<Appeal> mAppealList = new ArrayList<>();
    private static List<Appeal> mMyAppealList = new ArrayList<>();
    private static Appeal mAppeal = null;
    private static Integer mAppealsCurrentPage = -1;

    public static List<Appeal> getAppealList() {
        return mAppealList;
    }

    public static void setAppealList(List<Appeal> mAppealList) {
        AppealListSingleton.mAppealList = mAppealList;
    }

    public static List<Appeal> getMyAppealList() {
        return mMyAppealList;
    }

    public static void setMyAppealList(List<Appeal> mAppealList) {
        AppealListSingleton.mMyAppealList = mAppealList;
    }

    public static Integer getAppealsCurrentPage() {
        return mAppealsCurrentPage;
    }

    public static void setAppealsCurrentPage(Integer mAppealsCurrentPage) {
        AppealListSingleton.mAppealsCurrentPage = mAppealsCurrentPage;
    }

    public static Appeal getChangedAppeal() {
        return mAppeal;
    }

    public static Appeal setChangedAppeal(Appeal appeal) {
        mAppeal = appeal;
        return mAppeal;
    }
}
