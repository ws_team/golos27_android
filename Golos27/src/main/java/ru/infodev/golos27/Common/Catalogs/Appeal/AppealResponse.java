package ru.infodev.golos27.Common.Catalogs.Appeal;

import java.util.ArrayList;
import java.util.List;

import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;

@SuppressWarnings("unused")
public class AppealResponse {
    long date = -1;
    String text = "";
    String author = "";
    String post = "";
    String executorId = "";
    String executorName = "";
    ArrayList<AppealAttachment> attachments = new ArrayList<>();
    // ---------------------------------------------------------------------------------

    public long Date() {
        return date;
    }

    public AppealResponse setDate(long date) {
        this.date = date;
        return this;
    }

    public String Text() {
        return text;
    }

    public AppealResponse setText(String text) {
        this.text = text;
        return this;
    }

    public String Author() {
        return author;
    }

    public AppealResponse setAuthor(String author) {
        this.author = author;
        return this;
    }

    public String Post() {
        return post;
    }

    public AppealResponse setPost(String post) {
        this.post = post;
        return this;
    }

    public String ExecutorId() {
        return executorId;
    }

    public AppealResponse setExecutorId(String executorId) {
        this.executorId = executorId;
        return this;
    }

    public String ExecutorName() {
        return executorName;
    }

    public AppealResponse setExecutorName(String executorName) {
        this.executorName = executorName;
        return this;
    }

    public ArrayList<AppealAttachment> Attachments() {
        return attachments;
    }

    public AppealResponse setAttachments(ArrayList<AppealAttachment> attachments) {
        this.attachments = attachments;
        return this;
    }

    public AppealResponse addAttachment(AppealAttachment attachment) {
        this.attachments.add(attachment);
        return this;
    }

    public AppealResponse delAttachment(AppealAttachment attachment) {
        this.attachments.remove(attachment);
        return this;
    }

    public static AppealResponse fromJson(JSONObject source) {
        if (source == null) return null;
        AppealResponse newResponse = new AppealResponse();
        try {
            newResponse.setDate(source.getLong("date"))
                    .setText(source.getString("text", ""))
                    .setAuthor(source.getString("author", ""))
                    .setPost(source.getString("post", ""))
                    .setExecutorId(source.getString("executorId", ""))
                    .setExecutorName(source.getString("executorName", ""))
                    .setAttachments(AppealAttachment.fromJson(source.getJSONObject("documents", null)));

        } catch (Exception e) {
            Logger.Exception(e);
            return null;
        }

        return newResponse;
    }

    public JSONObject toJson() {
        JSONObject result = new JSONObject();
        JSONArray attachments = new JSONArray();
        JSONObject docRoot = new JSONObject();

        result.putSafe("date", date);
        result.putSafe("text", text);
        result.putSafe("author", author);
        result.putSafe("post", post);
        result.putSafe("executorId", executorId);
        result.putSafe("executorName", executorName);

        for (AppealAttachment attachment : this.attachments)
            attachments.put(attachment.toJson());
        docRoot.putSafe("count", attachments.length());
        docRoot.putSafe("items", attachments);
        result.putSafe("documents", docRoot);

        return result;
    }

    /***
     * Возвращает кол-во документов в данном обращении
     */
    public int getDocumentsCount() {
        int result = 0;
        if (attachments == null) return 0;
        for (AppealAttachment doc : attachments)
            if (!doc.MimeType().contains("image")) result++;

        return result;
    }

    /***
     * Возвращает изображений в данном ответе на обращение
     */
    public int getImagesCount() {
        int result = 0;
        if (attachments == null) return 0;
        for (AppealAttachment doc : attachments)
            if (doc.MimeType().contains("image")) result++;

        return result;
    }

    /***
     * Возвращает список изображений в данном ответе на обращение
     */
    public List<AppealAttachment> getImagesList() {
        List<AppealAttachment> result = new ArrayList<>();
        if (attachments == null) return result;
        for (AppealAttachment doc : attachments)
            if (doc.MimeType().contains("image")) result.add(doc);
        return result;
    }

    public List<AppealAttachment> getDocsList() {
        List<AppealAttachment> result = new ArrayList<>();
        if (attachments == null) return result;
        for (AppealAttachment doc : attachments)
            if (!doc.MimeType().contains("image")) result.add(doc);
        return result;
    }
}
