package ru.infodev.golos27.Widgets.Adapters;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextWatcherAdapter implements TextWatcher {

    public interface TextWatcherListener {
        void onTextChanged(EditText view, String text);
        void beforeTextChanged(CharSequence s, int start, int count, int after);
        void afterTextChanged(Editable s);
    }

    private final EditText view;
    private final TextWatcherListener listener;

    public TextWatcherAdapter(EditText editText, TextWatcherListener listener) {
        this.view = editText;
        this.listener = listener;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        listener.onTextChanged(view, s.toString());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        listener.beforeTextChanged(s, start, count, after);
    }

    @Override
    public void afterTextChanged(Editable s) {
        listener.afterTextChanged(s);
    }
}
