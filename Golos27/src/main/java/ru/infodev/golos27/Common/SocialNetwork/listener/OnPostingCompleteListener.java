package ru.infodev.golos27.Common.SocialNetwork.listener;

import ru.infodev.golos27.Common.SocialNetwork.listener.base.SocialNetworkListener;

/**
 * Interface definition for a callback to be invoked when posting complete.
 */
public interface OnPostingCompleteListener extends SocialNetworkListener {
    /**
     * Called when posting complete.
     * @param socialNetworkID id of social network where request was complete
     */
    void onPostSuccessfully(int socialNetworkID);
}

