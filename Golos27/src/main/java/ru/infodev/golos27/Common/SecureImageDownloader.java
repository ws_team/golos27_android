package ru.infodev.golos27.Common;

import android.content.Context;

import com.nostra13.universalimageloader.core.assist.FlushedInputStream;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import ru.infodev.golos27.Common.Utils.Logger;

public class SecureImageDownloader extends BaseImageDownloader {
    public static final String TAG = SecureImageDownloader.class.getName();


    public SecureImageDownloader(Context context, int connectTimeout, int readTimeout) {
        super(context, connectTimeout, readTimeout);
    }

    @Override
    protected InputStream getStreamFromNetwork(String imageUri, Object extra) throws IOException {

        URL url = null;
        try {
            url = new URL(imageUri);
        } catch (MalformedURLException e) {
            Logger.Exception(e);
        }
        HttpURLConnection http = null;
        if (url == null) return null;

        if (Scheme.ofUri(imageUri) == Scheme.HTTPS) {
            HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
            //https.setSSLSocketFactory(CustomTrustCA.getInstance());
            http = https;
            http.connect();
        } else {
            http = (HttpURLConnection) url.openConnection();
        }

        http.setConnectTimeout(connectTimeout);
        http.setReadTimeout(readTimeout);
        return new FlushedInputStream(new BufferedInputStream(http.getInputStream()));
    }
}
