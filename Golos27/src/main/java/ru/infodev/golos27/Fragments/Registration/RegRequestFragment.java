package ru.infodev.golos27.Fragments.Registration;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.regex.Pattern;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Common.Utils.PhoneUtils;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.LoginFragment;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.CustomEditText;

public class RegRequestFragment extends ParamFragment
        implements OnClickListener, TextWatcherAdapter.TextWatcherListener {

    public static final int FRAGMENT_ID = R.layout.registration_request;
    public static final int COMMAND_PRIVACY = 3;

    private Button nextButton;
    private CustomEditText lastNameEdit;
    private CustomEditText firstNameEdit;
    private CustomEditText phoneEdit;
    private CustomEditText emailEdit;
    private ProgressBar progressBar;
    private TextView warnTextEmailView;
    private TextView warnTextPhoneView;
    private TextView warn_text_first_name;
    private TextView warn_text_first_entry;
    private TextView help_text_first_entry;
    private TextView help_text_first_name;
    private String stringMandatory;

    private boolean lastNameGood = false;
    private boolean lastNameGood1 = false;
    private boolean lastNameGood2 = false;
    private boolean lastNameGood3 = false;
    private boolean phoneGood = false;
    private boolean firstNameGood = false;
    private boolean emailGood = false;

    // Регулярные выражения для тестирования полей ввода
    Pattern patternName = Pattern.compile("[а-яА-ЯёЁ\\s\\-?]*"); // Русские буквы и дефис
    Pattern patternName1 = Pattern.compile("^.*\\-+$"); // В конце 2 и более дефиса
    Pattern patternName2 = Pattern.compile("^\\-+.*"); // Дефис в начале
    Pattern patternName3 = Pattern.compile(".*\\-\\-+.*"); // Дефис в середине
    Pattern patternPhone = Pattern.compile("\\+7\\(\\d{3,}\\)\\d{3,}\\-\\d{2,}\\-\\d{2,}"); // +7(xxx)xxx-xx-xx
    Pattern patternEmail = Pattern.compile("[^@]+@[^@]+\\.[^@]+"); // Любые символы (кроме @) справа и слева от @

    public RegRequestFragment() {
        titleId = R.string.login_register;
        setFragmentAllowMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        setTitle(titleId);

        nextButton = (Button) v.findViewById(R.id.send_security_code);
        nextButton.setOnClickListener(this);

        // Навешивание обработчиков на поля ввода и кнопки
        Button haveAccountButton = (Button) v.findViewById(R.id.i_have_an_account);
        haveAccountButton.setOnClickListener(this);

        lastNameEdit = (CustomEditText) v.findViewById(R.id.last_name_edit);
        lastNameEdit.setTypeface(roboto_light);
        lastNameEdit.setOnTextChangedListener(this);

        firstNameEdit = (CustomEditText) v.findViewById(R.id.edit_first_name);
        firstNameEdit.setTypeface(roboto_light);
        firstNameEdit.setOnTextChangedListener(this);

        phoneEdit = (CustomEditText) v.findViewById(R.id.edit_phone_number);
        phoneEdit.setTypeface(roboto_light);
        phoneEdit.setOnTextChangedListener(this);

        emailEdit = (CustomEditText) v.findViewById(R.id.edit_email);
        emailEdit.setTypeface(roboto_light);
        emailEdit.setOnTextChangedListener(this);

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        warn_text_first_name = (TextView) v.findViewById(R.id.warn_text_first_name);
        warn_text_first_name.setTypeface(roboto_light);

        warn_text_first_entry = (TextView) v.findViewById(R.id.warn_text_first_entry);
        warn_text_first_entry.setTypeface(roboto_light);

        warnTextEmailView = (TextView) v.findViewById(R.id.warn_text_email);
        warnTextEmailView.setTypeface(roboto_light);

        warnTextPhoneView = (TextView) v.findViewById(R.id.warn_text_phone);
        warnTextPhoneView.setTypeface(roboto_light);

        help_text_first_entry = (TextView) v.findViewById(R.id.help_text_first_entry);
        help_text_first_entry.setTypeface(roboto_light);

        help_text_first_name = (TextView) v.findViewById(R.id.help_text_first_name);
        help_text_first_name.setTypeface(roboto_light);

        int colorPrimary = ContextCompat.getColor(getActivity(), R.color.colorPrimary);
        stringMandatory = getResources().getString(R.string.mandatory);

        // Блок "лицензионного соглашения"
        TextView privacyPolicy = (TextView) v.findViewById(R.id.privacy_policy);
        String privacyPart1 = getResources().getString(R.string.privacy_policy);
        String privacyPart2 = getResources().getString(R.string.terms_of_use);
        String privacy = privacyPart1 + privacyPart2;
        Spannable privacyPart2Span = new SpannableString(privacy);
        privacyPart2Span.setSpan(new ForegroundColorSpan(colorPrimary), privacyPart1.length(),
                privacy.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        privacyPolicy.setText(privacyPart2Span);
        privacyPolicy.setOnClickListener(this);

        return v;
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        CustomEditText editBox = (CustomEditText) view;
        switch (view.getId()) {
            case R.id.edit_email:
                emailGood = editBox.checkValue(patternEmail, true, false);
                if (emailGood) {
                    warnTextPhoneView.setText("");
                } else {
                    warnTextPhoneView.setText(stringMandatory);
                }
                break;

            case R.id.edit_phone_number:
                phoneGood = editBox.checkValue(patternPhone, true, false);
                if (phoneGood) {
                    warnTextEmailView.setText("");
                } else {
                    warnTextEmailView.setText(stringMandatory);
                }
                break;

            case R.id.last_name_edit:
                lastNameGood = editBox.checkValue(patternName, true, false);
                lastNameGood1 = !patternName1.matcher(text).matches();
                lastNameGood2 = !patternName2.matcher(text).matches();
                ((CustomEditText) view).setMistake(!lastNameGood2);
                lastNameGood3 = !patternName3.matcher(text).matches();
                ((CustomEditText) view).setMistake(!lastNameGood3);
                if (text.length() < 1) {
                    help_text_first_entry.setText(R.string.down_text_all_entry);
                } else {
                    if (!lastNameGood) {
                        ((CustomEditText) view).setMistake(true);
                        help_text_first_entry.setText(R.string.pattern_first_and_last_name_1);
                    } else {
                        if (!lastNameGood2) {
                            ((CustomEditText) view).setMistake(true);
                            help_text_first_entry.setText(R.string.pattern_first_and_last_name_2);
                        } else {
                            if (!lastNameGood1) {
                                ((CustomEditText) view).setMistake(true);
                                help_text_first_entry.setText(R.string.pattern_first_and_last_name_3);
                            } else {
                                if (!lastNameGood3) {
                                    ((CustomEditText) view).setMistake(true);
                                    help_text_first_entry.setText(R.string.pattern_first_and_last_name_4);
                                } else {
                                    help_text_first_entry.setText(R.string.down_text_all_entry);
                                    ((CustomEditText) view).setMistake(false);
                                }
                            }
                        }
                    }
                }
                break;

            case R.id.edit_first_name:
                firstNameGood = editBox.checkValue(patternName, true, false);
                lastNameGood1 = !patternName1.matcher(text).matches();
                lastNameGood2 = !patternName2.matcher(text).matches();
                ((CustomEditText) view).setMistake(!lastNameGood2);
                lastNameGood3 = !patternName3.matcher(text).matches();
                ((CustomEditText) view).setMistake(!lastNameGood3);
                if (text.length() < 1) {
                    help_text_first_name.setText(R.string.down_text_all_entry);
                } else {
                    if (!firstNameGood) {
                        ((CustomEditText) view).setMistake(true);
                        help_text_first_name.setText(R.string.pattern_first_and_last_name_1);
                    } else {
                        if (!lastNameGood2) {
                            ((CustomEditText) view).setMistake(true);
                            help_text_first_name.setText(R.string.pattern_first_and_last_name_2);
                        } else {
                            if (!lastNameGood1) {
                                ((CustomEditText) view).setMistake(true);
                                help_text_first_name.setText(R.string.pattern_first_and_last_name_3);
                            } else {
                                if (!lastNameGood3) {
                                    ((CustomEditText) view).setMistake(true);
                                    help_text_first_name.setText(R.string.pattern_first_and_last_name_4);
                                } else {
                                    help_text_first_name.setText(R.string.down_text_all_entry);
                                    ((CustomEditText) view).setMistake(false);
                                }
                            }
                        }

                    }
                }

                break;
        }

        if (lastNameGood && firstNameGood && (phoneGood || emailGood)) {
            nextButton.setEnabled(true);
        } else {
            nextButton.setEnabled(false);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // pass
    }

    @Override
    public void afterTextChanged(Editable s) {
        // pass
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.i_have_an_account:
                // При нажатии на эту кнопку просто просим Activity переключиться на фрагмент логина
                // Пользователь передумал. Будет логиниться
            case RegRequestFragment.FRAGMENT_ID:
                chainPopFragments(FRAGMENT_ID, 1, true);
                chainStartNextFragment(FRAGMENT_ID, LoginFragment.class, null, 0, false);
                break;

            case R.id.send_security_code:
                String lastName = lastNameEdit.getText().toString();
                String firstName = firstNameEdit.getText().toString();
                String phone = PhoneUtils.removeBracketsAndDashes(phoneEdit.getText().toString());
                String email = emailEdit.getText().toString();
                new RequestRegistrationTask().execute(firstName, lastName, email, phone);
                break;

            case R.id.privacy_policy:
                // Просим Activity переключиться на пользовательское соглашение
                fragmentResultListener.FragmentSendResult(FRAGMENT_ID, COMMAND_PRIVACY, null);
                chainStartNextFragment(FRAGMENT_ID, TermsOfUse.class, null, false);
                break;
        }
    }

    class RequestRegistrationTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            nextButton.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            // firstName, lastName, email, phone
            ServerTalk server = new ServerTalk();
            JSONObject response = server.
                    registerRequest(params[0], params[1], params[2], params[3]);
            try {
                response.put("input_first", params[0]);
                response.put("input_last", params[1]);
                response.put("input_email", params[2]);
                response.put("input_phone", params[3]);
            } catch (Exception e) {
                Logger.Exception(e);
            }
            return response;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            progressBar.setVisibility(View.INVISIBLE);
            nextButton.setVisibility(View.VISIBLE);
            nextButton.setEnabled(false);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    nextButton.setEnabled(true);
                    chainStartNextFragment(FRAGMENT_ID, RegConfirmFragment.class, result, 0, false);
                } else {
                    ResultDialog.showError(getActivity(), "Ошибка регистрации", errorCode, result.getErrorText());
                }
            } catch (Exception e) {
                Logger.Exception(e);
                ResultDialog.showError(getActivity(), "Ошибка регистрации", ErrorResult.INTERNAL_ERROR);
            }

            nextButton.setEnabled(true);
        }
    }
}
