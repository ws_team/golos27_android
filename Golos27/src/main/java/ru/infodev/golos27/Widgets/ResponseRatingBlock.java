package ru.infodev.golos27.Widgets;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.internal.LockOnGetVariable;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Utils.Json.JSONException;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Fragments.Dialogs.YesNoDialog;
import ru.infodev.golos27.MainActivity;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;

/**
 * Created by Maxim Ostrovidov on 05.10.17.
 * (c) White Soft
 */
public class ResponseRatingBlock extends RelativeLayout {

    private TextView text;
    private ImageView star1;
    private ImageView star2;
    private ImageView star3;
    private ImageView star4;
    private ImageView star5;
    private Button button;
    private ProgressBar progressBar;

    private String appealId;
    private int currentRating = 0;

    public ResponseRatingBlock(Context context) {
        super(context);
        init();
    }

    public ResponseRatingBlock(Context context, AttributeSet attrs) {
        super(context,attrs);
        init();
    }

    public ResponseRatingBlock(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setState(String appealId, Integer rating, boolean canSetRating) {
        this.appealId = appealId;
        if(rating == null) {
            if(!canSetRating) setVisibility(GONE); //hide whole block
            else setVisibility(VISIBLE);
        } else {
            setVisibility(VISIBLE);
            currentRating = rating;
            setStarsClickable(false);
            button.setVisibility(GONE);
            progressBar.setVisibility(GONE);
            text.setText(R.string.response_rating_text_rated);
            setStars(rating);
        }
    }

    private void init() {
        inflate(getContext(), R.layout.response_rating_block, this);
        text = (TextView) findViewById(R.id.response_rating_text);
        progressBar = (ProgressBar) findViewById(R.id.response_rating_submit_loading);
        button = (Button) findViewById(R.id.response_rating_submit_button);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showPrompt();
            }
        });

        star1 = (ImageView) findViewById(R.id.response_rating_star_1);
        star1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                handleStarClick(1);
            }
        });

        star2 = (ImageView) findViewById(R.id.response_rating_star_2);
        star2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                handleStarClick(2);
            }
        });

        star3 = (ImageView) findViewById(R.id.response_rating_star_3);
        star3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                handleStarClick(3);
            }
        });

        star4 = (ImageView) findViewById(R.id.response_rating_star_4);
        star4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                handleStarClick(4);
            }
        });

        star5 = (ImageView) findViewById(R.id.response_rating_star_5);
        star5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                handleStarClick( 5);
            }
        });
    }

    private void showPrompt() {
        new YesNoDialog(Application.AppContext, "Ваша оценка: " + currentRating,
                null, "Подтвердить", "Отменить", new DialogResultInterface() {
                    @Override
                    public void DialogSendResult(int dialog_id, int cmd) {
                        if (cmd == Activity.RESULT_OK) sendRating();
                    }

                    @Override
                    public void DialogSendParamResult(int dialog_id, int cmd, JSONObject params) {

                    }
                }, true).show((Activity) getContext());
    }

    private void handleStarClick(int newRating) {
        Log.d("ads", "handleStarClick " + newRating);
        clearStars();
        if(newRating == currentRating) {
            currentRating = 0;
            button.setEnabled(false);
        } else {
            currentRating = newRating;
            button.setEnabled(true);
            setStars(currentRating);
        }
    }

    private void clearStars() {
        Log.d("ads", "clearStars ");
        enableStar(star1, false);
        enableStar(star2, false);
        enableStar(star3, false);
        enableStar(star4, false);
        enableStar(star5, false);
    }

    private void setStars(int rating) {
        Log.d("ads", "setStars " + rating);
        if(rating >= 1) enableStar(star1, true);
        if(rating >= 2) enableStar(star2, true);
        if(rating >= 3) enableStar(star3, true);
        if(rating >= 4) enableStar(star4, true);
        if(rating == 5) enableStar(star5, true);
    }

    private void setStarsClickable(boolean clickable) {
        star1.setClickable(clickable);
        star2.setClickable(clickable);
        star3.setClickable(clickable);
        star4.setClickable(clickable);
        star5.setClickable(clickable);
    }

    private void enableStar(ImageView imageView, boolean enable) {
        Log.d("ads", "enableStar " + enable);
        if(enable) {
            imageView.setImageResource(R.drawable.megastar_active);
        } else {
            imageView.setImageResource(R.drawable.megastar_inactive);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void sendRating() {
        new AsyncTask<Integer, Void, JSONObject>() {
            @Override
            protected void onPreExecute() {
                button.setVisibility(GONE);
                progressBar.setVisibility(VISIBLE);
                setStarsClickable(false);
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                try {
                    int errorCode = (Integer) jsonObject.get("errorCode");
                    if (errorCode == ErrorResult.SUCCESS) {
                        progressBar.setVisibility(GONE);
                        text.setText(R.string.response_rating_text_rated);
                        Toast.makeText(getContext(), R.string.response_rating_rate_success, Toast.LENGTH_LONG).show();
                    } else {
                        handleRateError();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    handleRateError();
                }
            }

            @Override
            protected JSONObject doInBackground(Integer... integers) {
                ServerTalk server = new ServerTalk();
                return server.rateAppealResponse(appealId, integers[0]);
            }
        }.execute(currentRating);
    }

    private void handleRateError() {
        setStarsClickable(true);
        button.setVisibility(VISIBLE);
        progressBar.setVisibility(GONE);
        Toast.makeText(getContext(), R.string.response_rating_rate_error, Toast.LENGTH_LONG).show();
    }
}