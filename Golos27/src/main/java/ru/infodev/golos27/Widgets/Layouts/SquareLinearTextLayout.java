package ru.infodev.golos27.Widgets.Layouts;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class SquareLinearTextLayout extends LinearLayout {
    public SquareLinearTextLayout(Context context) {
        super(context);
    }

    public SquareLinearTextLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareLinearTextLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @SuppressWarnings("SuspiciousNameCombination")
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();
        int height = getMeasuredHeight();

        if (width < height) width = height;
        setMeasuredDimension(width, height);
    }
}
