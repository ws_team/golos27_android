package ru.infodev.golos27.Common.Catalogs;

import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.RestApi.ServerTalk;

/***
 * Методы обновления справочников сервера
 */
public class Catalogs {
    /***
     * Обновляет справочник населенных пунктов
     * (загружает с сервера и сохраняет в базе данных)
     * @return код ошибки или SUCCESS=0
     */
    public static ErrorResult UpdateOKTMOCatalog() {
        try {
            ServerTalk serverTalk = new ServerTalk();
            JSONObject result = serverTalk.getOKTMOList(Integer.MAX_VALUE);
            if (result == null) {
                Logger.Log("This is imposible but... serverTalk returned NULL!!!!");
                return new ErrorResult(ErrorResult.APPLICATION_ERROR, ErrorResult.getErrorText(ErrorResult.APPLICATION_ERROR));
            }

            if (result.has("errorCode") && !result.isNull("errorCode")) {
                ErrorResult retVal = new ErrorResult(result.getInt("errorCode"), result.getString("errorText"));
                if (retVal.getErrorCode() != ErrorResult.SUCCESS) return retVal;
            }

            Long start = System.currentTimeMillis();
            if (!OKTMO.updateListFromServer(result))
                return new ErrorResult(ErrorResult.DATABASE_ERROR);

            Long stop = System.currentTimeMillis();
            Long delay = stop - start;
            Logger.Log("OKTMOS update time elapsed: " + delay.toString() + "ms");

        } catch (Exception e) {
            Logger.Exception(e);
            return new ErrorResult(ErrorResult.APPLICATION_ERROR);
        }

        return new ErrorResult(ErrorResult.SUCCESS);
    }

    /***
     * Обновляет справочник тематик
     * (загружает с сервера и сохраняет в базе данных)
     * @return код ошибки или SUCCESS=0
     */
    public static ErrorResult UpdateSubjectsCatalog() {
        try {
            ServerTalk serverTalk = new ServerTalk();
            long version = Settings.getLong(Settings.SUBJECTS_VERSION);
            JSONObject result = serverTalk.getSubjectsList(version, Integer.MAX_VALUE);
            if (result == null) {
                Logger.Log("This is imposible but... serverTalk returned NULL!!!!");
                return new ErrorResult(ErrorResult.APPLICATION_ERROR, ErrorResult.getErrorText(ErrorResult.APPLICATION_ERROR));
            }

            if (result.has("errorCode") && !result.isNull("errorCode")) {
                ErrorResult retVal = new ErrorResult(result.getInt("errorCode"), result.getString("errorText"));
                if (retVal.getErrorCode() != ErrorResult.SUCCESS) return retVal;
            }

            Long start = System.currentTimeMillis();
            if (!Subject.updateListFromServer(result))
                return new ErrorResult(ErrorResult.DATABASE_ERROR);

            Long stop = System.currentTimeMillis();
            Long delay = stop - start;
            Logger.Log("Subjects update time elapsed: " + delay.toString() + "ms");

        } catch (Exception e) {
            Logger.Exception(e);
            return new ErrorResult(ErrorResult.APPLICATION_ERROR);
        }

        return new ErrorResult(ErrorResult.SUCCESS);
    }

    /***
     * Обновляет справочник исполнителей
     * (загружает с сервера и сохраняет в базе данных)
     * @return код ошибки или SUCCESS=0
     */
    public static ErrorResult UpdateExecutorsCatalog() {
        try {
            ServerTalk serverTalk = new ServerTalk();
            long version = Settings.getLong(Settings.EXECUTORS_VERSION);
            JSONObject result = serverTalk.getExecutorsList(version, Integer.MAX_VALUE);
            if (result == null) {
                Logger.Log("This is imposible but... serverTalk returned NULL!!!!");
                return new ErrorResult(ErrorResult.APPLICATION_ERROR, ErrorResult.getErrorText(ErrorResult.APPLICATION_ERROR));
            }

            if (result.has("errorCode") && !result.isNull("errorCode")) {
                ErrorResult retVal = new ErrorResult(result.getInt("errorCode"), result.getString("errorText"));
                if (retVal.getErrorCode() != ErrorResult.SUCCESS) return retVal;
            }

            Long start = System.currentTimeMillis();
            if (!Executor.updateListFromServer(result))
                return new ErrorResult(ErrorResult.DATABASE_ERROR);

            Long stop = System.currentTimeMillis();
            Long delay = stop - start;
            Logger.Log("Executors update time elapsed: " + delay.toString() + "ms");

        } catch (Exception e) {
            Logger.Exception(e);
            return new ErrorResult(ErrorResult.APPLICATION_ERROR);
        }

        return new ErrorResult(ErrorResult.SUCCESS);
    }

}
