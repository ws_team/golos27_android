package ru.infodev.golos27.Common.Catalogs.Appeal;

import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;

@SuppressWarnings("unused")
public class AppealGrade {
    public static final String NOTSATISFIED = "NOTSATISFIED";
    public static final String SATISFIED = "SATISFIED";
    public static final String GRATITUDE = "GRATITUDE";

    private long date = -1;
    private String value = "";
    private String description = "";

    public long Date() {
        return date;
    }

    public AppealGrade setDate(long date) {
        this.date = date;
        return this;
    }

    public String Description() {
        return description;
    }

    public AppealGrade setDescription(String description) {
        this.description = description;
        return this;
    }

    public String Value() {
        return value;
    }

    public AppealGrade setValue(String value) {
        this.value = value;
        return this;
    }

    public static AppealGrade fromJson(JSONObject source) {
        if (source == null) return null;
        AppealGrade newGrade = new AppealGrade();
        try {
            newGrade.setDate(source.getLong("date"))
                    .setValue(source.getString("value", NOTSATISFIED))
                    .setDescription(source.getString("description", ""));
        } catch (Exception e) {
            Logger.Exception(e);
            return null;
        }

        return newGrade;
    }

    public JSONObject toJson() {
        JSONObject result = new JSONObject();
        result.putSafe("date", date);
        result.putSafe("value", value);
        result.putSafe("description", description);
        return result;
    }
}
