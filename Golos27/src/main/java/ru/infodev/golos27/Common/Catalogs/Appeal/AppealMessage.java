package ru.infodev.golos27.Common.Catalogs.Appeal;

import java.util.ArrayList;

import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;

@SuppressWarnings("unused")
public class AppealMessage {
    long answerDate = -1;
    String text = "";
    String executorId = null;
    String executorName = null;
    String destinationId = null;
    String destinationName = null;
    String subjectId = null;
    String subjectName = null;
    ArrayList<AppealAttachment> attachments = new ArrayList<>();
    //--------------------------------------------------------------

    /***
     * Конструктор объекта из JSONObject
     */
    public static AppealMessage fromJson(JSONObject source) {
        AppealMessage newMessage = new AppealMessage();
        if (source == null) return null;
        try {
            newMessage.setAnswerDate(source.getLong("answerDate", -1l))
                    .setText(source.getString("text", ""))
                    .setExecutorId(source.getString("executor", null))
                    .setExecutorName(source.getString("executorName", null))
                    .setDestinationId(source.getString("destination", null))
                    .setDestinationName(source.getString("destinationName", null))
                    .setSubjectId(source.getString("subject", null))
                    .setSubjectName(source.getString("subjectName", null))
                    .setAttachments(AppealAttachment.fromJson(source.getJSONObject("attachments", null)));

        } catch (Exception e) {
            Logger.Exception(e);
            return null;
        }

        return newMessage;
    }

    /***
     * Преобразует объект в JSONObject
     */
    public JSONObject toJson() {
        JSONObject result = new JSONObject();
        JSONArray documents = new JSONArray();
        JSONObject docRoot = new JSONObject();

        result.putSafe("answerDate", answerDate);
        result.putSafe("text", text);
        result.putSafe("executor", executorId);
        result.putSafe("executorName", executorName);
        result.putSafe("destination", destinationId);
        result.putSafe("destinationName", destinationName);
        result.putSafe("subject", subjectId);
        result.putSafe("subjectName", subjectName);

        for (AppealAttachment document : attachments) documents.put(document.toJson());
        docRoot.putSafe("count", documents.length());
        docRoot.putSafe("items", documents);
        result.putSafe("attachments", docRoot);

        return result;
    }


    public long AnswerDate() {
        return answerDate;
    }

    public AppealMessage setAnswerDate(long answerDate) {
        this.answerDate = answerDate;
        return this;
    }

    public String Text() {
        return text;
    }

    public AppealMessage setText(String text) {
        this.text = text;
        return this;
    }

    public String ExecutorId() {
        return executorId;
    }

    public AppealMessage setExecutorId(String executorId) {
        this.executorId = executorId;
        return this;
    }

    public String ExecutorName() {
        return executorName;
    }

    public AppealMessage setExecutorName(String executorName) {
        this.executorName = executorName;
        return this;
    }

    public String DestinationId() {
        return destinationId;
    }

    public AppealMessage setDestinationId(String destinationId) {
        this.destinationId = destinationId;
        return this;
    }

    public String DestinationName() {
        return destinationName;
    }

    public AppealMessage setDestinationName(String destinationName) {
        this.destinationName = destinationName;
        return this;
    }

    public String SubjectId() {
        return subjectId;
    }

    public AppealMessage setSubjectId(String subjectId) {
        this.subjectId = subjectId;
        return this;
    }

    public String SubjectName() {
        return subjectName;
    }

    public AppealMessage setSubjectName(String subjectName) {
        this.subjectName = subjectName;
        return this;
    }

    public ArrayList<AppealAttachment> Attachments() {
        return attachments;
    }

    public AppealMessage setAttachments(ArrayList<AppealAttachment> attachments) {
        this.attachments = attachments;
        return this;
    }

    public AppealMessage addAttachment(AppealAttachment attachment) {
        if (this.attachments == null) this.attachments = new ArrayList<>();
        this.attachments.add(attachment);
        return this;
    }

    public AppealMessage delAttachment(AppealAttachment attachment) {
        if (this.attachments == null) this.attachments = new ArrayList<>();
        // TODO: remove by id
        this.attachments.remove(attachment);
        return this;
    }
}
