package ru.infodev.golos27.Fragments.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;

public class ResultDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final int DIALOG_ID = R.layout.dialog_result;
    private Context context;
    private String buttonText = null;
    private String resultText = null;
    private String fullResultText = null;
    private Drawable icon = null;
    private DialogResultInterface resultInterface;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Logger.Log("Creating ResultDialog");
        View currentView = inflater.inflate(DIALOG_ID, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        currentView.findViewById(R.id.button_ok).setOnClickListener(this);

        ImageView resultImage = (ImageView) currentView.findViewById(R.id.resultImage);
        if (icon != null) {
            resultImage.setImageDrawable(icon);
            resultImage.setVisibility(View.VISIBLE);
        } else {
            resultImage.setVisibility(View.VISIBLE);
        }

        if (buttonText != null) {
            Button button = (Button) currentView.findViewById(R.id.button_ok);
            button.setText(buttonText);
        }

        TextView resultTextView = (TextView) currentView.findViewById(R.id.resultText);
        if (resultText != null) {
            resultTextView.setText(resultText);
            resultTextView.setVisibility(View.VISIBLE);
        } else {
            resultTextView.setVisibility(View.GONE);
        }

        TextView fullTextView = (TextView) currentView.findViewById(R.id.fullResultText);
        if (fullResultText != null) {
            fullTextView.setText(fullResultText);
            fullTextView.setTypeface(roboto_light);
            fullTextView.setVisibility(View.VISIBLE);
        } else {
            fullTextView.setVisibility(View.GONE);
        }

        return currentView;
    }

    public void setMessages(String resultText, String fullResultText, String buttonText) {
        this.buttonText = buttonText;
        this.resultText = resultText;
        this.fullResultText = fullResultText;
    }

    @SuppressWarnings("unused")
    public void setMessages(int resultTextId, int fullResultTextId, int buttonTextId) {
        this.buttonText = context.getResources().getString(buttonTextId);
        this.resultText = context.getResources().getString(resultTextId);
        this.fullResultText = context.getResources().getString(fullResultTextId);
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public void setIcon(int iconId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.icon = Application.AppContext.getResources().getDrawable(iconId, null);
        } else {
            //noinspection deprecation
            this.icon = Application.AppContext.getResources().getDrawable(iconId);
        }
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public void show(FragmentManager manager, String tag) {
        if (manager.findFragmentByTag(tag) == null) {
            super.show(manager, tag);
        }
    }

    @Override
    public void onClick(View lv) {
        switch (lv.getId())
        {
            case R.id.button_ok:
                if (resultInterface != null) resultInterface.DialogSendResult(DIALOG_ID, Activity.RESULT_OK);
                dismiss();
                break;

            default:
                Logger.Log("Unknown click from " + lv.toString());
                break;
        }
    }

    public void setResultListener(DialogResultInterface listener) {
        resultInterface = listener;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if (resultInterface != null) resultInterface.DialogSendResult(DIALOG_ID, Activity.RESULT_CANCELED);
    }
}
