package ru.infodev.golos27.Widgets.SwipeToLoad;

public interface SwipeLoadMoreTrigger {
    void onLoadMore();
}
