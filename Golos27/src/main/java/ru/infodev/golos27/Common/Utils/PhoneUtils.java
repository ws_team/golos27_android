package ru.infodev.golos27.Common.Utils;

import android.text.Editable;

// Лапша-код. Смотреть не рекомендуется.
// Вредно для нервов и психического здоровья
public class PhoneUtils {
    private static final int NANP_STATE_START = 0;
    private static final int NANP_STATE_DIGIT_GROUP1 = 1;
    private static final int NANP_STATE_PLUS = 2;
    private static final int NANP_STATE_SEVEN = 3;
    private static final int NANP_STATE_DASH1 = 4;
    private static final int NANP_STATE_BRACKET_L = 5;
    private static final int NANP_STATE_BRACKET_R = 6;
    private static final int NANP_STATE_DIGIT_GROUP2 = 7;
    private static final int NANP_STATE_DIGIT_GROUP3 = 8;
    private static final int NANP_STATE_DIGIT_GROUP4 = 9;
    private static final int NANP_STATE_DASH2 = 10;


    public static void formatNumber(Editable text) {
        int state = NANP_STATE_START;
        int length = text.length();
        int numDigits = 0;

        if (length < 2) text.replace(0, length, "+7");
        if (length > "+7(xxx)xxx-xx-xx".length()) {
            // Строка слишком длинная!
            return;
        }

        CharSequence saved = text.subSequence(0, length);

        // Strip the dashes first, as we're going to add them back
        length = text.length();

        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            switch (c) {
                case '+':
                    if (i == 0) {
                        // Plus is only allowed as the first character
                        state = NANP_STATE_PLUS;
                        break;
                    } else {
                        text.replace(0, length, saved, 0, i);
                    }
                    break;
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                    if (state == NANP_STATE_PLUS) {
                        state = NANP_STATE_SEVEN;
                        break;
                    }
                    // fall through
                case '8':
                case '9':
                case '0':
                    // Цифра есть, плюса нет
                    if ((i == 0) && (state != NANP_STATE_PLUS)) {
                        text.insert(0, "+");
                        state = NANP_STATE_PLUS;
                        length++;
                        i++;
                    }

                    // Цифра есть, плюс есть, семерки нет
                    if ((i == 1) && (state != NANP_STATE_SEVEN)) {
                        text.insert(1, "7");
                        state = NANP_STATE_SEVEN;
                        length++;
                        i++;
                    }

                    // +7 есть скобки нет
                    if ((i == 2) && (state != NANP_STATE_BRACKET_L)) {
                        text.insert(2, "(");
                        state = NANP_STATE_BRACKET_L;
                        length++;
                        i++;
                    }

                    if (((state == NANP_STATE_BRACKET_L) || (state == NANP_STATE_DIGIT_GROUP1)) && (numDigits < 3)) {
                        state = NANP_STATE_DIGIT_GROUP1;
                        numDigits++;
                    }

                    if ((i == 6) && (state == NANP_STATE_DIGIT_GROUP1)) {
                        text.insert(6, ")");
                        length++;
                        state = NANP_STATE_DIGIT_GROUP2;
                        numDigits++;
                        i++;
                    }

                    if (((state == NANP_STATE_DIGIT_GROUP2) || (state == NANP_STATE_BRACKET_R)) && (numDigits >= 3) && (numDigits < 6)) {
                        state = NANP_STATE_DIGIT_GROUP2;
                        numDigits++;
                    }

                    if ((i == 10) && (state == NANP_STATE_DIGIT_GROUP2)) {
                        text.insert(10, "-");
                        state = NANP_STATE_DIGIT_GROUP3;
                        numDigits++;
                        length++;
                        i++;
                    }

                    if (((state == NANP_STATE_DIGIT_GROUP3) || (state == NANP_STATE_DASH1)) && (numDigits >= 6) && (numDigits < 8)) {
                        state = NANP_STATE_DIGIT_GROUP3;
                        numDigits++;
                    }

                    if ((i == 13) && (state == NANP_STATE_DIGIT_GROUP3)) {
                        text.insert(13, "-");
                        state = NANP_STATE_DIGIT_GROUP4;
                        numDigits++;
                        length++;
                        i++;
                    }

                    if (((state == NANP_STATE_DIGIT_GROUP4) || (state == NANP_STATE_DASH2)) && (numDigits >= 8) && (numDigits < 10)) {
                        state = NANP_STATE_DIGIT_GROUP4;
                        numDigits++;
                    }

                    break;

                case '(':
                    if (state == NANP_STATE_SEVEN)
                        state = NANP_STATE_BRACKET_L;
                    else
                        text.replace(0, length, saved, 0, i);
                    break;

                case '-':
                    if ((state == NANP_STATE_DIGIT_GROUP2) && (numDigits == 6))
                        state = NANP_STATE_DASH1;
                    else if ((state == NANP_STATE_DIGIT_GROUP3) && (numDigits == 8))
                        state = NANP_STATE_DASH2;
                    else
                        text.replace(0, length, saved, 0, i);
                    break;

                case ')':
                    if ((numDigits == 3) && (state == NANP_STATE_DIGIT_GROUP1))
                        state = NANP_STATE_BRACKET_R;
                    else
                        text.replace(0, length, saved, 0, i);
                    break;
                // Fall through
                default:
                    // Unknown character, bail on formatting
                    text.replace(0, length, saved, 0, i);
                    return;

            }
        }

        // Remove trailing dashes
        int len = text.length();
        while (len > 0) {
            if (text.charAt(len - 1) == '-') {
                text.delete(len - 1, len);
                len--;
            } else {
                break;
            }
        }

        // Remove trailing brackets
        len = text.length();
        while (len > 0) {
            if ((text.charAt(len - 1) == '(') || (text.charAt(len - 1) == ')')) {
                text.delete(len - 1, len);
                len--;
            } else {
                break;
            }
        }

    }

    /***
     * Возвращает "неудаляемый" префикс номера телефона
     * @return строка с префиксом
     */
    public static String getNumberPrefix() {
        return "+7";
    }

    /***
     * Удаляет скобочки и тире из телефонного номера
     * @param text строка с телефонным номером
     */
    public static String removeBracketsAndDashes(String text) {
        String result = text.replace("(", "");
        result = result.replace(")", "");
        return result.replace("-", "");
    }
}
