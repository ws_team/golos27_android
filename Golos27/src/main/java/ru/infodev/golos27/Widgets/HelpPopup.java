package ru.infodev.golos27.Widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import ru.infodev.golos27.R;

@SuppressWarnings("unused")
public class HelpPopup {
    public static final int MARGIN = 10;
    protected WindowManager mWindowManager;
    protected Context mContext;
    protected PopupWindow mWindow;
    private TextView mHelpTextView;
    private ImageView mUpImageView;
    private ImageView mDownImageView;
    protected View mView;
    protected Drawable mBackgroundDrawable = null;
    protected ShowListener showListener;

    public HelpPopup(Context context, String text, int viewResource) {
        mContext = context;
        mWindow = new PopupWindow(context);
        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setContentView(layoutInflater.inflate(viewResource, null));
        mHelpTextView = (TextView) mView.findViewById(R.id.text);
        mUpImageView = (ImageView) mView.findViewById(R.id.arrow_up);
        mDownImageView = (ImageView) mView.findViewById(R.id.arrow_down);
        mHelpTextView.setMovementMethod(ScrollingMovementMethod.getInstance());
        mHelpTextView.setSelected(true);
    }

    public HelpPopup(Context context) {
        this(context, "", R.layout.popup);
    }

    public HelpPopup(Context context, String text) {
        this(context);
        setText(text);
    }

    public void show(View anchor) {
        preShow();
        int[] location = new int[2];
        anchor.getLocationOnScreen(location);
        Rect anchorRect = new Rect(location[0], location[1], location[0] + anchor.getWidth(), location[1] + anchor.getHeight());
        mView.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        int rootHeight = mView.getMeasuredHeight();
        int rootWidth = mView.getMeasuredWidth();

        Point size = new Point();
        mWindowManager.getDefaultDisplay().getSize(size);
        final int screenWidth = size.x;
        final int screenHeight = size.y;

        int yPos = anchorRect.top - rootHeight;
        boolean onTop = true;
        if (anchorRect.top < screenHeight / 2) {
            yPos = anchorRect.bottom;
            onTop = false;
        }

        int whichArrow, requestedX;
        whichArrow = ((onTop) ? R.id.arrow_down : R.id.arrow_up);
        requestedX = anchorRect.centerX();
        View arrow = whichArrow == R.id.arrow_up ? mUpImageView : mDownImageView;
        View hideArrow = whichArrow == R.id.arrow_up ? mDownImageView : mUpImageView;
        final int arrowWidth = arrow.getMeasuredWidth();
        arrow.setVisibility(View.VISIBLE);
        ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) arrow.getLayoutParams();
        hideArrow.setVisibility(View.INVISIBLE);

        int xPos;
        if (anchorRect.left + rootWidth > screenWidth) {
            xPos = (screenWidth - rootWidth - MARGIN);
        } else if (anchorRect.left - (rootWidth / 2) < 0) {
            xPos = anchorRect.left;
        } else {
            xPos = (anchorRect.centerX() - (rootWidth / 2));
        }

        param.leftMargin = (requestedX - xPos) - (arrowWidth / 2);
        if (onTop) {
            mHelpTextView.setMaxHeight(anchorRect.top - anchorRect.height());
        } else {
            mHelpTextView.setMaxHeight(screenHeight - yPos);
        }

        mWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, xPos, yPos);
    }

    protected void preShow() {
        if (mView == null) throw new IllegalStateException("view undefined");
        if (showListener != null) {
            showListener.onPreShow();
            showListener.onShow();
        }
        if (mBackgroundDrawable != null) {
            mWindow.setBackgroundDrawable(mBackgroundDrawable);
        } else {
            mWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        mWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        mWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        mWindow.setTouchable(false);
        mWindow.setFocusable(false);
        mWindow.setOutsideTouchable(true);
        mWindow.setContentView(mView);
    }

    public void setBackgroundDrawable(Drawable background) {
        mBackgroundDrawable = background;
    }

    public void setContentView(View root) {
        mView = root;
        mWindow.setContentView(root);
    }

    public void setContentView(int layoutResID) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setContentView(inflater.inflate(layoutResID, null));
    }

    public void setOnDismissListener(PopupWindow.OnDismissListener listener) {
        mWindow.setOnDismissListener(listener);
    }

    public void dismiss() {
        mWindow.dismiss();
        if (showListener != null) showListener.onDismiss();
    }

    public void setText(String text) {
        mHelpTextView.setText(text);
    }

    public interface ShowListener {
        void onPreShow();

        void onDismiss();

        void onShow();
    }

    public void setShowListener(ShowListener showListener) {
        this.showListener = showListener;
    }
}
