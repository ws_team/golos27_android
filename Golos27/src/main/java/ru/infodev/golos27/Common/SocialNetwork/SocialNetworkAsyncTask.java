package ru.infodev.golos27.Common.SocialNetwork;

import android.os.AsyncTask;
import android.os.Bundle;
/**
 * Base class for social networks async tasks.
 *
 * @author Anton Krasov
 * @author Evgeny Gorbin (gorbin.e.o@gmail.com)
 */
public abstract class SocialNetworkAsyncTask extends AsyncTask<Bundle, Void, Bundle> {

    public static final String RESULT_ERROR = "SocialNetworkAsyncTask.RESULT_ERROR";

    @Override
    protected Bundle doInBackground(Bundle... params) {
        return null;
    }
}
