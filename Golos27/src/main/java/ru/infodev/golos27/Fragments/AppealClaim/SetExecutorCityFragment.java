package ru.infodev.golos27.Fragments.AppealClaim;

import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.NewAppeal;
import ru.infodev.golos27.Common.Catalogs.OKTMO;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.Adapters.OKTMOAdapter;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.Lists.NestedListView;
import ru.infodev.golos27.Widgets.MonitoredEditText;


public class SetExecutorCityFragment extends ParamFragment implements View.OnClickListener, TextWatcherAdapter.TextWatcherListener {
    public static final int FRAGMENT_ID = R.layout.set_executor_city;
    private OKTMOAdapter adapter;
    private NestedListView listView;
    private TextView notFoundTextView;

    public SetExecutorCityFragment() {
        titleId = R.string.executor_area;
        setFragmentAllowMenu(false);
        setActionButtonIcon(ICON_CHECK);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(FRAGMENT_ID, container, false);

        notFoundTextView = (TextView) v.findViewById(R.id.not_found_text);
        MonitoredEditText searchEditText = (MonitoredEditText) v.findViewById(R.id.search_edit_text);
        searchEditText.setOnTextChangedListener(this);

        listView = (NestedListView) v.findViewById(R.id.main_list);
        List<OKTMO> oktmos = OKTMO.All();
        OKTMO checkedOKTMO = NewAppeal.getOrSetExecutorFilterCity(null);
        if (checkedOKTMO != null && oktmos != null) {
            for (OKTMO current : oktmos) {
                current.setIsChecked(current.getCode().equals(checkedOKTMO.getCode()));
            }
        }

        if ((oktmos == null) || (oktmos.isEmpty())) {
            listView.setVisibility(View.GONE);
            notFoundTextView.setVisibility(View.VISIBLE);
        } else {
            notFoundTextView.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }

        adapter = new OKTMOAdapter(Application.AppContext, oktmos);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.setChecked(position, true);
                setActionButtonVisibility(true);
            }
        });

        return v;
    }

    /***
     * Выполняется при нажатии на кнопку ActionBar
     * @param item кнопка на которую нажали
     * @return обработано нажатие или нет
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_item) {
            NewAppeal.getOrSetExecutorFilterCity(adapter.getCheckedItem());
            chainPopFragments(FRAGMENT_ID, 1, false);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        ArrayList<OKTMO> oktmos;
        if (text.length() > 2) {
            oktmos = (ArrayList<OKTMO>) OKTMO.Search(text, Integer.MAX_VALUE);
        } else {
            oktmos = (ArrayList<OKTMO>) OKTMO.All();
        }

        if ((oktmos == null) || (oktmos.isEmpty())) {
            listView.setVisibility(View.GONE);
            notFoundTextView.setVisibility(View.VISIBLE);
        } else {
            listView.setVisibility(View.VISIBLE);
            notFoundTextView.setVisibility(View.GONE);
        }

        adapter.setData(oktmos);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}
