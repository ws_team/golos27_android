package ru.infodev.golos27.Fragments.AppealClaim;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Address;
import ru.infodev.golos27.Common.Catalogs.NewAppeal;
import ru.infodev.golos27.Common.Catalogs.OKTMO;
import ru.infodev.golos27.Common.Profile;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;
import ru.infodev.golos27.Widgets.Adapters.CityAutoCompleteAdapter;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.CityAutoCompleteEditText;
import ru.infodev.golos27.Widgets.CustomEditText;

public class SetAddressFragment extends ParamFragment implements TextWatcherAdapter.TextWatcherListener, View.OnClickListener {
    public static final int FRAGMENT_ID = R.layout.set_postal_address;
    private SwitchCompat saveAddressSwitch;
    private boolean cityGood = false;
    public boolean addressGood = false;
    private CityAutoCompleteEditText cityEditText;
    private CustomEditText addressEditText;
    private TextView cityHelp;
    private LinearLayout save_address_block;

    public SetAddressFragment() {
        titleId = R.string.subcategory_select;
        setFragmentAllowMenu(false);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        setActionButtonIcon(ParamFragment.ICON_CHECK);
        showSaveButtonIfCan();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_item) {
            if (NewAppeal.getInstance().ZipAddress() == null) NewAppeal.getInstance().setZipAddress(new Address());
            NewAppeal.getInstance().ZipAddress().setSettlement(cityEditText.getCity());
            NewAppeal.getInstance().ZipAddress().setCustomAddress(addressEditText.getText().toString());

            if (saveAddressSwitch.isChecked() && Settings.getBool(Settings.AUTHORIZED_FLAG)) {
                OKTMO city = cityEditText.getCity();
                String address = addressEditText.getText().toString();
                String cityCode = null;
                String cityData = null;
                if (city != null) {
                    cityCode = city.getCode();
                    cityData = city.getCityFullName();
                }

                new SaveAddressInfoTask().execute(cityCode, cityData, address);
            }

            chainPopFragments(FRAGMENT_ID, 1, false);
        }
        return super.onOptionsItemSelected(item);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        setRetainInstance(true);

        save_address_block = (LinearLayout) v.findViewById(R.id.save_address_block_1);
        saveAddressSwitch = (SwitchCompat) v.findViewById(R.id.save_address_switch_1);
        final View save_address_block = v.findViewById(R.id.save_address_block_1);
        save_address_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAddressSwitch.toggle();
            }
        });

        OKTMO city = NewAppeal.getInstance().ZipAddress().Settlement();
        String address = NewAppeal.getInstance().ZipAddress().CustomAddress();

        // Если в обращении адрес не задан - попробуем взять его из профиля пользователя
        if (city == null && address == null) {
            city = Profile.Settlement();
            address = Profile.Address();
        }

        addressEditText = (CustomEditText) v.findViewById(R.id.edit_street_address);
        addressEditText.setOnTextChangedListener(this);
        addressEditText.setText(address);


        CityAutoCompleteAdapter adapter = new CityAutoCompleteAdapter(Application.AppContext);
        cityHelp = (TextView) v.findViewById(R.id.help_text_mail_address);
        cityEditText = (CityAutoCompleteEditText) v.findViewById(R.id.edit_mail_address);
        cityEditText.setOnTextChangedListener(this);
        cityEditText.setAdapter(adapter);
        cityEditText.setThreshold(1);
        if (city != null) cityEditText.setCity(city);

        return v;
    }

    private void showSaveButtonIfCan() {
        if (actionButton != null) {
            if ((cityGood && addressGood) || (!cityGood && !addressGood)) {
                actionButton.setVisible(true);
                save_address_block.setVisibility(View.VISIBLE);
                saveAddressSwitch.setVisibility(View.VISIBLE);
            } else {
                actionButton.setVisible(false);
                save_address_block.setVisibility(View.INVISIBLE);
                saveAddressSwitch.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        switch (view.getId()) {
            case R.id.edit_mail_address:
                OKTMO city = cityEditText.getCity();
                cityGood = city != null;
                if (city != null) {
                    cityHelp.setText(city.getFullRegion());
                } else {
                    cityHelp.setText("");
                }
                showSaveButtonIfCan();
                break;

            case R.id.edit_street_address:
                addressGood = text.length() > 0;
                showSaveButtonIfCan();
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public void onClick(View v) {

    }

    class SaveAddressInfoTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            return server.setProfile(Profile.FirstName(), Profile.LastName(), Profile.MiddleName(), Profile.Gender(),
                    Profile.Birth(), params[0], params[1], params[2], Profile.Latitude(), Profile.Longitude());
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    Logger.Log("Save address success!");
                } else {
                    Logger.Warn("Save address error: " + result.getString("errorText"));
                }
            } catch (Exception e) {
                Logger.Exception(e);
                Logger.Warn("Save address error: Can't parse server reply!");
            }
        }
    }
}
