package ru.infodev.golos27.Common.Utils;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.util.Date;

public final class Logger {
    public static boolean Enabled = true;
    private static boolean EnableDebug = true;
    private static boolean EnableLog = false;
    private static int MaxLogSize = 10 * 1024;
    private static int BufferSize = 0;
    private static String LogFileName = "Infodev.log";
    private static final StringBuffer LogBuffer = new StringBuffer();

    public static void setEnableLog(boolean enableLog) {
        EnableLog = enableLog;
    }

    // "Enter" debug record
    public static void Enter() {
        Log.v("Infodev", "Entering " + Thread.currentThread().getStackTrace()[3].getClassName() + "::" + Thread.currentThread().getStackTrace()[3].getMethodName());
        if (EnableDebug) {
            WriteBuffer("Entering " + Thread.currentThread().getStackTrace()[3].getClassName() + "::" + Thread.currentThread().getStackTrace()[3].getMethodName());
        }
    }

    // Just log some debug data
    public static void Log(String data) {
        Log.i("Infodev", Thread.currentThread().getStackTrace()[3].getClassName() + "::" + Thread.currentThread().getStackTrace()[3].getMethodName() + " - \"" + data + "\"");
        if (EnableDebug) {
            WriteBuffer(Thread.currentThread().getStackTrace()[3].getClassName() + "::" + Thread.currentThread().getStackTrace()[3].getMethodName() + " - \"" + data + "\"");
        }
    }

    // Log exception
    public static void Exception(Throwable exception) {
        Log.e("Infodev", exception.getClass().getCanonicalName() + " in " + Thread.currentThread().getStackTrace()[3].getClassName() + "::" + Thread.currentThread().getStackTrace()[3].getMethodName(), exception);
        if ((EnableDebug | EnableLog)) {
            WriteBuffer(exception.getClass().getCanonicalName() + " in " + Thread.currentThread().getStackTrace()[3].getClassName() + "::"
                    + Thread.currentThread().getStackTrace()[3].getMethodName() + "::::: Message: " + exception.getMessage() + ExceptionToString(exception));
            Flush();
        }
    }

    // Set log options
    public static void SetOptions(boolean enableDebug, boolean enableLog, int bufferSize, int logsize, String logFileName) {
        if ((BufferSize > 0) && (bufferSize <= 0)) {
            Flush();
        }

        if ((EnableDebug != enableDebug) || (EnableLog != enableLog)) {
            Flush();
        }

        EnableDebug = enableDebug;
        EnableLog = enableLog;
        BufferSize = bufferSize;
        MaxLogSize = logsize * 1024;
        LogFileName = logFileName;
        if (BufferSize > 0) {
            LogBuffer.ensureCapacity(BufferSize);
        }
    }

    // Convert exception into string buffer (with stack trace)
    public static String ExceptionToString(Throwable exception) {
        StringBuilder result = new StringBuilder();
        StackTraceElement[] stackTrace = exception.getStackTrace();
        int traceLines = stackTrace.length;
        int i = 0;
        while (i < traceLines) {
            Object traceElement = stackTrace[i];
            result.append("\n").append(traceElement.toString());
            i += 1;
        }
        return result.toString();
    }

    // Log warning info
    public static void Warn(String data) {
        Log.w("Infodev", "WARNING! " + Thread.currentThread().getStackTrace()[3].getClassName() + "::" + Thread.currentThread().getStackTrace()[3].getMethodName() + " - \"" + data + "\"");
        if ((EnableDebug) || (EnableLog)) {
            WriteBuffer("WARNING! " + Thread.currentThread().getStackTrace()[3].getClassName() + "::" + Thread.currentThread().getStackTrace()[3].getMethodName() + " - \"" + data + "\"");
        }
    }

    // "Exit" debug record
    public static void Exit() {
        Log.v("Infodev", "Exiting " + Thread.currentThread().getStackTrace()[3].getClassName() + "::" + Thread.currentThread().getStackTrace()[3].getMethodName());
        if (EnableDebug) {
            WriteBuffer("Exiting " + Thread.currentThread().getStackTrace()[3].getClassName() + "::" + Thread.currentThread().getStackTrace()[3].getMethodName());
        }
    }

    // Write string to log buffer
    private static void WriteBuffer(String data) {
        try {
            if ((EnableLog) && (Environment.getExternalStorageState().equals("mounted"))) {
                File localFile = Environment.getExternalStorageDirectory();
                if ((localFile.exists()) && (localFile.canWrite())) {
                    if (BufferSize <= 0) {
                        WriteToFile();
                        return;
                    }

                    LogBuffer.append("\n");
                    LogBuffer.append(new Date().toString()).append(" # ").append(Thread.currentThread().getId()).append(" # ").append(data);
                    LogBuffer.append("\n");
                    if (LogBuffer.length() > BufferSize) {
                        WriteToFile();
                        LogBuffer.delete(0, LogBuffer.length());
                    }
                }
            }
        } catch (Throwable exp) {
            LogBuffer.delete(0, LogBuffer.length());
            exp.printStackTrace();
        }
    }

    // Flush buffer to file
    public static void Flush() {
        if (LogBuffer.length() > 0) {
            WriteToFile();
            LogBuffer.delete(0, LogBuffer.length());
        }
    }

    /* Write log buffer to File */
    private static boolean WriteToFile() {
        // Check file
        File f = new File(LogFileName);
        try {
            if (f.exists()) {
                if (f.length() > MaxLogSize) {
                    if (!f.delete()) Log.w("RBLOGGER", "Logger can't delete file " + f.getName());
                    if (!f.createNewFile()) Log.w("RBLOGGER", "Logger can't create new log file");
                }
            } else {
                File fdir = f.getParentFile();
                if (!fdir.mkdirs()) Log.w("RBLOGGER", "Logger can't make dirs for log path");
                if (!f.createNewFile()) Log.w("RBLOGGER", "Logger can't create new log file");
            }
        } catch (Exception exp) {
            EnableLog = false;
            EnableDebug = false;
            Warn("Can't create log file (" + f.getName() + ") -->" + exp.getMessage());
            return false;
        }

        // Write buffer to file
        try {
            FileWriter fw = new FileWriter(f, true);
            fw.append(LogBuffer.toString());
            fw.close();
            LogBuffer.delete(0, LogBuffer.length());
        } catch (Exception exp) {
            EnableLog = false;
            EnableDebug = false;
            Warn("Can't write file (" + f.getName() + ") -->" + exp.getMessage());
            return false;
        }

        return true;
    }
}
