package ru.infodev.golos27;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;

public class CrashActivity extends AppCompatActivity implements DialogResultInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new ResultDialog(Application.AppContext,
                R.drawable.error_grey,
                getString(R.string.error),
                getString(R.string.exception_error),
                getString(R.string.ok), this).show(this);
    }

    @Override
    public void DialogSendResult(int dialog_id, int cmd) {
        finish();
    }

    @Override
    public void DialogSendParamResult(int dialog_id, int cmd, JSONObject params) {

    }
}