package ru.infodev.golos27.Common.SocialNetwork;

/**
 * Class for Social Network Exception.
 *
 * @author Anton Krasov
 * @author Evgeny Gorbin (gorbin.e.o@gmail.com)
 */
public class SocialNetworkException extends RuntimeException {
    /**
     * Constructs a new exception with the specified detail message.
     * @param detailMessage the detail message constructed from social network error
     */
    public SocialNetworkException(String detailMessage) {
        super(detailMessage);
    }
}
