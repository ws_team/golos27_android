package ru.infodev.golos27.Fragments.Profile;
// TODO: исправить на "дизайнерское"

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Pattern;

import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Common.Utils.PhoneUtils;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.CustomEditText;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;

public class ChangeMobileFragment extends ParamFragment implements View.OnClickListener, TextWatcherAdapter.TextWatcherListener {
    public static final int FRAGMENT_ID = R.layout.profile_change_phone;

    private TextView showPasswordTextView;
    private CustomEditText passwordEdit;
    private CustomEditText phoneEdit;
    private boolean show_password_edit_phone = false;
    private boolean passwordGood = false;
    private boolean loginGood = false;
    Pattern patternPhone = Pattern.compile("\\+7\\(\\d{3,}\\)\\d{3,}\\-\\d{2,}\\-\\d{2,}"); // +7(xxx)xxx-xx-xx

    public ChangeMobileFragment() {
        titleId = R.string.phone_profile;
        setFragmentAllowMenu(false);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(FRAGMENT_ID, container, false);

        phoneEdit = (CustomEditText) v.findViewById(R.id.phone_edittext);
        phoneEdit.setOnTextChangedListener(this);

        showPasswordTextView = (TextView) v.findViewById(R.id.show_password_edit_phone);
        showPasswordTextView.setOnClickListener(this);

        passwordEdit = (CustomEditText) v.findViewById(R.id.password_phone_edittext);
        passwordEdit.setOnTextChangedListener(this);
        passwordEdit.setTypeface(phoneEdit.getTypeface());

        String confirmLogin = parameters.getString("confirm_login", "");
        if (!isEmpty(confirmLogin)) {
            // Логин был задан при регистрации. Сейчас пользователь
            // запустил процедуру его подтверждения (менять нельзя)
            phoneEdit.setEnabled(false);
            phoneEdit.setText(confirmLogin);
        } else {
            // Передано изменение существующего логина
            String login = parameters.getString("login_phone", "");
            phoneEdit.setText(login);
        }

        return v;
    }

    /***
     * Создает кнопку "сохранить" на тулбаре
     *
     * @param menu     меню на toolbar
     * @param inflater класс для распаковки видов
     */
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        setActionButtonIcon(ParamFragment.ICON_CHECK);
        showSaveButtonIfCan();
    }

    /***
     * Показывает или прячет кнопку "Сохранить" на доске задач
     * в зависимости от валидности введенных данных
     */
    private void showSaveButtonIfCan() {
        setActionButtonVisibility(loginGood && passwordGood);
    }

    /***
     * Вызывается если пользователь нажал кнопку на toolbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_item) {
            String login = PhoneUtils.removeBracketsAndDashes(phoneEdit.getText().toString());
            String password = passwordEdit.getText().toString();
            new LoginChangeActivate().execute(login, password);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_password_edit_phone:
                if (show_password_edit_phone) {
                    show_password_edit_phone = false;
                    String label_text = getResources().getString(R.string.show_password);
                    showPasswordTextView.setText(label_text);
                    passwordEdit.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    show_password_edit_phone = true;
                    String label_text = getResources().getString(R.string.hide_password);
                    showPasswordTextView.setText(label_text);
                    passwordEdit.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwordEdit.setSelection(passwordEdit.getText().length());
                }
                break;
        }

    }

    @Override
    public void onTextChanged(EditText view, String text) {
        CustomEditText editBox = (CustomEditText) view;
        switch (view.getId()) {
            case R.id.phone_edittext:
                loginGood = editBox.checkValue(patternPhone, true, false);
                break;
            case R.id.password_phone_edittext:
                passwordGood = text.length() >= 8;
                break;
        }
        showSaveButtonIfCan();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    class LoginChangeActivate extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setActionButtonIcon(ParamFragment.ICON_ADD_PROGRESS);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            return server.activateLoginStart(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            setActionButtonIcon(ParamFragment.ICON_DEL_PROGRESS);
            setActionButtonEnabled(false);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    result.put("input_phone", PhoneUtils.removeBracketsAndDashes(phoneEdit.getText().toString()));
                    result.put("input_password", passwordEdit.getText().toString());
                    setActionButtonEnabled(true);
                    chainStartNextFragment(FRAGMENT_ID, AccountConfirmFragment.class, result, 0, false);
                } else {
                    ResultDialog.showError(getActivity(), "Ошибка активации", errorCode, result.getErrorText());
                }
            } catch (Exception e) {
                Logger.Exception(e);
                ResultDialog.showError(getActivity(), "Ошибка активации", ErrorResult.INTERNAL_ERROR);
            }
            setActionButtonEnabled(true);
        }
    }
}