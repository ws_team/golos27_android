package ru.infodev.golos27.RestApi;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ru.infodev.golos27.Common.Catalogs.Address;
import ru.infodev.golos27.Common.Catalogs.Appeal.AppealAttachment;
import ru.infodev.golos27.Common.Catalogs.Appeal.AppealClaim;
import ru.infodev.golos27.Common.Catalogs.NewAppeal;
import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Profile;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

/***
 * Класс содержит методы для работы с API сервера
 */
public class ServerTalk extends ServerTalkBase {
    /***
     * Отправляет на сервер запрос на регистрацию
     *
     * @param firstName Имя пользователя
     * @param lastName  Фамилия пользователя
     * @param email     почтовый адрес пользователя
     * @return Map с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject registerRequest(final String firstName, final String lastName,
                                      final String email, final String phone) {

        HashMap<String, Object> query = new HashMap<String, Object>() {{
            put("lastName", lastName);
            put("firstName", firstName);
        }};

        if ((phone != null) && (!phone.equals(""))) query.put("phone", phone);
        if ((email != null) && (!email.equals(""))) query.put("email", email);
        return postServerForm("api/register/init", query);
    }

    /***
     * Отправляет на сервер запрос на подтверждение регистрации (с кодом,
     * полученным на телефон или почтовый адрес)
     *
     * @param code  Код, полученый при запросе регистрации
     * @param token Токен полученный при запросе регистрации
     * @return Map с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject registerConfirm(final String code, final String token) {
        return postServerForm("api/register/confirm", new HashMap<String, Object>() {{
            put("code", code);
            put("token", token);
        }});
    }

    /***
     * Отправляет на сервер запрос на завершение регистрации
     * (установка пароля, выбранного пользователем)
     *
     * @param password Пароль, выбранный пользователем
     * @param token    Токен полученный при подтверждении регистрации
     * @return Map с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject registerComplete(final String password, final String token) {
        return postServerForm("api/register/complete", new HashMap<String, Object>() {{
            put("password", password);
            put("token", token);
        }});
    }

    /***
     * Получение профиля пользователя
     *
     * @return Map с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject getProfile() {
        return getServerForm("api/private/user/profile", null, getAccessToken());
    }

    /***
     * Сохраняет на сервере данные профиля пользователя
     *
     * @param first   имя
     * @param last    фамилия
     * @param middle  отчество
     * @param gender  пол (Profile.MALE, Profile.FEMALE)
     * @param birth   дата рождения
     * @param oCode   код ОКТМО
     * @param oData   данные ОКТМО
     * @param address адрес пользователя
     * @return Map с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject setProfile(String first, String last, String middle, Integer gender,
                                 Date birth, String oCode, String oData, String address,
                                 Double latitude, Double longitude) {

        Profile.setFirstName(first);
        Profile.setLastName(last);
        Profile.setMiddleName(middle);
        Profile.setGender(gender);
        Profile.setBirth(birth);
        Profile.setOKTMOCode(oCode);
        Profile.setOKTMOData(oData);
        Profile.setAddress(address);
        Profile.setLatitude(latitude);
        Profile.setLongitude(longitude);

        String credentials = getAccessToken();
        HashMap<String, Object> request = new HashMap<>();
        HashMap<String, Object> addr = new HashMap<>();
        request.put("firstName", first);
        request.put("lastName", last);
        request.put("middleName", middle);

        addr.put("oktmoCode", oCode);
        addr.put("oktmoData", oData);
        addr.put("customAddress", address);
        addr.put("latitude", latitude);
        addr.put("longitude", longitude);
        request.put("address", addr);

        if (gender != null) {
            if (gender == Profile.MALE)
                request.put("gender", "MALE");
            else if (gender == Profile.FEMALE) {
                request.put("gender", "FEMALE");
            }
        } else request.put("gender", null);

        if (birth != null)
            request.put("birthDate", birth.getTime());
        else
            request.put("birthDate", null);

        return putServerForm("api/private/user/profile", request, credentials);
    }

    /***
     * Меняет пароль пользователя на сервере
     *
     * @param oldPassword старый пароль
     * @param newPassword новый пароль
     * @return JSONObject с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject changePassword(String oldPassword, String newPassword) {
        String credentials = getAccessToken();
        HashMap<String, Object> request = new HashMap<>();
        request.put("oldPassword", oldPassword);
        request.put("password", newPassword);
        return putServerForm("api/private/user/change-password", request, credentials);
    }

    /***
     * Запускает процедуру восстановления пароля пользователя
     *
     * @param login почтовый адрес или телефон пользователя
     * @return JSONObject с ответом сервера
     */
    public JSONObject initRestorePassword(String login) {
        HashMap<String, Object> request = new HashMap<>();
        request.put("login", login);
        return postServerForm("api/recovery-password/init", request);
    }

    /***
     * Отправляет на сервер запрос на подтверждение восстановления пароля
     * (с кодом, полученным на телефон или почтовый адрес)
     *
     * @param code  Код, полученый при запросе восстановления пароля
     * @param token Токен полученный при запросе восстановления пароля
     * @return JSONObject с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject restoreConfirm(final String code, final String token) {
        return postServerForm("api/recovery-password/confirm", new HashMap<String, Object>() {{
            put("code", code);
            put("token", token);
        }});
    }

    /***
     * Отправляет на сервер запрос на завершение восстановления пароля
     * (установка пароля, выбранного пользователем)
     *
     * @param password Пароль, выбранный пользователем
     * @param token    Токен полученный при подтверждении восстановления пароля
     * @return Map с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject restoreComplete(final String password, final String token) {
        return postServerForm("api/recovery-password/complete", new HashMap<String, Object>() {{
            put("password", password);
            put("token", token);
        }});
    }

    /***
     * Отправляет на сервер запрос на получение списка учетных записей
     *
     * @return JSONObject с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject getAccounts() {
        return getServerForm("api/private/user/accounts", null, getAccessToken());
    }

    /***
     * Отправляет на сервер запрос на активацию логина
     *
     * @param login    неактивированный логин пользователя
     * @param password пароль пользователя
     * @return JSONObject с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject activateLoginStart(final String login, final String password) {
        String credentials = getAccessToken();
        return postServerForm("api/private/user/activate-login", new HashMap<String, Object>() {{
            put("login", login);
            put("password", password);
        }}, credentials);
    }

    /***
     * Отправляет на сервер подтверждение активации логина
     * (с кодом, полученным на телефон или почтовый адрес)
     *
     * @param code  код подтверждения
     * @param token токен из предыдущей операции
     * @return JSONObject с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject activateLoginComplete(final String code, final String token) {
        String credentials = getAccessToken();
        return postServerForm("api/private/user/activate-login/complete", new HashMap<String, Object>() {{
            put("token", token);
            put("code", code);
        }}, credentials);
    }

    /***
     * Загрузка справочника населенных пунктов
     *
     * @param max строка для поиска
     * @return JSONObject с результами запроса (обязательные поля errorCode, errorText)
     */
    public JSONObject getOKTMOList(final int max) {
        return getServerForm("api/oktmos/list", new HashMap<String, Object>() {{
            put("pageSize", max);
        }});
    }

    /***
     * Загрузка справочника тематик
     */
    public JSONObject getSubjectsList(final long version, final long max) {
        return getServerForm("api/subjects/list", new HashMap<String, Object>() {{
            put("pageSize", max);
            put("version", version);
        }});
    }

    /***
     * Получение OKTMO-кода по адресу из Яндекс-карт
     */
    public JSONObject getOKTMOByAddress(final String region, final String area, final String settlement) {
        String credentials = getAccessToken();
        return getServerForm("api/private/yandex-addresses/get-code", new HashMap<String, Object>() {{
            put("adr1", region);
            put("adr2", area);
            put("adr3", settlement);
        }}, credentials);
    }

    /***
     * Получение списка исполнителей обращений
     *
     * @param version версия имеющегося локального списка
     * @param max     число записей в странице
     * @return ответ сервера в виде JSONObject
     */
    public JSONObject getExecutorsList(final long version, final long max) {
        return getServerForm("api/appeal-executors/list", new HashMap<String, Object>() {{
            put("version", version);
        }});
    }

    /***
     * Получение исполнителя по умолчанию для обращения по известному коду oktmo и тематике
     *
     * @param code    код oktmo населенного пункта
     * @param subject идентификатор тематики обращения
     * @return ответ сервера с идентификатором и именем исполнителя
     */
    public JSONObject getDefaultExecutor(final String code, final String subject) {
        String credentials = getAccessToken();
        return getServerForm("api/private/subject-executors/default", new HashMap<String, Object>() {{
            put("code", code);
            put("subjectId", subject);
        }}, credentials);
    }


    /***
     * Получает список обращений
     *
     * @param pageSize максимальный размер страницы
     * @param onlyMine возвращать только собственные обращения (требуется авторизация)
     * @param search   строка для поиска обращений
     * @return ответ сервера в виде JSONObject
     */
    public JSONObject getAppealsList(final Integer pageSize, final Integer page, final boolean onlyMine,
                                     final boolean onlySubscribed, final String[] statuses, final String search) {

        String credentials = getAccessToken();
        String uri = "api/appeals/list";
        HashMap<String, Object> params = new HashMap<>();
        if (pageSize != null) params.put("pageSize", pageSize);
        if (page != null) params.put("page", page);
        if (onlyMine) params.put("isOnlyMine", true);
        if (onlySubscribed) params.put("isSubscribed", true);
        if (isNotEmpty(search)) params.put("text", search);
        if (statuses != null && statuses.length > 0) {
            ArrayList<String> paramStatuses = new ArrayList<>();
            Collections.addAll(paramStatuses, statuses);
            params.put("statuses[]", paramStatuses.get(0));
        }

        params.put("sort", "APPLYING_DATE_DESC");
        if (credentials.equals(Transport.INVALID_CREDENTIALS_TOKEN)) credentials = null;
        return getServerForm(uri, params, credentials);
    }

    /***
     * Получает список уведомлений
     *
     * @param pageSize максимальный размер страницы
     * @return ответ сервера в виде JSONObject
     */
    public JSONObject getNotificationsList(final Integer pageSize, final Integer page) {
        String credentials = getAccessToken();
        String uri = "api/private/notifications/list";
        HashMap<String, Object> params = new HashMap<>();
        if (pageSize != null) params.put("pageSize", pageSize);
        if (page != null) params.put("page", page);
        return getServerForm(uri, params, credentials);
    }

    /***
     * Загрузка документа на сервер
     *
     * @param file файл для загрузки
     * @return ответ сервера в виде JSONObject
     */
    public JSONObject uploadDocument(File file) {
        String credentials = getAccessToken();
        return sendFile("api/docs/upload", file, credentials);
    }

    /***
     * Отправка нового обращения
     *
     * @param appeal объект с заявкой на обращение
     * @return ответ сервера в виде JSONObject
     */
    public JSONObject sendAppeal(final AppealClaim appeal) {
        String credentials = getAccessToken();
        HashMap<String, Object> request = new HashMap<>();
        HashMap<String, Object> address = new HashMap<>();

        if (appeal.Subject() != null) request.put("subjectId", appeal.Subject().Id());
        if (appeal.Executor() != null && isNotEmpty(appeal.Executor().Id()))
            request.put("executorId", appeal.Executor().Id());

        if (appeal.Message() != null) request.put("message", appeal.Message());
        if (appeal.DocumentCoverIdPlain() != null) request.put("documentCoverId", appeal.DocumentCoverIdPlain());

        request.put("isPublishPersonalData", appeal.IsPublishPersonalData());
        request.put("isPublishResponse", appeal.IsPublishResponse());
        request.put("isPublishAppeal", appeal.IsPublishAppeal());
        request.put("isSendResponse", appeal.IsGetMailResponse());

        // Добавим в запрос адрес проблемного места
        if (appeal.Address() != null && appeal.Address().Settlement() != null) {
            address.put("oktmoCode", appeal.Address().Settlement().getCode());
            address.put("oktmoData", appeal.Address().Settlement().getData());
            address.put("customAddress", appeal.Address().CustomAddress());

            Double latitude = appeal.Address().Latitude();
            Double longitude = appeal.Address().Longitude();
            if (latitude == Address.INVALID_COORDINATE) latitude = null;
            if (longitude == Address.INVALID_COORDINATE) longitude = null;
            address.put("latitude", latitude);
            address.put("longitude", longitude);

            if (latitude != null && longitude != null)
                request.put("address", address);
        }

        // Добавим в запрос почтовый адрес для получения ответа
        if (appeal.IsGetMailResponse() && appeal.ZipAddress() != null &&
                appeal.ZipAddress().Settlement() != null) {
            String zipFullAddress = NewAppeal.getInstance().ZipAddress().FullAddress(true);
            request.put("zipAddress", zipFullAddress);
        }

        // Если есть уже загруженные аттачи - добавим их в запрос
        List<AppealAttachment> attachments = appeal.Documents();
        if (attachments != null && attachments.size() > 0) {
            List<String> documents = new ArrayList<>();
            for (AppealAttachment entry : attachments) documents.add(entry.Id());
            if (documents.size() > 0) request.put("documentsId", documents);
        }

        return postServerForm("api/private/appeals/add", request, credentials);
    }

    /***
     * Возвращает ссылку на указанный в параметрах документ
     *
     * @param documentId идентификатор документа
     * @return полная ссылка для загрузки документа
     */
    public static String getDocumentUri(String documentId) {
        return getBaseUri() + "/api/docs/download/" + documentId;
    }

    /***
     * Загрузка справочника населенных пунктов
     *
     * @return JSONObject с результами запроса (обязательные поля errorCode, errorText)
     */
    public Integer getUnreadNotificationsCount() {
        Integer result = null;
        JSONObject reply = getServerForm("api/private/notifications/count", null, getAccessToken());
        try {
            if (((Integer) reply.get("errorCode")) == Transport.SUCCESS) {
                result = reply.getInt("quantity");
            }
        } catch (Exception e) {
            Logger.Exception(e);
        }

        return result;
    }

    public static final String ACTION_LIKE = "like";
    public static final String ACTION_UNLIKE = "unlike";
    public static final String ACTION_SUBSCRIBE = "subscribe";
    public static final String ACTION_UNSUBSCRIBE = "unsubscribe";
    public static final String ACTION_HIDE = "hide";
    public static final String ACTION_UNHIDE = "unhide";
    public static final String ACTION_HIDE_ANSWER = "hide-answer";
    public static final String ACTION_UNHIDE_ANSWER = "unhide-answer";
    public static final String ACTION_RATE_RESPONSE = "rate-respond";

    /***
     * Выполнить действие над обращением
     *
     * @param appealId идентификатор обращения
     * @param action действие над обращением
     * @return ответ сервера в виде JSONObject
     */
    public JSONObject appealAction(String appealId, String action) {
        String credentials = getAccessToken();
        String path = "api/private/appeals/" + appealId + "/" + action;
        return putServerForm(path, null, credentials);
    }

    /***
     * Поставить оценку ответу на обращение
     *
     * @param appealId идентификатор обращения
     * @param rating рейтинг ответа
     * @return ответ сервера в виде JSONObject
     */
    public JSONObject rateAppealResponse(String appealId, int rating) {
        String credentials = getAccessToken();
        String path = "api/private/appeals/" + appealId + "/" + ACTION_RATE_RESPONSE;
        HashMap<String, Object> params = new HashMap<>();
        params.put("rating", String.valueOf(rating));
        return putServerForm(path, params, credentials);
    }

    /***
     * Загрузить обращение по идентификатору
     *
     * @param appealId идентификатор обращения
     * @return ответ сервера в виде JSONObject
     */
    public JSONObject getAppeal(String appealId) {
        String credentials = getAccessToken();
        String path = "api/appeals/" + appealId;
        if (credentials.equals(Transport.INVALID_CREDENTIALS_TOKEN)) credentials = null;
        return getServerForm(path, null, credentials);
    }

    /***
     * Выполнить действие над обращением
     *
     * @param notificationId идентификатор обращения
     * @return ответ сервера в виде JSONObject
     */
    public JSONObject setNotificationRead(String notificationId) {
        String credentials = getAccessToken();
        String path = "api/private/notifications/" + notificationId + "/" + "read";
        return putServerForm(path, null, credentials);
    }

    /***
     * Возвращает число собственных обращений
     */
    public Integer getMyAppealsCount() {
        Integer result = null;
        HashMap<String, Object> params = new HashMap<>();
        params.put("pageSize", 1);
        params.put("isOnlyMine", true);
        JSONObject reply = getServerForm("api/appeals/list", params, getAccessToken());
        if (((Integer) reply.get("errorCode", ErrorResult.UNKNOWN_ERROR)) == ErrorResult.SUCCESS)
            result = reply.getInt("totalSize", 0);

        return result;
    }

    /***
     * Получает с сервера токен для подписывания на канал push-уведомлений (нам notification в пушах не нужен
     * так как удобнее все обрабатывать в одном месте и андроид это позволяет - запросим это у сервера)
     */
    public JSONObject subscribePushTopic() {
        String credentials = getAccessToken();
        if (credentials == null || credentials.equals(Transport.INVALID_CREDENTIALS_TOKEN)) return null;
        return postServerForm("api/private/fcm/topics/subscribe", null, credentials);
    }

    /***
     * Отписывается от канала push-уведомлений
     */
    public JSONObject unsubscribePushTopic() {
        String credentials = getAccessToken();
        return delServerForm("api/private/fcm/topics/unsubscribe", null, credentials);
    }

    public static String PUSH_TOKEN_FIELD = "topicUid";
}
