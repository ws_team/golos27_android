package ru.infodev.golos27.Common.Utils;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import ru.infodev.golos27.Application;

@SuppressWarnings("unused")
public class Utils {

    /***
     * Проверяет есть в указанном поле ImageView установленное
     * изображение любого типа
     */
    public static boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable)drawable).getBitmap() != null;
        }

        return hasImage;
    }

    public static boolean isNotEmpty(CharSequence str) {
        return !isEmpty(str);
    }

    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

    public static byte[] getIV() {
        return new byte[]{42, 13, 67, 35, 91, 17, 1, 2, 76, 12, 33, 81, 71, 51, 9, 53};
    }

    public static <T> String join(Collection<T> coll, String separator) {
        return join(coll, separator, null);
    }

    public static String join(Object[] arr, String separator) {
        return join(arr, separator, null);
    }

    public static <T> String join(Collection<T> coll, String separator, String terminator) {
        return join(coll.toArray(new Object[coll.size()]), separator, terminator);
    }

    public static String join(Object[] arr, String separator, String terminator) {
        StringBuilder sb = new StringBuilder(arr.length * 2);
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i]);
            if (i < arr.length - 1) {
                sb.append(separator);
            } else if (terminator != null && arr.length > 0) {
                sb.append(terminator);
            }
        }
        return sb.toString();
    }

    public static String urlEncode(String str) {
        try {
            return URLEncoder.encode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("failed to encode", e);
        }
    }

    public static String urlDecode(String str) {
        try {
            return URLDecoder.decode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("failed to decode", e);
        }
    }

    // Хранить это все в keystore проблематично - надо показывать пользователю кучу диалогов
    // заставлять его создавать/вводить пин для устройства. Поэтому храним в базе и шифруем
    // ключом. Ключ обфусцируем xor-ом.
    public static String secretKeyToString(SecretKey key) {
        if (key == null) return null;
        byte[] encoded = key.getEncoded();
        if (encoded == null) return null;
        for (int i = 0; i < encoded.length; i++) encoded[i] = (byte) ((encoded[i] ^ 42) & 0xff);
        return Base64.encodeToString(encoded, Base64.NO_WRAP);
    }

    public static SecretKey stringToSecretKey(String key) {
        byte[] encoded = Base64.decode(key, Base64.NO_WRAP);
        for (int i = 0; i < encoded.length; i++) encoded[i] = (byte) ((encoded[i] ^ 42) & 0xff);
        return new SecretKeySpec(encoded, 0, encoded.length, getDataProperty());
    }

    public static String getMD5(String str)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return getHash(str, "MD5", 32);
    }

    public static String getSHA1(String str)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return getHash(str, "SHA-1", 40);
    }

    public static String getHash(String str, String algorithm, int length)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        byte[] bytes = str.getBytes("utf-8");
        MessageDigest md = MessageDigest.getInstance(algorithm);
        byte[] digest = md.digest(bytes);
        BigInteger bigInt = new BigInteger(1, digest);
        String hash = bigInt.toString(16);
        while (hash.length() < length) {
            hash = "0" + hash;
        }
        return hash;
    }

    public static String trimString(String string, int length) {
        if (string == null || string.trim().isEmpty()) return string;
        StringBuilder sb = new StringBuilder(string);
        int actualLength = length - 3;
        if (sb.length() > actualLength) {
            return sb.insert(actualLength, "...").substring(0, actualLength + 3);
        }
        return string;
    }

    public static boolean isStringsSame(String string1, String string2) {
        return (string1 == null) && (string2 == null) ||
                !(string1 == null || string2 == null) && string1.equals(string2);

    }

    public static File getNewImageFile() {
        File image = null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "Golos27_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        try {
            //noinspection ResultOfMethodCallIgnored
            storageDir.mkdirs();
            image = File.createTempFile(imageFileName, ".jpg", storageDir);
        } catch (Exception e) {
            Logger.Exception(e);
        }

        return image;
    }

    /***
     * Освобождает память изображения со всеми проверками
     *
     * @param bitmap изображение
     */
    public static void recycleBitmap(Bitmap bitmap) {
        if (bitmap == null || bitmap.isRecycled()) return;
        bitmap.recycle();
        System.gc();
    }

    /***
     * Декодирует изображение из файла в bitmap с разрешением, близким к заданному
     *
     * @param path      путь до файла (file:// или content://)
     * @param reqWidth  требуемая ширина изображения
     * @param reqHeight требуемая высота изображения
     * @return декодированная картинка
     */
    public static Bitmap decodeFromFile(String path, int reqWidth, int reqHeight) {
        if (path != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);
            options.inSampleSize = calculateBitmapSize(options, reqWidth, reqHeight);
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(path, options);
        }

        return null;
    }

    public static String simpleEncodeString(String data) {
        try {
            byte[] bytes = data.getBytes("UTF-8");
            for (int i = 0; i < bytes.length; i++) bytes[i] = (byte) ((bytes[i] ^ 62) & 0xff);
            return Base64.encodeToString(bytes, Base64.NO_WRAP);
        } catch (Exception e) {
            Logger.Exception(e);
        }
        return data; // shit happens :(
    }

    public static String simpleDecodeString(String data) {
        try {
            byte[] bytes = Base64.decode(data, Base64.NO_WRAP);
            for (int i = 0; i < bytes.length; i++) bytes[i] = (byte) ((bytes[i] ^ 62) & 0xff);
            return new String(bytes, "UTF-8");
        } catch (Exception e) {
            Logger.Exception(e);
        }
        return data; // shit happens :(
    }

    /***
     * Декодирует изображение из файла в bitmap с полным разрешением
     *
     * @param uri путь до файла (file:// или content://)
     * @return декодированная картинка
     */
    public static Bitmap decodeFromFile(Uri uri) {
        String path = UriToPath.getPath(Application.AppContext, uri);
        if (path != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(path, options);
        }

        return null;
    }

    public static String getFullDataProperty() {
        return new String(new char[]{65, 69, 83, 47, 67, 66, 67, 47, 80, 75, 67, 83, 53, 80, 97, 100, 100, 105, 110, 103});
    }

    /***
     * Декодирует изображение из файла в bitmap с таким разрешением,
     * чтобы уложиться в заданный максимальный размер файла
     *
     * @param path путь до файла
     * @return декодированная картинка
     */
    public static Bitmap decodeFromFile(String path, int maxSize) {
        if (path != null) {
            File bitmapFile = new File(path);
            long fileLength = bitmapFile.length();
            if (fileLength > 0) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                Bitmap bitmap = BitmapFactory.decodeFile(path, options);
                // Если размер файла больше ограничения, то надо уменьшить размер
                // принимая допущение, что коэффициент сжатия файла не будет меняться
                // при уменьшении разрешения. Ну по крайней мере не сильно будет меняться :-)
                if (fileLength > maxSize) {
                    int width = options.outWidth;
                    int height = options.outHeight;
                    long rawLength = width * height * 4;
                    float compressRatio = (float) rawLength / (float) fileLength;
                    float aspectRatio = (float) width / (float) height;
                    long bpp = rawLength / width / height;

                    //long calculatedSize = (long) (width * height * bpp / compressRatio);
                    long calculatedSize = (long) (width * height * bpp / compressRatio);
                    while (calculatedSize > maxSize) {
                        width -= 10; // Шаг уменьшения ширины (высота уменьшается пропорционально)
                        height = (int) (width / aspectRatio);
                        calculatedSize = (long) (width * height * bpp / compressRatio);
                    }

                    // Здесь у нас есть вычисленные ширина и высота изображения
                    Logger.Log("Resize: fileSize=" + String.valueOf(fileLength) +
                            ", maxSize=" + String.valueOf(maxSize) +
                            ", calcSize=" + String.valueOf(calculatedSize) +
                            ", cWidth=" + String.valueOf(width) +
                            ", cHeight=" + String.valueOf(height));

                    // Ок. Все что нужно есть. Теперь нужно рескалить файл к требуемым
                    // высоте и ширине и как-то и избежать переполнения памяти.
                    // Первым делом пытаемся втупую загрузить изображение и изменить его
                    // размер. Ошибку OOM ловим и обрабатываем
                    try {
                        // Попытка 1 - пытаемся изменить размер как положено
                        options.inJustDecodeBounds = false;
                        bitmap = BitmapFactory.decodeFile(path, options);
                        return Bitmap.createScaledBitmap(bitmap, width, height, true);
                    } catch (OutOfMemoryError e) {
                        // Попытка 2 - декодим изображение из файла с помощью inSampleSize
                        // Да. Размер будет меньше, чем хотелось бы, но выбора нет...
                        System.gc();
                        return decodeFromFile(path, width, height);
                    }
                }

                return bitmap;
            }
        }

        return null;
    }

    /***
     * Вычисляет размеры изображения, кратные двум, близкие к заданным
     *
     * @param options   параметры декодирования bitmap
     * @param reqWidth  требуемая ширина изображения
     * @param reqHeight требуемая высота изображения
     * @return inSampleSize для декодирования
     */
    public static int calculateBitmapSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int thumbnailSize = 2;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / thumbnailSize) > reqHeight && (halfWidth / thumbnailSize) > reqWidth) {
                thumbnailSize *= 2;
            }
        } else {
            thumbnailSize = 1;
        }
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return thumbnailSize;
    }

    /***
     * Удаляет файл по заданному пути
     *
     * @param path путь до файла
     * @return результат выполнения операции
     */
    public static boolean deleteFile(String path) {
        File file = new File(path);
        return file.delete();
    }

    /***
     * Копирует файл из inputPath в outputPath
     *
     * @param inputPath  исходный путь до файла
     * @param outputPath путь до файла-копии
     */
    public static void copyFile(String inputPath, String outputPath) {
        int read;
        InputStream in;
        OutputStream out;
        try {
            in = new FileInputStream(inputPath);
            out = new FileOutputStream(outputPath);
            byte[] buffer = new byte[1024];
            while ((read = in.read(buffer)) != -1) out.write(buffer, 0, read);
            in.close();
            out.flush();
            out.close();
        } catch (Exception e) {
            Logger.Exception(e);
        }
    }

    public static String getCacheDirPath() {
        return Application.imagesCacheDirectory;
    }

    public static String getCacheThumbnailDirPath() {
        return Application.thumbsCacheDirectory;
    }

    public static String getImageCachePath(String path) {
        File file = new File(path);
        String name = file.getName();
        return getCacheDirPath() + File.separator + name;
    }

    public static String getImageThumbPath(String path) {
        File file = new File(path);
        String name = file.getName();
        return getCacheThumbnailDirPath() + File.separator + name;
    }

    /***
     * Получает ориентацию изображения в градусах для поворота preview
     *
     * @param context контекст
     * @param uri     ссылка на изображение
     * @return угол поворота изображения в градусах
     */
    public static int getOrientation(Context context, Uri uri) {
        if ("content".equals(uri.getScheme())) {
            Cursor cursor = context.getContentResolver().query(uri,
                    new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

            int orientation = -1;

            if (cursor == null || !cursor.moveToFirst()) {
                return orientation;
            }

            try {
                orientation = cursor.getInt(0);
            } catch (CursorIndexOutOfBoundsException e) {
                return orientation;
            } finally {
                cursor.close();
            }

            return orientation;
        } else {
            try {
                ExifInterface exif = new ExifInterface(uri.getPath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        return 270;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        return 180;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        return 90;
                    case ExifInterface.ORIENTATION_NORMAL:
                        return 0;
                    default:
                        return -1;
                }
            } catch (IOException e) {
                return -1;
            }
        }
    }

    public static int dpToPx(int dp) {
        DisplayMetrics displayMetrics = Application.AppContext.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static String getNumEnding(int iNumber, String[] aEndings) {
        String sEnding;
        int i;

        iNumber = iNumber % 100;
        if (iNumber >= 11 && iNumber <= 19) {
            sEnding = aEndings[2];
        } else {
            i = iNumber % 10;
            switch (i) {
                case (1):
                    sEnding = aEndings[0];
                    break;
                case (2):
                case (3):
                case (4):
                    sEnding = aEndings[1];
                    break;
                default:
                    sEnding = aEndings[2];
            }
        }
        return sEnding;
    }

    public static String readableFileSize(long size, String[] units) {
        if (size <= 0) return "0 " + units[0];
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    /**
     * <p>Checks if two calendars represent the same day ignoring time.</p>
     *
     * @param cal1 the first calendar, not altered, not null
     * @param cal2 the second calendar, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either calendar is <code>null</code>
     */
    public static boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    /**
     * <p>Checks if two dates are on the same day ignoring time.</p>
     *
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either date is <code>null</code>
     */
    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return isSameDay(cal1, cal2);
    }

    /**
     * <p>Checks if a date is today.</p>
     *
     * @param date the date, not altered, not null.
     * @return true if the date is today.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isToday(Date date) {
        return isSameDay(date, Calendar.getInstance().getTime());
    }

    /**
     * <p>Checks if a calendar date is today.</p>
     *
     * @param cal the calendar, not altered, not null
     * @return true if cal date is today
     * @throws IllegalArgumentException if the calendar is <code>null</code>
     */
    public static boolean isToday(Calendar cal) {
        return isSameDay(cal, Calendar.getInstance());
    }

    /**
     * <p>Checks if a date is today.</p>
     *
     * @param date the date, not altered, not null.
     * @return true if the date is today.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isYesterday(Date date) {
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        return isSameDay(date, yesterday.getTime());
    }

    /**
     * <p>Checks if a calendar date is today.</p>
     *
     * @param cal the calendar, not altered, not null
     * @return true if cal date is today
     * @throws IllegalArgumentException if the calendar is <code>null</code>
     */
    public static boolean isYesterday(Calendar cal) {
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        return isSameDay(cal, yesterday);
    }

    public static String getDataProperty() {
        String prop = "A";
        prop += "-E";
        return prop.replace("-", "") + "S";
    }

    public static Calendar convertToGmt(Calendar cal) {
        Date date = cal.getTime();
        TimeZone tz = cal.getTimeZone();
        long msFromEpochGmt = date.getTime();
        int offsetFromUTC = tz.getOffset(msFromEpochGmt);
        Calendar gmtCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        gmtCal.setTime(date);
        gmtCal.add(Calendar.MILLISECOND, offsetFromUTC);
        return gmtCal;
    }
}
