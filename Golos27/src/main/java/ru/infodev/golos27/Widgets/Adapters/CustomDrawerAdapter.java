package ru.infodev.golos27.Widgets.Adapters;

import android.content.Context;
import android.support.design.internal.NavigationMenuItemView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.infodev.golos27.Common.Interfaces.RecycleViewClickInterface;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.DrawerItem;

public class CustomDrawerAdapter extends RecyclerView.Adapter {
    private List<DrawerItem> data;
    private int layoutId;
    private LayoutInflater inflater = null;
    private RecycleViewClickInterface clickInterface;
    boolean UserHasAuthorized = false;

    public CustomDrawerAdapter(Context context, int layout_id, List<DrawerItem> list) {
        this.data = list;
        this.inflater = LayoutInflater.from(context);
        this.layoutId = layout_id;
    }

    /**
     * Handles click events for the menu items. The items has to be {@link NavigationMenuItemView}.
     */
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int itemId = (int) v.getTag();
            if (clickInterface != null) clickInterface.onItemClick(itemId);
        }
    };

    public void setOnItemClickListener(RecycleViewClickInterface listener) {
        clickInterface = listener;
    }

    public void setData(List<DrawerItem> list) {
        this.data = list;
    }

    public List<DrawerItem> getData() {
        return this.data;
    }

    public void setUserHasAuthorized(boolean userHasAuthorized) {
        UserHasAuthorized = userHasAuthorized;
    }

    @SuppressWarnings("unused")
    public DrawerItem getItemById(int id) {
        for (DrawerItem item : data) {
            if (item.getItemId() == id) return item;
        }
        return null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = inflater.inflate(layoutId, viewGroup, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int i) {
        DrawerItem item = getItem(i);
        final ItemViewHolder viewHolder = (ItemViewHolder) holder;
        viewHolder.text.setText(item.getItemName());
        viewHolder.item.setTag(item.getItemId());
        viewHolder.item.setOnClickListener(mOnClickListener);

        if (item.NewItemsCount() > 0) {
            viewHolder.notificationText.setText(String.valueOf(item.NewItemsCount()));
            viewHolder.notificationView.setVisibility(View.VISIBLE);
        } else {
            viewHolder.notificationView.setVisibility(View.GONE);
            viewHolder.notificationText.setText(null);
        }
    }

    @Override
    public int getItemCount() {
        int count = 0;
        for (DrawerItem element: data) {
            if (element.isVisible(UserHasAuthorized)) count++;
        }
        return count;
    }

    public DrawerItem getItem(int position) {
        for (int i=0; i<=position;i++) {
            DrawerItem element = data.get(i);
            if(!element.isVisible(UserHasAuthorized))
                position++;
        }
        return data.get(position);
    }

    /**
     * Реализация класса ViewHolder, хранящего ссылки на виджеты
     * Чтобы их не надо было каждый раз искать с помощью findViewById
     * При инициализации заполняет все ссылки
     */
    class ItemViewHolder extends RecyclerView.ViewHolder {
        private View item;
        private TextView text;
        private View notificationView;
        private TextView notificationText;

        // Конструктор - здесь ищем все заполняемые элементы
        public ItemViewHolder(View itemView) {
            super(itemView);
            item = itemView;
            text = (TextView) itemView.findViewById(R.id.drawer_itemName);
            notificationView = itemView.findViewById(R.id.newCountLayout);
            notificationText = (TextView) itemView.findViewById(R.id.newCountText);
        }
    }
}
