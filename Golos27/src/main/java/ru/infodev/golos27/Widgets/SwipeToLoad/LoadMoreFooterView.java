package ru.infodev.golos27.Widgets.SwipeToLoad;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class LoadMoreFooterView extends TextView implements SwipeTrigger, SwipeLoadMoreTrigger {
    public LoadMoreFooterView(Context context) {
        super(context);
    }

    public LoadMoreFooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onLoadMore() {
        setText("Загружаем...");
    }

    @Override
    public void onPrepare() {
        setText("");
    }

    @Override
    public void onSwipe(int yScrolled, boolean isComplete) {
        if (!isComplete) {
            if (yScrolled <= -getHeight()) {
                setText("Отпустите для загрузки");
            } else {
                setText("Потяните чтобы загрузить");
            }
        } else {
            setText("Отпускаем...");
        }
    }

    @Override
    public void onRelease() {
        setText("Загружаем...");
    }

    @Override
    public void complete() {
        setText("Загрузка завершена");
    }

    @Override
    public void onReset() {
        setText("");
    }
}
