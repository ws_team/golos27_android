package ru.infodev.golos27.Widgets.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import ru.infodev.golos27.Common.Catalogs.Appeal.AppealAttachment;
import ru.infodev.golos27.Common.Interfaces.RecycleViewClickInterface;
import ru.infodev.golos27.R;

import static ru.infodev.golos27.Common.Utils.Utils.readableFileSize;

/**
 * Класс адаптера наследуется от RecyclerView.Adapter с указанием класса, который будет хранить
 * ссылки на виджеты элемента списка, т.е. класса, имплементирующего ViewHolder. В нашем
 * случае класс объявлен внутри класса адаптера.
 */
public class DocListAdapter extends RecyclerView.Adapter {

    private List<AppealAttachment> data;
    private LayoutInflater inflater;
    private RecycleViewClickInterface clickInterface;
    private String[] units;

    /***
     * При создании адаптера загружаются все иконки для ускорения отрисовки
     * и инициализируется список с обращениями.
     *
     * @param context    контекст
     * @param appealList список обращений
     */
    public DocListAdapter(Context context, List<AppealAttachment> appealList, String[] units) {
        this.data = appealList;
        inflater = LayoutInflater.from(context);
        this.units = units;
    }

    public void setOnClickListener(RecycleViewClickInterface listener) {
        clickInterface = listener;
    }

    /***
     * Устанавливает данные в адаптер
     *
     * @param appealList список обращений
     */
    public void setData(List<AppealAttachment> appealList) {
        this.data = appealList;
    }

    /***
     * Возвращает список с данными привязанный к адаптеру
     *
     * @return список обращений
     */
    public List<AppealAttachment> getData() {
        return this.data;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые
     * впоследствии могут переиспользоваться.
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = inflater.inflate(R.layout.item_document, viewGroup, false);
        return new ItemViewHolder(v);
    }

    /**
     * Заполнение элементов данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int i) {
        AppealAttachment entry = getData().get(i);
        final ItemViewHolder viewHolder = (ItemViewHolder) holder;

        SpannableString name = new SpannableString(entry.Name());
        name.setSpan(new UnderlineSpan(), 0, name.length(), 0);
        name.setSpan(new ForegroundColorSpan(Color.BLUE), 0, name.length(), 0);
        viewHolder.fileNameView.setText(name);

        viewHolder.fileSizeView.setText(readableFileSize(entry.Length(), units));
    }

    @Override
    public int getItemCount() {
        return getData().size();
    }

    public AppealAttachment getItem(int position) {
        return data.get(position);
    }

    /**
     * Реализация класса ViewHolder, хранящего ссылки на виджеты
     * Чтобы их не надо было каждый раз искать с помощью findViewById
     * При инициализации заполняет все ссылки
     */
    class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView fileNameView;
        public TextView fileSizeView;
        public LinearLayout documentItemView;

        // Конструктор - здесь ищем все заполняемые элементы
        public ItemViewHolder(View itemView) {
            super(itemView);
            fileNameView = (TextView) itemView.findViewById(R.id.file_name);
            fileSizeView = (TextView) itemView.findViewById(R.id.file_size);
            documentItemView = (LinearLayout) itemView.findViewById(R.id.document_item);
            documentItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickInterface != null) clickInterface.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}
