package ru.infodev.golos27.Widgets.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.infodev.golos27.Common.Catalogs.Executor;
import ru.infodev.golos27.R;


public class ExecutorAdapter extends BaseAdapter {
    private ArrayList<Executor> mData;
    private LayoutInflater inflater;

    public ExecutorAdapter(Context context, List<Executor> items) {
        mData = (ArrayList<Executor>) items;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Executor getItem(int index) {
        return mData.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_executor, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.executor_TV);
            viewHolder.check = (ImageView) convertView.findViewById(R.id.check_name_executor);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Executor item = getItem(position);
        viewHolder.name.setText(item.Name());
        if (item.isChecked()) {
            viewHolder.check.setVisibility(View.VISIBLE);
        } else {
            viewHolder.check.setVisibility(View.INVISIBLE);
        }
        return convertView;
    }

    public ArrayList<Executor> getData() {
        return mData;
    }

    public void setData(ArrayList<Executor> mData) {
        this.mData = mData;
    }

    public Executor getCheckedItem() {
        if (mData == null) return null;
        for (Executor entry : mData)
            if (entry.isChecked()) return entry;
        return null;
    }

    static class ViewHolder {
        TextView name;
        ImageView check;
    }

    public void setChecked(int position, boolean checked) {
        if (mData == null) return;
        for (Executor entry : mData) entry.setIsChecked(false);
        Executor executor = mData.get(position);
        if (executor != null) executor.setIsChecked(checked);
        notifyDataSetChanged();
    }
}
