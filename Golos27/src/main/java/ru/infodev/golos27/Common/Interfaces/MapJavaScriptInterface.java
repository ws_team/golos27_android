package ru.infodev.golos27.Common.Interfaces;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.Address;
import ru.infodev.golos27.Common.Catalogs.NewAppeal;
import ru.infodev.golos27.Common.Utils.LocationHelper;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.AppealClaim.SetMapLocationFragment;
import ru.infodev.golos27.R;

@SuppressWarnings("unused")
public class MapJavaScriptInterface {

    private LocationResultInterface locationListener;
    private LocationHelper location;
    private String mInitialAddress;
    private double mInitialLatitude;
    private double mInitialLongitude;

    public MapJavaScriptInterface(Context context) {
        location = new LocationHelper(Application.AppContext);
        if (!location.hasLocationEnabled())
        {
            LocationHelper.openSettings(context);
            Toast.makeText(Application.AppContext, R.string.enable_gps_please, Toast.LENGTH_SHORT).show();
        }
        startLocationTracker();
    }

    public MapJavaScriptInterface(Context context, boolean enable_gps) {
        location = new LocationHelper(Application.AppContext);
        if (enable_gps) {
            if (!location.hasLocationEnabled()) LocationHelper.openSettings(context);
            startLocationTracker();
        }
    }

    public void startLocationTracker() {
        location.start();
    }

    public void stopLocationTracker() {
        location.stop();
    }

    public void setLocationListener(LocationResultInterface listener) {
        locationListener = listener;
    }

    // ---[ Интерфейсы взаимодействия с WebView ]----------------------------------------------------------------------
    @JavascriptInterface
    public void OnLocationData(String region, String area, String settlement,
                               String address, String latitude, String longitude, String zoomLevel) {
        double dLatitude = SetMapLocationFragment.INVALID_COORDINATE;
        double dLongitude = SetMapLocationFragment.INVALID_COORDINATE;
        float iZoomLevel = 6;
        try {
            dLatitude = Double.valueOf(latitude);
            dLongitude = Double.valueOf(longitude);
            iZoomLevel = Float.valueOf(zoomLevel);
        } catch (Exception e) {
            Logger.Exception(e);
        }

        if (locationListener != null)
            locationListener.OnLocationReceived(region, area, settlement, address, dLatitude, dLongitude, (int)iZoomLevel);
    }

    @JavascriptInterface
    public void OnPageLoaded() {
        if (locationListener != null) locationListener.OnPageLoaded();
    }

    @JavascriptInterface
    public String[] getLocation() {
        Location point = location.getLocation();
        if (point != null) {
            String latitude = String.valueOf(point.getLatitude());
            String longitude = String.valueOf(point.getLongitude());
            return new String[]{latitude, longitude};
        }
        return null;
    }

    @JavascriptInterface
    public String getLatitude() {
        Location point = location.getLocation();
        if (point != null) {
            return String.valueOf(point.getLatitude());
        }
        return null;
    }

    @JavascriptInterface
    public String getLongitude() {
        Location point = location.getLocation();
        if (point != null) {
            return String.valueOf(point.getLongitude());
        }
        return null;
    }

    @JavascriptInterface
    public String getSavedLatitude() {
        Address address = NewAppeal.getInstance().Address();
        if (address != null) {
            if (address.Latitude() != SetMapLocationFragment.INVALID_COORDINATE) {
                return String.valueOf(address.Latitude());
            }
        }
        return null;
    }

    @JavascriptInterface
    public String getSavedLongitude() {
        Address address = NewAppeal.getInstance().Address();
        if (address != null) {
            if (address.Longitude() != SetMapLocationFragment.INVALID_COORDINATE) {
                return String.valueOf(address.Longitude());
            }
        }
        return null;
    }

    @JavascriptInterface
    public String getSavedZoom() {
        Address address = NewAppeal.getInstance().Address();
        if (address != null) return String.valueOf(address.ZoomLevel());
        return null;
    }

    @JavascriptInterface
    public String getSavedAddress() {
        Address address = NewAppeal.getInstance().Address();
        if (address != null) return address.CustomAddress();
        return null;
    }

    /***
     * Пересчитывает размер из dp в px
     *
     * @return размер в пикселях
     */
    @JavascriptInterface
    public int getPxSize(int dp) {
        DisplayMetrics metrics = Application.AppContext.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

    @JavascriptInterface
    public String getIcon(int iconId) {
        switch (iconId) {
            case 1:
                return encodeIcon(R.drawable.ic_location);
            case 2:
                return encodeIcon(R.drawable.ic_plus);
            case 3:
                return encodeIcon(R.drawable.ic_minus);
            case 4:
                return encodeIcon(R.drawable.ic_placemark);
            case 5:
                return encodeIcon(R.drawable.ic_map_close);
        }
        return null;
    }

    @JavascriptInterface
    public String getInitialAddress() {
        return mInitialAddress;
    }

    @JavascriptInterface
    public double getInitialLatitude() {
        return mInitialLatitude;
    }

    @JavascriptInterface
    public double getInitialLongitude() {
        return mInitialLongitude;
    }

    private static Bitmap getBitmap(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable != null) {
            return getBitmap(drawable);
        } else {
            throw new IllegalArgumentException("unsupported drawable type");
        }
    }

    private static Bitmap getBitmap(Drawable vectorDrawable) {
        int h = vectorDrawable.getIntrinsicHeight();
        int w = vectorDrawable.getIntrinsicWidth();
        vectorDrawable.setBounds(0, 0, w, h);
        Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bm);
        vectorDrawable.draw(canvas);
        return bm;
    }

    /***
     * Кодирует drawable изображение в base64 для передачи в webview
     *
     * @param id идентификатор изображения для кодирования
     * @return строка с закодированным изображением
     */
    public static String encodeIcon(int id) {
        Bitmap bitmap = getBitmap(Application.AppContext, id);
        if (bitmap != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] bitmapByte = stream.toByteArray();
            return new String(Base64.encode(bitmapByte, Base64.NO_WRAP));
        }
        return null;
    }

    public void setInitialBalloon(String address, double latitude, double longitude) {
        this.mInitialAddress = address;
        this.mInitialLatitude = latitude;
        this.mInitialLongitude = longitude;
    }
}
