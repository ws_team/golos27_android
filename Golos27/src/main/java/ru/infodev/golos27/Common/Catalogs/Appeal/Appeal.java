package ru.infodev.golos27.Common.Catalogs.Appeal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ru.infodev.golos27.Common.Catalogs.Executor;
import ru.infodev.golos27.Common.Catalogs.Subject;
import ru.infodev.golos27.Common.Utils.Json.JSONArray;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;

import static ru.infodev.golos27.ApiConfig.FRONT_ADDRESS;

@SuppressWarnings("unused")
public class Appeal {

    // Статусы обращений
    public final static String STATUS_DRAFT = "DRAFT";
    public final static String STATUS_MODERATION = "MODERATION";
    public final static String STATUS_INWORK = "INWORK";
    public final static String STATUS_SOLVED = "SOLVED";
    public final static String STATUS_REJECTED = "REJECT";

    // Отметки обращения
    public final static String REG_DATE_VIOLATION = "REG_DATE_VIOLATION";
    public final static String RESPONSE_DATE_VIOLATION = "RESPONSE_DATE_VIOLATION";
    public final static String FORWARDING_DATE_VIOLATION = "FORWARDING_DATE_VIOLATION";
    public final static String REPLIED = "REPLIED";
    public final static String RATED = "RATED";
    public final static String PROLONGED = "PROLONGED";
    public final static String APPEAL_HIDE = "APPEAL_HIDE";
    public final static String MIGRATION_ISSUE = "MIGRATION_ISSUE";
    public final static String MAIL_RESPONSE = "MAIL_RESPONSE";
    public final static String HIDE_PERSONAL_DATA = "HIDE_PERSONAL_DATA";
    public final static String HIDE_RESPONSE = "HIDE_RESPONSE";
    public final static String APPEAL_FORWARDED = "APPEAL_FORWARDED";

    // Действия над обращениями
    public final static String ACTION_ACCEPT = "ACCEPT";   //Взять в работу
    public final static String ACTION_ACCEPT_AND_HIDE = "ACCEPT_AND_HIDE";    //Взять в работу и скрыть
    public final static String ACTION_FORWARD = "FORWARD";    //Перенаправить в другое ведомство
    public final static String ACTION_CHANGE_SUBJECT = "CHANGE_SUBJECT";    //Сменить тематику
    public final static String ACTION_REJECT = "REJECT";    //Отклонить
    public final static String ACTION_RESPOND = "RESPOND";    //Ответить
    public final static String ACTION_PROLONG = "PROLONG";    //Продлить срок обращения
    public final static String ACTION_MARK_MIGRATION = "MARK_MIGRATION";    //Пометить как относящееся к вопросам миграции
    public final static String ACTION_HIDE = "HIDE"; //Скрыть
    public final static String ACTION_HIDE_ANSWER = "HIDE_ANSWER";    //Скрыть ответ
    public final static String ACTION_UNHIDE = "UNHIDE";    //Показать обращение
    public final static String ACTION_UNHIDE_ANSWER = "UNHIDE_ANSWER";    //Показать ответ на обращение
    public final static String ACTION_HIDE_BY_OPERATOR = "HIDE_BY_OPERATOR"; //Скрыть от имени оператора
    public final static String ACTION_HIDE_ANSWER_BY_OPERATOR = "HIDE_ANSWER_BY_OPERATOR";    //скрыть ответ
    public final static String ACTION_LIKE = "LIKE"; //Поддержать обращение
    public final static String ACTION_DISLIKE = "DISLIKE";    //Отказаться от поддержки обращения
    public final static String ACTION_SUBSCRIBE = "SUBSCRIBE";    //Подписаться на рассылку по обращению
    // Орфографическая ошибка в названии действия. API менять не стали :(
    public final static String ACTION_UNSUBSCRIBE = "UNSUBSRIBE";    //Отписаться от рассылки на обращение
    public final static String ACTION_RATE_RESPONSE = "RATE_RESPONSE";    //Оценить ответ

    public static final Map<String, Boolean> MARKS;

    static {
        Map<String, Boolean> temp = new HashMap<>();
        temp.put(REG_DATE_VIOLATION, false);
        temp.put(RESPONSE_DATE_VIOLATION, false);
        temp.put(FORWARDING_DATE_VIOLATION, false);
        temp.put(REPLIED, false);
        temp.put(RATED, false);
        temp.put(PROLONGED, false);
        temp.put(APPEAL_HIDE, false);
        temp.put(MIGRATION_ISSUE, false);
        temp.put(MAIL_RESPONSE, false);
        temp.put(HIDE_PERSONAL_DATA, false);
        temp.put(HIDE_RESPONSE, false);
        temp.put(APPEAL_FORWARDED, false);
        MARKS = Collections.unmodifiableMap(temp);
    }

    // Поля обращения
    String mId = "";
    long mCreatingDate = -1;
    long mApplyingDate = -1;
    long mRegDate = -1;
    long mActualRegDate = -1;
    long mDecisionDate = -1;
    long mActualDecisionDate = -1;
    int mNumber = -1;
    int mSubscribersCount = 0;
    int mLikesCount = 0;
    String mStatus;
    boolean mIsSubscribed = false;
    boolean mIsLiked = false;
    Integer mResponseRating = null;

    Subject mSubject = new Subject();
    Executor mExecutor = new Executor();
    AppealClaim mAppealClaim = new AppealClaim();
    AppealResponse mResponse = null;
    AppealGrade mGrade = null;

    HashMap<String, Boolean> mMarks = new HashMap<>();
    ArrayList<AppealMessage> mMessages = null;
    ArrayList<AppealAction> mActions = new ArrayList<>();
    ArrayList<String> mSubscribers = null;
    ArrayList<String> mLikes = null;
    //---------------------------------------------------------------

    public String getStatusText() {
        switch (Status()) {
            case STATUS_DRAFT:
                return "На модерации";
            case STATUS_INWORK:
                return "В работе";
            case STATUS_MODERATION:
                return "На модерации";
            case STATUS_REJECTED:
                return "Отклонено";
            case STATUS_SOLVED:
                return "Решено";
        }
        return "Неизвестен";
    }

    public int getStatusColorId() {
        int status_color = R.color.colorGrayText;
        switch (Status()) {
            case Appeal.STATUS_DRAFT: status_color = R.color.colorModeration; break;
            case Appeal.STATUS_MODERATION: status_color = R.color.colorModeration; break;
            case Appeal.STATUS_INWORK: status_color = R.color.colorInTheWork; break;
            case Appeal.STATUS_REJECTED: status_color = R.color.colorBeenRejected; break;
            case Appeal.STATUS_SOLVED: status_color = R.color.colorCompleted; break;
        }
        return status_color;
    }

    /***
     * Преобразует объект в JSONObject
     */
    public JSONObject toJson() {
        JSONObject result = new JSONObject();

        result.putSafe("id", mId);
        result.putSafe("creatingDate", mCreatingDate);
        result.putSafe("applyingDate", mApplyingDate);
        result.putSafe("regRegDate", mRegDate);
        result.putSafe("actualRegDate", mActualRegDate);
        result.putSafe("decisionDate", mDecisionDate);
        result.putSafe("actualDecisionDate", mActualDecisionDate);
        result.putSafe("number", mNumber);
        result.putSafe("status", mStatus);
        result.putSafe("subscribesCount", mSubscribersCount);
        result.putSafe("likesCount", mLikesCount);
        result.putSafe("isLiked", mIsLiked);
        result.putSafe("isSubscribed", mIsSubscribed);
        result.putSafe("subject", mSubject != null ? mSubject.toJson() : null);
        result.putSafe("executor", mExecutor != null ? mExecutor.toJson() : null);
        result.putSafe("appeal", mAppealClaim != null ? mAppealClaim.toJson() : null);
        result.putSafe("response", mResponse != null ? mResponse.toJson() : null);
        result.putSafe("grade", mGrade != null ? mGrade.toJson() : null);

        if (mMarks != null) {
            JSONObject marks = new JSONObject();
            for (Map.Entry<String, Boolean> entry : mMarks.entrySet())
                marks.putSafe(entry.getKey(), entry.getValue());
            result.putSafe("marks", marks);
        }

        if (mMessages != null) {
            JSONArray messages = new JSONArray();
            for (AppealMessage message : mMessages) messages.put(message.toJson());
            result.putSafe("messages", messages);
        }

        if (mActions != null) {
            JSONArray actions = new JSONArray();
            for (AppealAction action : mActions) actions.put(action.toJson());
            result.putSafe("actions", actions);
        }

        return result;
    }

    public static Appeal fromJson(JSONObject source) {
        Appeal newAppeal = new Appeal();

        try {
            newAppeal.setIsLiked(source.getBoolean("isLiked", false))
                    .setIsSubscribed(source.getBoolean("isSubscribed", false))
                    .setId(source.getString("id"))
                    .setCreatingDate(source.getLong("creatingDate", -1L))
                    .setApplyingDate(source.getLong("applyingDate", -1L))
                    .setRegDate(source.getLong("regRegDate", -1L))
                    .setActualRegDate(source.getLong("actualRegDate", -1L))
                    .setDecisionDate(source.getLong("decisionDate", -1L))
                    .setActualDecisionDate(source.getLong("actualDecisionDate", -1L))
                    .setNumber(source.getInt("number", -1))
                    .setStatus(source.getString("status", STATUS_DRAFT))
                    .setSubscribersCount(source.getInt("subscribesCount", 0))
                    .setLikesCount(source.getInt("likesCount", 0));

            newAppeal.setSubject(Subject.fromJson(source.getJSONObject("subject")))
                    .setExecutor(Executor.fromJson(source.getJSONObject("executor")))
                    .setGrade(AppealGrade.fromJson(source.getJSONObject("grade", null)))
                    .setResponse(AppealResponse.fromJson(source.getJSONObject("response", null)))
                    .setResponseRating(source.optInteger("responseRating", null))
                    .setAppealClaim(AppealClaim.fromJson(source.getJSONObject("appeal")));

            newAppeal.setMarks(marksFromJson(source.getJSONObject("marks", null)))
                    .setActions(actionsFromJson(source.getJSONArray("actions", null)))
                    .setMessages(messagesFromJson(source.getJSONArray("messages", null)));

        } catch (Exception e) {
            Logger.Exception(e);
            return null;
        }

        return newAppeal;
    }

    private static ArrayList<AppealMessage> messagesFromJson(JSONArray messages) {
        if (messages == null) return null;
        ArrayList<AppealMessage> result = new ArrayList<>();
        for (int i = 0; i < messages.length(); i++) {
            try {
                JSONObject row = messages.getJSONObject(i);
                result.add(AppealMessage.fromJson(row));
            } catch (Exception e) {
                Logger.Exception(e);
            }
        }

        return result;
    }

    private static ArrayList<AppealAction> actionsFromJson(JSONArray actions) {
        if (actions == null) return null;
        ArrayList<AppealAction> result = new ArrayList<>();
        for (int i = 0; i < actions.length(); i++) {
            try {
                String row = actions.getString(i);
                result.add(AppealAction.fromJson(row));
            } catch (Exception e) {
                Logger.Exception(e);
            }
        }

        return result;
    }


    public String Id() {
        return mId;
    }

    public Appeal setId(String mId) {
        this.mId = mId;
        return this;
    }

    public long CreatingDate() {
        return mCreatingDate;
    }

    public Appeal setCreatingDate(long mCreatingDate) {
        this.mCreatingDate = mCreatingDate;
        return this;
    }

    public long ApplyingDate() {
        return mApplyingDate;
    }

    public Appeal setApplyingDate(long mApplyingDate) {
        this.mApplyingDate = mApplyingDate;
        return this;
    }

    public long RegDate() {
        return mRegDate;
    }

    public Appeal setRegDate(long mRegDate) {
        this.mRegDate = mRegDate;
        return this;
    }

    public long ActualRegDate() {
        return mActualRegDate;
    }

    public Appeal setActualRegDate(long mActualRegDate) {
        this.mActualRegDate = mActualRegDate;
        return this;
    }

    public long DecisionDate() {
        return mDecisionDate;
    }

    public Appeal setDecisionDate(long mDecisionDate) {
        this.mDecisionDate = mDecisionDate;
        return this;
    }

    public long ActualDecisionDate() {
        return mActualDecisionDate;
    }

    public Appeal setActualDecisionDate(long mActualDecisionDate) {
        this.mActualDecisionDate = mActualDecisionDate;
        return this;
    }

    public int Number() {
        return mNumber;
    }

    public Appeal setNumber(int mNumber) {
        this.mNumber = mNumber;
        return this;
    }

    public String Status() {
        return mStatus;
    }

    public Appeal setStatus(String mStatus) {
        this.mStatus = mStatus;
        return this;
    }

    public boolean isSubscribed() {
        return mIsSubscribed;
    }

    public Appeal setIsSubscribed(boolean mIsSubscribed) {
        this.mIsSubscribed = mIsSubscribed;
        return this;
    }

    public boolean isLiked() {
        return mIsLiked;
    }

    public Appeal setIsLiked(boolean mIsLiked) {
        this.mIsLiked = mIsLiked;
        return this;
    }

    public Subject Subject() {
        return mSubject;
    }

    public Appeal setSubject(Subject mSubject) {
        this.mSubject = mSubject;
        return this;
    }

    public Executor Executor() {
        return mExecutor;
    }

    public Appeal setExecutor(Executor mExecutor) {
        this.mExecutor = mExecutor;
        return this;
    }

    public AppealClaim AppealClaim() {
        return mAppealClaim;
    }

    public Appeal setAppealClaim(AppealClaim mAppealClaim) {
        this.mAppealClaim = mAppealClaim;
        return this;
    }

    public AppealResponse Response() {
        return mResponse;
    }

    public Appeal setResponse(AppealResponse mResponse) {
        this.mResponse = mResponse;
        return this;
    }

    public HashMap<String, Boolean> Marks() {
        return mMarks;
    }

    public Appeal setMarks(HashMap<String, Boolean> mMarks) {
        this.mMarks = mMarks;
        return this;
    }

    public ArrayList<AppealMessage> Messages() {
        return mMessages;
    }

    public Appeal setMessages(ArrayList<AppealMessage> mMessages) {
        this.mMessages = mMessages;
        return this;
    }

    public ArrayList<AppealAction> Actions() {
        return mActions;
    }

    public Appeal setActions(ArrayList<AppealAction> mActions) {
        this.mActions = mActions;
        return this;
    }

    public ArrayList<String> Subscribers() {
        return mSubscribers;
    }

    public Appeal setSubscribers(ArrayList<String> mSubscribers) {
        this.mSubscribers = mSubscribers;
        return this;
    }

    public ArrayList<String> Likes() {
        return mLikes;
    }

    public Appeal setLikes(ArrayList<String> mLikes) {
        this.mLikes = mLikes;
        return this;
    }

    public int SubscribersCount() {
        return mSubscribersCount;
    }

    public Appeal setSubscribersCount(int mSubscribersCount) {
        this.mSubscribersCount = mSubscribersCount;
        return this;
    }

    public int LikesCount() {
        return mLikesCount;
    }

    public Appeal setLikesCount(int mLikesCount) {
        this.mLikesCount = mLikesCount;
        return this;
    }

    public AppealGrade Grade() {
        return mGrade;
    }

    public Appeal setGrade(AppealGrade mGrade) {
        this.mGrade = mGrade;
        return this;
    }

    public Integer ResponseRating() {
        return mResponseRating;
    }

    public Appeal setResponseRating(Integer rating) {
        mResponseRating = rating;
        return this;
    }

    public String getUrl() {
        return FRONT_ADDRESS + "/appeal?id=" + Id();
    }

    /***
     * Если есть отметка PROLONGED возвращаем содержимое отметки
     *
     * @return содержимое отметки или false
     */
    public boolean IsAppealProlonged() {
        return mMarks.containsKey(PROLONGED) ? mMarks.get(PROLONGED) : false;
    }

    /***
     * Если есть отметка MAIL_RESPONSE возвращаем содержимое отметки
     *
     * @return содержимое отметки или false
     */
    public boolean IsGetMailResponse() {
        return mMarks.containsKey(MAIL_RESPONSE) ? mMarks.get(MAIL_RESPONSE) : false;
    }

    /***
     * Если есть отметка HIDE_RESPONSE возвращаем !содержимое отметки
     *
     * @return инвертированное содержимое отметки или true
     */
    public boolean IsPublishResponse() {
        return !mMarks.containsKey(HIDE_RESPONSE) || !mMarks.get(HIDE_RESPONSE);
    }

    /***
     * Если есть отметка REPLIED возвращаем содержимое отметки
     *
     * @return содержимое отметки или false
     */
    public boolean IsHasResponse() {
        return mMarks.containsKey(REPLIED) ? mMarks.get(REPLIED) : false;
    }

    /***
     * Если есть отметка APPEAL_HIDE возвращаем содержимое отметки
     *
     * @return содержимое отметки или false
     */
    public boolean IsPublishAppeal() {
        return !mMarks.containsKey(APPEAL_HIDE) || !mMarks.get(APPEAL_HIDE);
    }

    /***
     * Если есть отметка APPEAL_FORWARDED возвращаем содержимое отметки
     *
     * @return содержимое отметки или false
     */
    public boolean IsForwarded() {
        return mMarks.containsKey(APPEAL_FORWARDED) ? mMarks.get(APPEAL_FORWARDED) : false;
    }

    private static HashMap<String, Boolean> marksFromJson(JSONObject marks) {
        HashMap<String, Boolean> result = new HashMap<>();
        if (marks != null) {
            for (Iterator<String> iterator = marks.keys(); iterator.hasNext(); ) {
                String key = iterator.next();
                try {
                    boolean markVal = marks.getBoolean(key);
                    if (MARKS.containsKey(key)) {
                        result.put(key, markVal);
                    } else {
                        Logger.Warn("WARNING: Unknown Appeal MARK from server (" + key + ")");
                    }
                } catch (Exception e) {
                    Logger.Exception(e);
                }
            }
        }

        return result;
    }

    /***
     * Проверяет дозволяется ли по данному обращению выполнить заданное действие текущему пользователю
     */
    public boolean isAllowAction(String actionHideAnswer) {
        if (mActions == null) return false;
        for (AppealAction action : mActions)
            if (action.ActionName().equals(actionHideAnswer)) return true;
        return false;
    }
}
