package ru.infodev.golos27.Fragments.AppealClaim;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.List;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.NewAppeal;
import ru.infodev.golos27.Common.Catalogs.Subject;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.Adapters.SubjectsAdapter;
import ru.infodev.golos27.Widgets.Lists.NestedListView;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;

public class SetSubjectFragment extends ParamFragment {
    public static final int FRAGMENT_ID = R.layout.set_subject;
    private String categoryId = "unknown";
    private SubjectsAdapter adapter;
    private boolean notChain = false;

    public SetSubjectFragment() {
        titleId = R.string.empty_string;
        setFragmentAllowMenu(false);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        setRetainInstance(true);

        notChain = parameters.getBoolean(ParamFragment.NOT_CHAIN, false);
        NestedListView listView = (NestedListView) v.findViewById(R.id.main_list);
        String categoryName = parameters.getString("categoryName", "");
        if (!isEmpty(categoryName)) {
            setTitle(categoryName);
            categoryId = parameters.getString("categoryId", "unknown");
        }

        List<Subject> rootItems = Subject.GetSubjectsByParent(categoryId);
        List<Subject> allItems = new ArrayList<>();
        for (Subject subject : rootItems) {
            allItems.add(subject);
            subject.setGroupList(Subject.GetSubjectsByParent(subject.Id()));
            for (Subject subItem : subject.getGroupList()) {
                allItems.add(subItem);
            }
        }

        adapter = new SubjectsAdapter(Application.AppContext, allItems);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Subject item = adapter.getItem(position);
                JSONObject param = new JSONObject();
                NewAppeal.setSubject(item);

                if (notChain) {
                    chainPopFragments(FRAGMENT_ID, 2, false);
                } else {
                    chainStartNextFragment(FRAGMENT_ID, SetMapLocationFragment.class, param, false);
                }
            }
        });

        return v;
    }
}

