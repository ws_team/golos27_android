package ru.infodev.golos27.Widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Pattern;

import ru.infodev.golos27.Common.Utils.PhoneUtils;
import ru.infodev.golos27.R;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;

import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

/**
 * To change clear icon, set
 * <p/>
 * <pre>
 * android:drawableRight="@drawable/custom_icon"
 * </pre>
 */
public class CustomEditText extends AppCompatEditText
        implements OnTouchListener, OnFocusChangeListener, TextWatcherAdapter.TextWatcherListener {

    int resource_blue;
    int resource_gray;
    int resource_red;

    private OnTouchListener touchListener;
    private OnFocusChangeListener focusChangeListener;
    private TextWatcherAdapter.TextWatcherListener textChangeListener;
    private static final int[] STATE_MISTAKE = {R.attr.state_mistake};
    private static final int[] STATE_EMPTY = {R.attr.state_empty};
    private boolean mIsEmpty = true;
    private Context contextVal;

    private boolean format_phone = false;

    @SuppressWarnings("unused")
    public void setFormat_phone(boolean format_phone) {
        this.format_phone = format_phone;
    }

    @SuppressWarnings("unused")
    public void setEmpty(boolean isEmpty) {
        mIsEmpty = isEmpty;
        refreshDrawableState();
    }

    private boolean mIsMistake = false;

    public void setMistake(boolean isMistake) {
        mIsMistake = isMistake;
        refreshDrawableState();
    }

    private int empty_warning_id;

    @SuppressWarnings("unused")
    public int getEmpty_warning_id() {
        return empty_warning_id;
    }

    private int upper_name_id;

    @SuppressWarnings("unused")
    public int getUpper_name_id() {
        return upper_name_id;
    }

    private int bottom_help_id;

    @SuppressWarnings("unused")
    public int getBottom_help_id() {
        return bottom_help_id;
    }

    public interface Listener {
        void didClearText();
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);

        if (mIsMistake) {
            mergeDrawableStates(drawableState, STATE_MISTAKE);
        }

        if (bottom_help_view != null) {
            if (mIsMistake) {
                ((TextView) bottom_help_view).setTextColor(resource_red);
            } else {
                ((TextView) bottom_help_view).setTextColor(resource_gray);
            }
        }

        if (upper_name_view != null) {
            int upper_name_color = resource_gray;

            if (mIsMistake) {
                upper_name_color = resource_red;
            } else {
                if (isFocused()) {
                    upper_name_color = resource_blue;
                }
            }

            ((TextView) upper_name_view).setTextColor(upper_name_color);
        }

        if (mIsEmpty) {
            mergeDrawableStates(drawableState, STATE_EMPTY);
        }
        return drawableState;
    }

    @SuppressWarnings("unused")
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private Drawable xD;
    private Listener listener;

    public CustomEditText(Context context) {
        super(context);
        init(context, null);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        this.touchListener = l;
    }

    public void setOnTextChangedListener(TextWatcherAdapter.TextWatcherListener l) {
        this.textChangeListener = l;
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener f) {
        this.focusChangeListener = f;
    }

    @Override
    public void onSelectionChanged(int start, int end) {
        if (format_phone) {
            CharSequence text = getText();
            if (text != null) {
                if (start != text.length() || end != text.length()) {
                    setSelection(text.length());
                    return;
                }
            }
        }
        super.onSelectionChanged(start, end);
    }

    private boolean mFormatting;

    @Override
    public void onTextChanged(EditText view, String text) {
        if ((format_phone) && text.equals(PhoneUtils.getNumberPrefix())) {
            if (!hasFocus()) {
                text = "";
                if (!mFormatting) {
                    mFormatting = true;
                    view.setText("");
                    mFormatting = false;
                }
            }
            setClearIconVisible(false);
        } else {
            if (hasFocus()) {
                setClearIconVisible(isNotEmpty(text));
            } else {
                setClearIconVisible(false);
            }
        }
        if ((upper_name_view != null) && (text.length() > 0))
            upper_name_view.setVisibility(VISIBLE);
        if (textChangeListener != null) {
            textChangeListener.onTextChanged(view, text);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        if (textChangeListener != null) {
            textChangeListener.beforeTextChanged(s, start, count, after);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (textChangeListener != null) {
            textChangeListener.afterTextChanged(s);
        }

        if (format_phone) {
            if (!mFormatting) {
                mFormatting = true;
                if (s.length() > 0) PhoneUtils.formatNumber(s);
                mFormatting = false;
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (getCompoundDrawables()[2] != null) {
            int right = getWidth();
            int left = right - (xD.getIntrinsicWidth() * 2) - getPaddingRight();
            int bottom = getHeight();
            int top = 0;
            boolean tappedX = new Rect(left, top, right, bottom).contains((int) event.getX(), (int) event.getY());
            if (tappedX) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (format_phone) {
                        setText(PhoneUtils.getNumberPrefix());
                    } else {
                        setText("");
                    }

                    setClearIconVisible(false);
                    requestFocus();
                    if (listener != null) {
                        listener.didClearText();
                    }
                }
                return true;
            }
        }

        return touchListener != null && touchListener.onTouch(v, event);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (focusChangeListener != null) {
            focusChangeListener.onFocusChange(v, hasFocus);
        }

        if (format_phone) {
            String prefix = PhoneUtils.getNumberPrefix();
            if (hasFocus) {
                if (getText().length() <= 0) setText(prefix);
            } else {
                if (getText().toString().equals(prefix)) setText("");
            }
            setSelection(getText().length());
        }

        // Теперь обработаем видимость названия поля (если поле пустое - прятать)
        findSatellites();
        hide_label_by_focus(v, hasFocus);
        if ((hide_bottom_help) && (bottom_help_view != null)) {
            if (hasFocus) {
                bottom_help_view.setVisibility(VISIBLE);
                setClearIconVisible(isNotEmpty(getText().toString()));
            } else {
                bottom_help_view.setVisibility(INVISIBLE);
                setClearIconVisible(false);
            }
        }

        if (hasFocus) {
            setClearIconVisible(isNotEmpty(getText().toString()));
        } else {
            setClearIconVisible(false);
        }
    }

    private void hide_label_by_focus(View v, boolean hasFocus) {
        if (upper_name_view != null) {
            if (hasFocus) {
                upper_name_view.setVisibility(VISIBLE);
            } else {
                String text = ((EditText) v).getText().toString();
                if (text.isEmpty()) {
                    upper_name_view.setVisibility(INVISIBLE);
                } else {
                    upper_name_view.setVisibility(VISIBLE);
                }
            }
        }
    }

    private void init(Context context, AttributeSet attrs) {

        contextVal = context;
        resource_blue = ContextCompat.getColor(contextVal, R.color.colorPrimary);
        resource_gray = ContextCompat.getColor(contextVal, R.color.colorGrayText);
        resource_red = ContextCompat.getColor(contextVal, R.color.colorErrorText);

        empty_warning_id = -1;
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText, 0, 0);
            try {
                empty_warning_id = a.getResourceId(R.styleable.CustomEditText_empty_warning_id, 0);
                upper_name_id = a.getResourceId(R.styleable.CustomEditText_upper_name_id, 0);
                bottom_help_id = a.getResourceId(R.styleable.CustomEditText_bottom_help_id, 0);
                hide_bottom_help = a.getBoolean(R.styleable.CustomEditText_hide_bottom_help, false);
                format_phone = a.getBoolean(R.styleable.CustomEditText_format_phone, false);
            } finally {
                a.recycle();
            }
        }

        xD = getCompoundDrawables()[2];
        if (xD == null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                xD = context.getResources().getDrawable(R.drawable.text_clear, null);
            } else {
                //noinspection deprecation
                xD = context.getResources().getDrawable(R.drawable.text_clear);
            }
        }
        assert xD != null;
        xD.setBounds(0, 0, xD.getIntrinsicWidth(), xD.getIntrinsicHeight());
        setClearIconVisible(false);
        super.setOnTouchListener(this);
        super.setOnFocusChangeListener(this);
        addTextChangedListener(new TextWatcherAdapter(this, this));
        findSatellites();
        if ((empty_warning_view != null) && (getText().length() > 0)) {
            empty_warning_view.setVisibility(INVISIBLE);
        }
    }


    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (!enabled) {
            setCompoundDrawables(getCompoundDrawables()[0],
                    getCompoundDrawables()[1], null, getCompoundDrawables()[3]);
        }
    }

    protected void setClearIconVisible(boolean visible) {
        boolean wasVisible = (getCompoundDrawables()[2] != null);
        if (visible != wasVisible) {
            Drawable x = visible ? xD : null;
            if (!isEnabled()) x = null;
            setCompoundDrawables(getCompoundDrawables()[0],
                    getCompoundDrawables()[1], x, getCompoundDrawables()[3]);

        }
        // Проверка на предмет дополнительных связанных текстовых полей
        findSatellites();

        // Обновляем связанное поле с предупреждением
        if (empty_warning_view != null) {
            if (visible) {
                empty_warning_view.setVisibility(INVISIBLE);
            } else {
                if (getText().length() <= 0) {
                    empty_warning_view.setVisibility(VISIBLE);
                } else {
                    empty_warning_view.setVisibility(INVISIBLE);
                }
            }
        }
    }

    View empty_warning_view = null;
    View upper_name_view = null;
    View bottom_help_view = null;
    boolean hide_bottom_help = false;

    protected void findSatellites() {
        // Поиск текстового поля с предупреждением
        if (empty_warning_view == null) {
            try {
                if (empty_warning_id >= 0) {
                    View parent = (View) getParent();
                    if (parent != null) {
                        empty_warning_view = parent.findViewById(empty_warning_id);
                    }
                }
            } catch (Exception e) {
                //
            }
        }

        // Поиск текстового поля с предупреждением
        if (upper_name_view == null) {
            try {
                if (upper_name_id >= 0) {
                    View parent = (View) getParent();
                    if (parent != null) {
                        View p_parent = (View) parent.getParent();
                        if (p_parent != null) {
                            upper_name_view = p_parent.findViewById(upper_name_id);
                        }
                    }
                }
            } catch (Exception e) {
                //
            }
        }

        // Поиск нижней подсказки
        if (bottom_help_view == null) {
            try {
                if (bottom_help_id >= 0) {
                    View parent = (View) getParent();
                    if (parent != null) {
                        View p_parent = (View) parent.getParent();
                        if (p_parent != null) {
                            bottom_help_view = p_parent.findViewById(bottom_help_id);
                        }
                    }
                }
            } catch (Exception e) {
                //
            }
        }
    }

    /***
     * Проверяет поле с помощью прилагаемого регулярного выражения
     *
     * @param pattern         шаблон регулярного выражения для проверки
     * @param checkEmpty      проверять поле на пустое содержимое
     * @param emptySetMistake устанавливать ошибку если поле пустое
     * @return true если поле валидное
     */
    public boolean checkValue(Pattern pattern, boolean checkEmpty, boolean emptySetMistake) {
        return checkValue(pattern, checkEmpty, emptySetMistake, -1, -1);
    }

    /***
     * Проверяет поле с помощью прилагаемого регулярного выражения
     *
     * @param pattern         шаблон регулярного выражения для проверки
     * @param checkEmpty      проверять поле на пустое содержимое
     * @param emptySetMistake устанавливать ошибку если поле пустое
     * @param min_length      минимальная длина введенной строки
     * @param max_length      максимальная длина введенной строки
     * @return true если поле валидное
     */
    public boolean checkValue(Pattern pattern, boolean checkEmpty, boolean emptySetMistake, int min_length, int max_length) {
        boolean result = true;
        boolean mistake = false;

        if (!pattern.matcher(getText()).matches()) {
            result = false;
            mistake = true;
        }

        if ((min_length > 0) && (getText().toString().length() < min_length)) {
            mistake = true;
            result = false;
        }

        if ((max_length > 0) && (getText().toString().length() > max_length)) {
            mistake = true;
            result = false;
        }

        if (getText().toString().equals("")) {
            result = !checkEmpty;
            mistake = emptySetMistake;
        }

        setMistake(mistake);
        return result;
    }
}
