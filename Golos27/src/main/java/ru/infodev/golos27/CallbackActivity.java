package ru.infodev.golos27;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.RestApi.PushNotifications;

public class CallbackActivity extends AppCompatActivity {

    public static final String ESIA_LOGIN_SUCCESS = "ru.infodev.golos27.ESIA_LOGIN_SUCCESS";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
    public static final String EXPIRES_IN = "EXPIRES_IN";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_callback);
        handleUri(getIntent().getData());
    }

    private void handleUri(Uri uri) {
        switch (uri.getHost()) {
            case "esia":
                switch (uri.getPathSegments().get(0)) {
                    case "login":
                        switch(uri.getPathSegments().get(1)) {
                            case "success":
                                Settings.setParameter(Settings.USER_ACCESS_TOKEN, uri.getQueryParameter("access_token"));
                                Settings.setParameter(Settings.USER_REFRESH_TOKEN, uri.getQueryParameter("refresh_token"));
                                Settings.setParameter(Settings.USER_TOKEN_EXPIRES_IN, uri.getQueryParameter("expires_in"));
                                Settings.setParameter(Settings.USER_TOKEN_START_TIME, String.valueOf(System.currentTimeMillis()));
                                PushNotifications.Subscribe(true);
                                finish();
                                break;
                            case "failure":
                                Toast.makeText(this, R.string.auth_fail, Toast.LENGTH_LONG).show();
                                finish();
                                break;
                            default: finish();
                        }
                        break;
                    case "logout":
                        finish();
                        break;
                    default: finish();
                }
                break;
            case "auth":
                switch (uri.getPathSegments().get(0)) {
                    case "fail":
                        Toast.makeText(this, R.string.auth_fail, Toast.LENGTH_LONG).show();
                        finish();
                        break;
                    default: finish();
                }
                break;
            default: finish();
        }
    }
}
