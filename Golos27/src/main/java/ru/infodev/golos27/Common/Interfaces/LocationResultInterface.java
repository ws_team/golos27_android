package ru.infodev.golos27.Common.Interfaces;

public interface LocationResultInterface {
    void OnLocationReceived(String region, String area, String settlement, String address, double latitude, double longitude, int iZoomLevel);
    void OnPageLoaded();
    void OnGPSError(int code);
}
