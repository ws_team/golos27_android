package ru.infodev.golos27.Fragments.Profile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.regex.Pattern;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.CustomEditText;


public class ChangePasswordFragment extends ParamFragment implements
        OnClickListener,
        TextWatcherAdapter.TextWatcherListener {

    public static final int FRAGMENT_ID = R.layout.profile_change_password;
    Pattern patternFullPass = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,}$");

    private boolean FirstPassGood = false;
    private int colorBlack;
    private int colorGray;

    private ImageView az;
    private ImageView abc;
    private ImageView ABC1;
    private ImageView limit8;
    private ImageView num09;

    private boolean p1len = false;
    private boolean p1abcOK = false;
    private boolean p1ABCOK = false;
    private boolean p109OK = false;
    private boolean p1lim8OK = false;
    private boolean p1azOK = false;
    private boolean p1sameOK = false;

    private TextView txtabc;
    private TextView txtaz;
    private TextView txtABC;
    private TextView txtnum09;
    private TextView txtlim8;

    private String saved_p1 = "";
    private String saved_p2 = "";
    private boolean passwords_match = false;
    private Button changeButton;
    private CustomEditText passwordEditText_1;
    private CustomEditText passwordEditText_2;
    private CustomEditText currentPasswordEdit;
    private boolean showPassword_1 = false;
    private TextView showPasswordLabel;
    private TextView secondHelpText;
    private TextView firstHelpText;
    private ProgressBar progress;
    private String passwords_not_match_error = "";
    private String passwords_same_error = "";
    private String saved_current = "";

    public ChangePasswordFragment() {
        titleId = R.string.change_password;
        setFragmentAllowMenu(false);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        setTitle(titleId);

        colorBlack = ContextCompat.getColor(Application.AppContext, R.color.colorBlackText);
        colorGray = ContextCompat.getColor(Application.AppContext, R.color.colorGrayText);

        firstHelpText = (TextView) v.findViewById(R.id.help_text_one_password);
        secondHelpText = (TextView) v.findViewById(R.id.help_text_repeat_password);
        passwordEditText_2 = (CustomEditText) v.findViewById(R.id.edit_repeat_password);
        passwordEditText_2.setOnTextChangedListener(this);
        passwordEditText_1 = (CustomEditText) v.findViewById(R.id.edit_one_password);
        passwordEditText_1.setOnTextChangedListener(this);
        currentPasswordEdit = (CustomEditText) v.findViewById(R.id.current_password);
        currentPasswordEdit.setOnTextChangedListener(this);

        passwordEditText_1.setTypeface(secondHelpText.getTypeface());
        passwordEditText_2.setTypeface(secondHelpText.getTypeface());
        currentPasswordEdit.setTypeface(secondHelpText.getTypeface());

        showPasswordLabel = (TextView) v.findViewById(R.id.show_password_1);
        showPasswordLabel.setOnClickListener(this);

        az = (ImageView) v.findViewById(R.id.az_1);
        abc = (ImageView) v.findViewById(R.id.abc_1);
        ABC1 = (ImageView) v.findViewById(R.id.ABC);
        limit8 = (ImageView) v.findViewById(R.id.limit8ok1);
        num09 = (ImageView) v.findViewById(R.id.num_0_9);
        txtnum09 =(TextView) v.findViewById(R.id.txtnum09);
        txtaz =(TextView) v.findViewById(R.id.txtaz);
        txtabc =(TextView) v.findViewById(R.id.txtabc);
        txtABC =(TextView) v.findViewById(R.id.txtABC);
        txtlim8 =(TextView) v.findViewById(R.id.limit8);

        progress = (ProgressBar) v.findViewById(R.id.progressBar);
        changeButton = (Button) v.findViewById(R.id.change_password);
        changeButton.setOnClickListener(this);

        passwords_not_match_error = getResources().getString(R.string.password_do_not_match);
        passwords_same_error = getResources().getString(R.string.passwords_same_error);

        return v;
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        CustomEditText editBox = (CustomEditText) view;
        switch (view.getId()) {
            case R.id.current_password:
                saved_current = text;
                break;

            case R.id.edit_one_password:
                saved_p1 = text;
                passwords_match = saved_p2.equals(text);
                FirstPassGood = editBox.checkValue(patternFullPass, true, false);
                p1len = text.length() > 0;
                p1ABCOK = text.matches(".*[A-Z].*");
                p1abcOK = text.matches(".*[a-z].*");
                p109OK = text.matches(".*[0-9].*");
                p1lim8OK = text.length() >= 8;
                String noDigitText = text.replaceAll("\\d","");
                p1azOK = noDigitText.matches(".*[a-zA-Z]+.*");

                if (saved_current.equals(saved_p1)) {
                    firstHelpText.setText(passwords_same_error);
                    passwordEditText_1.setMistake(true);
                    p1sameOK = false;
                    FirstPassGood = false;
                } else {
                    firstHelpText.setText("");
                    passwordEditText_1.setMistake(false);
                    p1sameOK = true;
                }

                if (passwords_match || saved_p2.length() <= 0) {
                    secondHelpText.setText("");
                    passwordEditText_2.setMistake(false);
                } else {
                    passwordEditText_2.setMistake(true);
                    secondHelpText.setText(passwords_not_match_error);
                }
                break;

            case R.id.edit_repeat_password:
                saved_p2 = text;
                if (saved_p1 != null) {
                    passwords_match = saved_p1.equals(text);
                    if (passwords_match || text.length() <= 0) {
                        secondHelpText.setText("");
                        editBox.setMistake(false);
                    } else {
                        editBox.setMistake(true);
                        secondHelpText.setText(passwords_not_match_error);
                    }
                } else {
                    editBox.setMistake(true);
                    secondHelpText.setVisibility(View.INVISIBLE);
                }
                break;
        }

        if (p1len) {
            showView(ABC1,txtABC, p1ABCOK);
            showView(abc,txtabc, p1abcOK);
            showView(num09,txtnum09, p109OK);
            showView(az,txtaz, p1azOK);
            showView(limit8,txtlim8, p1lim8OK);
        } else {
            hideAllImages();
        }

        changeButton.setEnabled(FirstPassGood && passwords_match);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // pass
    }

    @Override
    public void afterTextChanged(Editable s) {
        // pass
    }


    public void hideAllImages() {
        showView(ABC1, txtABC, false);
        showView(abc, txtabc, false);
        showView(num09,txtnum09, false);
        showView(az,txtaz, false);
        showView(limit8,txtlim8, false);
        // здесь всем текстам черный цвет задать
        txtnum09.setTextColor(colorBlack);
        txtlim8.setTextColor(colorBlack);
        txtaz.setTextColor(colorBlack);
        txtabc.setTextColor(colorBlack);
        txtABC.setTextColor(colorBlack);

    }


    public void showView(View view, TextView text, boolean show) {
        if (show) {
            view.setVisibility(View.VISIBLE);
            // сюда цвет серый
            text.setTextColor(colorGray);
        } else {
            view.setVisibility(View.INVISIBLE);
            // сюда черный
            text.setTextColor(colorBlack);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.show_password_1:
                if (showPassword_1) {
                    showPassword_1 = false;
                    String label_text = getResources().getString(R.string.show_password);
                    showPasswordLabel.setText(label_text);
                    passwordEditText_1.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwordEditText_1.setSelection(passwordEditText_1.getText().length());
                    passwordEditText_2.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwordEditText_2.setSelection(passwordEditText_2.getText().length());
                    currentPasswordEdit.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    currentPasswordEdit.setSelection(currentPasswordEdit.getText().length());
                } else {
                    showPassword_1 = true;
                    String label_text = getResources().getString(R.string.hide_password);
                    showPasswordLabel.setText(label_text);
                    passwordEditText_1.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwordEditText_1.setSelection(passwordEditText_1.getText().length());
                    passwordEditText_2.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwordEditText_2.setSelection(passwordEditText_2.getText().length());
                    currentPasswordEdit.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    currentPasswordEdit.setSelection(currentPasswordEdit.getText().length());
                }
                break;

            case R.id.change_password:
                String password = passwordEditText_2.getText().toString();
                String oldPassword = currentPasswordEdit.getText().toString();
                if (password.equals(oldPassword)) {
                    ResultDialog.makeDialog(getActivity(), R.drawable.error_grey,
                            "Ошибка изменения пароля", getString(R.string.err_new_password_eq_old), "OK")
                            .show(getActivity());
                } else {
                    new RegistrationCompleteTask().execute(oldPassword, password);
                }
                break;
        }
    }

    /***
     * Отправка кода подтверждения на сервер, при неудаче - диалоговое окно
     * при успехе переход на другой фрагмент
     */
    class RegistrationCompleteTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            changeButton.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.VISIBLE);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            return server.changePassword(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            progress.setVisibility(View.INVISIBLE);
            changeButton.setVisibility(View.VISIBLE);
            changeButton.setEnabled(false);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    changeButton.setEnabled(true);
                    fragmentResultListener.FragmentSendResult(FRAGMENT_ID, 0, result);
                } else {
                    ResultDialog.showError(getActivity(), "Ошибка смены пароля", errorCode, result.getErrorText());
                }
            } catch (Exception e) {
                Logger.Exception(e);
            }
            changeButton.setEnabled(true);
        }
    }
}