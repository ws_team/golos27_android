package ru.infodev.golos27.Common;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.R;

public class DBHelper extends SQLiteOpenHelper {

    private final static int DB_VERSION = 16;

    Context context;

    public DBHelper(Context context) {
        super(context, context.getResources().getString(R.string.app_name), null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        CreateDatabaseTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion == newVersion) return;

        //since 15th version OKTMO's types (level) been changed
        if(oldVersion < 15 && newVersion >= 15) {
            dropAndCreateOKTMOTable(db);
            return;
        }

        Logger.Exception(new Exception("Old database format. Dropping database"));
        dropEntireDatabase(db);
        CreateDatabaseTables(db);
    }

    private void CreateDatabaseTables(SQLiteDatabase db) {
        Logger.Log("Creating new database (first run ?)");

        // Настройки приложения
        db.execSQL("CREATE TABLE \"" + context.getString(R.string.settings_table_name) + "\" " +
                "(\"name\" varchar(64) NOT NULL, " +
                "\"value\" varchar(64) NOT NULL, " +
                "UNIQUE (name) ON CONFLICT REPLACE" +
                ");\n");

        // Список населенных пунктов
        db.execSQL("CREATE TABLE \"" + context.getString(R.string.oktmos_table_name) + "\" " +
                "(\"guid\" varchar(64) NOT NULL, " +
                "\"code\" varchar(64) NOT NULL, " +
                "\"name\" varchar(255) NOT NULL, " +
                "\"type\" varchar(128) NOT NULL, " +
                "\"area\" varchar(64) NOT NULL, " +
                "\"region\" varchar(64) NOT NULL, " +
                "\"level\" INT NOT NULL DEFAULT 0, " +
                "\"search\" varchar(255) NOT NULL, " +
                "UNIQUE (code) ON CONFLICT REPLACE" +
                ");\n");

        // Категории и тематики обращений
        db.execSQL("CREATE TABLE \"" + context.getString(R.string.subjects_table_name) + "\" " +
                "(\"guid\" varchar(64) NOT NULL, " +
                "\"name\" varchar(255) NOT NULL, " +
                "\"parent\" varchar(128) NOT NULL, " +
                "\"resource\" varchar(64) NOT NULL, " +
                "\"version\" INT NOT NULL DEFAULT 0, " +
                "\"priority\" INT DEFAULT 0, " +
                "UNIQUE (guid) ON CONFLICT REPLACE" +
                ");\n");

        // Исполнители по обращениям
        db.execSQL("CREATE TABLE \"" + context.getString(R.string.executors_table_name) + "\" " +
                "(\"id\" varchar(64) NOT NULL, " +
                "\"name\" varchar(512) NOT NULL, " +
                "\"code\" varchar(128) NOT NULL, " +
                "\"categoryId\" varchar(128) NOT NULL, " +
                "\"version\" INT NOT NULL DEFAULT 0, " +
                "\"search\" varchar(512) NOT NULL, " +
                "UNIQUE (id) ON CONFLICT REPLACE" +
                ");\n");

        // Неотправленное обращение (отправка в offline)
        db.execSQL("CREATE TABLE \"" + context.getString(R.string.appeals_table_name) + "\" " +
                "(\"id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                "\"date\" INT NOT NULL DEFAULT 0, " +
                "\"tries\" INT NOT NULL DEFAULT 0, " +
                "\"content\" varchar NOT NULL, " +
                "UNIQUE (id) ON CONFLICT REPLACE" +
                ");\n");
    }

    private void dropEntireDatabase(SQLiteDatabase db) {
        try {
            db.execSQL("DROP TABLE " + context.getResources().getString(R.string.settings_table_name));
        } catch (Exception e) {
            Logger.Exception(e);
        }

        try {
            db.execSQL("DROP TABLE " + context.getResources().getString(R.string.oktmos_table_name));
        } catch (Exception e) {
            Logger.Exception(e);
        }

        try {
            db.execSQL("DROP TABLE " + context.getResources().getString(R.string.subjects_table_name));
        } catch (Exception e) {
            Logger.Exception(e);
        }

        try {
            db.execSQL("DROP TABLE " + context.getResources().getString(R.string.executors_table_name));
        } catch (Exception e) {
            Logger.Exception(e);
        }

        try {
            db.execSQL("DROP TABLE " + context.getResources().getString(R.string.appeals_table_name));
        } catch (Exception e) {
            Logger.Exception(e);
        }
    }

    private void dropAndCreateOKTMOTable(SQLiteDatabase db) {
        try {
            db.execSQL("DROP TABLE " + context.getResources().getString(R.string.oktmos_table_name));
        } catch (Exception e) {
            Logger.Exception(e);
        }
        db.execSQL("CREATE TABLE \"" + context.getString(R.string.oktmos_table_name) + "\" " +
                "(\"guid\" varchar(64) NOT NULL, " +
                "\"code\" varchar(64) NOT NULL, " +
                "\"name\" varchar(255) NOT NULL, " +
                "\"type\" varchar(128) NOT NULL, " +
                "\"area\" varchar(64) NOT NULL, " +
                "\"region\" varchar(64) NOT NULL, " +
                "\"level\" INT NOT NULL DEFAULT 0, " +
                "\"search\" varchar(255) NOT NULL, " +
                "UNIQUE (code) ON CONFLICT REPLACE" +
                ");\n");
    }
}