package ru.infodev.golos27.Common;

import android.os.AsyncTask;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import ru.infodev.golos27.Common.Catalogs.Appeal.AppealAttachment;
import ru.infodev.golos27.Common.Catalogs.Appeal.AppealClaim;
import ru.infodev.golos27.Common.Catalogs.Executor;
import ru.infodev.golos27.Common.Catalogs.OKTMO;
import ru.infodev.golos27.Common.Catalogs.Subject;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

public class OfflineAppealSender {
    public static void executeJobs() {
        List<AppealClaim> savedAppealClaims = AppealClaim.getSavedAppealClaims();
        if (savedAppealClaims == null) {
            Logger.Log("Got null saved appeal claims list!");
            return;
        }

        for (AppealClaim entry : savedAppealClaims) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH);
            Logger.Log("-[saved claim " + String.valueOf(entry.getDatabaseId()) + "]---------------------------------");
            Logger.Log("Date added: " + sdf.format(entry.getDate()));
            Logger.Log("Try number: " + String.valueOf(entry.getTries()));
            Logger.Log("Subject: " + entry.Subject().toString());
            String text = isNotEmpty(entry.Message()) ? entry.Message() : "";
            Logger.Log("Text: " + text.substring(0, Math.min(text.length(), 50)));
            Logger.Log("Address: " + entry.Address().FullAddress());
            List<GalleryEntry> documents = entry.ImagesList();
            int docsSent = 0;
            for (GalleryEntry doc : documents) if (isNotEmpty(doc.getAttachmentId())) docsSent++;
            Logger.Log("Attachments (sent/total): " + String.valueOf(docsSent) + "/" + String.valueOf(documents.size()));
            Logger.Log("Fail reason: " + entry.getFailReason());
        }

        new OfflineSendAppealsTask(savedAppealClaims).execute();
    }

    static class OfflineSendAppealsTask extends AsyncTask<String, Void, Boolean> {
        private List<AppealClaim> appealClaims;

        public OfflineSendAppealsTask(List<AppealClaim> appealClaims) {
            this.appealClaims = appealClaims;
        }

        @Override
        protected final Boolean doInBackground(String[] params) {
            Boolean success = true;
            Boolean attachesOk;
            String lastErrorText = "";
            ServerTalk server = new ServerTalk();
            Logger.Log("Total appeals to send: " + String.valueOf(appealClaims.size()));

            for (AppealClaim appealClaim : appealClaims) {
                List<GalleryEntry> images = appealClaim.ImagesList();
                Long id = appealClaim.getDatabaseId();
                Logger.Log("Appeal to send #" + String.valueOf(id) + ", attaches=" + String.valueOf(images.size()));
                attachesOk = true;
                for (GalleryEntry doc : images) {
                    File file = new File(doc.ImagePath());
                    if (isEmpty(doc.getAttachmentId())) {
                        JSONObject result = server.uploadDocument(file);
                        if (result.has("errorCode") && !result.isNull("errorCode")) {
                            ErrorResult retVal = new ErrorResult(result.getInt("errorCode", 0), result.getString("errorText", ""));
                            if (retVal.getErrorCode() != ErrorResult.SUCCESS) {
                                Logger.Warn("Can't offline send appeal claim attachment: " + file.toString());
                                success = false;
                                attachesOk = false;
                                lastErrorText = result.getString("errorText", "");
                                break;
                            } else {

                                AppealAttachment attachment = new AppealAttachment();
                                attachment.setId(result.getString("id", null));
                                attachment.setIndex(result.getInt("index", -1));
                                attachment.setLength(result.getInt("length", -1));
                                attachment.setMimeType(result.getString("mimeType", null));
                                attachment.setName(result.getString("name", null));
                                appealClaim.AddDocument(attachment);
                                doc.setAttachment(attachment);
                                Logger.Log("Attachment saved to server: " + file.toString());
                                if (appealClaim.DocumentCoverIdPlain() == null)
                                    appealClaim.setDocumentCoverId(attachment.Id());
                            }
                        }
                    }
                }

                if (attachesOk) {
                    if (appealClaim.Executor() == null || isEmpty(appealClaim.Executor().Id())) {
                        // Если обращение создавали при отсутствии сети, и не выбрали исполнителя вручную,
                        // то надо получить исполнителя по умолчанию
                        OKTMO settlement = appealClaim.Address().Settlement();
                        Subject subject = appealClaim.Subject();
                        if (settlement != null && subject != null) {
                            JSONObject result = server.getDefaultExecutor(settlement.getCode(), subject.Id());
                            String name;
                            String executorId;
                            if (result == null) {
                                success = false;
                                lastErrorText = "Null result from API";
                            } else {
                                int errorCode = result.getInt("errorCode", ErrorResult.UNKNOWN_ERROR);
                                if (errorCode != Transport.SUCCESS) {
                                    success = false;
                                    lastErrorText = result.getString("errorText", "");
                                } else {
                                    name = result.getString("name", null);
                                    executorId = result.getString("executorId", null);

                                    if (name != null && executorId != null) {
                                        Executor executor = Executor.Get(executorId);
                                        if (executor != null) appealClaim.setExecutor(executor);
                                        else success = false;
                                        Logger.Log("Received default executor for claim #" + String.valueOf(id));
                                    } else {
                                        success = false;
                                    }
                                }
                            }
                        } else {
                            Logger.Log("Background appeal sender. Removing appeal with empty subject/settlement!");
                            appealClaim.remove();
                        }
                    }

                    if (success) {
                        JSONObject result = server.sendAppeal(appealClaim);
                        try {
                            int errorCode = (Integer) result.get("errorCode");
                            if (errorCode == Transport.SUCCESS) {
                                appealClaim.remove();
                                Logger.Log("Appeal was sent offline!");
                                continue;
                            }
                        } catch (Exception e) {
                            Logger.Exception(e);
                        }
                        lastErrorText = result.getString("errorText", "");
                    }
                }

                appealClaim.setFailReason(lastErrorText);
                appealClaim.incTries();
                appealClaim.save();
                success = false;
            }

            return success;
        }
    }
}
