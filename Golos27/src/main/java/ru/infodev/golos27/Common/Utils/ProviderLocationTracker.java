package ru.infodev.golos27.Common.Utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import ru.infodev.golos27.Common.Interfaces.LocationTracker;

public class ProviderLocationTracker implements LocationListener, LocationTracker {

    private static final long MIN_UPDATE_DIST = 10;
    private static final long MIN_UPDATE_TIME = 1000 * 2;
    private static final long MIN_UPDATE_TIME_GPS = 1000 * 2;
    private LocationManager lm;

    public enum ProviderType {NETWORK, GPS}

    private String provider;
    private Context context;
    private Location lastLocation;
    private long lastTime;
    private boolean isRunning;
    private LocationUpdateListener listener;

    public ProviderLocationTracker(Context context, ProviderType type) {
        this.context = context;
        lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (type == ProviderType.NETWORK) {
            provider = LocationManager.NETWORK_PROVIDER;
        } else {
            provider = LocationManager.GPS_PROVIDER;
        }
    }

    public void start() {
        Logger.Log("Start " + provider);
        if (isRunning) return;
        isRunning = true;

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        long min_update_time = provider.equals(LocationManager.GPS_PROVIDER) ? MIN_UPDATE_TIME_GPS : MIN_UPDATE_TIME;
        Logger.Log("Requesting updates for " + provider + " with min_update_time = " + String.valueOf(min_update_time));
        try {
            lm.requestLocationUpdates(provider, min_update_time, MIN_UPDATE_DIST, this);
        } catch (Exception ignored) {
        }
        lastLocation = null;
        lastTime = 0;
    }

    public void start(LocationUpdateListener update) {
        start();
        listener = update;
    }

    public void stop() {
        if (isRunning) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            lm.removeUpdates(this);
            isRunning = false;
            listener = null;
        }
    }

    public boolean hasLocation() {
        return lastLocation != null && System.currentTimeMillis() - lastTime <= 5 * MIN_UPDATE_TIME;
    }

    public boolean hasPossiblyStaleLocation() {
        return lastLocation != null ||
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED ||
                lm.getLastKnownLocation(provider) != null;
    }

    public Location getLocation() {
        if (lastLocation == null) return null;
        if (System.currentTimeMillis() - lastTime > 5 * MIN_UPDATE_TIME) return null;
        return lastLocation;
    }

    public Location getPossiblyStaleLocation() {
        if (lastLocation != null) {
            return lastLocation;
        }
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        return lm.getLastKnownLocation(provider);
    }

    @Override
    public void onLocationChanged(Location newLoc) {
        Logger.Log("New location (" + provider + "): " +
                String.valueOf(newLoc.getLatitude()) + ", " + String.valueOf(newLoc.getLongitude()) +
                ", acc=" + String.valueOf(newLoc.getAccuracy()));

        long now = System.currentTimeMillis();
        if (listener != null) listener.onUpdate(lastLocation, lastTime, newLoc, now);
        lastLocation = newLoc;
        lastTime = now;
    }

    @Override
    public void onProviderDisabled(String arg0) {
        Logger.Log("Provider disabled: " + arg0);
    }

    @Override
    public void onProviderEnabled(String arg0) {
        Logger.Log("Provider enabled: " + arg0);
        stop();
        start();
    }

    @Override
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
        Logger.Log("Status changed: " + arg0 + ", " + arg1);
    }
}
