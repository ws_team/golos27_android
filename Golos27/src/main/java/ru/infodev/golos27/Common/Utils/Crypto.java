package ru.infodev.golos27.Common.Utils;

import android.util.Base64;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import ru.infodev.golos27.Common.Settings;

import static ru.infodev.golos27.Common.Utils.Utils.getDataProperty;
import static ru.infodev.golos27.Common.Utils.Utils.getFullDataProperty;
import static ru.infodev.golos27.Common.Utils.Utils.getIV;
import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.simpleDecodeString;
import static ru.infodev.golos27.Common.Utils.Utils.simpleEncodeString;

public class Crypto {
    public static SecretKey getSecretKey() {
        SecretKey secretKey;
        String secretKeyString = Settings.getStr(Settings.PROVIDER_HASH_DATA);
        if (isNotEmpty(secretKeyString)) {
            secretKey = Utils.stringToSecretKey(secretKeyString);
        } else {
            secretKey = generateKey();
            if (secretKey != null)
                Settings.setParameter(Settings.PROVIDER_HASH_DATA, Utils.secretKeyToString(secretKey));
        }
        return secretKey;
    }

    public static SecretKey generateKey() {
        SecretKey secretKey = null;
        try {
            final int outputKeyLength = 256;
            SecureRandom secureRandom = new SecureRandom();
            KeyGenerator keyGenerator = KeyGenerator.getInstance(getDataProperty());
            keyGenerator.init(outputKeyLength, secureRandom);
            secretKey = keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException e) {
            Logger.Exception(e);
        }
        return secretKey;
    }

    public static String encrypt(String data, SecretKey key) {

        try {
            Cipher cipher = Cipher.getInstance(getFullDataProperty());
            IvParameterSpec ivSpec = new IvParameterSpec(getIV());
            cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
            byte[] encrypted = cipher.doFinal(data.getBytes("UTF-8"));
            String temp = new String(Base64.encode(encrypted, Base64.NO_WRAP), "UTF-8");
            return simpleEncodeString(temp);
        } catch (Exception e) {
            Logger.Exception(e);
        }

        return null;
    }

    public static String decrypt(String data, SecretKey key) {
        try {
            data = simpleDecodeString(data);
            Cipher cipher = Cipher.getInstance(getFullDataProperty());
            IvParameterSpec ivSpec = new IvParameterSpec(getIV());
            cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
            byte[] bytes = Base64.decode(data, Base64.NO_WRAP);
            byte[] decrypted = cipher.doFinal(bytes);
            return new String(decrypted, "UTF-8");
        } catch (Exception e) {
            Logger.Exception(e);
        }

        return null;
    }
}
