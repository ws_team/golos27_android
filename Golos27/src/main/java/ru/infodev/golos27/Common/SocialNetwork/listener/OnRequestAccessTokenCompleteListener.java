package ru.infodev.golos27.Common.SocialNetwork.listener;

import ru.infodev.golos27.Common.SocialNetwork.AccessToken;
import ru.infodev.golos27.Common.SocialNetwork.listener.base.SocialNetworkListener;

/**
 * Interface definition for a callback to be invoked when access token request complete.
 */
public interface OnRequestAccessTokenCompleteListener extends SocialNetworkListener {
    /**
     * Called when access token request complete.
     * @param socialNetworkID id of social network where request was complete
     * @param accessToken that social network returned
     */
    void onRequestAccessTokenComplete(int socialNetworkID, AccessToken accessToken);
}
