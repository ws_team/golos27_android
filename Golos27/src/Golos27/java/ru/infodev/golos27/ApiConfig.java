package ru.infodev.golos27;

/**
 * Created by Maxim Ostrovidov on 10.08.17.
 * (c) White Soft
 */
public class ApiConfig {
    public static final String SERVER_SCHEMA_VALUE = "https";
    public static final String SERVER_ADDRESS_VALUE = "tc.khv.gov.ru";
    public static final String SERVER_PORT_VALUE = "443";

    public static final String FRONT_ADDRESS = "http://open27.ru";
}

//public class ApiConfig {
//    public static final String SERVER_SCHEMA_VALUE = "http";
//    public static final String SERVER_ADDRESS_VALUE = "192.168.0.203";
//    public static final String SERVER_PORT_VALUE = "8080";
//
//    public static final String FRONT_ADDRESS = "http://open27.ru";
//}