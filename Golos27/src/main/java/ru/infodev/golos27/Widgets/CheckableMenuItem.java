package ru.infodev.golos27.Widgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.shehabic.droppy.DroppyMenuItemAbstract;
import com.shehabic.droppy.views.DroppyMenuItemIconView;
import com.shehabic.droppy.views.DroppyMenuItemTitleView;
import com.shehabic.droppy.views.DroppyMenuItemView;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.R;

public class CheckableMenuItem extends DroppyMenuItemAbstract {

    private Drawable iconDrawable;
    protected DroppyMenuItemView renderedView;

    void initMenuItem(int id, String title, boolean checked)
    {
        this.title = title;
        this.icon = checked ? R.drawable.delete_selection_media : -1;
        this.id = id;
    }

    public CheckableMenuItem(int id, String title, boolean checked) {
        initMenuItem(id, title, checked);
    }

    public CheckableMenuItem(int id, int current_id) {
        String title = Application.AppContext.getString(id);
        initMenuItem(id, title, id == current_id);
    }

    public void setIcon(Drawable iconDrawable)
    {
        this.iconDrawable = iconDrawable;
    }

    @Override
    public View render(Context context) {

        renderedView = new DroppyMenuItemView(context);
        DroppyMenuItemTitleView droppyMenuItemTitle = new DroppyMenuItemTitleView(context);
        droppyMenuItemTitle.setText(this.title);
        renderedView.addView(droppyMenuItemTitle);

        if (this.icon != -1) {
            DroppyMenuItemIconView droppyMenuItemIcon = new DroppyMenuItemIconView(context);
            droppyMenuItemIcon.setImageResource(this.icon);
            renderedView.addView(droppyMenuItemIcon);
        } else if (this.iconDrawable != null) {
            DroppyMenuItemIconView droppyMenuItemIcon = new DroppyMenuItemIconView(context);
            droppyMenuItemIcon.setImageDrawable(iconDrawable);
            renderedView.addView(droppyMenuItemIcon);
        }

        return renderedView;
    }
}
