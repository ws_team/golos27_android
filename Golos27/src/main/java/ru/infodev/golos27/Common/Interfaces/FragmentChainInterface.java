package ru.infodev.golos27.Common.Interfaces;

import ru.infodev.golos27.Common.Utils.Json.JSONObject;

public interface FragmentChainInterface {
    void FragmentChain(int fragment_id, Class chainNext, JSONObject params, int popCount,
                       boolean lockWindow, boolean authNeeded);
    void FragmentPop(int fragment_id, int popCount, boolean lockWindow);
    void FragmentClearFocus();
}
