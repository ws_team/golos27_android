package ru.infodev.golos27.Widgets.Layouts;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

@SuppressWarnings("unused")
public class SwitchableScrollView extends ScrollView {
    boolean allowScroll = true;

    public boolean isAllowScroll() {
        return allowScroll;
    }

    public void setAllowScroll(boolean allowScroll) {
        this.allowScroll = allowScroll;
    }

    public SwitchableScrollView(Context context) {
        super(context);
    }

    public SwitchableScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SwitchableScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return allowScroll && super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return allowScroll && super.onInterceptTouchEvent(ev);
    }
}
