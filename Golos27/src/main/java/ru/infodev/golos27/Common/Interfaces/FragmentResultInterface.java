package ru.infodev.golos27.Common.Interfaces;

import ru.infodev.golos27.Common.Utils.Json.JSONObject;

public interface FragmentResultInterface {
    void FragmentSendResult(int fragment_id, int cmd, JSONObject result);
}
