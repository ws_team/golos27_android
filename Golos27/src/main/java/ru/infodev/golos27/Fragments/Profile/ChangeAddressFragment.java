package ru.infodev.golos27.Fragments.Profile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.Common.Catalogs.OKTMO;
import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Profile;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;
import ru.infodev.golos27.Widgets.Adapters.CityAutoCompleteAdapter;
import ru.infodev.golos27.Widgets.Adapters.TextWatcherAdapter;
import ru.infodev.golos27.Widgets.CityAutoCompleteEditText;
import ru.infodev.golos27.Widgets.CustomEditText;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.isStringsSame;

public class ChangeAddressFragment extends ParamFragment implements TextWatcherAdapter.TextWatcherListener {
    public static final int FRAGMENT_ID = R.layout.profile_change_address;
    private CustomEditText addressEdit;
    private CityAutoCompleteEditText cityEdit;
    private TextView cityHelp;
    private String entered_oktmo = "";
    private String entered_address = "";
    private boolean addressGood = false;
    private boolean cityGood = false;

    public ChangeAddressFragment() {
        titleId = R.string.mail_address;
        setFragmentAllowMenu(false);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(FRAGMENT_ID, container, false);

        CityAutoCompleteAdapter adapter = new CityAutoCompleteAdapter(Application.AppContext);
        cityHelp = (TextView) v.findViewById(R.id.help_text_mail_address);
        cityEdit = (CityAutoCompleteEditText) v.findViewById(R.id.edit_city);
        cityEdit.setOnTextChangedListener(this);
        cityEdit.setAdapter(adapter);
        cityEdit.setThreshold(1);

        OKTMO city = OKTMO.Get(Profile.OKTMOCode());
        if (city != null) cityEdit.setCity(city);

        addressEdit = (CustomEditText) v.findViewById(R.id.edit_street);
        addressEdit.setOnTextChangedListener(this);
        addressEdit.setText(Profile.Address());

        return v;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        setActionButtonIcon(ParamFragment.ICON_CHECK);
        showSaveButtonIfCan();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_item) {
            OKTMO city = cityEdit.getCity();
            String address = addressEdit.getText().toString();
            String cityCode = null;
            String cityData = null;
            if (city != null) {
                cityCode = city.getCode();
                cityData = city.getCityFullName();
            }
            new SaveAddressInfoTask().execute(cityCode, cityData, address);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTextChanged(EditText view, String text) {
        switch (view.getId()) {
            case R.id.edit_city:
                OKTMO city = cityEdit.getCity();
                cityGood = city != null;
                if (city != null) {
                    cityHelp.setText(city.getFullRegion());
                    entered_oktmo = city.getCode();
                } else {
                    cityHelp.setText("");
                    entered_oktmo = "";
                }
                break;
            case R.id.edit_street:
                addressGood = text.length() > 0;
                entered_address = text;
                break;
        }

        showSaveButtonIfCan();
    }

    private void showSaveButtonIfCan() {
        if (addressGood || cityGood || (isEmpty(entered_oktmo) && isEmpty(entered_address))) {
            String addr = Profile.Address();
            String code = Profile.OKTMOCode();
            if (!isStringsSame(addr, entered_address) || !isStringsSame(code, entered_oktmo)) {
                setActionButtonVisibility(true);
                return;
            }
        }

        setActionButtonVisibility(false);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // Не требуется, но обязателен
    }

    @Override
    public void afterTextChanged(Editable s) {
        // Не требуется, но обязателен
    }

    class SaveAddressInfoTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setActionButtonIcon(ParamFragment.ICON_ADD_PROGRESS);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            return server.setProfile(Profile.FirstName(), Profile.LastName(), Profile.MiddleName(), Profile.Gender(),
                    Profile.Birth(), params[0], params[1], params[2], Profile.Latitude(), Profile.Longitude());
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            setActionButtonIcon(ParamFragment.ICON_DEL_PROGRESS);
            setActionButtonEnabled(false);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    fragmentResultListener.FragmentSendResult(FRAGMENT_ID, 0, result);
                    setActionButtonEnabled(true);
                } else {
                    ResultDialog.showError(getActivity(), "Ошибка сохранения", errorCode, result.getErrorText());
                }
            } catch (Exception e) {
                Logger.Exception(e);
                ResultDialog.showError(getActivity(), "Ошибка сохранения", ErrorResult.INTERNAL_ERROR);
            }
            setActionButtonEnabled(true);
        }
    }
}