package ru.infodev.golos27.Fragments.AppealClaim;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.infodev.golos27.Application;
import ru.infodev.golos27.BuildConfig;
import ru.infodev.golos27.Common.Catalogs.Address;
import ru.infodev.golos27.Common.Catalogs.Appeal.AppealAttachment;
import ru.infodev.golos27.Common.Catalogs.Appeal.AppealClaim;
import ru.infodev.golos27.Common.Catalogs.Executor;
import ru.infodev.golos27.Common.Catalogs.NewAppeal;
import ru.infodev.golos27.Common.Catalogs.OKTMO;
import ru.infodev.golos27.Common.CustomFontLoader;
import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.GalleryEntry;
import ru.infodev.golos27.Common.Interfaces.DialogResultInterface;
import ru.infodev.golos27.Common.Profile;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialog;
import ru.infodev.golos27.Fragments.Dialogs.ResultDialogFragment;
import ru.infodev.golos27.Fragments.Dialogs.YesNoDialog;
import ru.infodev.golos27.Fragments.Dialogs.YesNoDialogFragment;
import ru.infodev.golos27.Fragments.ParamFragment;
import ru.infodev.golos27.R;
import ru.infodev.golos27.RestApi.ServerTalk;
import ru.infodev.golos27.RestApi.Transport;
import ru.infodev.golos27.Widgets.Adapters.GalleryAdapter;
import ru.infodev.golos27.Widgets.Lists.NestedGridView;

import static ru.infodev.golos27.Common.Utils.Utils.isEmpty;
import static ru.infodev.golos27.Common.Utils.Utils.isNotEmpty;

public class ConfirmAppealFragment extends ParamFragment
        implements View.OnClickListener, DialogResultInterface {
    public static final int FRAGMENT_ID = R.layout.confirm_appeal;

    private SwitchCompat portalHideSwitch;
    private SwitchCompat hidePortalFioSwitch;
    private SwitchCompat hideAnswerSwitch;
    private SwitchCompat postMailAnswerSwitch;
    private LinearLayout postalAddressLayout;
    private Button sendAppealButton;
    private View showFioLayout;
    private View showAnswerLayout;
    private String zipFullAddress = "";
    private ProgressBar progressBar;
    private AppealClaim appealClaim;

    public ConfirmAppealFragment() {
        titleId = R.string.appeal_general_info;
        setFragmentAllowMenu(false);
        setFragmentIconCross(true);
    }

    @Override
    public boolean ExitRequest() {
        YesNoDialog.makeDialog(Application.AppContext, R.string.exit_question,
                R.string.cancel_appeal, R.string.yes, R.string.no, this).show(getActivity());
        return false;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(FRAGMENT_ID, container, false);
        setRetainInstance(true);
        setTitle(titleId);

        // Инициализация текста
        Typeface roboto_light = CustomFontLoader.getTypeface(Application.AppContext, 0);
        TextView problemDescriptionTextView = (TextView) v.findViewById(R.id.ProblemDescription);
        problemDescriptionTextView.setTypeface(roboto_light);
        problemDescriptionTextView.setText(NewAppeal.getInstance().Message());
        TextView zipAddressTextView = (TextView) v.findViewById(R.id.zip_address_text);

        boolean autoZipAddress = true;
        if (NewAppeal.getInstance().ZipAddress() != null) {
            zipFullAddress = NewAppeal.getInstance().ZipAddress().FullAddress();
            autoZipAddress = !(NewAppeal.getInstance().ZipAddress().isChangedByUser());
        }

        // Если в обращении адреса нет попробуем скопировать его из профиля
        if (isEmpty(zipFullAddress) && autoZipAddress) {
            OKTMO settlement = OKTMO.Get(Profile.OKTMOCode());
            String address = Profile.Address();
            if (settlement != null && isNotEmpty(address)) {
                NewAppeal.getInstance().setZipAddress(new Address(settlement, address));
                zipFullAddress = NewAppeal.getInstance().ZipAddress().FullAddress();
            }
        }

        if (isEmpty(zipFullAddress)) {
            zipAddressTextView.setText(getString(R.string.enter_address_here));
        } else {
            zipAddressTextView.setText(zipFullAddress);
        }

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        sendAppealButton = (Button) v.findViewById(R.id.send_appeal_button);
        sendAppealButton.setEnabled(!(NewAppeal.getInstance().IsGetMailResponse() && isEmpty(zipFullAddress)));
        sendAppealButton.setOnClickListener(this);

        // Название тематики
        TextView subjectNameTextView = (TextView) v.findViewById(R.id.subjectName);
        subjectNameTextView.setTypeface(roboto_light);
        if (NewAppeal.getInstance().Subject() != null)
            subjectNameTextView.setText(NewAppeal.getInstance().Subject().Name());

        // Адрес проблемного места
        TextView locationNameTextView = (TextView) v.findViewById(R.id.locationAddress);
        locationNameTextView.setTypeface(roboto_light);
        locationNameTextView.setText(NewAppeal.getInstance().Address().CustomAddress());

        showFioLayout = v.findViewById(R.id.show_portal_fio_block);
        showAnswerLayout = v.findViewById(R.id.view_hide_answer_block);
        postalAddressLayout = (LinearLayout) v.findViewById(R.id.go_set_address);
        postalAddressLayout.setOnClickListener(this);
        NestedGridView imagesGrid = (NestedGridView) v.findViewById(R.id.images_list);

        // Нажатие на блок - переключает свитч
        hidePortalFioSwitch = (SwitchCompat) v.findViewById(R.id.hide_portal_fio_switch);
        final View portal_fio_block = v.findViewById(R.id.portal_fio_block);
        portal_fio_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidePortalFioSwitch.toggle();
            }
        });

        // При переключении сохраняем флаг в текущем обращении
        hidePortalFioSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                NewAppeal.getInstance().setIsPublishPersonalData(!isChecked);
            }
        });

        // Нажатие на блок переключает свитч
        hideAnswerSwitch = (SwitchCompat) v.findViewById(R.id.hide_response_switch);
        final View hide_answer_block = v.findViewById(R.id.hide_answer_block);
        hide_answer_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideAnswerSwitch.toggle();

            }
        });

        // При переключении сохраняем флаг в текущем обращении
        hideAnswerSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                NewAppeal.getInstance().setIsPublishResponse(!isChecked);
            }
        });

        // При нажатии на блок переключаем свитч
        postMailAnswerSwitch = (SwitchCompat) v.findViewById(R.id.post_mail_answer_switch);
        View post_mail_answer_block = v.findViewById(R.id.post_mail_answer_block);
        post_mail_answer_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.post_mail_answer_block:
                        postMailAnswerSwitch.toggle();
                }
            }
        });

        portalHideSwitch = (SwitchCompat) v.findViewById(R.id.portal_hide_switch);
        View portal_hide_block = v.findViewById(R.id.portal_hide_block);
        portal_hide_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                portalHideSwitch.toggle();
            }
        });

        // При переключении сохраняем данные в обращении и скрываем/показываем зависимые флаги
        portalHideSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                NewAppeal.getInstance().setIsPublishAppeal(!isChecked);
                if (isChecked) {
                    portal_fio_block.setVisibility(View.GONE);
                    hide_answer_block.setVisibility(View.GONE);
                    showAnswerLayout.setVisibility(View.GONE);
                    showFioLayout.setVisibility(View.GONE);
                } else {
                    portal_fio_block.setVisibility(View.VISIBLE);
                    hide_answer_block.setVisibility(View.VISIBLE);
                    showAnswerLayout.setVisibility(View.VISIBLE);
                    showFioLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        // При переключении сохраняем данные и показываем/прячем раздел с адресом
        postMailAnswerSwitch = (SwitchCompat) v.findViewById(R.id.post_mail_answer_switch);
        postMailAnswerSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                NewAppeal.getInstance().setIsGetMailResponse(isChecked);
                sendAppealButton.setEnabled(!(isChecked && isEmpty(zipFullAddress)));
                if (isChecked) {
                    postalAddressLayout.setVisibility(View.VISIBLE);
                } else {
                    postalAddressLayout.setVisibility(View.GONE);
                }
            }
        });

        // Навесим обработчики на кнопки редактирования данных
        View problemDescriptionButton = v.findViewById(R.id.go_describe_problem);
        problemDescriptionButton.setOnClickListener(this);
        View setCategoryButton = v.findViewById(R.id.go_set_category);
        setCategoryButton.setOnClickListener(this);
        View setLocationButton = v.findViewById(R.id.go_set_location);
        setLocationButton.setOnClickListener(this);
        View setPictureButton = v.findViewById(R.id.go_set_picture);
        setPictureButton.setOnClickListener(this);
        View setPerformerButton = v.findViewById(R.id.go_set_performer);
        setPerformerButton.setOnClickListener(this);

        View performerIcon = v.findViewById(R.id.go_set_performer_icon);
        if(BuildConfig.FLAVOR.equals("Golos41")) performerIcon.setVisibility(View.GONE);

        // Выставим свичи согласно сохраненным данным (вызывается два раза,
        // чтобы код показывающий или скрывающий разделы гарантированно отработал
        boolean portalHide = NewAppeal.getInstance().IsPublishAppeal();
        portalHideSwitch.setChecked(portalHide);
        portalHideSwitch.setChecked(!portalHide);

        boolean answerHide = NewAppeal.getInstance().IsPublishResponse();
        hideAnswerSwitch.setChecked(answerHide);
        hideAnswerSwitch.setChecked(!answerHide);

        boolean fioHide = NewAppeal.getInstance().IsPublishPersonalData();
        hidePortalFioSwitch.setChecked(fioHide);
        hidePortalFioSwitch.setChecked(!fioHide);

        boolean mailAnswer = NewAppeal.getInstance().IsGetMailResponse();
        postMailAnswerSwitch.setChecked(!mailAnswer);
        postMailAnswerSwitch.setChecked(mailAnswer);

        // Отобразим изображения, добавленные в обращение
        List<GalleryEntry> originalList = NewAppeal.getInstance().ImagesList();
        if (originalList != null && originalList.size() > 0) {
            // Добавим максимум три картинки
            List<GalleryEntry> previewList = new ArrayList<>();
            GalleryAdapter imagesAdapter = new GalleryAdapter(Application.AppContext, previewList);
            imagesGrid.setAdapter(imagesAdapter);
            int count = Math.min(originalList.size(), 3);
            for (int i = 0; i < count; i++) previewList.add(originalList.get(i));
            imagesAdapter.notifyDataSetChanged();

            v.findViewById(R.id.no_images_text).setVisibility(View.INVISIBLE);
            v.findViewById(R.id.no_images_add).setVisibility(View.GONE);
        } else {
            imagesGrid.setVisibility(View.INVISIBLE);
            v.findViewById(R.id.no_images_text).setVisibility(View.VISIBLE);
            v.findViewById(R.id.no_images_add).setVisibility(View.VISIBLE);
            ((ImageView)v.findViewById(R.id.image_add_arrow))
                    .setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.blue_arrow));
        }

        TextView executorView = (TextView) v.findViewById(R.id.executor_text);
        executorView.setTypeface(roboto_light);
        Executor executor = NewAppeal.getInstance().Executor();
        if (executor != null)
            executorView.setText(executor.Name());
        else
            executorView.setText(Application.AppContext.getString(R.string.auto_executor));

        return v;
    }

    @Override
    public void onClick(View v) {
        JSONObject result = new JSONObject();
        result.putSafe(ParamFragment.NOT_CHAIN, true);

        switch (v.getId()) {
            case R.id.go_describe_problem:
                chainStartNextFragment(FRAGMENT_ID, SetProblemFragment.class, result, false);
                break;
            case R.id.go_set_address:
                chainStartNextFragment(FRAGMENT_ID, SetAddressFragment.class, result, false);
                break;
            case R.id.go_set_category:
                chainStartNextFragment(FRAGMENT_ID, SetCategoryFragment.class, result, false);
                break;
            case R.id.go_set_location:
                chainStartNextFragment(FRAGMENT_ID, SetMapLocationFragment.class, result, false);
                break;
            case R.id.go_set_picture:
                chainStartNextFragment(FRAGMENT_ID, SetPictureFragment.class, result, false);
                break;
            case R.id.go_set_performer:
                if(BuildConfig.FLAVOR.equals("Golos41")) return;
                chainStartNextFragment(FRAGMENT_ID, SetExecutorFragment.class, result, false);
                break;

            case R.id.send_appeal_button:
                new SendAppealTask(getActivity(), this).execute();
        }
    }

    @Override
    public void DialogSendResult(int dialog_id, int cmd) {
        if (dialog_id == YesNoDialogFragment.DIALOG_ID) {
            Logger.Log("Got result from cancel confirm appeal dialog: " + String.valueOf(cmd));
            NewAppeal.getInstance().Cleanup(true);
            if (cmd == Activity.RESULT_OK) chainPopFragments(FRAGMENT_ID, 1, false);
        }

        if (dialog_id == ResultDialogFragment.DIALOG_ID) {
            Logger.Log("Got result from result dialog: " + String.valueOf(cmd));
            NewAppeal.getInstance().Cleanup(false);
            chainPopFragments(FRAGMENT_ID, 1, false);
        }
    }

    @Override
    public void DialogSendParamResult(int dialog_id, int cmd, JSONObject params) {
    }

    class SendAppealTask extends AsyncTask<String, Void, JSONObject> {
        private Activity activity;
        private DialogResultInterface listener;
        public SendAppealTask(Activity activity, DialogResultInterface listener) {
            this.activity = activity;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            sendAppealButton.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected final JSONObject doInBackground(String[] params) {
            ServerTalk server = new ServerTalk();
            appealClaim = NewAppeal.getInstance();
            List<GalleryEntry> images = appealClaim.ImagesList();
            for (GalleryEntry doc : images) {
                File file = new File(doc.ImagePath());
                if (doc.getAttachment() == null) {
                    JSONObject result = server.uploadDocument(file);
                    if (result.has("errorCode") && !result.isNull("errorCode")) {
                        ErrorResult retVal = new ErrorResult(result.getInt("errorCode", 0), result.getString("errorText", ""));
                        if (retVal.getErrorCode() != ErrorResult.SUCCESS) {
                            Logger.Warn("Can't sent appeal claim attachment: " + file.toString());
                            return result;
                        }

                        AppealAttachment attachment = new AppealAttachment();
                        attachment.setId(result.getString("id", null));
                        attachment.setIndex(result.getInt("index", -1));
                        attachment.setLength(result.getInt("length", -1));
                        attachment.setMimeType(result.getString("mimeType", null));
                        attachment.setName(result.getString("name", null));
                        appealClaim.AddDocument(attachment);
                        doc.setAttachment(attachment);
                        Logger.Log("Attachment saved to server: " + file.toString());
                        if (appealClaim.DocumentCoverIdPlain() == null)
                            appealClaim.setDocumentCoverId(attachment.Id());
                    }
                }
            }

            return server.sendAppeal(appealClaim);
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);

            progressBar.setVisibility(View.INVISIBLE);
            sendAppealButton.setVisibility(View.VISIBLE);
            sendAppealButton.setEnabled(false);

            try {
                int errorCode = (Integer) result.get("errorCode");
                if (errorCode == Transport.SUCCESS) {
                    sendAppealButton.setEnabled(true);
                    if (appealClaim != null) appealClaim.Cleanup(true);
                    ResultDialog.makeDialog(activity, R.drawable.check_big,
                            activity.getString(R.string.appeal_sent), null, activity.getString(R.string.go_main), listener)
                            .show(activity);
                    return;
                }
            } catch (Exception e) {
                Logger.Exception(e);
            }

            // Здесь случилась проблема. Отправка не удалась.
            // Надо сохранить заявку на обращение для последующей фоновой отправки
            if (appealClaim != null) {
                appealClaim.setFailReason(result.getString("errorText", ""));
                appealClaim.setDate(new Date().getTime());
                appealClaim.incTries();
                appealClaim.save();
            }

            ResultDialog.makeDialog(activity, R.drawable.error_grey,
                    activity.getString(R.string.network_error),
                    activity.getString(R.string.appeal_saved_to_send), null, listener)
                    .show(activity);

            sendAppealButton.setEnabled(false);
        }
    }
}
