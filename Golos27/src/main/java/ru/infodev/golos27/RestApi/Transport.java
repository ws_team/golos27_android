package ru.infodev.golos27.RestApi;

import android.os.Build;
import android.util.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import ru.infodev.golos27.Common.ErrorResult;
import ru.infodev.golos27.Common.Profile;
import ru.infodev.golos27.Common.Settings;
import ru.infodev.golos27.Common.Utils.Json.JSONObject;
import ru.infodev.golos27.Common.Utils.Logger;
import ru.infodev.golos27.Common.Utils.MapQuery;

public class Transport {
    private String server = null;
    private URI basePath = null;
    private URI rootPath = null;

    public static final int SUCCESS = 0;
    public static final int REQUEST_GET = 0;
    public static final int REQUEST_POST = 1;
    public static final int REQUEST_PUT = 2;
    public static final int REQUEST_DEL = 3;
    public static final String CHARSET = "UTF-8";
    private static final String LINE_FEED = "\r\n";
    public static final String INVALID_CREDENTIALS_TOKEN = "INVALID_CREDENTIALS_TOKEN";
    private static final Pair<ErrorResult, String> networkErrorPair = Pair.create(
            new ErrorResult(ErrorResult.NETWORK_ERROR,
            ErrorResult.getErrorText(ErrorResult.NETWORK_ERROR)), null);

    /***
     * Возвращает проинициализированный асс. массив для построения запроса
     *
     * @return Map с уже заполненными данными клиента
     */
    public HashMap<String, Object> getQuery() {
        HashMap<String, Object> query = new HashMap<>();
        query.put("client_id", Settings.getStr(Settings.CLIENT_ID));
        query.put("client_secret", Settings.getStr(Settings.CLIENT_SECRET));
        return query;
    }

    /***
     * Конструктор. Инициализирует транспорт. Требуется указание
     * схемы, адреса сервера и пароля
     *
     * @param scheme схема (http или https)
     * @param server строка с адресом сервера
     * @param port   порт на сервере
     */
    public Transport(String scheme, String server, String root, int port) {
        this.server = server;

        String serverRoot = root;
        int serverPort = port <= 0 ? 80 : port;
        if (!serverRoot.endsWith("/")) {
            serverRoot += '/';
        }

        // Мы поддерживаем sdk c 14 версии, но это на всякий случай!
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
            System.setProperty("http.keepAlive", "false");
        }

        // Абсолютный путь до сервера конструируется заранее
        try {
            basePath = new URI(scheme, null, server, serverPort, serverRoot, null, null);
            rootPath = new URI(scheme, null, server, serverPort, null, null, null);
        } catch (URISyntaxException e) {
            Logger.Exception(e);
        }
    }

    @SuppressWarnings("unused")
    public String getRootPath() {
        return rootPath.toString();
    }

    public String getBasePath() {
        return basePath.toString();
    }

    /***
     * Построение url из частей, указанных в запросе и установление соединения с сервером
     *
     * @param path    относительный путь на http сервере
     * @param request запрос в формате name1=xxx&name2=xxx (urlencoded)
     * @return Pair. First - ошибка, Second - результат (HttpUrlConnection)
     */
    protected Pair<ErrorResult, HttpURLConnection> getConnection(String path, String request) {
        if (server == null) {
            return Pair.create(new ErrorResult(ErrorResult.TRANSPORT_NOT_INIT,
                    ErrorResult.getErrorText(ErrorResult.TRANSPORT_NOT_INIT)), null);
        }

        URI uri;
        try {
            URI relative = new URI(path + '?' + request);
            uri = basePath.resolve(relative);
        } catch (URISyntaxException e) {
            Logger.Warn("Can't create uri for get: " + e.toString());
            return Pair.create(new ErrorResult(ErrorResult.URI_BUILD_ERROR,
                    ErrorResult.getErrorText(ErrorResult.URI_BUILD_ERROR)), null);
        }

        URL full_url;
        try {
            full_url = uri.toURL();
        } catch (MalformedURLException e) {
            Logger.Warn("Can't create url from uri: " + e.toString());
            return Pair.create(new ErrorResult(ErrorResult.URI_BUILD_ERROR,
                    ErrorResult.getErrorText(ErrorResult.URI_BUILD_ERROR)), null);
        }

        try {
            HttpURLConnection urlConnection = (HttpURLConnection) full_url.openConnection();
            //SSLSocketFactory instance = CustomTrustCA.getInstance();
            //if (instance != null) urlConnection.setSSLSocketFactory(instance);
            return Pair.create(new ErrorResult(ErrorResult.SUCCESS, null), urlConnection);
        } catch (IOException e) {
            Logger.Warn("Can't open connection to server: " + e.toString());
            return Pair.create(new ErrorResult(ErrorResult.CANT_CONNECT,
                    ErrorResult.getErrorText(ErrorResult.CANT_CONNECT)), null);
        }
    }

    /***
     * Отправляет запрос GET на сервер и возвращает полученные данные в виде пары
     *
     * @param path        путь относительно корня сервера
     * @param request_map запрос в формате request_map[name1]=value1, request_map[name2]=value2
     * @param cred        токен для авторизации в системе
     * @return пару строк, первая строка - ошибка, вторая - результат
     */
    Pair<ErrorResult, String> get(String path, Map<String, Object> request_map, String cred) {
        return send(path, request_map, null, cred, REQUEST_GET);
    }

    /***
     * Отправляет запрос POST на сервер и возвращает полученные данные в виде пары
     *
     * @param path        путь относительно корня сервера
     * @param request_map запрос в формате request_map[name1]=value1, request_map[name2]=value2
     * @param data        данные для отправки на сервер
     * @param cred        токен для авторизации в системе
     * @return пару строк, первая строка - ошибка, вторая - результат
     */
    Pair<ErrorResult, String> post(String path, Map<String, Object> request_map, JSONObject data, String cred) {
        return send(path, request_map, data, cred, REQUEST_POST);
    }

    /***
     * Отправляет запрос DEL на сервер и возвращает полученные данные в виде пары
     *
     * @param path        путь относительно корня сервера
     * @param request_map запрос в формате request_map[name1]=value1, request_map[name2]=value2
     * @param data        данные для отправки на сервер
     * @param cred        токен для авторизации в системе
     * @return пару строк, первая строка - ошибка, вторая - результат
     */
    Pair<ErrorResult, String> del(String path, Map<String, Object> request_map, JSONObject data, String cred) {
        return send(path, request_map, data, cred, REQUEST_DEL);
    }

    /***
     * Отправляет запрос PUT на сервер и возвращает полученные данные в виде пары
     *
     * @param path        путь относительно корня сервера
     * @param request_map запрос в формате request_map[name1]=value1, request_map[name2]=value2
     * @param data        данные для отправки на сервер
     * @param cred        токен для авторизации в системе
     * @return пару строк, первая строка - ошибка, вторая - результат
     */
    Pair<ErrorResult, String> put(String path, Map<String, Object> request_map, JSONObject data, String cred) {
        return send(path, request_map, data, cred, REQUEST_PUT);
    }

    /***
     * Отправляет запрос на сервер и возвращает полученные данные в виде пары ошибка/результат
     *
     * @param path         путь относительно корня сервера
     * @param request_map  запрос в формате request_map[name1]=value1, request_map[name2]=value2
     * @param data         данные для отправки на сервер
     * @param cred         токен для авторизации в системе
     * @param request_type тип запроса (GET/POST/PUT)
     * @return пару строк, первая строка - ошибка, вторая - результат
     */
    Pair<ErrorResult, String> send(String path, Map<String, Object> request_map, JSONObject data,
                                   String cred, int request_type) {

        String request = "";
        if (request_map != null) {
            request = MapQuery.urlEncodeUTF8(request_map);
        }

        Pair<ErrorResult, HttpURLConnection> conn_result = getConnection(path, request);
        if ((conn_result.first != null) &&
                (conn_result.first.getErrorCode() != ErrorResult.SUCCESS)) {
            return Pair.create(conn_result.first, null);
        }

        try {
            String json;
            StringBuilder buffer = new StringBuilder();
            if (conn_result.second != null) {
                String localUrl = conn_result.second.getURL().toString();
                if (!localUrl.contains("password"))
                    Logger.Log("API send url: " + localUrl);

                HttpURLConnection urlConnection = conn_result.second;
                urlConnection.setReadTimeout(Settings.getInt(Settings.SERVER_READ_TIMEOUT));
                urlConnection.setConnectTimeout(Settings.getInt(Settings.SERVER_CONNECT_TIMEOUT));
                switch (request_type) {
                    case REQUEST_GET:
                        urlConnection.setRequestMethod("GET");
                        break;
                    case REQUEST_PUT:
                        urlConnection.setRequestMethod("PUT");
                        break;
                    case REQUEST_POST:
                        urlConnection.setRequestMethod("POST");
                        break;
                    case REQUEST_DEL:
                        urlConnection.setRequestMethod("DELETE");
                        break;
                    default:
                        urlConnection.setRequestMethod("GET");
                }

                if (cred != null) {
                    if (cred.equals(INVALID_CREDENTIALS_TOKEN)) {
                        Profile.RemoveAllData();
                        Logger.Warn("Received INVALID SAVED CREDENTIALS DATA! Removing all auth fields!");
                        return Pair.create(new ErrorResult(ErrorResult.INVALID_SAVED_CRED,
                                "Пользователь не авторизирован"), null);
                    }

                    if (!cred.isEmpty()) urlConnection.addRequestProperty("Authorization", "Bearer " + cred);
                }

                urlConnection.addRequestProperty("Content-type", "application/json; charset=utf-8");
                urlConnection.setDoInput(true);

                // Если в запросе есть данные - необходимо записать их в выходной поток
                if ((data != null) && ((request_type == REQUEST_POST) || (request_type == REQUEST_PUT))) {
                    urlConnection.setDoOutput(true);
                    OutputStream outputStream = urlConnection.getOutputStream();
                    outputStream.write(data.toString().getBytes("UTF-8"));
                    outputStream.flush();
                    outputStream.close();
                } else {
                    urlConnection.setDoOutput(false);
                }

                int responseCode;
                try {
                    responseCode = urlConnection.getResponseCode();
                } catch (IOException e) {
                    responseCode = urlConnection.getResponseCode();
                }

                if ((responseCode == HttpsURLConnection.HTTP_OK) ||
                        (responseCode == HttpsURLConnection.HTTP_CREATED) ||
                        (responseCode == HttpsURLConnection.HTTP_ACCEPTED)) {

                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    while ((line = br.readLine()) != null) buffer.append(line);
                    json = buffer.toString();
                    return Pair.create(new ErrorResult(ErrorResult.SUCCESS, null), json);
                } else {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream()));
                    while ((line = br.readLine()) != null) {
                        buffer.append(line);
                    }
                    json = buffer.toString();
                    int errorCode = ErrorResult.BAD_REQUEST;
                    String errorText = ErrorResult.getErrorText(ErrorResult.BAD_REQUEST);
                    if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        errorCode = ErrorResult.UNAUTHORIZED;
                        errorText = ErrorResult.getErrorText(ErrorResult.UNAUTHORIZED);
                    }

                    // Помимо ошибок сервер также возвращает свое "видение" ситуации
                    return Pair.create(new ErrorResult(errorCode, errorText), json);
                }
            }
        } catch (SocketTimeoutException e) {
            Logger.Warn("Can't send request " + path + " (socket timeout)");
            return networkErrorPair;
        } catch (ConnectException e) {
            Logger.Warn("Connection to " + path + " failed");
            return networkErrorPair;
        } catch (IOException e) {
            Logger.Exception(e);
            return networkErrorPair;
        } catch (Exception e) {
            Logger.Exception(e);
            return networkErrorPair;
        }

        return Pair.create(new ErrorResult(ErrorResult.INTERNAL_ERROR,
                ErrorResult.getErrorText(ErrorResult.INTERNAL_ERROR)), null);
    }


    /***
     * Отправка файла на сервер (загрузка документа)
     *
     * @param path относительный путь до метода на сервере
     * @param file путь до загружаемого файла
     * @param cred авторизационный токен
     * @return пару строк, первая строка - ошибка, вторая - результат
     */
    Pair<ErrorResult, String> postFile(String path, File file, String cred) {
        Pair<ErrorResult, HttpURLConnection> conn_result = getConnection(path, "");
        if ((conn_result.first != null) && (conn_result.first.getErrorCode() != ErrorResult.SUCCESS)) {
            return Pair.create(conn_result.first, null);
        }

        try {
            String json;
            StringBuilder buffer = new StringBuilder();
            if (conn_result.second != null) {
                String boundary = "===" + System.currentTimeMillis() + "===";
                HttpURLConnection urlConnection = conn_result.second;
                urlConnection.setReadTimeout(Settings.getInt(Settings.SERVER_READ_TIMEOUT));
                urlConnection.setConnectTimeout(Settings.getInt(Settings.SERVER_CONNECT_TIMEOUT));
                urlConnection.setRequestMethod("POST");

                urlConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
                urlConnection.setRequestProperty("User-Agent", "AndroidGolos27 Agent");
                if ((cred != null) && (!cred.isEmpty()))
                    urlConnection.addRequestProperty("Authorization", "Bearer " + cred);

                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setUseCaches(false);

                OutputStream outputStream = urlConnection.getOutputStream();
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream, CHARSET), true);

                String fileName = file.getName();
                writer.append("--").append(boundary).append(LINE_FEED);
                writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"")
                        .append(fileName).append("\"").append(LINE_FEED);
                writer.append("Content-Type: ").append(URLConnection.guessContentTypeFromName(fileName))
                        .append(LINE_FEED);

                writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
                writer.append(LINE_FEED);
                writer.flush();

                FileInputStream inputStream = new FileInputStream(file);
                byte[] inputBuffer = new byte[4096];
                int bytesRead;
                while ((bytesRead = inputStream.read(inputBuffer)) != -1) {
                    outputStream.write(inputBuffer, 0, bytesRead);
                }
                outputStream.flush();
                inputStream.close();
                writer.append(LINE_FEED);
                writer.append(LINE_FEED).flush();
                writer.append("--").append(boundary).append("--").append(LINE_FEED);
                writer.close();

                int responseCode = urlConnection.getResponseCode();
                if ((responseCode == HttpsURLConnection.HTTP_OK) ||
                        (responseCode == HttpsURLConnection.HTTP_CREATED) ||
                        (responseCode == HttpsURLConnection.HTTP_ACCEPTED)) {

                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    while ((line = br.readLine()) != null) buffer.append(line);
                    json = buffer.toString();
                    return Pair.create(new ErrorResult(ErrorResult.SUCCESS, null), json);
                } else {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream()));
                    while ((line = br.readLine()) != null) buffer.append(line);
                    json = buffer.toString();
                    int errorCode = ErrorResult.BAD_REQUEST;
                    String errorText = ErrorResult.getErrorText(ErrorResult.BAD_REQUEST);
                    if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        errorCode = ErrorResult.UNAUTHORIZED;
                        errorText = ErrorResult.getErrorText(ErrorResult.UNAUTHORIZED);
                    }

                    // Помимо ошибок сервер также возвращает свое "видение" ситуации
                    return Pair.create(new ErrorResult(errorCode, errorText), json);
                }
            }
        } catch (SocketTimeoutException e) {
            Logger.Warn("Can't send request " + path + " (socket timeout)");
            return networkErrorPair;
        } catch (ConnectException e) {
            Logger.Warn("Connection to " + path + " failed");
            return networkErrorPair;
        } catch (SocketException e) {
            Logger.Warn("Can't send request " + path + " (socket error)");
            return networkErrorPair;
        } catch (IOException e) {
            Logger.Exception(e);
            return networkErrorPair;
        } catch (Exception e) {
            Logger.Exception(e);
            return networkErrorPair;
        }

        return Pair.create(new ErrorResult(ErrorResult.INTERNAL_ERROR,
                ErrorResult.getErrorText(ErrorResult.INTERNAL_ERROR)), null);
    }
}
